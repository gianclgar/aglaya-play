{
	"name" : "Aglaya_DT_Redisseny4",
	"version" : 1,
	"creationdate" : 3648417194,
	"modificationdate" : 3654679541,
	"viewrect" : [ 506.0, 101.0, 300.0, 500.0 ],
	"autoorganize" : 0,
	"hideprojectwindow" : 1,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"Launcher_v6.0.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"audiobuttonlogo.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"FastSwitch_OK.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"load.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"butcontrol.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"buttotog.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"harmonizer_pfft.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"switchbakground.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"volslider.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"ct.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"inletoutlet.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"instrument.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"keys.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"multisampler.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"MultiSamplerSingle_v4.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"loading.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"prova save load.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"Record_v0.1.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"tester.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"sampler.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"MultiSampler_v4.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"media" : 		{
			"z_add_device.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_logo_Aglaya_BW.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_logo_Aglaya.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"bg_load.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"loadbuttons.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butON_load.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butOFF_load.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_faderhueco.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_quadradetsct.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_multiplayer_single.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_stopbutton.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_playbuttonv3.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_plusbuttonnew.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_recstopbutton.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_recbutton.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_newrecording.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_boletes_newkeys.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_carpetarecord.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_playerinterfacenovol.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_bolaverda.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_bolablava.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_bolaroja.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_bolagroga.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_bolamorada.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_nomsnotes.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_bkbut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"bg_ct.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"bg_instrument.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"bg_keys.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"bg_multisampler.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"bg_sampler.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butOFF_ct.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butOFF_instrument.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butOFF_keys.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butOFF_multisampler.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butOFF_sampler.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butON_ct.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butON_instrument.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butON_keys.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butON_multisampler.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"butON_sampler.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_nomsnotescd.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_LAYER_sampler.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_pausebuttonv3.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_bbut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_bklbut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_blbut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_gbut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_glbut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_obut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_olbut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_pbut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_plbut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_bgkeys.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_LAYER_multisamplersimple.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_boletesblanc.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_boletagris.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_zoomtext.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_porttext.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_corda0.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_corda1.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_corda2.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_corda3.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_corda4.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_device_v3.1.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_hostiptext.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_LAYER_keys.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_LAYER_multisampler.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_playerinterface.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zzboletesblanc.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_ebut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"z_elbut.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_launchbuttons.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_bgsamplermulti.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_bgbrokenwhite.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_bginstrument.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_purpleback.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_bgct.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}
,
			"zz_greenback.png" : 			{
				"kind" : "imagefile",
				"local" : 1
			}

		}
,
		"externals" : 		{

		}
,
		"other" : 		{
			"Icon\r" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"Elements Grafics alias" : 			{
				"kind" : "file",
				"local" : 1
			}
,
			"zzzOld alias" : 			{
				"kind" : "file",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 1633771873,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
