{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 0,
			"revision" : 8,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 125.0, 246.0, 1197.0, 704.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 43.0, 439.0, 81.0, 22.0 ],
					"text" : "routepass 1 0"
				}

			}
, 			{
				"box" : 				{
					"annotation" : "file name",
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgcolor2" : [ 1.0, 1.0, 1.0, 0.14 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgfillcolor_color2" : [ 1.0, 1.0, 1.0, 0.14 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontsize" : 5.0,
					"gradient" : 1,
					"id" : "obj-412",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1343.0, 112.499992000000006, 80.0, 14.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 3.666664, 73.349236000000005, 88.0, 14.0 ],
					"text" : ".",
					"textcolor" : [ 0.996078431372549, 0.996078431372549, 0.996078431372549, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1449.0, 167.0, 41.0, 22.0 ],
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 834.0, 1461.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 892.0, 1549.0, 55.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-93",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 834.0, 1504.0, 55.0, 22.0 ],
					"text" : "hidden 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 898.0, 1515.5, 91.0, 22.0 ],
					"text" : "r #0_file-loaded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 726.0, 672.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 865.25, 393.299987999999985, 52.0, 20.0 ],
					"text" : "max i8u"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 918.25, 393.299987999999985, 48.0, 22.0 ],
					"text" : "pipe 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 948.0, 358.0, 54.0, 22.0 ],
					"text" : "t 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"local" : 1,
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 957.0, 439.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 749.5, 852.5, 70.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 840.75, 476.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 430.5, 586.383495145631059, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 366.0, 1057.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 822.25, 618.0, 29.5, 22.0 ],
					"text" : "t i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 786.0, 714.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 764.0, 767.0, 52.0, 22.0 ],
					"text" : "gate 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 943.0, 1471.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 617.0, 974.0, 52.0, 22.0 ],
					"text" : "gate 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 617.0, 947.5, 90.0, 22.0 ],
					"text" : "r #0_playstatus"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 862.0, 1233.0, 81.0, 22.0 ],
					"text" : "snapshot~ 50"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-53",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 979.0, 1250.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-215",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 660.221099853515625, 877.0, 41.0, 22.0 ],
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-212",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 660.221099853515625, 800.0, 52.0, 22.0 ],
					"text" : "gate 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 660.221099853515625, 732.0, 90.0, 22.0 ],
					"text" : "r #0_playstatus"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-205",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 873.5, 727.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-203",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 229.0, 609.0, 92.0, 22.0 ],
					"text" : "s #0_playstatus"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "int" ],
					"patching_rect" : [ 555.0, 957.0, 29.5, 22.0 ],
					"text" : "t b i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-194",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 861.0, 688.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-191",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 861.0, 655.0, 44.0, 22.0 ],
					"text" : "sel 0 1"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-185",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1101.0, 1364.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-183",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 989.5, 1388.0, 102.0, 35.0 ],
					"text" : "if $f1 == $f2 then bang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-173",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "int", "int" ],
					"patching_rect" : [ 939.028900146484375, 1279.5, 61.0, 22.0 ],
					"text" : "change 0."
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-168",
					"maxclass" : "number~",
					"mode" : 2,
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "float" ],
					"patching_rect" : [ 756.5, 1181.0, 56.0, 22.0 ],
					"sig" : 0.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1087.0, 1295.0, 90.0, 22.0 ],
					"text" : "scale 1. 0. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1087.0, 1233.0, 78.0, 22.0 ],
					"text" : "r #0_revOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 688.5, 822.0, 97.0, 22.0 ],
					"text" : "r #0_speedvalue"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-140",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 576.0, 1141.5, 35.0, 23.0 ],
					"text" : "sig~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 471.0, 1122.0, 32.0, 22.0 ],
					"text" : "t 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 471.0, 1098.0, 70.0, 22.0 ],
					"text" : "r #0_STOP"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-135",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 310.0, 54.0, 125.0, 22.0 ],
					"text" : "r #0_playbackfinished"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 989.5, 1471.0, 77.0, 22.0 ],
					"text" : "s #0_FINISH"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 411.0, 915.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 335.5, 939.0, 94.0, 22.0 ],
					"text" : "2361.337868"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 366.0, 822.0, 44.0, 22.0 ],
					"text" : "sel 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 452.0, 995.0, 80.0, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 307.0, 900.0, 69.0, 17.0 ],
					"text" : "r #0_file-duration"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 366.0, 798.0, 78.0, 22.0 ],
					"text" : "r #0_revOUT"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-36",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 602.221099853515625, 1057.0, 90.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 471.0, 900.0, 29.5, 22.0 ],
					"text" : "-1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 502.5, 900.0, 29.5, 22.0 ],
					"text" : "1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 576.0, 1004.0, 29.5, 22.0 ],
					"text" : "* 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 712.0, 1057.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-15",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 576.0, 900.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 842.25, 822.0, 99.0, 22.0 ],
					"text" : "s #0_speedvalue"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-180",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 677.0, 1333.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-176",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 660.0, 1304.0, 73.0, 22.0 ],
					"text" : "routepass 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-175",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 660.0, 1364.0, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-174",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 660.0, 1282.0, 87.0, 17.0 ],
					"text" : "r #0_channel-number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1658.0, 979.0, 35.0, 22.0 ],
					"text" : "sel s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-138",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1241.0, 1316.0, 49.0, 22.0 ],
					"text" : "delay 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "int", "int" ],
					"patching_rect" : [ 1241.0, 1282.0, 85.0, 22.0 ],
					"text" : "live.thisdevice"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 678.0, 1558.0, 80.0, 13.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 582.0, 1558.0, 80.0, 13.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 996.0, 254.5, 79.0, 22.0 ],
					"text" : "s #0_filepath"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 797.333495999999968, 169.5, 76.0, 22.0 ],
					"text" : "r #0_loadfile"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1357.0, 1403.0, 64.0, 22.0 ],
					"text" : "pak f i i i s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "float", "int", "int", "int", "" ],
					"patching_rect" : [ 1356.0, 905.0, 83.0, 22.0 ],
					"text" : "unpack f i i i s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-127",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1681.0, 1017.0, 78.0, 22.0 ],
					"text" : "s #0_loadfile"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-126",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1519.0, 1017.0, 63.0, 22.0 ],
					"text" : "s #0_loop"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1596.0, 1017.0, 73.0, 22.0 ],
					"text" : "s #0_speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1431.0, 1017.0, 57.0, 22.0 ],
					"text" : "s #0_rev"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1356.0, 1017.0, 55.0, 22.0 ],
					"text" : "s #0_vol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1520.0, 1282.0, 86.0, 22.0 ],
					"text" : "r #0_loopOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1705.0, 1282.0, 77.0, 22.0 ],
					"text" : "r #0_filepath"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1357.0, 1282.0, 79.0, 22.0 ],
					"text" : "r #0_volOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1607.0, 1282.0, 96.0, 22.0 ],
					"text" : "r #0_speedOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1438.0, 1282.0, 80.0, 22.0 ],
					"text" : "r #0_revOUT"
				}

			}
, 			{
				"box" : 				{
					"comment" : "save out",
					"id" : "obj-116",
					"index" : 0,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1357.0, 1491.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "load in",
					"id" : "obj-115",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1356.0, 820.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.835941, 0.83802, 0.894216, 1.0 ],
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 356.25, 1166.5, 88.0, 22.0 ],
					"text" : "s #0_loopOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.835941, 0.83802, 0.894216, 1.0 ],
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 82.0, 1021.5, 81.0, 22.0 ],
					"text" : "s #0_volOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.835941, 0.83802, 0.894216, 1.0 ],
					"id" : "obj-100",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 891.25, 599.0, 98.0, 22.0 ],
					"text" : "s #0_speedOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.835941, 0.83802, 0.894216, 1.0 ],
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 542.25, 672.0, 82.0, 22.0 ],
					"text" : "s #0_revOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 323.0, 1126.0, 61.0, 22.0 ],
					"text" : "r #0_loop"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 410.0, 444.0, 63.0, 22.0 ],
					"text" : "s #0_loop"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 410.0, 401.0, 64.0, 22.0 ],
					"text" : "route loop"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 542.25, 613.0, 55.0, 22.0 ],
					"text" : "r #0_rev"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 918.25, 528.0, 71.0, 22.0 ],
					"text" : "r #0_speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 322.0, 444.0, 57.0, 22.0 ],
					"text" : "s #0_rev"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 229.0, 444.0, 73.0, 22.0 ],
					"text" : "s #0_speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 322.0, 401.0, 58.0, 22.0 ],
					"text" : "route rev"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 229.0, 401.0, 74.0, 22.0 ],
					"text" : "route speed"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 126.0, 401.0, 93.0, 22.0 ],
					"text" : "routepass bang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 809.25, 1321.0, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 764.0, 1264.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 122.0, 66.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 764.0, 1321.0, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-80",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1014.5, 692.0, 122.0, 18.0 ],
					"size" : 2.0
				}

			}
, 			{
				"box" : 				{
					"comment" : "speedin",
					"id" : "obj-72",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1060.5, 627.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"checkedcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-64",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 865.25, 568.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1.666664, 48.349236000000005, 14.833335999999992, 14.833335999999992 ],
					"uncheckedcolor" : [ 0.239216, 0.254902, 0.278431, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"local" : 1,
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 764.0, 1417.0, 45.0, 45.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"checkedcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-10",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 542.25, 643.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1.0, 25.633324000000009, 15.499999999999993, 15.499999999999993 ],
					"uncheckedcolor" : [ 0.239216, 0.254902, 0.278431, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.164706, 0.176471, 0.172549, 0.0 ],
					"checkedcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-3",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 323.0, 1151.5, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 0.685378, 16.499999999999993, 16.499999999999993 ],
					"uncheckedcolor" : [ 0.239216, 0.254902, 0.278431, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-165",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 323.0, 1195.0, 53.0, 23.0 ],
					"text" : "loop $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 63.0, 907.0, 105.0, 22.0 ],
					"text" : "scale 0. 1. 0. 158."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 577.0, 1525.5, 85.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 58.0, 1228.0, 105.0, 22.0 ],
					"text" : "scale 0. 1. 0. 158."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 58.0, 992.0, 105.0, 22.0 ],
					"text" : "scale 0. 158. 0. 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 181.0, 1030.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 181.0, 1060.0, 71.0, 22.0 ],
					"text" : "clickadd $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 131.0, 1060.0, 37.0, 22.0 ],
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 0.0, 2, 0.154255, 0.72, 2, 1.0, 1.0, 2 ],
					"domain" : 1.0,
					"id" : "obj-63",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 58.0, 1103.0, 200.0, 100.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-82",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 261.0, 543.0, 24.0, 22.0 ],
					"text" : "t 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 261.0, 512.5, 75.0, 22.0 ],
					"text" : "r #0_FINISH"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 58.0, 836.0, 53.0, 22.0 ],
					"text" : "r #0_vol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-76",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 31.0, 544.0, 55.0, 22.0 ],
					"text" : "s #0_vol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 31.0, 401.0, 57.0, 22.0 ],
					"text" : "route vol"
				}

			}
, 			{
				"box" : 				{
					"comment" : "in",
					"id" : "obj-70",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 56.0, 303.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 181.0, 907.0, 85.0, 22.0 ],
					"text" : "loadmess 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 672.0, 1525.5, 85.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.309804, 0.298039, 0.298039, 0.0 ],
					"id" : "obj-54",
					"knobcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 58.0, 957.0, 85.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 85.333344000000011, 0.685378, 12.666655999999989, 69.663858000000005 ],
					"stripecolor" : [ 0.376471, 0.384314, 0.4, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "Audio Out 2",
					"id" : "obj-52",
					"index" : 0,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 628.0, 1623.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "Audio Out 1",
					"id" : "obj-51",
					"index" : 0,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 578.0, 1623.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 183.0, 494.5, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 189.0, 692.0, 72.0, 22.0 ],
					"text" : "s #0_STOP"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-48",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 234.0, 95.0, 57.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 234.0, 54.0, 70.0, 22.0 ],
					"text" : "r #0_STOP"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 151.0, 95.0, 57.0, 22.0 ],
					"text" : "hidden 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 151.0, 54.0, 68.0, 22.0 ],
					"text" : "r #0_PLAY"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 94.0, 692.0, 70.0, 22.0 ],
					"text" : "s #0_PLAY"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 94.0, 613.0, 107.0, 22.0 ],
					"text" : "sel 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 94.0, 575.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 576.0, 834.0, 68.0, 22.0 ],
					"text" : "r #0_PLAY"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgcolor2" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgfillcolor_color2" : [ 1.0, 1.0, 1.0, 0.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontface" : 0,
					"fontsize" : 48.0,
					"gradient" : 1,
					"id" : "obj-6",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 555.0, 249.299988000000013, 65.0, 62.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 21.333335999999999, 2.133324, 60.0, 62.0 ],
					"text" : "+",
					"textcolor" : [ 0.921569, 0.917647, 0.933333, 0.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 3347, "png", "IBkSG0fBZn....PCIgDQRA...rH...vhHX....PTYn1+....DLmPIQEBHf.B7g.YHB..LnbRDEDU3wY6c1ESbbcEG+uoUp9AGXqTkZI1FuCJFGm9PVC3lp9UVSxjMsNIlJAhzl1ls1hl5zHAkp9Pk.AtsRVNB4RjqSbTppgXhphBkv5TGIBF2MoOXvXvak7tfCM7wttwIO0Yw90joOL6r9NWlY2kkY24qyOoQbuCyt6cm4+dNm64dm4tE3cHH.7Af.L+EYJ+f440tJ.VIS4UxrIAfXY1jLyFpcksX0MfRDAyrEHy1tJwedoghnIJye8DBHmHA.PmP4hjrMYaE.L..Ztj8stLiS1xR..DFJWLxqkCQwPXm0TC1cc6AaaaaC0U2tQk2y8..f8d+6AUUUkF9ZmZ5YxV9pyNG..t1byh0VaMLxa8lEZ6MB.FKylizpiSSr3GJBjvHGBDAgZwScnlw8s65PiMTedECaVhmXdrvMVDKt3hX1qNSgHfFB.CBEKgDlLAgxuHMzreGc1k7YGZX4qGOgrUijTZ4wmXR4t6oWYAgZymqpNgRP1DaRBCkSnFJPFYzHxRRosZ8QN45wSHe7SzetDNR.nOnX4jXCRXXfHQTLj7YGZXau.wHFehIk6nytxk0lAAYoofnYXfHo6d5U9xScEq9ZsowpISIepSeFir1nZogDM5P.XP2d6tmdkWMYJq9ZaICIozxiLZDYQwPFIZBaEWPri3CJ+BxyIRziQFMhQVZhh6lwYOIAgNtbN7QZ2yIRXQRJctbO0mkbkxhY.nSfqtoXR1rrZxTFEHbL3QrxD.JeY0bB3Tm9LN1d2Tp4xScE8hmw0GKSyP4KY1uzszZa1hjnY2QRJs7wOQ+dltYuN2NG+D8SVS1fL9DSpWrLwfKQv3CbooWPnV4wmXRq97tikUSlRtkVaSO2RN53X7At3SH2NlGc2Su5IXbjSGB+fSnzQmcQtcLYFYzH5EGSXK5ZdQQ.vEHa28zqUed00xkm5J5EGSXK6p+Ff0ITN6PCa0mOc8b83IbbBF+fSnLxnQr5yidFbRBl0ELKITJ+3DDLjPwFgABFaS2p0jGEJFEqGcDL1h7vnIyrTudrOb83IrUY5sY1FSGc1kUe9gfCcxCyXVgPQSWjao01nDtYS4Tm9LV9bhIa.sBB0Rov2liNyKlxV7KZhSgFTP6ORRo4mSLqfxP7KAYEJG+D8a0mGHJPzIf2AJkBEefYNyRwo37Pm3WBVpDK8w9AQwo3LgatvrBJAtiBvJTN0oOiU+clnHQG2Q8Y1hknpu4hhgH2ONbzwcjeyRnnI4azsqgyGc5cTAkrtJJfiIaTyG9Hsiu4Cs+hPuQXmnpppDO2Qed1ccHT.A69Exy+OL.dV0JiEIRI8ghCQ4iGXu6Aot4Giqcs4T2k.Tt0RLj7YYoO0Bc2SunlctiMS6ivlQ6s2Na0GF4w5RtrrDFLVUN2vCSVUbYricr8Mj0kbYYoO0BjUE2KaDqKFYYIH.5PsBYUw8hNVW1BLn2QFYYoS0BjUE2Os8zOMa0mEFj2E8DK9gRWo..vAO3AMwlEgcjG6QaBhhgX2UX8NN8DKYOPQwPTdU7H7i+I+T1pg06XxoXg6MvSve+sOO9yu7qhjotoU2TJq7COzSxVcWn.RRmlALzqMFPrOCTDDp0y88maF0MHu3f2xRX0BczYWdtd.s3GdirkWd4kv7KbibbztO9AZiOccOQF3EKYOfu626gKQMIB6JO1i1DDDpUsZUfSvvJVzrt77nMErD2zHri7LZiSMHakJz6e3EcAA.bOYVRYT4+9w2xhZIVGA1W8rUMzxR1+A2Kvyv8s65zT+V2x6IV37nrKvjfNVwR1fT1eidSwBgxbcokVaicWAUKTA+NDDpEe8GXukkFFg8jFZTShXCpVXchkm5PNxmgcDlH0W+9Xql8tWrB9cv62lv6wCs+FYqlcYLdchkFafhWwqSUUUI+.KFD3thkr4WYu2+dJesJBaK6rlZXqF.PQrDTcOhhg7j4WgX8ru5afspe.EwR1aeQN0DgGlpqtZ1pYsrjMdkcWG4BhPgseuZDK9.3rrrssssxbShvtBWrqOH.mkk5pa2k4lDgcE8hcUyTTnRtARivaCyzU..HfF2PDDrbfldD1p9p.LYnixwBQtPiaHJGKD4hunU2.1LDOw73jm7OYZueoRlTS8yO1aiqM2ra5228UeC3Ed9maS+9XGH6L51owgOR65sRdYK2bhOC93N+FrPdX9PXBb6aeGqtIrYwmiVr3TtCDZo01bC2YmRN5XVB+ydFzzAdX7wlzDq9BW3B3O9GNV15c2Sulx85sKPn..Gd.t..0rycXZOkGt5ryoo9W8qUsq4BsYPE.XU0JSM8LVXSgvtSEP4orLA.tycb7AgZpr1ZqootiN.WyF160Y.ZJlNxa8lrUiowxBuOaBBFjH2PD5R7DyyVMMfhaHI08XFo1lvc.WRDiAnHVhotG9.ZH7tvERxJ.bhEt.ZH7v7oehlDctBvccCkVcub9p7T7OuzjZpeuZmzxdJVXgEXqFE3tccNq0kEtwhkuVjMikWdIM08xO+e47xrBvcEKQU26hK5cEKDJvkI+zfSrj0xxrWkR4uWmEtwGxVMpZg0YYYj25MQ5zTuh7xD6ZZ5ITT0BphEIvLfhSOyUKKMJ6D7ChJ2S+HOEmOhl04gnpEXGanrGw+5C9fReKxlSkU5Mm75SM8LrA5mFLgnvJVhpV3MF9bkkFFg8inuuFCEQYqnqkkkWdIOc9V7xboKNAaUM9i3mhBQTK7N+i2sD1jr+v87IwSP7DyiIlXb1coQrvuRlsUj44g6Rez+AczQGvqvN1w1wm84JOS8N3S7j3nO2u.acqeIqtYUV4ud1WGSdw2SsZDjmUiUef4dcY7Ilzpu0UHJivsvfGlWbv6FRB.CoV4cuvEJRMJgSi26hWJmtf.zeZUNnZgWZfS54Vjl7pvYXXHvLOmTQOwRTvjfty+Nj0E2NIScS7RCbR1cMndGmQSX6ATKbx9eQJ8+tbdsW6uvV8eCt7qnhQhkAQl43xxKuDt3kz80R3BHc503SB6.FcrFIVjXeQu5q7xlSKiv1w4di+Fa58WE4o6xFgevzM5QFMhU2yNBSFIozxBB0x1c49xkfHW2jYq.ltQ+a+M+ZJ1EWFuxq9Z7CZngtf.VeFb4IJ.NJ.1pjz+Caem9w2P6JFAgCkjotIdhC98Y20uCFDXqJ461WUSrKmr+Wjx6hKgicreOa0UQdrp.jeKK.JymgrVWjk2Bd7GOT9dMD1XlZ5YvK7qNJ6t94.XACN7MLgASvtWdpqX0wlQTjHIkleLfhZVhDVhp9AHJFRVRJsU+8lnH33mne9GNh9KTAPg3FRkX.3WB.rzReDp5K+Uv24a+s1.ubBqloldF7id5VY20wfNCXnYQeflBCNRzw8Srbbc1zHl5GnfPsxqlLkUedfn.niN6h28SfbcQ1rH.T5RsL.jao01r5yCD4gyNzv7Bk9JlK7ajXVT4Sxr0L.PhDwwm84.McffEymOQIl3IlGgD0r5d79PmYAWolAAM1Q1ZVMYJ9w9QBVzRFjOvD+Bn7uXqPRJsbKs1lkDmhQ3CLwuHHTqibAMvMhNBkvVmL4tnIfWRvX8zcO8xKTx639TNoYvz3HAi0gNBkAsNYgwDFjfwRQGgRIK6rlAgAIXrDzQnDCNfEK0vfDLkMLnWONBghJgA2x9FkGFymUSlROgxXvAITTIL3DLm5zmwpO+5Z35wSvmvMaavrEJZ5VM.j6nytn4BylDcFqGaW2iKVB.tL8JJFhhioHPRJsdidrsIgalE9fhuTxsTQxkm5J7yGE0w5wRSgeoj9.mfokVairxjCjjRq2TgTcty53BjciR.nbCrsNqLTrLZw.qIE87QwohOnDPllSBhhgntXKqzkXtUqc17m3Zc6jOBBcrxb3iztmb5NjCWNdNqIFgOnSrLvCIZjjRKepSeF8xahZrI9sfqK1Z7Cc5wDxHZbi2IAqlLkb28zqQhjUPlotJgwDDL2ParahhgjO6PC63CDd7Ilzn7kn1c3Nsfy6NZBBCDMHSlfcRVatd7DxG+D8aTuaTsjzI7.cGtTRPvM4vY2DDpUt6d5UdjQiX6r3b4otR9DHp8vIb49jZwvVr5FvF.+P4jZX.rKiNnVZsMzPi6G0W+9vCs+FQUUU9VcOlZ5YvB23CQrqMGNejwV2xnGGCAkeDDsbz1LCbRhEVBBEQSy.npbcfhhgvNqoFru5a.UWc0X62a0Xu2+d1Thn3IlG2912AWc14vm9I2BKrvBE5JWaDnDD+XPmmyr1cbphEVZFJhmlQNr3nGBB0hCzzij+CDJq40EwRYbZnX4vwJPXwMHVXwOTDNAgRlNevx7mup3PcqrbymWtvsIVzifPQ33Oye8gMuHRcE9ZkLaQYJ6ZwKHVxEphmBkXvg6JYyv+Gel5bbxaTSz......IUjSD4pPfIH" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-1",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 56.0, 188.633330999999998, 82.0, 82.0 ],
					"pic" : "zz_plusbuttonnew.png",
					"presentation" : 1,
					"presentation_rect" : [ 18.333335999999999, 0.917414, 65.999998986721039, 65.999998986721039 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1045.5, 358.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hltcolor" : [ 1.0, 1.0, 1.0, 0.501961 ],
					"id" : "obj-7",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 126.0, 439.0, 82.0, 37.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 18.333335999999999, 0.917414, 66.999998986721039, 66.377141838073726 ],
					"rounded" : 100.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 72.5, 763.0, 150.0, 20.0 ],
					"text" : "PLAYSTOPVOL"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-178",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "signal", "signal", "signal" ],
					"patching_rect" : [ 576.0, 1208.5, 142.0, 23.0 ],
					"saved_object_attributes" : 					{
						"basictuning" : 440,
						"followglobaltempo" : 0,
						"formantcorrection" : 0,
						"loopend" : [ 51635.804988999996567, "ms" ],
						"loopstart" : [ 0.0, "ms" ],
						"mode" : "basic",
						"originallength" : [ 49570.371879999998782, "ticks" ],
						"originaltempo" : 2624.061842826624343,
						"phase" : [ 0.0, "ticks" ],
						"pitchcorrection" : 0,
						"quality" : "basic",
						"timestretch" : [ 0 ]
					}
,
					"text" : "groove~ #0_audiofile 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1306.0, 445.299987999999985, 50.0, 22.0 ],
					"text" : "2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1146.0, 444.799987999999985, 50.0, 22.0 ],
					"text" : "44100."
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1373.699706999999989, 596.799987999999985, 88.0, 17.0 ],
					"text" : "s #0_channel-number"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 989.333495999999968, 16.633324000000002, 150.0, 20.0 ],
					"text" : "LOAD FILE"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-92",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1192.0, 215.0, 59.0, 17.0 ],
					"text" : "r #0_filename"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-61",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 555.0, 129.5, 66.0, 17.0 ],
					"text" : "r #0_file-loaded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 555.0, 159.5, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 555.0, 212.0, 63.0, 22.0 ],
					"text" : "hidden $1"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1192.0, 314.799987999999985, 66.0, 17.0 ],
					"text" : "r #0_file-loaded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1192.0, 344.5, 69.0, 22.0 ],
					"text" : "delay 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 617.0, 159.5, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1465.0, 52.5, 29.5, 22.0 ],
					"text" : "set"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1465.0, 16.299987999999999, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1192.0, 236.299988000000013, 74.0, 22.0 ],
					"text" : "fromsymbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-465",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1192.0, 372.799987999999985, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-453",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1256.5, 592.0, 72.0, 17.0 ],
					"text" : "s #0_file-duration"
				}

			}
, 			{
				"box" : 				{
					"format" : 6,
					"id" : "obj-450",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1256.5, 562.5, 111.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-448",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 9,
					"outlettype" : [ "float", "list", "float", "float", "float", "float", "float", "", "int" ],
					"patching_rect" : [ 1192.0, 402.5, 105.0, 22.0 ],
					"text" : "info~ #0_audiofile"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-410",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1183.5, 592.0, 60.0, 17.0 ],
					"text" : "s #0_filename"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-399",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1045.5, 393.299987999999985, 68.0, 17.0 ],
					"text" : "s #0_file-loaded"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-156",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 982.0, 119.5, 320.0, 62.0 ],
					"text" : "\"Macintosh HD:/Users/gian/Google Drive/Proyectos/Adolf/AGLAYA /Recursos/Sons/SAMPLERS_CAIXA_DE_SONS/ArtificialIntelligence_Mini_SP/Am_TimeTheifFX_01_679.wav\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-154",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 885.0, 112.499992000000006, 69.0, 22.0 ],
					"text" : "opendialog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-153",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 885.0, 71.499992000000006, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-151",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 885.0, 169.5, 59.0, 22.0 ],
					"text" : "tosymbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-150",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 885.0, 254.5, 95.0, 22.0 ],
					"text" : "prepend replace"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-149",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "float", "bang" ],
					"patching_rect" : [ 885.0, 320.5, 136.0, 22.0 ],
					"text" : "buffer~ #0_audiofile 0 2"
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 5283, "png", "IBkSG0fBZn....PCIgDQRA...7J....rHX....PdR3yN....DLmPIQEBHf.B7g.YHB..TnURDEDU3wY6c9FbSbdmG+KTlz7BSv2qry0Dzd0zjbmIQhPuBWvLrpjN.gdItxmgROlqtxjYNldyXKeu+X88tlYhE4UPGrnJyvcMVT7QHomDYFBxwlf6zlv5.cXfJQD4ZJRuomMqdQeSu8dw50rZ0yp8OZ092mOyrC1Onc0iz9c+4eO+d9876YcfR6R2.HlENuh1b+Hzw5b6NfO.lUOXQiB08XSW+U..+p+bQ.r7p+N+p+LEMfJdaDFHINYW8esKApU49.nBjD0EAUP2.gcwKCjDpxGQbuthgYI7Hw7Ec0dhKSXT7FC.Ct5QT63BNThDn6t61vu96842CW8pEsi2Z.f2EORHWwttn9ABKhWF.LNjDrl155PIRfHQhf92Z+3q8W90PuO4Shd6sWzSO8z1crxkKi50qi6d26.AAA7wW6iaGw8R.H6pGAd2KBxh2tgjXcbXBKrCkHAdgW3EvN1wNvV9FeCzWe80w5f5wRKsDt6cuCV75KhaveCyJneWHIhC0tV32fAOxxindGwiyJNIGm3ByOunfffnWlpUqJVHedwIRkRjgIhte1V8nB.3fzCyT7nvBIe+z8F5PIRHlK2LhkJUxs0isE777hoSOkX73rFUHmERObSwi.KLfnMdbVwb4lQrZ0ptslqiPoRkDmjiynVjyBpH1UgE5HZYXhHNIGmu2BqYoP97hilLIUD6AIFzQzJak0q6CamlpUqZTqwYA0m3NJcCoujaonsP97tslwygfffXlLSqmHdYHMvNJ1LiiVD8fgRjPjmm2s0HddLnHtBjbIiRaRKcQfZo0ZXPQ7IA0UBKCGZw.wxkaF2VC36QPPPLc5ozyUhAcg689VXfT1SQ7KzzomJzOPL6lRkJoWzInVgM.i.M7sk5WammB4y2JWI3g0R59.OsLRBYxLsaeeMzffff3DoR0Jqvi37xCuKLPC2DFJQhP2DL3UXg4muUVgy5FBEuFwfFtIP8s08oZ0psxWXdDh8CdDnQjDVX94c66aTTPlLS2pnQD57ClCZ3lPPMwY76zB2HBUgSKKHHbmjii5lfGmRkJINThDg1AxkEznI3qQPPnU9AOhqnpb.xBBefoSuq+jI43BMB3rfNvr.GsXpkGwcjY1OYAAgKM9sACJjOefU.mETgafmfn.NKnB2PCAIA73fJbCcng.1WMQFi.BOAREtgCzX13VFcfE54Wwludw.v6.fGWYiExmG+sequkM+VQwKx111Kh0ut0ghEKpr4GG.wgj13OYWuW1o3saHsjcZn.dUHedru8uea7sghWm8vxBgG9Pb8EWTYy8Bf+ZHIfsErSwaAnplfMIGGF8XGyFeKn3WXW6ZW3t24N31291Ja94fT8wqnqzoz.NnxOmQSlzsc+hhKS0pU0JYdXsCQmcTkHYAvUU1P73r3RW58PWc0kMb4o3mYokVBwh0TvFVARCfqsJCqqucNYH4maSkPyyblooBWJ..HZznHWtYT27lfMrZLZWedeGnxO2b4lACLvtayKKkfD82e+jF.2yAIKvKR9r5rLHT4KyDoR41tYQwihff.I+eaq3+ZUed6FRkEnMI2.CSDbyadKp6BTzDM7+cNXwAvYU2FNM.1gxFd+268wV1xVr3kyYnVsZzGtbQ5s2dQ2cuIb4KeYkMy.osrKdhmjMCK7YtKnrXZHWeynK4H2AAAARUx8kgCsRjqn7MlgIhmVHnUrFYXhHlIyzd59dPEdddRw98jcZgaSYKlWeY7nwWTTQrKiFKinNV1m0MTUfP7CyhldhWpH1cPinOTrSId4TeC2OjliFU7pTDSKepNCZj9jr1svkQ8axjbbt8mcCgYEuxGwiyRqJkN.DF7lsG0grPk0I+RksQPPPSKrFQDOZxj9lOq9QzX0WLhcIbYTew8aEIDRhRdddwB4ya3MfOp+vcNHTDSpXWh2rJuvd8PiQBsDuxXTQL0UhNCZ3ZWaW+yZJBC9Mqthh5KdkIWtYLj6Dz5pl8CAquEaWwKmxKnezpqnnwEuhhOZmxQOALsh+Xurv7ya6QdnAqt9kHLnFyHdkQmBq7ZGSjJku7AZuHDbcKqUEtin9FkecT2VQ7JiNk2dpUXaDMh7.iUDuEgJKL9UHYA0L9taf8qLpUXaBBFJ3zRfp0x.hA.6QYC+S+venUd.HPPWc0EFe7TfmmGCkHglutoRmFO+yuUbsEVvA6cAKFarwT2zHZ8Z0R7NtxeIdbVDMZTMdogGhFMJ9EW3BHSlo070Tox8w.6d23eexIQ850cvdWvfibjef5lh.MF3lVh2Fhw1wO9wa6NkaQ850wxK2VKR0lHYxQQoRkZoU3SvwgW8U+6wRKsjs9dGzomd5AilLo5lGwnmeLDPFnlnHY+cgMFuZiDVM51xk4fv.2Lr0mSp7D8Co8XqPKAkcNYK5rQiHBP2PDMCZjOJMMiajbangWzvCOrQE89Jd3Censcs5qu9P129sQ5zSo4q4ByNK1xV1BN6YyXauuAU5pqtvDoRotYcmt3lbYvu+m6zJFscp+hBOOut4IA0Jr9XDWGTa4sA08PIR36Wss68auWG88KZzn3RW58HY4XMtvryhW9k2KN+4y4f8L+E6ZfAT2zlfpnNzRw6g+9G116TgA5pqtvaN0TXg4mGLLQH9ZpT493PG5v3XiNJpUqlC2C89ngqCrJ+Ekh2tgpR2zK9hauizwbR1zl1j9unND6ZfAvhK9qHE5m0HyYOK14N2A0JLA14e2NU2TCFWUJdYU9eDONK5qu95L8JGj92Z+t56eO8zClNSFREat0P1J7+vPCgxkK6f8NuMDLdFEJpuCZJdY2SC+Jk1jgG9P5NwFzHRzH80Wejb6hU9GzT7ticzP0bJvgcOqaFg95qOcmdY.fQG8XTqvqRhuWSOrSr1NDXlUMknQRNKB.WseYjPpAPW6b4xMi5uSJpV3xp7EvvDws6y1FsZou61XzTsLLu14JUpDouS.vibanASwNcrQcCzJDVNIJS0x3wY070c0qVDwhECm7joCcYplFAMHFfFh2WZWuTGtK497Fuwa31cg0PdhMZ0zKC.jJ0DgxLUiPnFY.dj3kQ4+yy9LOaGuC4Tzau8RrcuVLrMqU3vT9Bu4MuY0M0fk2FV0D89jOoCzkbF5omdH1tW8FurUX8hHwI33BMqZim5oeJ0Msl3soh5aPXxI7yzUWcYnDdWdUa7uNwDd1GFsC191+lpaZMMKKTMx1fFfvH38SidOrWGIpVsJwHNzjk2u9e0W2NenghMfrU3VkiDxVgChI5iVt9sdnJRCa842pSzebTHM.n6d2637cj1f95qu0xQhVElufZh9P3dHaSqjhm3IdBGoy3jP5ulHHH3B8j1mgG9P5loZAwzsjz8vlr7twMtQmp+PwhnLS0BiVgWEll748YBPw3MnirU3VspMBJoaIgINioc23ro3xzSO8n6p1.HXltkTwa.gcMv.3l27VszJLPvJcKaxmW+9BtLLiQV6b.MZE1GO4FcudnXyuFH7L6Z1YcavqgrUX8RzmQG8X9lD8gP91DKT31.orj6V27VtPOw4HnktkcQHJXgBwaXlfb5VREug.LqUX+Rh9PEugHhFMJ9vO7p5ltk9khjMU7FBwOltkUevCZps0CfUT1PPH9eTzGitL7ksBe4BEbndFY9x+vWptohqGp1nh8BOkQw4vnoa49OvA7bI5C0sAJlNcKcaqvxPEuTVCiltk6+.GvwGLGoIUZ8PUQ68S9jeiS0en3QYe6ee59ZtxUthCzSdDDlTI9M.IedeMGsmPwyw0VXAL6ryhoRm1PudBqnWmlk2f5V98+O+d2niPwEnVsZ3m+y+OwktzkvUuZQCedLLQvAO32sy0wHvU9vlszKa4cM9hu3Kbp9CEWhKWn.N+4OOxb1yZpyKdbVbzidTbnCcXGO6CqT49pap3FfJedu2meOGqCQw4nb4x3+3bmC+rr+LRBgVxjbbXu6cuj1mHbDzZtG1.TUxHMye9fh2l50qiqsvB3Lm4L3ByNqoN23wYwwO9wwANvq3543Mg4dXN.IwKfzrrsVd8Vtb4PSd8FDYokVBu6EuHNAGmoNOFlH3GMxOB+iG8ndp6+Dh.1x.OR7xCE0qrR+temmpySQepUqF9nOZNbpScJS+WOGMYRL7vCicMv.ttUVRPHHB7.ZHdILOxT7nX1PbICCSDL1XigibjeflUjFuBe1m8YpapAwaEk+Oe709XjL4nc9dkCQPaI+HGhq25sdKSO3qIRkBIRjv0F7kUff+5MY4cMHESM+LjVxO9sBns7fuZmPbcvC9c87VYUCgU1wJXUisxh2hJ+eqT49zAs4QncCw0qM3fHZzn5+h8nPnlxslgVkyv1bPgeue5m9ITwqKQ850Q97+2Xl2YFSGhqgRj.G96eXOQHtrCV75Kptohx+fRwaCCZawquHFd3C0Q6XTZjfVHtrCl8+poGdKJ+CaPUiio7jdyoZ8JNkR6SsZ0vu7W993bm6bAtPb0tTtbYhSKr7OnV7tFT+d6rDFBwU6xm9oeh5llS4unT7tLT426byUjJdsQBag3pcYl2ooMa7hs50ONTT2+GJQB2d6HvVfgIRS6oAExm2QduEDDDKjOu3nISp69Jg5i3wYEykal.yVoqYPPPfz2ID22gkIl5SHH7EGIgQmdCUoToRhSxwQ7AG8NljiyWsguzInP97p+dohZwp5jQmG.2G.qsJ79nOZNZTGLHsaHtd8W+0CrC9xr7AevGntohF47NIBXtNfNrk2EledwI43LsEVFlHhSxwIVpTIaquDDPCWFFzHh2.mqCcBwa0pUEyjYZw3wYMsnczjIcLet8iPvkgFVvD5QEkmb5zS41edrLZrk2a4GHKjOu3DoRYoAekN8T9dCANACkHg5u+xZFwKm5u38qvyySTLYFpVspX5zSYoAeMQpTA1clxNAZXrokQYPMLpu.90a.VU7JGhKBVALjU1vZHtZWHL1gJlQ3JyEUdQFMYR29ykkvrhWZHtbODDDH8893VQ7Nn5aN9QKI4xMCQKiJQPPPLWtYrzfuFJQBwB4yKJHH3ReBCNP5dETsOApjlJ5HJ3hPULe+om9z3e6Dmv7OF3hPZaZUdq.8ZKr.txUtBMKt7HbpScJ0M81vjQZPIMLcwvGZ8MSloa5oYFlHzPb4wfP3wDgzXurLcCIkeC904mHc5oLsHU4Q73rhYxLsu6gV+FDLlTrcDtxvo9Fpe5FoURHF.ZHtbRzvpKqdBSiTedOITU5++om9zF3z7eDONKxkaFHHHf2bpoBUoenaxO4M9IpaZNXSVdA7wVe0yxqb9EPCwk6fUs5ZFZx22IRkxs+baHzJgYng3x8QPPni4qqZ3TK.7CVqDDDVy5KCSDwzomhlEWdDHEIHXBqtqyDh2tgzT0sVA4anDIvu3BWvDWBJTjnVsZn2d6UcyuKLXpOB.7ULw62eB.UUdwu8suM5u++Fze+8ahKCEJ.iO1X3F23Fpa9.vDSJgYr7JSQnXQZxvDAKt3uJvuRVoXebsEV.Cr6cqt4IgjqoFFyX4UFd.7OK+KKu7J3+6O+mw91m96fLTnTudc7c9NuLVd4Fh958AvHP5ut2wogkJDf+MkIo3rnQh7aX+bUhUba.PZva7PQR6Pcefhdng6BlZPZJwJtM.HYdmGRl5Afj6C+u+w+Hd0WitktQoYpUqFdkC9JpcWXEHEZLK4tfUEu.RgM6u..6TtgabiafMu4mFaaauXabYoDDY7wFiTsX6HPUsg1IQN1u9tIufhygFSFQ11U7YUedURL.zP.6n9+RQFM7y89PR2X4DMGn8baPlpPx2k8K2vxKuB976cO7JG7f3wdrGyFdKn3Gob4xXvu2fj7yc+vhKrRkXGhW.fEAv1.vyI2vsu8swi+U+pXOrr1zaAE+D0qWG+K+3eLtdyU17iCfBtPWpkHG9rF7swOWvRnXcznjAj0kzlFBY+XZnSSW2WgKzHMTcsnJXFZpVmAp.NzfFB2JnEKgcuFi.BBX5THGrQCg6xvjkqIu.Msr4YXhPSD7.JAIgqLYAU.G3oE0l3QbGYm8QVP8ANvRPV3JSVPEvANBCBWYxBBePykaF29d.EShffPqJv1i3JpKGfrfvGX+VIjJLiffPqpYwi3JpJGDNnQcTfVCE71TpTIspawKCatPg3kYDPP.GONKMRDdTznx136CGlUYPPXpjA0OXOEBBBsZfY7nMKCo9YhABIyNfT4jh5Fg6RoRkZk+sWD9no7sSQ2PpVPzzWPLLQnqJCWBMJw9xGbtfNwSCGz3KqzomhZE1gnZ0pspxZtLr3J8ML.KzvO33wYoI1SGFMVqYT+aMAcCUaiVJOlHUJeSsA1u.OOud6QGbtfNvWyHPCqv.PLSlootRzlTsZU81JZ4QHLLX1EszJLCSDZX0r.5D9Kp0ValAgFgTCq5OLMIezGAAAwLYlVuc2yhfZs01oaHYMPSWIj2yeotSzHUqV0HaIsUPHH2DbaXfFI3i7gb46OrOvNdddi3dvxPxnPneBGbRhAMlbCkGSjJUnxkBStOJyApn0UgEsXPcPg03I43BrI9yByOudQNPok1SBZLa8Tv.cbmP9HdbVwzomxWOoGBBBhExmWbhTozyWVp6A9HjGXWEX.gLCSDwIRkRLWtY77Vk444EyjY5VkrLZEq1Qbpu7cRripDoWlAgzMNCWwqYXhf89s2Kdoc8RX6a+ahd6sWWoZWVtbYT8AO.+5eyuF25l2BYN6YMyouBjbk5jvmToZrBAcwqLcCIg73.HpYOYYA8l27lwS8zOEd1m4YQWabissvtb4xnd85n5Cd.9x+vWhe6s9sXkUVwrBUYjErxGAdBKhWkv.oA4MHLgEYcunqJv0CKJL0hPmfUIgQwqZXgjPNFTr+x4QYEHEdP4i.qKAFAp3sYXW8HFjrRaZ2LrQlCRC7rHjDpgZwpZnhWiAKj7aNlh+En8sTuBdjfrnh+sBrgJGdPGp309PVXqGU.UXZK7+CzLStsDhLO50.....jTQNQjqBAlf" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-18",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 151.0, 188.633330999999998, 96.184105000000002, 96.73372845714286 ],
					"pic" : "zz_playbuttonv3.png",
					"presentation" : 1,
					"presentation_rect" : [ 18.333335999999999, 0.917414, 65.999998986721039, 66.37714183807374 ]
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 19638, "png", "IBkSG0fBZn....PCIgDQRA..B.C..H.LHX.....lwG0a....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wY6cmGcUVkm+u+ic2007qWYpWqRKYzbRQX71ECBB0jDPBwpTQrjH3TYDLNWsHZWc0cKRTqesU40A7myhCAGgRTFDGAwDp6sMoPHvuUmvjxPRLX0ceWxIvZ0d60p659GObjPLIbd1O6moyy6WqEKQf897sJQ7S16u68VB...Hl4TB6B..gpwIohO12uji8M0G+85X+ZGqOTGGTRGnG+XGVRaue96q2GpC.DSP.FfbSYBlThNdHjxO1esDIclAcA4iRqiGr4.G6acOrS8AdEA.eGAX.hmJVNgTJ4Xeabc6GqnPqph1Z3X+050wC3b.8MW4G.DCP.FfnsRN12Juae+oFVESNrcHmfLaWGOXy16me8.HjQ.FfnixkyJnjYkUHnR3q6Aap+X+0CGh0C.NFBv.DNJWmXfkbodRIWWldto9t8WITCP.i.L.9uLgTJ+X+U+3T7fv0A02LTC.7QDfAv9JWGOrR4hlpMopAwpz.3aH.Cf2Ud29F8sB5K6PNAYx7MBz.3ADfAv8xrxJY9FqvBLQlUnIy2.fKP.FfSthkSPkYer+JMbKrszxIDyZN1e8.gXs.DKP.FfdWIxIvxrEaKDBdY1to5D2GM.8JBv.bbiSRUKmPKrJK8g4T0bUgEVnUmym+4VlUmubLokyJyj4a.PDfAHy1BknBsz8PHieBm0W+iO7gWlJrfB95+9QMxQnhJxtgUxVM1zVNg+9OYqa6q+9e5d2iNxQNhjRbge59VMsFQi.iDLBvfjnY2suky0.tYBmT1vGgxO+70.Fv.zfF3.TAEjuFynGUXWd9l1ZuC0YmGRccjin8rm8JIol21VkTNcHm0JByfDJBvfjhbpsGJSHkwOgyR4me9ZjiX3ZfCb.ZnCYvgcoEYkNcWZm6Z25y67P5PG5Pe8p3jCEtY4hsYBIHDfA4xJQNgVpVwzPKyop4pAMnAogU1v0vGdYZPCb.4zqhRXIyp27Iaca5O9EGRc1Ymw4fMY5YlkJZ.XjCi.LHWSwxYUVVnhYWY+yeA0nxF9HzYbFmgF4HFtlxjmTXWRIdsz5N0m24gzd1ydUyaaq5i1zGp8u+8E1kkabP4bRlpSbzrQNFBvfbEkKmUZ4pC2xH6LmplqF4HGo9tCqLMoINAVUkXj1ZuCsqcuGsss0r16d1cbZkZZPGOLCPrGAXPbVwxIzxBUDeKhl+BpQieBmkl3YMgP8j8.+QiMsEsqcuGs8l2lV2ZWSTeUZRqiGjgsXBwVDfAwQkqH7psjJUoZVWzr03F+DXqfRnZq8NzV9jsos271TSM1n1vFd+vtj5KrpLH1h.LHtHRuZKYVgkoU94v1Agug1ZuCsoOpgn7JzjYUYVpnWYPLAAXPTWIRpVEwtyVpnhJ0zmQEZBSX7ZlyX5gc4fXlVZcm5ipeypg5+HspWekgc4zSqUNAYpOjqCf9EAXPTU4xI3Rj4cH5VW3hz3F+Dzzm1T49VAVS5zcoMto501ada5Ud4WJJs5LGTN+6f0EtkAPui.LHpoZ47GZF5aSTpTkpq3JuJ8iOmyQSdRSjFuEAhFaZKp9F1r1zF2PTo2YRKmUjYoha6WDgP.FDETrb5skpUHGbo6gVXqgPXqkV2odq0+NZqexVhBa0TlKHuZE8ICh.H.CBSYBtrPEh82RpTkpq6FtIcgWvOkFvEQVYZD32d8uUTHLyxEAYPHi.LHLThb9C+BsiAMqzBhyxrxLQfsYpA47uKWeXVDHYh.LHHUhB4fK25BWj9om+4SnEjynkV2oVwJVYX2.vDjAANBvffPIJDCtLmplql2kc4ZFSubZDWjS6C13lz671usdjk9PgUIPPFDXH.C7SknPJ3B80BRxRmtKs509V5Ue4WJr1hIBx.eGAXfenX47GdcqA8G77WPM5md9WftjKdVA8GMPjTiMsE81u8aqe88d2gwGeCxoI84MWBVGAXfMEJmpnLq1xkeYykKXNf9PHupLbpkf0Q.FXKKTN+ATAVvkL81Bq1Bf6zXSaQqXEqHL5UlkKm+rBtP7fmQ.F3UUq.9ly8NW7Rz4e9mOuxy.dTas2gd0Wak5YdpmHHOASby9Bqf.LvTkq.7sJhsIBv+j48X5oexmHH2dIdqkfmP.F3Vkn.7jEUQEUpK+JuJcwWzExQfFH.7Fqdc5cd60qm+4VVP8QtC4rsR0GTefH2.AXP1JPaP2JpnRc823MQ+s.DRZrosnksrkEjAYVqb9yWNPP8Ah3MBvfrwrky9U6684x7WPMplZpg9aAHhns16PKaYOaPcLryzeL0FDeXHdi.Ln+Thb1eZeuOWH3BPzV.Gj4fxY0XVSP7gg3IBvfdSlsKZI98GDAW.hWB3fLrsRnOQ.FzSAx1EQvEf3s.LHCaqD5UDfAYThb9CItH+7CgfK.4VBvfLbZkvIf.LPJ.tEcqnhJ08bu2KAW.xQ0V6cn69tumf3TK8Hx4OuhKAuDNBvjrMN4rpK9VS5xwgFHYIfN90zjuf.LIX0JerIcSkpTsn63Wpa4ltd+5i..QXevF2jdf6+986a120JmmyDVMlDHBvj7LN4bznGqe8Abe+1GP230WC2bt.P08huhtmZuK+7sVJsbBwvpwjvP.ljkZkOtpKyeA0nkrj6h2pH.bBRmtK8RuxqoewMeC94GCqFSBCAXRF70UcgFzE.Yi.nQeY0XRP9yB6B.9tLG6PqGdIUpR0Kr7WVevG7dDdA.mTCcHCVO2y9L5ia7OnJpnR+3inHIsZ47ErUre7Afni+7vt.fuoD47Ugb8RJOaO424hWhdwkubBt..WavCdP5ptpqTe6Se.ZW6rUc3C+k19iXbR5xjz1E2hu4rH.StoYKo2SRiv1S7bpZt54d9WP+7q5JTd4cp1d5APBxYOoIpK9mcI5O8mNE0Tiersm9hky1IcJhK+tbRzCL4VJVN2qKWssmXNVz.vO4yG65cHmuvtC3GSNBGrBL4NFmb1xHquwxyeA0n5pqNUwLltsmZ..II8cKMkl0rlkJ5u5aqObiefsm9yPRWij10w9FxAP.lbCKTNMt1YXyIMUpR0Je8UoEsv+FtSW.fuKu7NU8i9g+.U0kNW8u9u8uqVasEqN8RZdRJkb1RouxlSNBdDfIdqXIsBIcq1dhu0EtHshU7ZZLidj1dpA.5Wm9ocZ5RqZN9US9NNI8SjTiR5Kr4DifEAXhuFmb9pHlrMmzJpnR8Ju5qoqcAWCMoK.BUYZx2t55np4l2lMm5yPNmRouPNmTIDCQ.l3opkz6JKeOGbe+1GP2+u82ng8cK0lSK.fwJpnB0EMqKTkTZY5+8N1tMWMl7jSi8xVJESQ.l3khkzSIK+b.jJUo5ceu2WyatygUcA.QRiareO+Z0XxrkRum3YHHVg.LwGkHm+ELqdJixzqKrpK.HpKypw7sO8An28cVuMm5LmRoFEG05XCBvDOj4ho6Ls0Dl4DFcy2zMvpt.fXkydRSzONoR4It36hUH.Sz2BkzKHK9b.j4dcYBi2WdaGA.7cYNoR48WVfsu2XJWRiWNeQizWLQXbS7Fc4K2ptuvxeYU8O+Jr4TB.DpZrosnK+xlm1+92mMmVt8di330nNZpD4rDlVK7REUTo9WZoUBu.fbNSYxSRM2byZ9KnFaNsiUNGw5xs4jB6gUfI5Iy86RQ1ZBuyEuDcG29h31zE.47p6EeEcMW8UZ6o8ZjTc1dRg2POvDsTsbteWrR+tjJUo5Ye95zMeSWOMpK.RDF2X+dppKctZu68S0912mYqoMy8EyZr0DBui.LQG0JoG1VS1bpZt5YdlmQS8b9Q1ZJA.hEN8S6zzrl0rz+wW8eplZ7is0zNNIMM4Dhgl6MBfsPJZnNYw9cgsLB.vgOrkR6PN8ECW5cgLBvDtJVN86h0NOybJi..NQ9voTJsbBwv6nTHhSgT3oDYwvKoRUJmxH.fdQlSozbpZt1ZJKRN+42y1VSHbO5Alvw3jyUVsUtYcmSUyUuya+1ZHCYv1X5..x4jWdmpsu36xSRySRGTrRLgBBvD7JWN2viV4XRemKdI5IehGiSYD.PV3G8C+A5udrSP+tUtBaMkyVNaoTi1ZBQ1gdfIXUsbdV.rhU8lqUWxEOKaMc..IFsz5N0EdAWfM6KlkKm+LdDPnGXBNUKKEdIS+tP3E..yLlQOJa2WLWs3xtKPwVHELpUV5NdYNUMW8Zu1qoxF120FSG.PhUd4cppxYNS8W7sNUs4M2fMlRtqXBPDfw+UmjtUaLQ25BWjd3G5A024zOMaLc..Id4k2opoOsx029zGfd22Y81XJKQR+DIsBQHFeEAX7W0IKcA0cmKdI59+s2GMqK.fO3rmzD0e8Xmf1xe3OnCe3uzqS2YHBw36nId8O0IKEdgl0E.HXX4l6kasWeDq.i+nNYgvKoRUpV4quJcgm+448JB..mTm9ocZZFULS8u9u8uqVasEuNcYVIl2SDhw5XEXrqhkSyaMUuNQoRUpdq0udMlQOJuWU..vURmtKcs0bcZUu9JsxzId5ArNVAF6Iy6Zzj85Dk4kjlvK..gCKeBkxSRWlbVIluvyEGjDAXrEq8nLNmplqd1k8L7r...DxxbBk9u9uEgXhfH.i2YsvKyeA0nG+wdTUTQE54hB..1wzmV415MThPLVD8.i2XsvK24hWht26oVuNM..vm7FqdcZN+rKxFSE8DiEP.FuYMRxy+tYBu..DOX4PLiSRGvFSVRDuERlqNQ3E.fDkK4hmkV0atVaLUEImuH3hswjkDwJvXl5jEtmWdzG+ozsbSWu2qF..DnZrosnK+xlmMtv63xtyPDfw8pSVH7B2tt..waV7V6kPLFf.LtSchvK..3XHDS3gdfI6UqH7B..5lwL5Qo2Z8qWoRUpWmpwJmdhAYIBvjcpVRKwqSBgW..x8XwPLSUNqzOxBrERmbUKoWvqSBgW..xsYwsSZ4x4+1C5GbS71+FmjdWuNIDdA.H2WlWx528ceWc3C+kdYpFmbVfg5sRgkihUfouMN47adJxKSBgW..RVr3JwbMhsTpOQ.ldWwx41QjvK..v0rXHlKVzbu8JZh2uoLuuQdJ7x88ae.Bu..jPMlQOJ8pu1JrwTUmb1Q.zCDf4aZMxiONi24hWh9U+xa2RkC..hilxjmjMd1AJRNeQ0k30IJWCAXNQ0Imiwlw3sMB..YXo2NId2j5EDf43Vn73EUGgW..POYoPLbQ20CbLpcTsjdRuLAyop4pm7IdL6TM..HmxnG0HTd+kEnObiefWllRjTJQPFIQ.FImliZERJOSmf4T0b0ytrmQ4k2oZupB..4T9Q+vef9u9uk17lavKSy3jzAkz1sSUEekzCvTrb9MAFehipnhJ0K+RunJpnBsWUA.fbRSeZkaiPLyVRMHmq6iDqjd.lFkzYZ5fSkpTs7W7E0PFxfsXIA.fbYm0Dlf9zOa+p0VawKSyEKo2SRegcpp3mjb.l5jTklN3ToJUu05WuFynGk8pH..jyKu7NUU4LmoWCwjmj99xoEH9JqUbwHI0.LKTR+cdYBd1muNM0y4GYoxA..II4k2opwLlw302MoyPRiRNgXRbRhAXJWRulWlfWX4urt74UkcpF..jHYoG+wQpD5C+XRK.SwxouWL9DGcmKdIZQK7uwdUD..RrN8S6zzfGZJ86VomVDkxkzNjztrRQESjzBv3ol1c9KnFszG9AsX4..fjtQOpQnu8oO.8tuy58xz7SjyVIcX6TUQeIo.LKURWjoCdNUMW83O1ixc8B..rtydRSzqGu57jzzTBpodSJAXpVR2moCNUpR0y7LOCGWZ..3al9zJWsz5t7xIS5Ljz.TB4l58TB6BH.LN4zbSFeY08wM9GzTl7jrVAA..zaRmtKM9wOds+8uOuLMWibtpPxokq+XNVrb9GhFGd4EV9KS3E..DHJpnB0as90qToJ0KSyRkyW7dNsb8.LKUNufmF4NW7RT0+7qvhkC..P+aLidT5+qG7g8xTTjb9h2K1JETDUtbOvTsjVhoCdNUMW8atu+IZZW..D3rvqWcNe+vjqFfoD47OzL59dIUpR0q8Zul9Nm9oY0hB..Ha8i9g+.0dGcpladalNE4zub04pMw61kG15HZZW..DEXgl5MsbBxb.qUTQD4h8.im56kG8weJBu..fHgLM0qWlBkitMR4ZagT4R5IMcv25BWjtqE+OXupA..viN8S6zTIkVlV6ZdSSmhyP4fuWR4RAX7z6bTEUTodhm3woocA.Pjy3F62SGN8QTSM9wlNEkKoFTNzVIkK0CLqQd3oB3eokV0XF8nrX4...XOoS2kpppKUaXCuuoSwAkS+vjS7dIkqzCLUKODd4EV9KS3E..DoUTQEpGdod59g4LkSehlSHWH.SIxC+CjacgKhKqN..DKLlQOJ8BK+k8xTb0RZ1VpbBU4B8.yZjzHLYfoRUpVwJdM56E..DaXg9g4mHomRw7Ws53dOvrPIY75ow88B..hirv8CyZULekXhyagTIRpVSGL22K..HtpnhJTu5qsBuLEWjh4AXhyagjwaczbpZt5At+eikKG..ffyfG7f756kTrdqjhqAXVnjtdSFXl24nhJpPKWR...AqezO7GnVZcWp0VawjgmmjFkj7zR4DVhi8.SIx4sNpHSF7Kr7WlScD..xYzRq6T+eNlQ6ko3hUL74FHN1CL0ICCuL+ETCgW..PNEKbzpqSN2l8wJwssPZ1R5uyjAlJUoZ0u4axQlF..4bF2X+dp8N5TM271LY34Io+GR58raU4uhSagTwx4MbvnUe482vGpYNioa0BB..Hpns16PkO0o5kiV8zTL5AeLNsER0JCCubqKbQDdA..4zF5PFrtqZuGuLE0YoRIPDW1BoxkzSZx.411E..IEdbqjJVN6LS8Vsn7IwksPZ6RZrlLP15H..jjXgsRJkbZYiHs3vVHUqLL7x7WPMDdA..IJIksRJpuERkHm+Ox7b6.SkpTUWc0wEVG..RbF2X+dd4BtqDIsCIsKqVTVVTeKjVibduFbMtv5..PRlGuf6NnjFmjNr8pH6JJuBLkKo6yjAN+ETiVxh+Gsa0...Dib5m1oou8oO.8tuy5MY3EKo++TDtgdixq.iwMt6+RKspwL5QY4xA..HdIc5tTUUcoZCa38McJhrMzaTsIdWnLL7xi93OEgW...jTQEUntm68d8xTrTaUK1VTbKjJVN89hqab2JpnR8qu26g67E..fiYvCdP5voOhZpwO1jgORI0fhfqBSTbEXpUFdi6dG+xeIm5H..fdXQK51TpTkZ5vijqBSTaEXJQRulICb9KnFsnE92X2pA..HGPQEUn9y+V4YZC8dFx4TIsc6VUdSTqIdM9XSev1ZWCcHC1xkC..PtiYNyyyzF5MsbVjgHywpNJsERkKCCu7nO9SQ3E..fSBOzPuEImCXSjQTZKjpSNo6bEZbW..friGan2wKoUnHxpvDUVAlpkzTMYfW+MdSz3t...YoEsnayzgVjbNnMQBQk.L0Zxfl+BpQWxEOKKWJ...4tF5PFrdzG+oLc3WsLX2R7CQg.LUKoyzjAVSM0X2JA..HA3pthKyKGq5ZsXoXrv9THUrbNVVtN.yct3kn68dp05ED.feKc5tzN20t60ettNxQzd1yd62w2711pq+LKnfBzvJa3eie7IdVS3q+9iZjifsjOA4MV85zb9YFc1YjjllB42IovN.SsRZIlLPN1zn69fMtoS5eneX3O9EGRc1YmgcYf.xGsoOT6e+6KrKCOKUpR0zl9490gdl3YMAMkIOovtrfOvCGq5FjyoGNzDlAXJVNWMwt9V2899sOf9U+xa25EDhedrm3o0C8.2eNw+QCfnrToJUWwUdU5GeNmil4LldXWNvRZrosnu+TNaSGdnuJLgkZkzexseKUpR+SG9vo+S.24hWhq+8O7M9Fey6eqhJp7O8wM9GB6+H.XIyeA0X5uWndEhBql3sXY3EhyhtCduifzu49eP8qu26NrKCfDoMrg2We+ob1Zg21sqzo6JrKG3Qd3.wLUEhaiTXcQ18qjTktcPbo0AIo1ZuCcAm+OIrKCfDulZ7i0m9Y6WiYLiQm9ocZgc4.C4wK2tTx4hnMvEFq.iwq9BWZcPRZYK6YC6R..Gypd8UpK7Bt.0V6cD1kB7.Ob41EZqBSXrBLFu5K22+jwugCHGx4N8oE1k..5lCe3uTczQmpxYNSVg7XphJpP8e8eKs4M2fICOTVElfdEX7zpu.zXSaIrKA.zKV0quR8jO8xB6x.dPM0bslNzPYUXB5.LKTFbroqnhJ4IC..Hh6u+u6NTKstyvtLfgF5PFrtyEuDSGdsVrTxJgQ.FWiUeAY7IacagcI.f9wJVwJC6R.dPbZUXBx.LUKV8E.fbZ+568togdiw73pvTsEKkSpfL.SslLHV8E.f3kM8QF0HnHhvCqByUq.7kpNnBvTsL3AajUeA.H942a1IYAQDwkdgInBvPuu.qvjWgW.Drd9miSiTbmGWElhsXozmBh.LkKow51Awpu..DewUdP7lGWEFiVzB2JHBvvpu..jv74cdnvtDfG4gUgYgJ.VEl+Bed9KQRWjaGTpTkxpufdUWc4sGNtJpnRMjgNTKUMtSAETfFVYCOT9rQzQ94muF4H7+eev.G3.zPGxf62eMM1zVzm24gzueyMnGYoOjU+7OzgH.SbWlUgwfGN2hjzrkOe67dJ94jKmh+pc6fdzG+ozsbSWu8qFD6cJmh29sru+F9PMyYLcKUM.4NZq8NzC8POr0BxbmKdI5dumZsxbgvSiMsE88mxYaxPOn74SjjetEREKmDXtRpTkpq5JtLenb.jJrfBB6R.HRZnCYvZoO7CpG8weJqLec1YmVYdP3ZJSdRZ9KnFSF5YJe9hsyOCvTsL3hq65tAdwoA.BK2xMc8l9evB4nl67lmoCsVKVFeC9Y.FiZd2K+xlqsqCjiHcZu0+K.H63g+CVesOZSenEpDDELyYLcUQEUZxPmp7wsQxuBvLaYvEW2ct3kbRa5LjbsycsaOOGiZjivBUBPtsYNioqToJ0Syw92+9rT0fn.Obxf8siTseEfoZSFz4e9mukKCfSDaOIP1YZS+bC6R.QHyX5kaZn1pkOcjp8i.LkHCN5zyop4poL4IY+pA..t13mvYE1k.hPJpnB0htieoQCUFbfdxF9Q.FiVtn4cYWtsqCjigKFK.fvyrtPi2kDeYaj7i.LU61AvEWGxFd8hwZNUQChCjsFv.FPXWBHhwCOu.iURiyxki0CvTsL3nSa3xRA3JEVH8+BP1ZPCj.L3axC8pp0WEF+H.iq4gkkB...AjoL4IY5Qpd1xxMyqMCvThbNy2txstvEwQmFYkOcu6IrKAfDC54LzWL7HUa8l40lAXp1jAMOKbgIgjgibji3owW1v4NfAHawiwH5Kd7HUaMgZ.lJpnRN5zHvje94G1k..PrWQEUnthq7pLYnV8l40VAXJWFby6d4l8+AfDpt5hmR..fnf4MOiOUmVqYdsU.lpMYPSeZttkYPB1pd8UF1k.PhA8bF5OiYzixzqlBq0GL1H.SwxfBhl2EAsgO7xB6R.H1vq8bFx8c9WvEZxvNS4rqMdlMBvLaYvc+xOk28HDvJrfBB6R.H1vqaYqWeLHQz2EeQFEfQxRMyqsBv3JoRUpl4LltE9nQRQas2QXWB.IJdcKa4wfL2WQEUno2LuVYaj7Z.lhkAObiW2MX7yxMRn5j6jBf.S5zzv7H67iOmywjgUjrvpv30.LFU.W3E7S83GKf6MpQx8.CP1Xm6Z2gcIfXhYNioa51E54UgIvCvLmplqFynGkG+XAbuhJh2BIffRAzyYIFFtqJWj73SKfWBvThbdgIcEC6ZYjv8IacagcI.jXXi+8sgU1vsPkf3.OrqJdZUX7R.Fi9f4teA..H2wXF8n7xC7nw7R.lpc6.l+Bpg69EDJL7BWB..YACuY88z1HYZ.lRjAaezO87u.C+3PR2QO5Q8z3KrP5+EfrkW+22jjl3YMAKTIHtvC6thwqBioAXL5CbFSubC+3PR2d2CmJBffB+6avsF5PFbf+zBDXAXt0EtHNEH...4nL7P5X71HYR.lhkyShsq7iOGZdWDdJa3bGv.DjlxjmTXWBHf4gsQpbSFjIAXX6iPf6i1zG5owme94aoJA..8lfdajBj.Lr8Qvq1+92WXWB..3jvvsQJvBv3529nwMd5Fc.f3h1aqMOM94ufZrTkf3FC2FohjAaijaCvvkWGBb13gka3CuLKTI.ICaXCueXWBHlZnCYvA1kZmuGfYNUMWt75fmXiGVtB4cYAHvv6fTx1zmQElLLeO.S4t8Cf29H.fjEdGjR1L7sQ5LkykjaVyMAXF2w9.bkIMQ5+E..fjhwL5QoToJ0jg5pUgwMAXJ2c0gTEUToFynGkaGFvIXW6dOddNF0H4dfAHnv0V.tBydajJ2M+hcS.FWu+TFtOX.m.a7trvw3GH6zV6c344XjifsPJo6GeNmiICyUmxY2Dfw0GknILgw61g..fPTmcdnvtDPNfIOoIZ5PKOa+ElsAXL53SOyYLcSFFvIvFq.C..BNEUTglde.k04Mx1.Lk61J3VW3hb6P.5Ud8kw0vq1Z.XHdGjfjwuAhkms+B8s.Lb66hnhBKj9eA.HnY3oPdrJKecpyl.LEerIzU33SC..jb4giSc4YyunrI.SVMQcWpTkxwmFViWeIpAP1yFWaA.Y3mGmZeI.igELPuxquD0kMbtCX.xVdso44gbDcmgsSR4YyuHeI.C8+BhR3R0B.Hb3m8AyIK.C8+BBU13knF..gCO75TW9I6WvIK.yIcB5oJpnRd8og0XiWhZ..Ddl7TlhICq7S1u.qGfwvBEv2L7gWVXWB..IV9UevbxBvLN29IR+ufnlBKnfvtD.hM9z85sSgTA7uugdvC8AS+5jEfw0Widz+Kvl9jstsvtD.RTNxQNhmF+vJiGxQbh7q9fo+BvzuCr2jJUoz+K...3D3G8AS+Uql7RE...H.jDQAQEfw0aezrtHidyGA7U7tr..Dt9tCyndQreygX0Ufg9eA11e7KNTXWB..viLr8RJu+9Is5JvLxQvdeB6pyN6LrKA..3QF97BUjjJou9I6q.LEKoyzseRrT8..waO+ysLOM9ALfAXoJA4ZL7YlnOWLk9J.iqW8Ed+KPTD+9Rff0fFHAXPua7S3rLYXtN.S4t8SfGLO3G75WMH..hFL7REs795mvZq.SYkwscJ...5cF1mr9+VHMxQP.F...z6F5PFrRkpT2Nr9rQd6s.LF0.uF1gw.8o1ZuCOOGFtmq..vGLsoetlLrdcQU5s.Lz.uHRnyN4NfAHH0XSawyyw.oIdQ+vlMxqUBvPC7B..IwyIC5WF1Hu9X.FZfW3C5xiOpb..HZwlMxauEfoD2Nybt+geXO6YuddNl3YwyaA.PTggqPWu1Wt8V.lo51YlafW...jMLruYKum+.8L.SItcFmSUy0jBA..QLrssHHXXeyVRO+A7b.lAMnAYRg.bR8o6cOgcI.jn30sskufVjMNiy3LLYXkzyefdFfob2NiCqLdApg+3HV3qFjs2DH3TXgEF1k.hALrQdKum+.8L.SwtcFM7HQA..fDnQMR+YKjb8QntvBJvjBA3jpqt5JrKA..XYEUTgl7jB7MNIRdN.CKQO7Kq50WYXWB..vGXimTfdFfoH2LSUTQklT..ABdhK.bmidziF1k.RHF3.GnIC6DZyktGfob2NSCYnC0jB...QP6cO61SimmUFjs9NmgQW.tk28+ld6hrKqwuYE9Ea7nxAffU94meXWBHlvvC.j8VAF9Mq...vsL7..0m8.iqOB07NyfnLCe11A.fOyvC.TetBLt9DHA3W97NOTXWB..vGYvQodrc+uwSq.CGgZ3WNzgH.CPPq81ZKrKAjfX3Qo9qypz8.LisW9E1mLH4D..hv1vFdeOMdZq.3FE3w9fw3SgjgIm.BL7GlB.Dc402RwLAXJ2yUBfE0711ZXWB..H5o7LeGiWAFNgG...vTdcUxyDfgSfD...h59F8.iqOARFdK5AjUrwogfSIGP1qs16HrKAjvLpQZzs4eudJjbECuE8.xJd8zP..2oSKb2KwWz.bihJpPSF12H.S4tcFJn.dFA...f4pnhJc6P95q7EiWAlwL5QY5PA...zPF5PMdrF2CL.9Ea7RTO+ETiEpD..DAUhzwCv3pagWCVxG...fSPYC2nF4sDIC2BIurjO..H5YW6dOdZ777x.Sje9l2OsF2CL.9EdIpABdG8nG0SimmWFDfJVxI.S4tcjF9.LAjUrwKQM2Tz..QeCX.CvjgMNICWAFu9.LA...LnAZT.FIwVHA..fXn+LwQnFQL7RTC.f9QIRNAXb8C4ng6YEPfwquxo.IMe5d81oPhdiDlvv2CoRjLbKj7xdVA.fnmibji3owSuQBSX36gjjnGXPDjMdIpA.PtMBvfHGdIpA.vICMwKxIMkIOovtD..f+nDICahWCa5F...fSfAO9tmojgagjWZ5Ff9iMdIpAf68Qa5C8z34zohfF8.C..z92+97z34zohfFAXPNGCVNR..DyP.FDovKQM..xFDfAQJ13knF..49H.C...hcH.Cx4L9IbVgcI.DqXiS+2.oIdQ.i.LHRwqOnb.HbLzgL3vtDPBCAXPjhWeP4..Px.AX...PrCAXPNmIdVSHrKA..3yH.ChTd9maYgcI..fX.Bv..jv8IacadZ7yop4ZoJAH6Q.FjyofBxOrKAfDkBKjG3WD7H.ChLZq8Nrx7LlQOJqLO..H5h.LHxnSdGj..PVh.L...H1g.LHmRpTkF1k.PryQO5QC6R.v0H.ChL10t89yHvzl94ZgJAHYYu6Y2dZ7kM7QXoJAH6Q.FDYvWEHP7T94yI+CAOBv...fXGiBvXqi6JfsUPAED1k...bgt5pKiF2eljp2sChi6J7CMuss544XXkMbKTI..HnrpWektcH6PhsPB.HwyzuBXfPxgkH.C.PhmAeEvm.dA3QXf.LHmBmFB.fjAiBv74zCLvG77O2x77bLxQPOv..DW3kCEjQMw6gNDAX...f2X3gBZ6RrER...Hdgl3EQGoSamSAQAEPOv.3Fbudg3JiBv7o606uYM.c2N2k2dKVxXLidTVYd.RJrw850Tl7jrPkfjHu7F3YTOvbjibDi+.A...jL9MvqdI1BI...DCYT.lOZSensqCjv0kEVUuToJ0BUB..BJ+wuv7svLS.lFbyf1+92mwef.8l8rm8544XZS+bsPk..ffRmc1oICiiQM.PRmWuXRYkOQHvaGi5FaZK1qT..PnvqWLorxmvK7RKojI.S8VoR.BQETPAgcI...WvfVR4qa4EiWAFdOjfM0711pmmigUFuCR..wEd8BLMS.lC31Ax6gD...LkgWfoGHy2w3.LFd4y....XpCj46jI.ygc6Lr28Xmq9c.aI+74cPBvs75SCC8dFL0mr0s4owmI.y18do.Xtm+4VlmmiQNB5AF.2xqOMLz6YHfUeluiwMwqM9O3...fjIud3M5d.FWca7B...DvpOy2wS2DubY1gnjBJfdfA.Htvq6jS2CvTuaGrMd.9.Zo0cZk4YLidTVYd..PjzIrSQdZEXrwCvGvQNBGIefvhWtJ2kjFv.FfkpDjjXicvwSq.iWdFrA.P3yfqx8SvfFHAXf6Y3N3Te2+a5d.FWeWvX3yfMf0wKhK.P7gg6fyIjSo6AXb8cAiWW5Q.I67tZwKhK.P7gg6fyIjSom8.yAcyL40kdDPh2UK.fjFC2AmCz8+ldFf4.xk3nTC...2vviP8A59eSOCv35sQxFK+OfWw6wBf6Yiu.zARS7BWps16vjgsid9C34Ufgk+GQA7dr.DNF5PFbXWBHloSyV3iCzye.OuBLd8sL.feOD.PxgguB0ei7IdN.S6s0lIEB...RfrwIPR5aFf4vRJsalwMrg22jBAvpxOedGj..hCrwIPRp2eJAb8pvvIQBgsQNB5AF.f3.COARmzUfoW+EcxrqcuGSJF.Iw1PBDVLrWD9Zyop4ZoJAIEF9389MNARR8d.lC31Y9y9TdTGg4XaHAhmJrvBC6R.wLFd0qbfd6GzNq.yt1kaGBfUUPAzCL..QcF9FH0q4R5s.L061YdUu9Jc6P.rpwL5QE1k...NIL7Zyn9d6Gr2BvH0G62T+wv80B...IDF9HPefd6GruBv35sQZKeh2ZFLjLQvWfvyQO5QC6R.IHs0dGl7HPmV9c.FZjWXhibDu+GflJUoVnR.Rd16d1smFeYCeDVpRPRfgKzQelGwZAXnQdQXYZS+bC6R.HQhKPR3F6cuFsPG02W+D8U.l9b.8EZjW...zWLbE+b8JvHYPi7xMxK...5MFdC7Vee8SzeAXb81H40a0Qj7Xiaw4BJn.KTI..vuX3BbbP47FM1qrZ.FCOe2HAyFmBhgUFuCR.lnqt5JrKAjPX3WrZ+lCo+BvTua+jL77cC.fPfW6cwgO7xrTkfbcauYi1gl56uexS1Jvj1MeR6e+6i60C.fDhBY6aQVZcqcMlLLiWAlS5f6Mbg1gfFGkS.fnqVZcmlbA1I4gUf4jN3digKSDRn9z858l3cjifdfA.HpxvE1ngS1u.qGfwvkIBITG4HGIrKA..3iL7l5u9S1u.qGfg9fA.H5qs16HrKAjP7Ju7KYxvp+j8K3jEfQJKVFmdh9fAAoANvAD1k.PrSmcdHOOGSYxSxBUBxk4g9e4j1CtYS.l5c8mJ8ACBPCcHCNrKA..zKLbAM1g5mKvtL7k.LzGLHawEoE.PtK+39eICeI.C8ACxV7HfB.j6xvEzn9r4WT1DfQxf9f4ipeytcH.tVEUTYXWB..nWzXSawz9eIqR8jsAXp2se5MT+G41g.3ZCYnCMrKAfXoO2BMwKP+wvG34rdAS7s.Lq50WoRml9a..HJ5PGxaAXl+BpwRUBxUY3BYTe19KzMAXb06hjjzF2TVWG...HGQ5zcYZONl0MMS1FfQhiSMrLabQZMvANPKTI..vlLbALRKW7FL5l.LttUhM712CID13hz56bFbI1A.D0762rqO6ORtbgR70UfgiSM..PxigGeZWMH2Df4.x41wyUdq0+NtcH..vm8G+BusBnETPAVpRPtlOXiaxziOc8t4WraBv35IWRZSabCtcH.YsAL.1BI.SzYmc5owOrxFtkpDjqYaaqYSF1NjyBkj0ba.FWulPaXCuOu5ov2LHdHGA.hTLbgKbc9BSVAFWebp2zGYTy7fbbccji344nfBx2BUB..rgVZcmZCa38MYn9d.Fi9Pd60+VF7wfbc6YO60yywXF8nrPk...avv9d8fxEGe5LBj.Lq50WIaiDrtToJMrKA..zMFt8Q0axfLI.iQePrMRv1l1zO2vtD.hs9nM8gdZ74mOaeKNQA41GIYV.lCKo051Aw1HAaiagW.yY3wb8qMxQvoPBmHC29nzJ.CvHS9vXajfswsvK.PzQPc5ixHvBvHw1HA6Z3CurvtD..fB9sORx7.LrMRvy9z8tGOM9B4l.E.HRHn29HIyCvHS9PYajP2cDOdOvLpQNBKUI..vKB5sORJfCvHI8pu1J8vGIvwUTQEF1k.PrT5zc444XfbKXii4C13lB7sORxaAXLZaj3sQB1.2AL.lam6Z2ddNF5PFrEpDjK32u4Maxv7z1GI4s.LxjO7Mrg2WM1zV73GKR53NfAvb7Db.aIc5tzq7xujIC0SgWjBg.LRRu8a+1d7iE..lhmfCXKabS0a5cJTnGf4vRZ4tcPuxK+RVYOXA..P34cd60axv771GI48.LxjhX+6eeZiapdK7Qi3rt5hPr..wUs0dG54etkYxPqyFe91J.SZ2NHCSsgbHq504DoADGUQEUF1k.h.V2aYb6fTmM97sQ.FICJlm+4VF2IL..wPCYnCMrKADArt0rZSF1Akz1swmenEfQh6DljNNJz..wSd3teYo1pFrU.lsKoc31A8LO0SXoOdDG4kiBc6s0lEqD..3Fui4mlXO27tYXq.LRFrJL6e+6SuwpWmEKAjTrgM79bR1..BAs0dG5QV5CYxPWqjNfspiPM.iDMyKLWSa4SB6R..HwIrad2LrY.FitSXnYdStF3.GnmFuGVBS..XHCadWqb2uzc1L.ijgoqV1xdVKWFHN36bFd6wf6QV5Cw1HA.DfdiUuNSad25rboX8.L0KmiHkqvMyaxz.Ff2eMaeoW40rPk..frgGZ6Cqc5ixv1AXjLnH2+92mV8ZeKenTPT1HGQYddNdnG39YKHAboVZcmgcIfXHOby61frXy6lgeDfoNSFzqZ1qYIhwrwCJ292+9zC8POrEpFfjiibjiF1k.hg7P6dX8UeQxeBvXTy7tgM795C13l7gxAQYyeA03443QV5Co5dwWwBUCPxvm24gB6R.wLoS2k9026caxPOnrby6lgeDfQxvUgYkqXEVtLPT2O9blpUlmq4puRtSg.xR6cu6MrKADy3g9MrNKVFm.+J.S8xfal2m+4VF6MaByzmlcBvHIMme1EoG6IdZqMe.4p15mrkvtDPLigGcZIeZ6ij7u.LRFVzqXE79HkjLzgLXMmplq0luewMeCppKcdpwl3OfFn2jNcW7RvCWwCGc5kKm1JwW3mAXpSNWbMtxu9duaNUIILy6xtbqNeq50Wo99S4r0Bt1qi9pBnG13lp2yyQAETf2KDDa7zOowuag91puH4uAXjLr34UpNYYFSube4ko94etkoJq3bUok9c0BusaW08huBqLCR7Vwq8pddNFVYC2BUBhCZrosX5puzfbdnm8MmheN4RpDIse2NnToJUM2bypnhJz9UDhjdrm3o0u3lug.6yaNUMWUXgEpxF9HT94muF9vKSEVPApfBx2JGuafnnFaZK56Oky1yyyi93Oktka55sPEgntEbsWmo28KWi7wF3Ux+CvH47+.tZ2NH9WPRVRmtKUUUWpoI88EYB4TPAE70eEmCX.CPCZfN2fvCbfCPCcHCNLKQfrlM+2w3OeNYvCAdOnbV.CeUPDfoDwpvfrvGrwMoJq3bC6xvHUTQkZHCcnRRmPfGI80qtiyOGqvCBdoS2kt1ZtNq07tebi+AMkIOIqLWH5xCq9xsIet+WjBl.LRNGqZWedYIkexyhuqZM8xRJVJUpR0zl9wCsMvANvS3QtL+7yWibDGOLDAffa0XSaQO3C9fV8jGQ.lbedX0WRKmEtv2N8QYDTAXJWRejaGDqBSxis+JESBxrUWYzyU.R5aFDJC9OBk6Ic5tzN20t0t18dzueyMX5WAc+5O8m9SVeNQzhG9hIuaIUqcqldWPEfQxoajGqaGzKr7WVU+yuBenbPTU5zcowO9wq8u+8E1kRhTOWUntqmqPTOMwyZBmz4OorJRs0dGpyr3J6+S1515yetidzip8tmc2m+7ezl9v.+eOohJpTevG7dA5mIBVs0dG5LG5PLc3+UJ.V8EIo+hf3C4XVpjdA2NnW8keIBvjvTTQEp2Z8qWW3EbADhIDr+8uOs+mK59+uai2Oqrker5EwcidLiIrKA3y7vi1nudw00SA4JvH47bZeltcPrJLISs0dG51u86fsSBHBYUu4Z0kbwyJrKC3S73pujRN+24CD98EYWOUqIC5dp8tT5zcY4RAQcCcHCVO6xdFcqKbQgco.fiYRS7juMgH95gdnG1zgtbEfgWjj9yCxOL4zGLWijJ1MC5vG9K0fFRI5rmzD8mpBQV4k2opy67pTkTZY5+8N1tN7g+xvtj.RrlSUyU0bsyOrKC3SZros3k+46Eq.b6ijB9UfQxvUg4gdf6mUgIAq5e9UnlatYcmKdIgco.jXY62sLDsrrkYbOeE3q9hTvuBLRrJLvP4k2opoOsxUUW5b025+iSUM03GG1kDPhQEUTotu+o6MrKC3SZrosna4luQSGdfu5KRgyJvHwpv.OXLidTZoO7CpC1V65NW7R7kGBR.bht9a7lB6R.9n31puHE7mBot6.xfSjz88ae.8q9k2t8qFDakNcWZiapd862bC5QV5CE1kCPNm4ufZzy8rOSXWFvm3wG4y.8jG0cgY.lpkA2KLRRGrs14QzC8pzo6RMskOQ+9MuY0TiMFodbHAhiRkpTUeCMvelaNrYNyyyz+rxkKm+a4ghvL.ijgqByct3kn68dp05ECx8zV6cncs68nsssl0d2ytCkatTf3pToJUu05Weh3lSNo5MV85zb9YWjoCOzV8EovO.S0xvUggGSLXpLWw66Z26QewW7EZu6Y2pqt5hKLOftohJpTO7ReXBujCKc5tTUUcowxUeQJ7CvHY3pvvdxB+RiMsEII84cdHcnC47V17o6cO5HG4HRhqWdj66VW3hzcW6R3gzMG2i8DOs9E27MX5vC0UeQJZDfY1RZ0lLv2eCenl4LltkKGfrWKstScjibTIIsqcuGczi578+iewgTmc1ojDqtChMpnhJ08bu2KqtcBPas2gJepS0zsTOzW8EonQ.FIo5kzTc6f3UQEwQcOzSWG4HZO6Yue8OW2C9HI0dasQiHCe2stvEo4Mu4QvkDjEeW0pe88d2lLzzRpDEB26K8TTI.S4R5iLYfO5i+T5Vtoq2tUCPDUl92IidF.R5D2tqLn4kQ2UQEUpIOkonwM9InYL8xYqhRX73wl9tkg2ka1VTI.ijgqBSpTkplatY9W.AbodFFJit26O8Tyaaq847wVkYlJpnRMjgNzd8myzfmoRUpl1zOWIIUPAEngU1vU94muF4HFtF0HGA+4kIbK3ZuNS6kuHypuHEsBvThj1uICjiUMP7Q22BM2p+BW4Gx7ez2TrkLHpwiGa5qQR0YupwahRAXjb9+XtZSFHGqZ..f9V5zcowO9wa51IeP4rPCQFg0agTeYgxYIpbsG7AePKWJ...4NdxmdYdoW3p1hkhUDFuF08muRR4Iml50UZs0VTIkVlF2X+dVun...hyZo0cpK4hMdqiZPQjF2s6hZagTFGPFb41QC8B..7M4gF2UJBbo00ahZq.SFGPRyysC5vG9K0+wW8epy67pz9UD..PLzar50o+9e0eqoC+QjzJrX4XMQ0UfQxviUsDMzK..fjmab2H0wltmhZMwa2UsoC7tV7hU5zcYwRA..H9YI0d2dowcqUQzvKRQ2sPRx4+S6uRRSwsCbe66yzfFRI5rmzDseUA..DCzXSaQ0bsy2zguCEAO4QcWTdKjjjJVN8CSQlL3+kVZkmBd..j3jNcWppptTu7VpMM4zJGQVQ4sPRxYUXp1zAWasF8PUA..Dq8jO8x7R3kkqHd3Eon8VHkwtjSRvRb6.as0Vz29zG.akD..RLZrosnKadUY5vSKoySN2KaQZQ8sPJiRjguSRRrUR..H4Xly777xpuDodui5OwgUfQxYqjNEYvMzqjz+5+1+ttzpliUKH..fnlG6IdZ8XO5iX5vaPNOoOwBwk.LRN6G20HmF60UXqj..PttFaZK5RL+klVxocMhrGa5dJp2Du8T0lNvewMeCpkV2oEKE..fngzo6R20hWrWlh6VQvmKf9SbZEXjb9+bM5tgQhsRB..4lV5+qGSOyS+jlN7cHCd9dBaws.LRRMJoKSrUR...d8TGI47eS8.1oZBNwkSgTOMaIsZSGLuUR..HWfGeqijbdrFiMMta2EGWAFIm6FlwKoQZxfa6fsoYMqYo7x6TsaUA..Df9U+8+C58du2wzgeP4r0QQ967kdSbM.ijz6IoaTR441Atu88Y5+3q9O04cdUZ+pB..H.7Fqdc51usa0KSwEKmEDHVJNGf4qjy+GuQMdTSM9w5udrSPidTivtUE..fOqkV2otlpuZc3C+klNEOhjdJKVRAt3ZOvzc0Ioq1jAlJUop9FZPCcHC1tUD..fOppKcdZUu9JMc3GTRiSwn67kdSb6dfo2rP47OLbs8u+8oa+1uCKWN...9mey8+fdI7hjyAgIVGdQJduERY7URZ6xvK4tVasEk2eYA5G8C+AVsn...rsOXiaRU+yuRuLE2sjVgkJmPUtvVHkwRkjwcyz6ugOTybFS2hkC..f8zV6cnxm5T8xQldGxYqixIjKrERYrP47ObLxMbc0n1ZuCKVN...1yse62gWBujVd343IJJWJ.ijy+vIsICLS+vjNcW1sh...7HKz2K0Jm1sHmQtPOvzcegb5IlyyjA2Zqsn+hu0opoOsxsZQA..Xp2X0qSWeMy2KSwZUL811s+jqEfQx4sRx3ao2Mu4F39gA..QBV39d4fx4KpOVda61exkZh2tqX47vTUjoSv+RKspwL5QYsBB..vMRmtKcs0bcdcqilljp2NUTzRtVOvjwgky4b2X21BuM5GF..DZVRs2sWCub2JGM7hTt4VHkwAjyJLUtICde66yz+5+1+u5hl0EZyZB..3j5wdhmVK4t9G8xTzfxwN0Q8Ttb.FImjmSSRkXxfat4so+q+aQS8B.f.yGrwMoKedWpWlhzRZJJGruW5tb0dfo67b+vrp2bs5Rt3YYsBB..n2zRq6TW3EbAd49dQJGtuW5tb0dfo67b+vLme1EoVZcmVpb...9lRmtKcaK717Z3kb59do6RBAXjb9Gl2lWlfK7Bt.todA.fu4Zq45zF1v66koXsx4BqKQHWuGX5NOc+vb3C+kpiN5TUNyYp7x6TsakA.fDsEeW0pm5IebuLE4r22K8kjT.FIo2SR+DIcFlL3VasENYR..vpp6EeEc62lwuEwRNMs64Im98LwHoEf4qjyJwbYRJOSl.NYR..vVdiUuNcEWlmNwQRR2nb9BzSTRZAXjbduj1kjlmoSvl2bCpjRKSiareO6UU..HQokV2oJep+XuNMOhj9MVnbhcRhAXjbBvX7kbmjzZWyaxalD..LRliKsGdiijbtr5L9KFOtKIbOvzepSRWsWl.dyj..fajNcWppptTudhi1gb9hvOrUJpXnjxwnturP47aBL1EdAW.2QL..Hqj4AZzigWRKmmIfDa3EIVAFIKbS8lJUop9FZPCcHC1ZEE..x8rfq85zy+bKyqSyEKo0XgxIVKouBLRNIXKWNIZMx92+9zse62Aud0..nOs36pVaDd4ZDgWjTxsId6ou3Xey3mbfVasE8oe194htC..eCK9tpU+568t85zrbkftocOYH.ywscc7KCHiPHF..zS+l6+A0RV7+nWmlFjGeW+x0P.lSTiRJkjFmoSPqs1h9K9VmJWzc..PuwpWmt9ZluWmlcnD1yDP1f.LeSqQNOE4kX5Dr4M2.2Vu..IbuwpWmlyO6h75zjVNeQ0I5SbTug.L8t0HO7lIIQHF.fjLKFdobkvdiixVDfo28URZEx4MSpXSmDBw..j7XovKRRee4zelnWP.l91WIo5kGd3GkHDC.PRhECubMJA9.M5FDfo+8Ex42.QHF..zurb3k5rwDkKi.LmbegbNcRU6kIgPL..4trX3kD6qKsaQ.lryAjzAkGOC9DhA.H2iECurbIcC1XhRBH.S1a6hPL..narb3kpswDkTP.F2gPL..PRDdIrQ.F2a6x4U7tbuLIDhA.H9xhgWxbK6BWh.Llod4wmb.oiGh4rlvD3sSB.HlvxgWJW7DAXDBvXt0HKEhgG.R.f3gEeW0pa4lsRe1lI7BOQ.Fh.LdiUBwvqXM.Pz2huqZ0u9duaaLUDdwBH.i2Y0PLSYJSQEUTg1ox..fUP3knmSIrKfbH0Ioq1qSRpTkp2Z8qWiYzix6UD..7jzo6RWaMWmV0quRaLcDdwhH.icUmHDC.PNABuDsQ.F6qNYgPLRRq5MWqtjKdV1Xp..fKzRq6TW3EbAZ+6ee1X5H7hOfdfw9rROwHI86V4JTIkVlF2X+ddup..PVgvKwCDfwerFI8WIoo30IZsq4M4BuC.Hf7Fqdcp7o9i0gO7WZioivK9HBv3edOYgmc.Im6Jl16nSU9TmJGyZ..exi8DOstlq9Js0zsb4bC6xkTmOg.L9Kq71YWi+sH..vPUIQTPTIII0byaiiYM.fOHc5tz+y+o6S+8+c+s1ZJ4sMJ.PS7FLpVRufMlHNgR..1ikOoQRDdIvP.lfS4xo2XJxFSFmPI..uwxMqqjzcKoZs0jg9GagTv4.xouXtLIkmWmre2JWgx6ur.8i9g+.uNU..INuwpWmtlpuZaFd4ZjzRs0jgSNBvDr9B4Dh4mHoh85j8ga7Cn4dA.boey8+f55qY915jFkVR2nbtCvP.h.LAuuPN+F8ehjNCuNYM271zV2ZyZBm0YoS+zNMuNc..4rRmtKcy2xuPOv8+ar1TJm1C38r0DhrGAXBGekjVgj99RpDuNY6aeeld228c0fGZJM5QMBuNc..4bZo0cpErfqUuwp9c1ZJOnbNlza2VSHbGBvDd9J4rRLV4V68vG9K0uakqfK8N.fdHykS2912mYqobGx4hJ8.1ZBg6Q.lv2ZjyxPdd1Xx17laPsz5t39hA.IdYteWtka9Fr4ztV4b2dwsqaHi.LQCMJmkibZxBmPoVasEs10rVMpw7Wquaoo7bwA.D2zRq6TK51uC8TO4iayoc4RZdhaW2HABvDcrcYwiY8gO7WpW9kdQNp0.HwIyQj9e9+m+us4zdMh63kHEBvDs7Exo4dmlrvITRx4nVyVJAfjftukQV5HRK4rE++D4rc+HBg.LQOGVNgXFfrPy8Jc7sThSoD.xU4SaYzNjS+tznMmTXGDfIZ5qjSZ+SQN2w.dV2OkRm0Dl.W7c.HmQcu3qnJq3bUqs1hMm1LMq6Ar4jB6g.LQa0KmuBfehrPewH4bJk15VaVkM7QnAO3AYioD.HTj4hoq165Ns8Te2R5FDMqajFOliwCiSN2YLi0lS5i93Oktka55s4TB.DH9fMtIcCWWM17sLRxoeWpVzuKwBrBLwCYZt2QIoQZqI8cem0qVZcWZLiYL7LD.fXgzo6R+p+9+AcK27MZyF0U5386R81bRg+gUfI9YgR5gs8jxpw.fntFaZK5xur4Y6UcQx49cYghKmtXEVAl3mFkTCR5hkk5KFImUio8N5Tie7imiaM.hTRmtKsz+WOltr4UksW0EIoaSR+JQ+tD6P.l3oCHomRV5wfLiladaZsqYs5O+akmN6IMQaMs..Fqwl1htoa7lzy7zOosm5L294zuKwTDfI9JyiAo0Np0RNG2Z5MF.D1xzqK0bsy2lOBiYrV479yc.aOwH3POvjanb47UQTjsmX5MF.Dz7oSXTF2ljVpeLwHX8mE1E.rh5kyVIsVaOw+ha9FzLm44oFaZK1dpA.NAs0dGZAW60oJq3b8ivKGTRiWDdImAagTtiuRNG05zxYoQsl8suOSO2ytLtEeAfu4wdhmVW+0Ui9nM8g9wzubwspaNG1BobS9xEemjTpTkp6p16QU+yuBaO0.HApwl1htqEuXsgM799wzyESWNLVAlbSYt369eHooXyI9vG9K0ZWyapVZcWZnC8L44H..Fos16POvC9P5p+4WoezjtRNW2Dmm3gXLmEAXxc8UR58jy+R7zjTw1bxas0VXak.fQxrcQqas91Bibax4sLhKltbXDfI22Ajy1IY0mgfL17laPqbk+Nt6X.vIUiMsEM+4u.8XO5i3GWHcRG+wuksLJAf.LICYZvWq9xVmQl6Nl+4+4F02Y.CTe2RSYyoG.wbs0dGZQ29cna4luQ+Z6hjbdAommb1Bcj.P.ljkcImavWeY0X1299L8xuzKp16nSMrgMLtD7.R3RmtK8+7e59zO6huH0byayu9Xx7ElsB+5C.QSDfI4wWWMFImmjfm3Idb8e8eKMrgMLdak.RXRmtK8rOecp5q9m6m84hDq5RhFGi5jshkTsR5V8yOjG8weJcUWwkQPFfDf2X0qS+s29s4W2htYzfbd8n2te9gfnMBv.Immhf5jzY5We.oRUpVzc7KIHCPNp2X0qSO8S9D9084RFokyWzE2ltf.L3DTqjVhe9AvEgGPtkFaZK5AevGTq50Woe+QsV4rpKGvu+fP7.AXPOUhbVMlo5meHUTQk55uwaRWxEOK+7iA.9jFaZKZYKaY54etk42eTGTNAW3nQiS.AXPeoZ4rLsV+Ett6HHCP7R.FbQxoIcWp3BoC8BBvf9SfzjuRGOHyLld4ziL.QPevF2jV4JVQPEboA47EQcff3CCwSDfAYiwImuJHecakjnYeAhZBnlyMC1tHj0H.CbiYKmfL91oUJiLAYl0Ed9ZnCYv98GG.5lzo6RabS0GjAWRKm+rE1tHj0H.CbqhkyWgzBkO2eLYbmKdIplZtVBx.3yRmtK8RuxqoG5Ate+9dbo6Vtb1p5CDTefH2.AXfoJVNe0RWcP8AN+ETipolZzTl7jBpORfDg1ZuCsrk8r5Weu2cP9w1fbBtTeP9ghbGDfAdUf0eLYPC+BXGAbi4lA84BrBBv.aob47USEXAYRkpTcc2vMoK+xlKauDPVJc5tzpW6aoW8keofp+V95OZ4Dbotf7CE4tH.CrspkSPFeuQe6t4ufZzbm27zLmwzCxOVfXiVZcmZEqXkA81DIQC5BeBAXfeoZEBAYpnhJ0rl8EyoWBPG+zDshW6UChq5+uwGuH3B7QDfA9spUHDjQxYUY9om+Evs7KRbZrosn29se6vX0VxfaPW36H.CBJ0JmvLAdPlToJUWwUdUZdyatZLidTA8GOPfns16Pq6sdast0r5ft2V5NNRzHvP.FDzpVgzJxHwVLgbKg7VD80kgbVsk5DAWP.h.LHrTsBwfLRRyop4py+BtPM8oMUByfXiLgV98atA8HK8gB0RQziKHDQ.FD1pVgbPFIByfnsHTnEIBtfHBBvfnhxU.eOxzWHLChBhXgVjbt.5pUbOtfHBBvfnlwImK6p.6IJn+LmplqlZ4SSSq7ygF.F9t1ZuCsoOpAs8l2VTIzhjyU9echfKHhg.LHppD4r8RA1iF4ISEUTol7Tlh9wmy4nIOoIxyX.rhFaZKp9F1r15mrkvrQb6MKWNgVpObKCfdGAXPTWwRZ1JBzmL8z7WPM5GeNSUSZhSfUmAYs1ZuCskOYa52u4Fz5V6ZBxW84rAmnHDaP.FDmTtbVUlHw1K0cYtqYF23mflzDm.8NC9ZoS2kZZKeh111ZVaZiaHLuiV5OrMQH1g.LHNpD4DjoZEwVUlLxrcSDnI4o6AVhfaKT2kVNuHzKURaOjqE.Wi.LHta1xIHyEEx0Q+JUpR0rtnYqwM9InQNhgqoL4IE1kDrjVZcmZW6duZ6MuM0TiMFUWgktaGxIzxZDGCZDiQ.FjqnD4DlYgJhtpL8z7WPMprgOBUVYkoQNhxnOZhAZq8Nzt18dzd1ydUyaaq5i1zGF05gk9RZc7sHhUaA4DH.CxEk4nXOaEQNASYqtGpYPCb.rRMgnVZcm5y67PwwvJc2ZkSnk0Dx0Af0Q.FjqKVrES8m4T0b0fFzfzvJa3Z3C2IXCqVi8zV6cnN67P5S151ze7KNj5ryN0y+bKKrKKuXG53q1BaQDxYQ.FjTj43XOaEiCyzcUTQkZHCcnprgOBke94qIdVSPETP9DtoWjIjxt18dzQO5Q0mt28nO+y+7nbC15VYBsrFwweFIDDfAIQkniuxLiMTqDeRpTkpoM8yUETPAZXkMbIIMwyZBRRZfCb.4TmJpzo6R6bW6VR5qCnjYkTZus1hCMUqoNnbBrTmnuVPBDAXPRWIx49kImYkYbi4T0bUgE5biBmYkbxHSfmL76fOcOHRFedmGRG5PG5q+6adaa8q+9w7s4wTYVok5EgVPBGAX.NttuMSkqXVC.GEL+ETSe9yESaB1nfFjyJsv1CAzMDfAnukIHyrUL4nYibBYtf4pWbWs.zmH.CP1oDc7.MItsZB9tcniuJKr0P.YABv.Xlx0wCzjS1HvvWcPc7UXodwpr.3ZDfAv6JVNAYx7MBzfdJSfkLe6.gWo.jaf.L.1Wl.Mi6X+0oFlECBE6PG+jBUuHvBf0Q.FffQ453AZFmnofykjVGOnR8G66yVBA3yH.CP3nXchAZHTS7P2Cqr8i8sCDh0CPhEAX.hNxDpIy2JQr8SgocHmvIceafNPnUM.3DP.FfnuRzwuwfy78Gm3h1yVZPNa4S2WQENJy.QbDfAHdqbc7Uto6+UNITG2A0wW8jt+M5UEfXLBv.j6JSfFoiGro6+Xkn3ce2joeTjNws2o9d4GC.4XH.C.jN9VSkQl.OcW4mj43jsxOYVIj9SuspH02suels5A.Ib++CD+zvWinZjba.....jTQNQjqBAlf" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-5",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 244.898070999999987, 188.633330999999998, 82.468575000000001, 82.468575000000001 ],
					"pic" : "zz_stopbutton.png",
					"presentation" : 1,
					"presentation_rect" : [ 18.333335999999999, 0.917414, 65.999998986721039, 65.999998986721039 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"degrees" : 360,
					"floatoutput" : 1,
					"hidden" : 1,
					"id" : "obj-24",
					"maxclass" : "dial",
					"needlecolor" : [ 0.309803921568627, 0.501960784313725, 0.282352941176471, 1.0 ],
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"outlinecolor" : [ 0.453266441822052, 0.719768941402435, 0.402827620506287, 1.0 ],
					"parameter_enable" : 0,
					"patching_rect" : [ 845.0, 1574.5, 40.0, 40.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 69.333334986721042, 54.349236000000005, 16.0, 16.0 ],
					"size" : 1.0,
					"thickness" : 117.0
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.2, 0.2, 0.2, 0.0 ],
					"bordercolor" : [ 0.349019607843137, 0.349019607843137, 0.349019607843137, 0.0 ],
					"fontsize" : 5.0,
					"id" : "obj-34",
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1343.0, 76.5, 80.0, 18.999992000000006 ],
					"presentation" : 1,
					"presentation_rect" : [ 4.0, 74.349236000000019, 87.666663999999997, 10.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"background" : 1,
					"data" : [ 8091, "png", "IBkSG0fBZn....PCIgDQRA...3L...PsHX....vE5Crw....DLmPIQEBHf.B7g.YHB..eHURDEDU3wY6c1GcTTju2+SdEDAlfxph71j3ifBbulH5p68dtvHRTW2UdOvthKDAA8QWeEVw64rHAwyYubOvtQ2c0k2z.qrKR.j.rJXPbBm84YgKhDNWdSTRh.91tbgL.hDRB49GU2SpomWxjLSOc2yTeNm9LUWcOcUS282op5W8qpJMTX0jOPN.dLrudXWQ365CnZsv0KE1qg8UHHGD2ScqsAh6Q0aHt5z1zOVPjVGHwkev1QNtWssTQxWayChGRCOAjlUg3k.uHdQHUTLULvy.bKcfuaE.kPTbeyMPYZmXKlzV8Z+PR1wMhGZaDwuYy59Y68d+F0xWtMme11FxAweXDv8fwM4w2x3l73C48mPcrzSOceX3dkwRbxO8zSupKe4K28XMGOrQLL5Yu9d92eCqd8g5zdVfRi0zxlgafwf3Eyn5e3l1zmAW+0e8bsWWunqcsqbSCb..v0e88h9029D1u2AOzg4bm67.vQ9jix4O+44a95uhu7K+RdiUrrnM+teD+Q4FQTxTRCcoKc4udgKbg+U.xM273Adx6izFzk7e7rNU1zRVMSStZlL8kAoc9Lokd1HM0oKSVmJaV+b2J0VaM5m9JQ7LEHXgiWzp9vvFwv3etvAQmulLfN0BM1yKQrRVeQm3.apF1ZEumdT5J4PVORGD4PqhkHV8qoM8YPA25PY.C3F4lF3.hnvHV43m3jbjO4nbzi9oruOduQiXpJZUD4zelTLvaBBQy3Wv80teGNqSkMK5gdM8c+bjJ0QV33FnV8DZbkNRZpSWtiloCKY1P5bv23Dxhmwh3AkSD2Hp+6XHLMheBEMIF5sc63Y3Ci67Nt8DXVKzrqcuG7V0NYuezdXck+1g6z7g3YRI3bKEpNf9CvrWwSPi8tgNzE4ertuk+zx+y565Wujoz4judf6dhdLEQC.M0oKyPFUdr0JBHccZBGOHZi1nC0AmPQShezO9AXD20vM0RT5Hbm2ws6W.e7SrH1wGVE+ksrYihHW.SUaqBDUm1aBNqFK3FMQy3l736vhF.tpaHf+OzMZ+QR5RQ5W3jy.thNbBEMzXuaf6az+P8c8XpIV7EOHdA5CwfnI2byie0BWDG3fGhxW6Zn3oLYamnwH8qu8ghmxjo70tFNvAOD+pEtHxM27LdZiFwuWu3bdV4QOPeF50FSWnNeMYHuqa8.oGzYBzROaLlRrng9bS8xzSi3HtQTp3Ghg1vLghlDqaCUPM0bLdgmeVL3AcyVQ9KlYvC5l4Ed9YQM0bLV2FpfITzjLdJCGwu+Mh82Zbt0Cj0MzTLcgBmVHjBGypZZxjUm8qjyORmmESNHplRsXnDloM8YveaW+WT9ZWCierixRxblEierihxW6Z3usq+Kl1zmgwCOZD2OJkH2ecVItAgAth02kC22OHgSHJp1T3J6W15AiTOiakLFD0m8okiTWvrhkuTaQi8MSty631YEKeogS.8zHt+LlDdFqswMP.cGR7lfDNE7uTfokXNDxAQ0QdGjD0EV38x1p7CRIDLFQW.ssJ+.Jrv6U9PtPbeZiXCK8oaW8UZZWaYgiW.N0W8OLsDyAfdoL9qVVt4lG+1e+ef2+82J2yHGgkkwrCbOibD79u+V4296+CFqYxnwFV5SW5QmhKWmwM4wGTbAUhyN2wNiKIlCibPzweATJyzl9LvaUUwO+weTqJeYK4m+3OJdqpJiUeSuzmxvFV5S7lPZbfDL9r3zOeDk1NU8HxM273MW4awJV9Rs8lT1pne8sOrhkuTdyU9VFK8YpHteZmM5SLicP3Xkdq6XP7P1uOkMghlDdqpJJdJS1xxTNIJdJSFuUUkQyWeKHtuZqp5Vb.+kjZYVUyFPwXnpY+pEtHJesqQUJS6j9029P4qcM7qV3hjiVupaEaIYJyA+khlpZUsxPyA.AweVrtMTAuvyOKqKGkDvK77yh0sgJL9muuIh62IUXGppl6Db5UFRsmovBuW17V1RRWmXZUL9wNJ17V1RnZ2SYVSNxbvNHb5eBLsJCIQyDJZRTd4q0w5lL1UF7ftY1291mw183XEOc9JxNn3BpebRhoLLHZV9xVJtbEyiYOEg.Wt5NKeYKMoP73p2cMn3rCk3jHnLjDM+x4NOJesqQIZLYb4p6T9ZWC+x4NO4ncjhGiXWDNtMwqcYXPzrfWpDSL4TXjE7RkjrHdBu4nsHbaRW2RQIZrErfWpDd5m44jiZp37luIBu4nST7sGO1mCCZCJFIOadBEMIl8rdtve1JLcleIyyXaddZLw944Jt1faTe7BiBGeIJm7z2Wbdy7xmOR8SixP.1CBiACdSLI2yI83iOdF5qsg8qdm6XmjYClaAQY1P5xS.BP70hd5ykV.hN2bwKdQJQiMAWt5NKdwAMDs8hCywPCoBowikYnhNtwE93VLyKuWjbilMuksnbgFaF8qu8gMuksHGkKbXcGRHENUukCXZk5jouLXqq5Cjipp33kuDjbXy0sgJTctoMkAOnal0sgJji5VP77yQfQ0gWPLlbN3abBxzWFA+MhAxrgz4f+oiKO6HBwOui1Cfead9Km67TtQiMmwO1QYzL0yCGxLoiw5jUFZu7s0JdO1ZEvTepofK2ckrckl+oGTPHBR6TYE0Izk70B6YKGHTCTt3gII0GHZ.JKn4jX1y54X26ZWTYkaSOpxPXr.ayLIZVtB98biBm5.dXjrH0Je0UYl4oGl3yLEYIH4yakTx7TFCvgfKWcmeSo+FFxfGjdT8GwySayjxe1tBdQ8HTMjoLfBPLISaViNyJ.tKhO8drGj5ule6u+OnZWiCiAOnale6u+OHG0SiMuJagy7YUSqcLk9hwiL0Q3KovSajldaybU6C+U0aBEMI07CfCke9i+nTk2OTdp3sTrwC+5nwty0S66k81y4FqDvhEzrlkZfn4jYVyZVxBmaAwyWaoa4XW7UsNB4fj4K+kycdoby2YIabm2wsazJakfMsiQCkvYs.yILm+HANFstZUUIPOhhiOTo3NMvLiC48mAsN5L2bySYEsjDl8rdNYuJvE1HiDHSnDN8HDwAhW9qDXoHVmPtJsy8+HJNt90LMfEBrDfXYVAwMR8YyyM6mWYEsjDb4p67by94ki5Yv9LIu6VOP6opZKAwK8KTa+yfPjLxn735ne7XQ3ThdfBK7dUFDHIie1j+oxS0ttv93QA96xinU3LTssk1AOdO.zcWf4fPTs2nLsMRNHMFadz+uOdG7xnvthKWc23y0ohMqsNQqvQupV0Dh3OSTd77PzFmYBLQs36H3uNuEV38pbqljTF+XGkwI3caUachVgi9K4Fa+yLAJOJNNHDUoAbC.au8kM8SNHcC7AeneVG7xnvIfgmuOC1nRcBkvIOBtzf8psIaEsJ09boQwwCmAGZu3eQpM2byiwN5GHNcYUXGYri9ALZgMayTpanDN0fng9sHskGPgHDTmVKtdHEGQww6nUMSF+k1LyG6wUVRKIGWt5Ny7wBnsN1lpqEJgSgHpRk7VMHdwW9X2FA1llHc7EpserP9H4k.O3OMn0nREIgX347sfMwMbbRdNPw5Al1zmQJ2n5bW6dO76dskvt18dr5rRBk9029Xbc3oXKJqD.NRgy8+i9wVX1Hwyt18d3Gbmeedxm3w3Gbmeed+suCqNKkPwvyaaQ6bbJBG+FE.fQNBOVVFwJ3i16GGv9G8nepEkSrFL77t+XCFxANEgiG8.O8y7bobFEnqcMv4t3u4q+JKJmXM3xU2MNYFZ4k53TDN9uQ8uMrgak4CKgaZfCHf8+xu7Ksnbh0ggm6JgSTP9H4iPoZUSSgfPTcMK05ZlgvwMhpVEpsNxOVO5Al1zmQJW0zTHvkqtaz5ZdrnrBP3GAnESqBfPQrVeoJP3wqQyTCk+7PA25PiwjUgSlBt0gBqXY565AKbzgZT3nO8wdKAepwUFs11cQaOTq8nG31F5sZd4HE1dL772iEkM.BV3TJRhlwM4wy09+4p.fqresuY98V5ZSzjql8ueVeQmn9i9c7Aq0q7DRXYD4AojajLCsZnQmZigm+tP79QcVQdQV3Dv3bY1q3Inwd2.B2NCZjFhoDpwd2.WYuSmw+Ocer94tUcwidi7BWU17nGvP8aUjhxzl9L3MBr5ZkYE4CYiC3ug6S8olhlnI9Si87R7.O48IGUjLsna8.23.FnojeT3rvv6AtsnrQ.BGO5At5apalZhl1ftDCaDCSe2HYoMO5Atwa7FMyrjBGBFdOviEkMBs4nS6paxzS3bGha8fQZvI4+j5802KSL2nvofg2CbaQYiPKbjaTuYQW5g+kKK2Q3z72wmJCCn.B58f9Gtyyrwx7b.o0mwv8i2eU3Lr5coHEGCuOXIdPPPBmwM4wmXR31d8YzeU3tqQb2lZdQgyBCuOXIyCAxBmpA3Bm+BVQ9HT31py.JbD31JRTYgS8fXAkxlfa8.JWsQgLFdevsUjGbBdGsBE1NrCBm34hmqBEIDrCBmvgG8.8pWp9vQQqX38AKwpZAMrBRTVUq8fSqyO846rrnE+qiaiTyyd1yFv96rJuL8GI1WoT5V25Fy2AtdoZ38AKwpZQyJxlYisYZMMdw6Twl4kWv7Msq+m8YeJe1mEelvNxufakhmxjiKWqTIrCUUyrG6OIbN+4OuUmEhZbR4U6DA0ONJhctKOCyQ3sC4ladbWdFVaehJBB4ppUukkKRxXvC5lYe6aeb3i7Iwkq2Q9jixCO0Gx+9SnnIEWVnfu4aZfNt12.vYO24r5rfsnMNfncNFEtUi1bavGs2O1w4jmtb0cSKO28tadWam.FlPFsjZJYGZiCDZSJpJATQzfk7dhkIbt3e27G5BJTXVDjvIQ4jmm6qh9zIUaJeUQjwN79fQgSUIJm7r1CTm7tgpdpd0CjJNkuJicnwv1IL79fWqHODxppkQMs8fkIVnkCkM6bG6TNpHVOUi8bdpFFWcBR08V7Sb7ia0YgPKbp6+2WaZIXlMjNUukCHG0mGlS0eoPqq721zxOJbdTYkaSdWagU0pFfMr50S8u6kLkD7TevEMVZi2vbpATJzwOwIMk7iBmEG7PG1XT1BqpsQ8.q7UWE66+7nzxgxlLaH1M9Vl9xf+w59VV4qtJiGprH707OjCNxmbzXNOnv4yW7kAXX.KaHoXrCP8BrRzlQO24N1Y.kNXzyo22++8IOc11QXkD4F2UGZcB5QO5mx8LxQDKokhj.Lzdu5hz4dw+dyjQuMm7Qn7bfhQjgJFCy.MaX0qOdkt9PLOUWRabdUilH9y9zT2RbLZ9UiqPaoRX38fH19lluXyjgIkOBmK2Th1V9ZatCw43MDwkCA6E.dP7CTutndMrejv+MlMUwFozeyhihuRxGFMGuwUnsTI1TEaTdWKywjaKeUqZZ+YtM11mRTiW8.0VaMb7SbxTtkocEsxwOwIM1z.uVTVw13qZQB+M.bOezGGoySQRNFd9aoyUENAgi+R7pdeJgSpLFd960hxF.ssvoHDKPNs.TIf9nypGRwKukGvwz9d5LRzWjc5X3upeq9s9iwvkw4hz5ACfXbzjJhgm+dsnrAPaKb5Av1AtJs8+HfgBbFs8uAfzj1pQaS1mPlIvRig7nW8.0VaMgpCvR4vIN3yhUN3gNrso8MPzWUsy.THvdAlSabtaGQoLfP3UDP4cnbWqTgdfMuk2MFuTJbhX34dEg67RTzdaiy109rGZedLZsZZGS5bFJsJZpQ560Qwe001w1qLFuTNaJrv60pyBVBFdtGOsbaGhnYnSqWsrYpsMQZssNWkzw0YuZwMRssXszFPbi5MAgC9cvCcXF7ft43vk0Yv51PE7t+ks..O2y8rVbtIwyAOzgM5XmNBgitABNCBQydo0phYTznid00JB31hw7HH5rzJPrDuyZVyayBdoRhCWVmAierihwO1QY0YCKi0rl.7N9JvFLr5iFiCrTfW.QUtpDQoNmQ53gh8pcd6UaKdP.VWymuT6wnSpB97cViVSyxKsAh913rPDkb7B.KgVa2xoo0137QRmud0yjsl1Lo01A0QnLD93F0VaMr8c3MFtTJbJr8c3U1ZZ9vhVd1MRaIbVHviZX+znUqrIaJZ4pjUiVbxBmkhv70wBkoGXIu9qEiWJENAL7btLKJaDDNAOGPlR0CTYkaics68Xk4EElL6Z26wnQAJMbmahFmlvoNjrg+xV1xB+YpvwigmuUPaL9aRj3zDNfz+57FqXYpRcRRYW6dOFc0HaSoMfyT33EIOiUUpSxIFdtVEVrK1Xj38bGsaZcPuYbPsoO30h1AwVjnDfODDk5LiYLiT54R4jMBQoMkXQYkvRnDN4fXXS6g.WzmbiggRcLPUHtY3sC988pcMFN.KdwKlxW6ZhG4KE1.V7hCXj9Z6JsABtpZtQThvuAQuzObos1knI2byKRKKhCGQIFio8bMMPI5AVW4uMq+c1TLboTXWX8uylLNO5UhEkUhHFKwoTjDHS8olBcqWcgNeMgXJOnSsPi87Rj0oxFZHsfNbKcsIZxUy7z+rGQjP9xfFpIM9KK88k6PqRoi2SvdQZF44WLqmkQNBOojtbexB97cV9EyJ.ewqslEjrLjKwIGz7ErbyMOl8a83jy8mMYTPSzXuaH3sdJlvBarmWJjGuIWAtZDzjqlIiBZhwU5HYXiv+p.V+I1K0wu2D75KQYn.mLu9RVlQuDnDqK2DYjEN9aH+cOQO9EFwaZpSWlu+j+mjiJVVtsqCIyT9uOmYqLOsCkcs68v+9blsbTkhMpeaLhrvwidft2+qvTSzF6cCxk53IBmZzPI.6WemWbtyU4.nNL746r7hyctxQserwk1.goebR6paxzS3bGh634kqX8.UV41XQK9WGOu1JLYVzh+0FcslhsnrRTSHENFaehYPW5g+kRD2wgKW0.yWemWdAyWYkMGBq+c1Du7BlubTyGGvJftk44.Ww0lsdv3UeCUBRUY6WLqmUMwdXy4fG5vFshlsuJZ5DjvIB88R7MgMm0tpwfjU1Joj4qZuiMEe9NKkTx7MZEsXwBqITjENUCIt0.TSh5Pp9wqq72l4Ux7C6Iqv5XdkLeiczYwXishlD4.AJbpGfD0Z.pIxFQp8NuRo+Zl6KVh0kaTDDy8EKgWoz.Lfy7wlLjniBxGbldGczPIHMtcTFKv9PHLFPE3PZWiL1AgiYM4YWLRFKXBiazJwiEy5emMwDF2nkiZ+3.L8bnvNHbLKpGQmq5ew4UIdrNBgn4yQ77wLlpm7ZBWy.vxrpVBh5QxRafR7XEDBQitEzr74GsNJ1gRbbaxW+pQ7OaJwiEPXDMdvAzImQB6fvId0AnQhPJdJaUqNAjzotT1pVcRonABQ+3jDSPhmGdpOjxT0lDy8EKgGdpOjbTIMhFHD8iSRN5hG+Va6kWv7Y5OxLUdXPbBe9NKS+QloQSNueD8+QRgnArGUUCBbtMvrIHwyarhkQQEMQkusEibvCcXJpnIZbh1X+HteWWhN+36KNuocssKBmXYvr0QP2T0qTOhJqbaLjAOHU6d5fT1pVMCYvCx3vCXkXdlbtM4hemoLXL8.Vnv4aOt4LBSaGTOhNeK.2y8gm5Cop5V6.8plYn8Lf39Zwj70Df5gfEN9N0W8ORHo9ENSCIjzIJnTf6BIiF7FqXYTPAEv6u8cXc4JG.u+12AETPAFqZlOD2OsUy7lwQpFBV3T8N2wNIyFL+Bh14l+qx650zSvHiWD8mje+aq1Zqg6sv6lo+HyjiehSZU4KaIG+Dmjo+Hyj6sv613BZaEHtO50JxWIRBoBowiEumfOCjVNT1FugaGP2KCdVLT5S+6We428ZKIku5a97cV9cu1Rn+8qugpTlmEGt2.DNt3eO3QDcHENGspZMsLQlMjNUukCHG09C24ZQTJBiUDfym9jOwiQAETPJqGGr92YSTPAEvS9DOlwCUEh6WIqUMiluXaKbpFDiIm5e2KYJUY6q174Xm6XmxQ4MtmHwN0gv5IiEImDs1ZqgILtQy8bO2WJi.Z8uyl3dtm6iILtQarVBeNh6OdvYL.zhqXTY3evDsxWcUrmW4Hz79xTLacFij0WzINwe7L7mV9e13gJKlu3lGaDw+lNejp9VkUtM+BnxV0pS5pBmOemkxV0p8KXLXhYeHtejONmAeVbGiMlwKRSqr6bG6zXoCwadEr+8lb8HFnUko84T0OPkUtMprxswKkadL4G5mwO4mLIG8xH+AOzgYMq4sY0u0eLbsAckHtGTWhLeYyHjliFD1d+gwba6g9+Z8LlXZDuoND2axEoNNEDUg6kWv7YHCdPTzD+IT1pVsiwRbG+DmjxV0ponI9SXHCdP7xKX9gRzrRD+tKlTaQCn8G8gy7Ykos4VaSdstw35aS8DnKyXbcwQl5z9918RYhD0g3EnmQZyk9AWW4us+IghITzj3G8ie.FwcMb5We6ShOmFFN9INI63Cqh+xV1rwILCY7gnA+khC0RYhIdldXJW6fWlATzQnXssgGtSnvBuWti67NI+BtUtoAdiIzpzcvCcXNxm7oT899X18t1kw1rXjpn0+3zoRI.yCfmdqORLewtzeKMd84uLPXPD2P7eEYKUkxn0RnKVaKfwYjd6gzI2byi6ZD2M23.FHW20ccbSCb.b8WeuhoRlN9INIe4W9UbjO4n70e8WymdzOgObGePzzmYeNstjqTWGNCj7Sc5ALJbFJvRz9DfW.X6.ej19mQKtkpE2PM78eTf7.lig3Wp126z.kCrPf8FC+.rqTGh+sqDDhnwnsETIQ0VaMT6JB+KzSa5yHpSTCcFYzRUHDJIihEunUhSlMjNM0oKGSWru4yNsdv5zCHKbxCnRZUXnyH09LMDBhkfPLcFDBE4yEsyYoZGSl7z9buZWC.lHfsyEBhSTGs1FgbPzeGdPz9uvVkNc5fhgHQUHZaoWsMGY6VZuj1oxB5cbyuHqSOfrvQuzisGgu3BA9OnUQPGgEpsMGDB0BI4U7nS8z5+tqS9Zat09LGhBAUTPUz5hTbc37MFSGg5zC7sG+RjcuisK1FV850C5UOfrvY6HdA9XZ6WChRM5As9h8bPTRyd0heIzZoG.bCZwOSsMcdTBVPtPZsZcFKcJUfH8BsaZeShI0QxW0shEpCQ615esUeRF3OniqbxzW.Kim9edIKbNCvsoEtG.qUa6EP7BdKHDPST6bySKb4FRq7PHJdgPDuQ1KA2dHEJgP7fMB7zash2igLpmfF6fUW6BGxe6i1ORUuMbNiltQ.5AsVhSZHJQQujidncdFIbwGJFJImFIPg0iemN8.aplNjeWl0oxlstpOHnqGDnv4Xz5+9qWMrH0dmyPn6covEudIN5GSu5bFKwRgh3A0g1ju+Vq38XOuxQnkC019bYl9xfLpoST+6dIVzC8ZTas0v0ccW2QIB8q0LQXt3Vz1VKhWxmCsZNZYliz4pu0CDVgyX7EQqVsSOtSipZZJLYx+Vy+Mvv6iCaDCqkwM4w6ey3wk25e+c+YjXmLYTnv1P9HJwnNhfHwvVcH5OtPJZTtbihTQzM+uaB15kxlwWgBEJTnPgBEJTnPgBEJTnPgBEJrRbh8iSjlSCriTGJG1Loi1R3DMujp2YRgBOsw20MIlkxvjY7QpSm0kORSLJgfHcunNB7Ov7Z3y1EFEN5C0WOndgVQpCeNsN5XipgRttvIGsuzsX7DxM27nf+kBnaW8URW5Qm.fL5bFz4qIfA3CW7u2bHmicSjjkqrHaWNmZepeO6Jt1rI8NkXSyvQhLuDMb4Ffu6ahuqkRMdwl4zmTLwrJM5NkY+HFFAkEtqg9aYUijnYXiXX7OW3fny8McZrmV9B.kBElJY5KCZ7Kx.e0cd14l+qxyJPeNh4MuflpeSCwTYzapGwTepoPN2erOWQqPgSkVNT1T8VNf7z+7qfgYc1LP35z2D.O3i7S4pG8UjHyiJTX6HsuWyz2a66w.tlAy9289A3NQTHiW8yIcjr7UudftkfyhJTXOooNcYx49ylo9TSQOp4gjVIczLu2C9H+zXdhaSghjMx49ylwM4wquaI5A7OmCbU2PjLOtBEotj6n5odvgiwkqcilWVgBEBZxUybei9Gpu6XfvO8PoPgBIxMe+SF9JgiBEQKWY+72EM8GDBmOGB8RRsBEJDzRWaRdWOoile4bw5UdHfBEgilbEXAKoiVm5n66NJTnnsIczbC6v3raJTnffV0BZsDG.xnFajawpPgMhzNe.Kdg0mNhktfp.3+4HmyJxSJTX6wfwypV2bzaDfU9pqpCsbHnPQxNm9X9sAv9gV6Gmxzi8BebKI1bjBE1bxrgz4Os7+r9tdgVEN0CrR.15p9.UoNJTHggBSJEBzyAJIyLy5b0VaM7UaV0VGEJ.wpx1qOe+q.3qDs98T1Fa0e4Ke4uBXL+2e7A3F57.w0MzYtblpptoH0jrNU1r94tUpu9yPm5TmNeyM27jPac.0nKQWMBWvYL+2e7A3q28Y3ZSu2bkYzExHc3xcVIhTj7SVmJaNs2uiWeNKm5qWrb11byM+nH00MgaJgwSm6bmeqKdwKFz5b88M5eHcoqcgqpOtHqNKzcRN.Gsz0lBx8DTnvJHquHz8K42d7.curu4yNs+vFcDft0st+cm6bm8AwvD1QaMWJULhIoffl1nZuLrQLL5Yu9ds6u2EN+EXqU7dwZxqPQ6kOGg0lKEokocch1IgL2z5DUnahCBIEJrITk1mdQHPpVaKHwhLw5r2Wjl9a0waLlFx3zl2nSlvaL988DmOu1KdkBWGw3748+Kf.ThBZjk3bA.....jTQNQjqBAlf" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-2",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 566.0, 421.5, 174.0, 152.883495145631088 ],
					"pic" : "zz_multiplayer_single.png",
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 0.685378, 98.0, 86.106796116504853 ]
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"background" : 1,
					"bgcolor" : [ 0.811764705882353, 0.811764705882353, 0.811764705882353, 1.0 ],
					"hidden" : 1,
					"id" : "obj-32",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 46.5, 1417.0, 128.0, 128.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 0.0, 100.0, 90.0 ],
					"proportion" : 0.5,
					"prototypename" : "referencepanel_360x220"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"midpoints" : [ 1458.5, 192.0, 1434.0, 192.0, 1434.0, 33.0, 1352.5, 33.0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"source" : [ "obj-109", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 1 ],
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 2 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 4 ],
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 3 ],
					"source" : [ "obj-120", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 1 ],
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"order" : 0,
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-150", 0 ],
					"order" : 1,
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"source" : [ "obj-128", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"source" : [ "obj-128", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"source" : [ "obj-128", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"source" : [ "obj-128", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 0 ],
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-135", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-140", 0 ],
					"source" : [ "obj-136", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-138", 0 ],
					"source" : [ "obj-137", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"source" : [ "obj-138", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"order" : 0,
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"order" : 1,
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"source" : [ "obj-143", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 1 ],
					"order" : 1,
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 0 ],
					"order" : 0,
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"order" : 0,
					"source" : [ "obj-149", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"order" : 1,
					"source" : [ "obj-149", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"source" : [ "obj-150", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"order" : 0,
					"source" : [ "obj-151", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-150", 0 ],
					"order" : 1,
					"source" : [ "obj-151", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-154", 0 ],
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"order" : 1,
					"source" : [ "obj-154", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 1 ],
					"midpoints" : [ 894.5, 146.499992999999989, 966.0, 146.499992999999989, 966.0, 112.499993000000003, 1292.5, 112.499993000000003 ],
					"order" : 0,
					"source" : [ "obj-154", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"source" : [ "obj-156", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 0 ],
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-176", 0 ],
					"source" : [ "obj-174", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 1 ],
					"order" : 0,
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"order" : 1,
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"source" : [ "obj-176", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"source" : [ "obj-176", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 1 ],
					"order" : 0,
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 1 ],
					"order" : 1,
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 1 ],
					"order" : 0,
					"source" : [ "obj-178", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"order" : 1,
					"source" : [ "obj-178", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"source" : [ "obj-178", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"order" : 2,
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-133", 0 ],
					"order" : 0,
					"source" : [ "obj-183", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 1,
					"source" : [ "obj-183", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-194", 0 ],
					"source" : [ "obj-191", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 0 ],
					"order" : 0,
					"source" : [ "obj-191", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"order" : 1,
					"source" : [ "obj-191", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 1 ],
					"order" : 1,
					"source" : [ "obj-194", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"order" : 0,
					"source" : [ "obj-194", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-140", 0 ],
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 1 ],
					"source" : [ "obj-200", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 1 ],
					"source" : [ "obj-200", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 1 ],
					"order" : 2,
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 1 ],
					"order" : 1,
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"order" : 0,
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"midpoints" : [ 564.5, 237.0, 333.0, 237.0, 333.0, 154.0, 65.5, 154.0 ],
					"order" : 1,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"order" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 0 ],
					"source" : [ "obj-213", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-215", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-140", 0 ],
					"order" : 1,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"order" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-399", 0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"order" : 1,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"midpoints" : [ 1201.5, 270.0, 1508.0, 270.0, 1508.0, 162.0, 1458.5, 162.0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"order" : 1,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"order" : 0,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"order" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"order" : 1,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-200", 0 ],
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-200", 0 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 1 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 1 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-203", 0 ],
					"order" : 0,
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"order" : 1,
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-39", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-465", 0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"midpoints" : [ 1352.5, 132.0, 1314.0, 132.0, 1314.0, 57.0, 894.5, 57.0 ],
					"source" : [ "obj-412", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 1 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 1 ],
					"order" : 1,
					"source" : [ "obj-448", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"order" : 0,
					"source" : [ "obj-448", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 1 ],
					"source" : [ "obj-448", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-410", 0 ],
					"source" : [ "obj-448", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-450", 0 ],
					"source" : [ "obj-448", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-453", 0 ],
					"source" : [ "obj-450", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-448", 0 ],
					"source" : [ "obj-465", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-54", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"midpoints" : [ 626.5, 192.299988000000013, 564.5, 192.299988000000013 ],
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"order" : 0,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"order" : 1,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-173", 0 ],
					"order" : 1,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"order" : 2,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"order" : 0,
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-153", 0 ],
					"midpoints" : [ 564.5, 349.299987999999985, 775.0, 349.299987999999985, 775.0, 66.299987999999999, 894.5, 66.299987999999999 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"order" : 0,
					"source" : [ "obj-62", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"order" : 0,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"order" : 1,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"order" : 1,
					"source" : [ "obj-62", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"order" : 0,
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"order" : 2,
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"order" : 1,
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 0 ],
					"source" : [ "obj-65", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 1 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"order" : 3,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"order" : 0,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"order" : 5,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"order" : 4,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"order" : 2,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"order" : 1,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 0 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 1 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-78", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 0 ],
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-85", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"order" : 0,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"order" : 1,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"order" : 0,
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"order" : 1,
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"order" : 1,
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"order" : 0,
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "zz_multiplayer_single.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "zz_stopbutton.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "zz_playbuttonv3.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "zz_plusbuttonnew.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "sliderGold-1",
				"default" : 				{
					"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ],
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ],
		"bgcolor" : [ 0.32549, 0.345098, 0.372549, 0.0 ],
		"editing_bgcolor" : [ 0.878431, 0.878431, 0.858824, 1.0 ]
	}

}
