{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 1,
			"revision" : 0,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 131.0, 840.0, 816.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 0,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"isolateaudio" : 1,
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-256",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2229.5, 49.0, 59.0, 22.0 ],
					"text" : "r #0_note"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3310.599999999999909, 915.0, 61.0, 22.0 ],
					"text" : "s #0_note"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-296",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3115.5, 875.0, 71.0, 22.0 ],
					"text" : "s #0_oct-"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-297",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3143.0, 915.0, 74.0, 22.0 ],
					"text" : "s #0_oct+"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-295",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3056.25, 915.0, 56.0, 17.0 ],
					"text" : "s #0_panic"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-294",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2997.0, 887.0, 74.0, 17.0 ],
					"text" : "s #0_volsliderIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-287",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 401.0, 644.0, 50.0, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-269",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 6,
					"outlettype" : [ "", "", "", "", "", "" ],
					"patching_rect" : [ 2997.0, 842.0, 411.0, 22.0 ],
					"text" : "route /keys/vol /midipanic /keys/oct- /keys/oct+ /keys"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-150",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2900.0, 770.0, 85.0, 22.0 ],
					"text" : "r #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-149",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3070.0, 208.0, 87.0, 22.0 ],
					"text" : "s #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-147",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3090.0, 165.0, 75.0, 22.0 ],
					"text" : "route OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2894.5, 262.0, 84.0, 22.0 ],
					"text" : "v vstinstances"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-255",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2662.0, 282.0, 111.0, 22.0 ],
					"text" : "s #0_vstinstance"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-164",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2782.0, 160.0, 48.0, 22.0 ],
					"text" : "del 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-148",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1904.0, 539.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-146",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1848.0, 605.0, 50.0, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-163",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "int", "int" ],
					"patching_rect" : [ 3521.0, 134.0, 83.0, 22.0 ],
					"text" : "live.thisdevice"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-154",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3521.0, 167.0, 149.0, 22.0 ],
					"text" : "0.8 3 0 0 blank blank 1 0 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1856.0, 573.0, 117.0, 22.0 ],
					"text" : "v #0_vstsinstance"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3769.0, 267.0, 111.0, 22.0 ],
					"text" : "s #0_vstinstance"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-143",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3770.0, 567.0, 109.0, 22.0 ],
					"text" : "r #0_vstinstance"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2662.0, 224.0, 109.0, 22.0 ],
					"text" : "r #0_vstinstance"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-131",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2782.0, 224.0, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2782.0, 262.0, 117.0, 22.0 ],
					"text" : "v #0_vstsinstance"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-142",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2782.0, 194.0, 84.0, 22.0 ],
					"text" : "v vstinstances"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-153",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 2782.0, 126.0, 58.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 128.0, 420.0, 32.0, 22.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-73",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 202.0, 390.0, 57.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 274.0, 402.0, 32.0, 22.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 156.0, 249.0, 121.0, 22.0 ],
					"text" : "r #0_notenamesIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-140",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 283.0, 341.720886000000007, 136.0, 22.0 ],
					"text" : "s #0_notenamesOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-138",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3630.5, 567.0, 134.0, 22.0 ],
					"text" : "r #0_notenamesOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-137",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3623.5, 267.0, 129.0, 22.0 ],
					"text" : "s #0_notenamesIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 211.0, 52.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 318.489379999999983, 120.0, 96.0, 22.0 ],
					"text" : "enablehscroll $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 211.0, 120.0, 95.0, 22.0 ],
					"text" : "enablevscroll $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 211.0, 163.0, 67.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 211.0, 76.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 304.0, 249.0, 70.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2022.0, 720.0, 89.0, 22.0 ],
					"text" : "s chaosmidiout"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-136",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 283.0, 505.441772000000014, 217.0, 35.0 ],
					"text" : "presentation_rect 11. 53.272209 279.5 113.455581"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-135",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 42.5, 515.441772000000014, 175.0, 35.0 ],
					"text" : "presentation_rect 13. 54.272209 275.5 111.831882"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 106.0, 262.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 228.0, 341.720886000000007, 50.0, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.862745, 0.870588, 0.878431, 0.2 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 0.2 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.862745, 0.870588, 0.878431, 0.2 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 0.2 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontsize" : 8.0,
					"gradient" : 1,
					"id" : "obj-75",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 106.0, 228.0, 63.0, 17.0 ],
					"presentation" : 1,
					"presentation_linecount" : 2,
					"presentation_rect" : [ 49.244681999999997, 184.5, 43.0, 26.0 ],
					"text" : "NOTE NAMES",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 106.0, 295.0, 61.0, 22.0 ],
					"text" : "counter 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 315.0, 442.0, 109.0, 22.0 ],
					"text" : "pic nomsnotes.png"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 156.0, 468.0, 122.0, 22.0 ],
					"text" : "pic nomsnotescd.png"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-19",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 172.0, 435.0, 57.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 69.0, 396.0, 57.0, 22.0 ],
					"text" : "hidden 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 106.0, 341.720886000000007, 72.0, 22.0 ],
					"text" : "select 0 1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1625.0, 1391.0, 36.0, 22.0 ],
					"text" : "*~ 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1581.0, 1391.0, 36.0, 22.0 ],
					"text" : "*~ 1."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1540.0, 1172.0, 36.0, 22.0 ],
					"text" : "*~ 4."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1496.0, 1172.0, 36.0, 22.0 ],
					"text" : "*~ 4."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2229.5, 213.0, 34.0, 22.0 ],
					"text" : "+ 48"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patching_rect" : [ 2229.5, 89.0, 61.0, 22.0 ],
					"text" : "unpack i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2407.75, 742.0, 73.0, 22.0 ],
					"text" : "fromsymbol"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-268",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 2388.5, 709.0, 96.0, 22.0 ],
					"text" : "regexp (.+)\\\\..+$"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-266",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2298.5, 684.5, 57.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-262",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2366.5, 639.5, 111.0, 22.0 ],
					"text" : "loadmess hidden 1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.376471, 0.384314, 0.4, 0.06 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 0.07 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.376471, 0.384314, 0.4, 0.06 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 0.07 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontface" : 0,
					"fontsize" : 8.0,
					"gradient" : 1,
					"id" : "obj-261",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2369.75, 782.0, 40.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 188.235656500000005, 190.935547828674316, 114.264358500000014, 17.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-259",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 2291.5, 639.5, 71.0, 23.0 ],
					"text" : "strippath"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-257",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1145.0, 1315.0, 75.0, 22.0 ],
					"text" : "s #0_note"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-218",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 920.734069999999974, 1170.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-219",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 920.734069999999974, 1199.0, 41.0, 22.0 ],
					"text" : "23 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-220",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 920.734069999999974, 1100.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 262.989380000000097, 41.0, 40.510634999999979, 42.0 ],
					"varname" : "but23"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-221",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 841.734069999999974, 1170.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-222",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 841.734069999999974, 1199.0, 41.0, 22.0 ],
					"text" : "21 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-223",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 841.734069999999974, 1100.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 220.989379999999983, 42.0, 40.510634999999979, 41.999999999999986 ],
					"varname" : "but21"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-224",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 757.734069999999974, 1170.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-225",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 757.734069999999974, 1199.0, 41.0, 22.0 ],
					"text" : "19 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-226",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 681.734069999999974, 1170.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-227",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 681.734069999999974, 1199.0, 41.0, 22.0 ],
					"text" : "17 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-228",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 757.734069999999974, 1100.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 178.489365000000021, 41.0, 40.510634999999979, 41.999999999999986 ],
					"varname" : "but19"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-229",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 681.734069999999974, 1100.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 134.5, 41.0, 40.510635000000008, 41.999999999999986 ],
					"varname" : "but17"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-230",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 597.734069999999974, 1170.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-231",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 597.734069999999974, 1199.0, 41.0, 22.0 ],
					"text" : "16 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-232",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 597.734069999999974, 1100.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 91.755316999999991, 41.0, 40.510635000000008, 41.999999999999986 ],
					"varname" : "but16"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-233",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 513.734069999999974, 1170.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-234",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 513.734069999999974, 1199.0, 41.0, 22.0 ],
					"text" : "14 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 437.734069999999974, 1170.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-236",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 437.734069999999974, 1199.0, 41.0, 22.0 ],
					"text" : "12 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-237",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 513.734069999999974, 1100.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 49.244681999999997, 41.0, 40.510634999999994, 41.999999999999986 ],
					"varname" : "but14"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-238",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 437.734069999999974, 1100.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 6.0, 41.0, 40.510634999999994, 41.999999999999986 ],
					"varname" : "but12"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-239",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 879.244689999999991, 1011.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-240",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 879.244689999999991, 1040.0, 41.0, 22.0 ],
					"text" : "22 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-241",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 879.244689999999991, 941.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 241.235656500000005, 0.0, 40.510634999999979, 42.0 ],
					"varname" : "but22"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-242",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 795.244689999999991, 1011.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-243",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 795.244689999999991, 1040.0, 41.0, 22.0 ],
					"text" : "20 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-244",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 719.244689999999991, 1011.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-245",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 719.244689999999991, 1040.0, 41.0, 22.0 ],
					"text" : "18 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-246",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 566.244689999999991, 1011.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-248",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 566.244689999999991, 1040.0, 41.0, 22.0 ],
					"text" : "15 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-249",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 488.244689999999991, 1011.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-250",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 488.244689999999991, 1040.0, 41.0, 22.0 ],
					"text" : "13 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-251",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 795.244689999999991, 941.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 198.000014999999991, 0.0, 40.510634999999979, 42.0 ],
					"varname" : "but20"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-252",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 719.244689999999991, 941.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 155.489380000000011, -0.636364, 40.510634999999979, 42.0 ],
					"varname" : "but18"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-253",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 566.244689999999991, 941.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 73.98938050000001, 0.0, 40.510634999999979, 42.0 ],
					"varname" : "but15"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-254",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 488.244689999999991, 941.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 30.489379999999997, -1.0, 40.510634999999994, 42.0 ],
					"varname" : "but13"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-191",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 926.489379999999983, 789.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-192",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 926.489379999999983, 818.0, 40.0, 22.0 ],
					"text" : "11 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-217",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 926.489379999999983, 719.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 258.042554999999993, 134.0, 40.510634999999979, 42.0 ],
					"varname" : "but11"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-178",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 847.489379999999983, 789.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-179",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 847.489379999999983, 818.0, 34.0, 22.0 ],
					"text" : "9 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-180",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 847.489379999999983, 719.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.531919999999985, 132.550247387559807, 40.510634999999979, 42.0 ],
					"varname" : "but9"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-181",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 763.489379999999983, 789.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-183",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 763.489379999999983, 818.0, 34.0, 22.0 ],
					"text" : "7 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-184",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 687.489379999999983, 789.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-185",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 687.489379999999983, 818.0, 34.0, 22.0 ],
					"text" : "5 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-186",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 763.489379999999983, 719.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 171.021285000000006, 132.550247387559807, 40.510634999999979, 42.0 ],
					"varname" : "but7"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-187",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 687.489379999999983, 719.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 127.510649999999998, 132.550247387559807, 40.510635000000008, 42.0 ],
					"varname" : "but5"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 603.489379999999983, 789.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-169",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 603.489379999999983, 818.0, 34.0, 22.0 ],
					"text" : "4 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-170",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 603.489379999999983, 719.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 85.000014999999991, 132.550247387559807, 40.510635000000008, 42.0 ],
					"varname" : "but4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-172",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 519.489379999999983, 789.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-173",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 519.489379999999983, 818.0, 34.0, 22.0 ],
					"text" : "2 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-174",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 443.489379999999983, 789.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-175",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 443.489379999999983, 818.0, 34.0, 22.0 ],
					"text" : "0 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-176",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 519.489379999999983, 719.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 42.489379999999997, 132.550247387559807, 40.510634999999994, 42.0 ],
					"varname" : "but2"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-177",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 443.489379999999983, 719.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 131.550247387559807, 40.510634999999994, 42.0 ],
					"varname" : "but0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-165",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 885.0, 630.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 885.0, 659.0, 41.0, 22.0 ],
					"text" : "10 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-167",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 885.0, 539.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 234.239380000000011, 92.0, 40.510634999999979, 42.0 ],
					"varname" : "but10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 801.0, 630.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-162",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 801.0, 659.0, 34.0, 22.0 ],
					"text" : "8 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-159",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 725.0, 630.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-160",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 725.0, 659.0, 34.0, 22.0 ],
					"text" : "6 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-157",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 572.0, 630.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-158",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 572.0, 659.0, 34.0, 22.0 ],
					"text" : "3 $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-156",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 494.0, 630.0, 54.0, 22.0 ],
					"text" : "buttotog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-155",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 494.0, 659.0, 34.0, 22.0 ],
					"text" : "1 $1"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-129",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 801.0, 539.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 192.25531749999999, 92.0, 40.510634999999979, 42.0 ],
					"varname" : "but8"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-128",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 725.0, 539.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 147.74468250000001, 92.0, 40.510634999999979, 42.0 ],
					"varname" : "but6"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-127",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 572.0, 539.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 67.0, 93.0, 40.510634999999979, 42.0 ],
					"varname" : "but3"
				}

			}
, 			{
				"box" : 				{
					"handoff" : "",
					"hilite" : 0,
					"id" : "obj-126",
					"maxclass" : "ubutton",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 494.0, 539.0, 65.510634999999994, 61.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 24.489365000000006, 92.0, 40.510634999999994, 42.0 ],
					"varname" : "but1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 37.0, 24.0, 69.0, 22.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"text" : "thispatcher"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "int", "int" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 0,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "vel",
									"id" : "obj-4",
									"index" : 2,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 255.0, 158.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "keynum",
									"id" : "obj-3",
									"index" : 1,
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 138.0, 158.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "vel",
									"id" : "obj-2",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 255.0, 246.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "keynum",
									"id" : "obj-1",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 138.0, 246.0, 30.0, 30.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 2229.5, 179.0, 54.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p holder"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-193",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1664.0, 1425.0, 118.0, 22.0 ],
					"text" : "send~ #0_meterR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-195",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1550.5, 1425.0, 116.0, 22.0 ],
					"text" : "send~ #0_meterL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-196",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1449.542480000000069, 1678.09997599999997, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "int" ],
					"patching_rect" : [ 1459.542480000000069, 1501.0, 30.0, 22.0 ],
					"text" : "t b i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-197",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1409.042480000000069, 1574.59997599999997, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-121",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1459.542480000000069, 1575.59997599999997, 43.0, 22.0 ],
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-198",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1459.542480000000069, 1713.59997599999997, 109.0, 22.0 ],
					"text" : "s #0_ctnumOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-199",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1455.542480000000069, 1453.600097999999889, 94.0, 22.0 ],
					"text" : "r #0_ctnumIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1436.542480000000069, 1756.09997599999997, 107.0, 22.0 ],
					"text" : "sprintf set CT%sR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-201",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1325.542480000000069, 1756.09997599999997, 105.0, 22.0 ],
					"text" : "sprintf set CT%sL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-202",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1430.542480000000069, 1646.699951000000056, 61.0, 22.0 ],
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-203",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1332.542480000000069, 1543.59997599999997, 57.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-204",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1392.542480000000069, 1543.59997599999997, 57.0, 22.0 ],
					"text" : "hidden 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-205",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 1256.62768600000004, 1469.699951000000056, 46.0, 22.0 ],
					"text" : "sel 1 0"
				}

			}
, 			{
				"box" : 				{
					"autoscroll" : 0,
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"bordercolor" : [ 0.376471, 0.384314, 0.4, 0.0 ],
					"fontface" : 0,
					"fontname" : "Comic Sans MS",
					"fontsize" : 10.0,
					"hidden" : 1,
					"id" : "obj-206",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1430.542480000000069, 1609.59997599999997, 35.0, 27.502929999999999 ],
					"presentation" : 1,
					"presentation_rect" : [ 333.0, 192.5, 28.0, 27.502929999999999 ],
					"text" : "1",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.862745, 0.870588, 0.878431, 0.0 ],
					"bgoncolor" : [ 0.862745, 0.870588, 0.878431, 0.0 ],
					"fontlink" : 1,
					"fontname" : "Comic Sans MS",
					"fontsize" : 10.0,
					"id" : "obj-207",
					"maxclass" : "textbutton",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1294.62768600000004, 1609.59997599999997, 28.822265625, 21.935547828674316 ],
					"presentation" : 1,
					"presentation_rect" : [ 306.989379999999983, 192.5, 28.822265625, 21.935547828674316 ],
					"rounded" : 13.01,
					"text" : "CT",
					"textcolor" : [ 0.0, 0.0, 0.0, 0.29 ],
					"texton" : "CT",
					"textoncolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"usebgoncolor" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-208",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1294.62768600000004, 1415.0, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-209",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1294.62768600000004, 1662.0, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-210",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1364.042480000000069, 1415.0, 81.0, 22.0 ],
					"text" : "r #0_toctIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-211",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1325.542480000000069, 1662.0, 96.0, 22.0 ],
					"text" : "s #0_toctOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-212",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1271.585205000000087, 1784.699951000000056, 51.0, 22.0 ],
					"text" : "gate~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1271.585205000000087, 1938.0, 116.0, 22.0 ],
					"text" : "send~ intramoduls2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-214",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 1153.0, 1779.699951000000056, 51.0, 22.0 ],
					"text" : "gate~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-215",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1120.5, 1938.0, 116.0, 22.0 ],
					"text" : "send~ intramoduls1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-216",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 1192.5, 1853.0, 44.0, 44.0 ],
					"prototypename" : "helpfile"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"lastchannelcount" : 0,
					"maxclass" : "live.gain~",
					"numinlets" : 2,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "", "float", "list" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1390.199951000000056, 1224.5, 48.0, 136.0 ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_initial" : [ -40 ],
							"parameter_shortname" : "live.gain~",
							"parameter_type" : 0,
							"parameter_unitstyle" : 4,
							"parameter_mmin" : -70.0,
							"parameter_longname" : "live.gain~[1]",
							"parameter_initial_enable" : 1,
							"parameter_mmax" : 6.0
						}

					}
,
					"varname" : "live.gain~[1]"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1569.5, 93.428589000000002, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1538.0, 93.428589000000002, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 1563.0, 56.428589000000002, 46.0, 22.0 ],
					"text" : "sel 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1563.0, 29.158508000000001, 94.0, 22.0 ],
					"text" : "r #0_toctOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1431.199951000000056, 44.295287999999999, 130.0, 22.0 ],
					"text" : "receive~ #0_meterR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1416.949951000000056, 9.5, 128.0, 22.0 ],
					"text" : "receive~ #0_meterL"
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-82",
					"lockeddragscroll" : 0,
					"maxclass" : "bpatcher",
					"name" : "volslider.maxpat",
					"numinlets" : 4,
					"numoutlets" : 3,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "", "float", "float" ],
					"patching_rect" : [ 1350.449951000000056, 136.0, 199.75, 248.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 306.989379999999983, 0.0, 43.304932000000008, 198.636352999999986 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-194",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1526.199951000000056, 399.428588999999988, 90.0, 17.0 ],
					"text" : "s #0_volmeterROUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-188",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1435.199951000000056, 399.428588999999988, 89.0, 17.0 ],
					"text" : "s #0_volmeterLOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-189",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1349.0, 408.428588999999988, 83.0, 17.0 ],
					"text" : "s #0_volsliderOUT"
				}

			}
, 			{
				"box" : 				{
					"color" : [ 0.05951, 0.50193, 0.998455, 1.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-190",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1348.0, 109.700042999999994, 73.0, 17.0 ],
					"text" : "r #0_volsliderIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-182",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1208.5, 56.428589000000002, 81.0, 22.0 ],
					"text" : "r #0_setvel"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1227.699951000000056, 403.428588999999988, 127.0, 22.0 ],
					"text" : "s #0_volsliderstatus"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-50",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1306.5, 77.5, 51.0, 17.0 ],
					"text" : "r #0_vol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1215.5, 88.428589000000002, 82.0, 22.0 ],
					"text" : "loadmess 0.8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3527.5, 267.0, 96.0, 22.0 ],
					"text" : "s #0_ctnumIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3514.5, 567.0, 107.0, 22.0 ],
					"text" : "r #0_ctnumOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2138.0, 762.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2138.0, 793.0, 83.0, 22.0 ],
					"text" : "s #0_setvel"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 2144.5, 610.0, 69.0, 22.0 ],
					"text" : "opendialog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2219.5, 713.5, 99.0, 22.0 ],
					"text" : "s #0_plugOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2216.0, 610.0, 84.0, 22.0 ],
					"text" : "r #0_plugIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3415.5, 567.0, 97.0, 22.0 ],
					"text" : "r #0_plugOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3437.5, 267.0, 86.0, 22.0 ],
					"text" : "s #0_plugIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3315.5, 563.0, 90.0, 22.0 ],
					"text" : "r #0_savevst"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3353.5, 267.0, 89.0, 22.0 ],
					"text" : "s #0_loadvst"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1703.0, 720.0, 82.0, 22.0 ],
					"text" : "prepend read"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1703.0, 529.0, 87.0, 22.0 ],
					"text" : "r #0_loadvst"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-87",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1730.0, 688.0, 92.0, 22.0 ],
					"text" : "s #0_savevst"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 1826.0, 534.0, 33.0, 22.0 ],
					"text" : "t s b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1826.0, 630.0, 145.0, 22.0 ],
					"text" : "sprintf symout %s-%s.fxp"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1826.0, 688.0, 83.0, 22.0 ],
					"text" : "prepend write"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1826.0, 504.0, 57.0, 22.0 ],
					"text" : "r savedir"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 9,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3105.0, 659.0, 103.0, 22.0 ],
					"text" : "pak f f f f s s i i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 9,
					"outlettype" : [ "float", "float", "float", "float", "", "", "int", "int", "int" ],
					"patching_rect" : [ 3168.0, 208.0, 111.0, 22.0 ],
					"text" : "unpack f f f f s s i i i"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-59",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3219.5, 271.0, 52.0, 17.0 ],
					"text" : "s #0_hold"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3200.5, 563.0, 94.0, 22.0 ],
					"text" : "r #0_toctOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3277.0, 267.0, 83.0, 22.0 ],
					"text" : "s #0_toctIN"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3115.0, 563.0, 72.0, 17.0 ],
					"text" : "r #0_holdstatus"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3054.5, 267.0, 47.0, 17.0 ],
					"text" : "s #0_vol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3028.0, 563.0, 85.0, 22.0 ],
					"text" : "r #0_octave"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3118.0, 262.0, 99.0, 22.0 ],
					"text" : "s #0_octaveIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2049.5, 262.0, 97.0, 22.0 ],
					"text" : "r #0_octaveIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2073.0, 462.0, 87.0, 22.0 ],
					"text" : "s #0_octave"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2894.5, 563.0, 125.0, 22.0 ],
					"text" : "r #0_volsliderstatus"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"linecount" : 4,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3168.0, 57.0, 171.0, 60.0 ],
					"text" : "SAVELOAD\n\nvol, octave, hold, toct, vstininfo"
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-18",
					"index" : 0,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3105.0, 713.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-17",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3090.0, 118.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.376471, 0.384314, 0.4, 0.29 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 0.09 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.376471, 0.384314, 0.4, 0.29 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 0.09 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontface" : 1,
					"fontsize" : 9.0,
					"gradient" : 1,
					"id" : "obj-14",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2144.5, 529.0, 38.0, 19.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 146.021285000000006, 189.935547828674316, 38.0, 19.0 ],
					"text" : "LOAD",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgcolor2" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor_color2" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontsize" : 8.0,
					"gradient" : 1,
					"id" : "obj-98",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2338.5, 394.0, 34.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 5.0, 188.5, 34.0, 17.0 ],
					"text" : "PANIC",
					"textcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 12.0,
					"id" : "obj-102",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1872.0, 400.0, 31.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 94.244698, 188.935547828674316, 28.0, 20.0 ],
					"text" : "C3",
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"id" : "obj-93",
					"maxclass" : "incdec",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2061.25, 336.0, 20.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 116.88031749999999, 186.935547828674316, 21.486111111111114, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-171",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2087.0, 371.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontsize" : 8.0,
					"id" : "obj-99",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2338.5, 356.5, 60.0, 17.0 ],
					"text" : "r #0_panic"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2395.5, 404.5, 48.0, 33.0 ],
					"text" : "PANIC\nstop all"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2338.5, 424.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-66",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2289.5, 553.0, 34.0, 22.0 ],
					"text" : "$1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-64",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2366.5, 467.0, 37.0, 22.0 ],
					"text" : "set 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 4,
					"outlettype" : [ "int", "", "", "int" ],
					"patching_rect" : [ 2289.5, 517.0, 61.0, 22.0 ],
					"text" : "counter"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "int" ],
					"patching_rect" : [ 2289.5, 467.0, 49.0, 22.0 ],
					"text" : "uzi 133"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 0,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 80.0, 118.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-10",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 212.0, 326.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-9",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 168.0, 249.0, 30.0, 22.0 ],
									"text" : "dec"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 255.0, 249.0, 29.5, 22.0 ],
									"text" : "inc"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 255.0, 208.0, 36.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 168.0, 208.0, 36.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-2",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 168.0, 171.0, 69.0, 22.0 ],
									"text" : "r #0_oct-"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 255.0, 171.0, 72.0, 22.0 ],
									"text" : "r #0_oct+"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-2", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-9", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "sliderGold-1",
								"default" : 								{
									"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ],
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 2061.25, 302.0, 37.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p oct"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2403.5, 239.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2360.0, 208.0, 73.0, 22.0 ],
					"text" : "routepass 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 33.5, -12.0, 141.0, 22.0 ],
					"text" : "loadmess presentation 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2031.25, 223.5, 72.0, 22.0 ],
					"text" : "loadmess 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1606.0, 533.0, 45.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1602.0, 477.0, 99.0, 22.0 ],
					"text" : "scale 0. 1. 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1606.0, 565.0, 83.0, 22.0 ],
					"text" : "s #0_tovel"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2216.0, 362.0, 81.0, 22.0 ],
					"text" : "r #0_tovel"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 1992.0, 434.0, 29.5, 22.0 ],
					"text" : "+"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1872.0, 362.0, 88.0, 22.0 ],
					"text" : "sprintf set C%i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-95",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2030.0, 401.0, 32.0, 22.0 ],
					"text" : "* 12"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2031.25, 367.0, 29.5, 22.0 ],
					"text" : "- 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2031.25, 336.0, 28.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-89",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1992.0, 508.0, 30.0, 22.0 ],
					"text" : "t b l"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1985.0, 559.0, 57.0, 22.0 ],
					"text" : "74 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1992.0, 477.0, 29.5, 22.0 ],
					"text" : "join"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-83",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1503.0, 418.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1503.0, 448.0, 71.0, 22.0 ],
					"text" : "clickadd $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-79",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1453.0, 448.0, 37.0, 22.0 ],
					"text" : "clear"
				}

			}
, 			{
				"box" : 				{
					"addpoints" : [ 0.0, 0.0, 2, 0.154255, 0.72, 2, 1.0, 1.0, 2 ],
					"clickadd" : 0,
					"domain" : 1.0,
					"id" : "obj-76",
					"maxclass" : "function",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "float", "", "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1390.199951000000056, 499.0, 200.0, 100.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-247",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1390.199951000000056, 634.0, 103.0, 22.0 ],
					"text" : "scale 0. 1. -70. 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2105.0, 286.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2138.0, 286.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2165.0, 400.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2126.5, 400.0, 29.5, 22.0 ],
					"text" : "45"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 2126.5, 362.0, 68.0, 22.0 ],
					"text" : "sel 1 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-49",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2173.0, 318.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2369.75, 812.0, 37.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2144.5, 720.0, 50.0, 22.0 ],
					"text" : "plug $1"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1985.0, 673.0, 117.0, 23.0 ],
					"text" : "prepend midievent"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1985.0, 643.0, 68.0, 23.0 ],
					"text" : "zl group 3"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 7,
					"numoutlets" : 2,
					"outlettype" : [ "int", "" ],
					"patching_rect" : [ 1985.0, 605.0, 82.0, 23.0 ],
					"text" : "midiformat"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1967.0, 290.5, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 1985.0, 758.0, 92.5, 22.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~",
							"parameter_shortname" : "vst~",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"text" : "vst~",
					"varname" : "vst~",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"annotation" : "dragpanel",
					"drag_window" : 1,
					"grad1" : [ 0.376471, 0.384314, 0.4, 0.0 ],
					"grad2" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"id" : "obj-8",
					"ignoreclick" : 0,
					"maxclass" : "panel",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 228.0, 804.0, 79.5, 76.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 16.0, 403.555374000000029, 590.0, 367.5 ],
					"proportion" : 0.39
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"forceaspect" : 1,
					"hidden" : 1,
					"id" : "obj-2",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 106.0, 622.141113000000018, 145.0, 57.826643894107598 ],
					"pic" : "nomsnotes.png",
					"presentation" : 1,
					"presentation_rect" : [ 11.0, 53.272208999999997, 279.5, 111.465841161400519 ]
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"data" : [ 42613, "png", "IBkSG0fBZn....PCIgDQRA..BLG..D.YHX....vRK1Kr....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wI6clGeSTl+G+SOCWsDPNKELHs1BJRE4viUH.hUtqt1ZQT23xg5ptV.uVki.p6OPAEWDEPbIfxgnBEDAQ.IE2U4lhBEP.2fTtwRO3l1le+wzY5jjYRRadlLSl788qWOunjNYlm7oey2muOeeth.DAaLB.y.HspJFAPO8y26QAfip9YGh9Y6t8uDbXBbZM++ZD.cxOeu6A.EW0O6npRw.HeQ+KAQffIvF6SdaRx9TZLARmUC3aeSNbfpaCiH.IB0tBDlfI.jC3bj3uNQpsbTv4fIevEbmcE94o0vL.xnpxMpvOq8.W0ZxwNguf21zLH6SkDy.vBHcNXPZn5DTXB9exIDSInZcygaE9.mIHTMr.tub6TkK4VUcwa8RJTFifKXYGPc04hAfMv0XMAAOFAfUP1mJMjeffGoAfYffmV6.bsiYEbANRPDTHCn9NTjqXCbeQTuPNfy4oZqqR4PeFfqmpDguXEZW6SqP+XeZEjNGLvB3xTlVPWsA8c.yDpHFAWuGTaCc+oXGg1NXRCZCmJ9SYFP+lUTBoIMnc6Pmdx9j7CDbPKmfBGfKf4PUskPigYnM6YnuJVYuTn3jCTecqlVJFTuHCWvJTe6svA6SxOfxSnTBJ3yBJAQsFKP8MjCjR9HzoWM1f5qWARwFqEDBME1f5aiENXeZCpuVo204zPnYBJxG5qoRDQPBKP8MdYQwAz9eAvFTechUNaBUBdlv+wFTeaKVTrCss8oMn9ZjdWms.0WeBzhEFqID5Xx.puAKKKECsqykY.0WeXYIba6KPuidy9LW1JOLC8lNamopCavBTecgUEKLUYHzsXGpuwJqKZ0rFo15hRTrwRAhPUQsskBWrOUaMQuqylf5qGrtXgg5CgNDqP8MRCGbtXD5mgUQpRNLSoHTCBklf30lhEloTAFjNGbvJTesPIJlYmDQnWvHzmYjy8hVXEWEJssCTaKEiP6sHlvYBk19QBksOI+.AGzp6Ym5E8kPCQ3fSEwF+p4vsZF5WGKtWryDEiHXRFfrOCFP5rxidezOTa8kPiQn5xzNPJyfIJWMGK9Y8SOUzBYBkv+vBTe6kvA6SK0x5ZnbIXqyFQ3SBJbBsyvYSnRDNFHGewTfKe0Hrvf5bnXwQfKcDAAr.02VIbv9zBCpyghEGAtz42DtEHGu9pEWfeDAALgv2.4bhfa14LqPeFBUJVBTAjPQQusUDUSKAqrFYNH84QqVBF5b3Xfb7EqAt7QDpQ3rAOeIXM24Bmy9IewdfJhDJFj8Yvw9jzYkWmC2aWyQ.qfDgbnmWJ70jhk.TG8Eg6NWDWLEXRIgBfQn+W0pZA6SxOPvQmo10zgiBRjpcEPCSN.XHpckPifRm1+Y.fNovOiPEnEBg1Ca.3FU6JgFAkz9zFH+.7nT5rUPsqAP9YCavDnT86dQoFp0v84gj6E5X9RaAYeFbrOIcV40YyZfOWZoBsPHBCvNTeCMsVQI5IiQPAMKUgbxnMfrOCN1mjNq75LMUA7rXI.zSMGzvr5IY.fdp1UBMHlUf6oU.zPE39FpiY0tBP..x9TNLy36mUP5rTXlg2qb.MUAbGypcEfkPAy4Ip0FkqVmzX78yD.dAFeO0KvZslnliIP1mxAKsOMARmkCVoyl.vDYz8ROgY0tBvRnf4bEKf58hbv5rUZkw2O8DlU6J.AYe5ELyv6kUFduzaXlQ2GqL59n23FgNZJsDgZWAzX3.Tvbdi1B1rG8XB.+OFbezqbTPaQIpIl.Ye5MXk8oIP5r2fE5rIPZr2nWPmr+dRYlqZx.Tfb9BSL59jCitO5UH6P0Ex9z6vJ6SRm8NrPmsxf6gdFcyTZgBlqZrn1UfP.XUJosvn6idFSpcEHLFKpcEHD.V3KvBCtG5cLEfu2+BapF5VzMCyJELGGl.sQJ5OvhdwjAnUtl+fI0tBDlhEP1m9CApu.xOf+go.38ZgQ0A8LTl4zYP6FzAOHslPKCYeFbvhZWABCvhZWABAfxLmNCypcEHLBpwRBsLTF5CNXVsq.5bRCzbuMrBJXNNHG3AGLCZnUHztXVsq.gIXFje.kFpSygYPAyQNvClXVsq.DDdAypcEHLAypcEHL.ypcEfH3BELGYzGLwrZWAHH7BlU6JPXBlU6JPX.zQRYXFTvb5nUyRH.jVSnkgrOCNP5rxhY0tBPD7gBli1BHpITb.7dMBZdxPncwDH6yZB0VeAjefZF0FclBVNLDJXNfNo1UfPHxO.dujClZF1U6JPXFlT6JPHF0VeAjefZF0Fc1DqqDDZeB2ClS2rGyPPPDPP9BBNP5rxCEvr+ic0tBvJB2CliL5qYXO.dulYTcHbf8n1UfvPHeA9O4E.uWRm8eH+.D9Mg6AyQ3+ThZWABiHPlahDDJMj8YvAG0x2GsRV8eBjoNjlBJXNB+EciQeH.1U6J.AgWf7EDbfzYkGGpcEfUPAyQ3uPNVBdPZMgVF6pcEHLAxOfxitQiof4H7Wrq1UfvHzMNXHzkP1mAGHcVYQWMmDof4H7WHGKAGNJzQo9mP2wQAMm4BFP9ATdrq1U.VBELGg+.4XI3gc0tBPP3Erq1UfvDrq1Ufv.zUInHbOXN6pcEHDgbYv8vNCtGgCXWsq.gonqbrqfDn9BryhJQX.AhNeTlUKz2XWsq.rjv8f4H7Orq1UfvHXQfyD0bngNz+vtZWABSvd.7dcvn5fdl8.clNQAyEXa.lgCTBnLyErXkfBpPsfxLmugE1m1YP8PuSfpyjODeic0tBvZnf4zYQmq.vxLEQo+26PYkS8nXP1m9BVYeR5r2IP0YpiI9FapcEf0PAyQF99BaL7dQZs7vpLfRT6grOkGVZeR5r7vBclzWuyQgNTinf4zgoakgbTvV8gk2K8F4BZ3QTarq1U.MLrz9zNitO5QXgNq6BTgwLC0tBPnb3..NohGkbB.MUJLoA9LoUKzAPt5iIn91AZ0hoZsp5Ilz.edzpEV4GvgF3yhVsXpVqpZXnLywgc0tBnAoDv94UfCny10sYD4Ap2zZAb.Z9bIE4A1N2hc.RmkBV5GflxFRyBfNcdxSAywgM0tBnAYFPYF1OaJv8LTGqpcEfP.apcEPChUE3dRC0kmXkg2K6L7domfr6BCvAT+z+pUJEC.iAjZJOlz.e9zRE6AhXRvbLA02lPKUrGHhoWvjF3ylVpXOPDSYvgF3ykVpXOPDSsNQo1U.MFOfZWAzHLU.7sJz8tX.zVPyQLddRnSS6eHJECfaG.op1UDMBOH.NkBbeIc1UTB+.FAfYFeOCkg70FlfQv4fQs68fZWbDf5n+fYU3ykVrXKvjQBEByP8sMzBEaAlL5SLqA9LpEJ1BLYTVLoA9roUJ1BHkjHjCqP8M5T6h4.TC8WrGD9rnkKJ4PYSD3XGpuMR3f8ocU3ylVpnz5rMMvmQ0tTLzoqfUB4wHBummAAyIGpIE5yPnRIi.VAITRRCpuMR3f8IoyJKlz.eFU6Bq2hsHBQHCn9FepQIeD7yTzLXTcOTqXiAZGgxCYeFbfzYkEqAgOKZ0h8.V8HBoIWn9FgAyRwPcVPBgiYBUMBZln1AYeFbfzYkkvQ80IngWk.geKFB0bH+BmFlE0JnYhZOlg5a2DLsOMwBQqVfYeT2zSE0vOfYFTuC0JzTYg..gOAYXgQ5UfPNP80gfQgBjKzjvA6SsPGMHcVYIbZ3ro4IGgKXApuQoRVrwJghAXCpudnjEKrRnHTErA02FRIKZkrXXCpuVnm8CjOTeMPoK1XkXQnuvJTeiyvECdaP80EknXgcRDgJhcn91RgC1m504rrEFpQ0Vz6yeNarRnHzmXCpuQJKKZ0TPaD5qFLKFzNvtdBiPekYCsvPqJE5Qc1LKEn.jzf9bNgaigZDgNFKP8MVYgSEKrUVTDrA0WqXgVqEanjHvvHzG1mNf129zFTecRupy5s.5rwT0gP2iED59E.GPa5TQNBkmrt1As8in2grOCNP5rxQZPejATKLVWHBSHT7K.4BssSE4HCD5E7rUkPHHzjP1mAGHcV4HTdpsn0F9ZhPPLhPidL5.ZmUoVsESHzvYS9HzJymDrASHzv9zNBssOMARmURrB0W6pIkP0DTPnQwLztYoyJzWF6Vf1bUXEpLODITVr.sY1ib.8k8oEnM0Y8fe.yPa5i0ccNTOAEDZXr.syWBrA86QXhQvEjpVvYdwP+EvLQfAYeFbfzYkEqPans5cclPCiEnNCEfCDdYnaDbauJNPvWqyGb+cNbQqIp4nErOCGfzYkC9UssZGTWwPemfBBMNl.2bpyATdi7v8TNaFJuSGGf6umghyEFB0EyfrOCFXFjNqDvmETGH3FDmCvEnN0oY+jHT6JPX.l.W.WlAmSfarVdeJAb8FztnBgqjF3z5z.md2vZ484nvUsN+.upQPHXeZtpelrOUFHcVYHCTstVaaGyabTvsvFrARqqwPAyoNX1s+UNbTUIev0iShZFFAmyb9+0avqw1U35DAAOj8YvARmYO7cX1L3RXQmpE2i8.WCV1ACpWgsPAyQPPPPPPDnXBUO213CdlmhQ0YayAn.2HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHzbDpcbdYFbGSHlP0mwdd6fTdOn5yzTGn5iPD9yeOwuFQ0XBUqwFg2OSCsK5moirkZFlg2OBb3wAbUKsW0+JVuIjGyfzYkFyfz3fAlgq9iqo5r6+LgmvqulP01ylk4Zc+bS2dU+aP2dVqGLmI.jQUkdpfOm8fpOP6sivOicSfSiMWUQtfiqMTB3zUGUUrCO+BP3.FgqZ7Mx36ednZGH7EGL9YDJfQvouY.NGx0lC.buAoyjFGLQrOCRmUFHeyJHV.2GXmpXwN.xAUGYtdCiPc047AvL.2WhzyXF.4B0Qic..af6uyR06c8DlA2mURmUNLC0SiKFbeOxBz2ZL.W.x1.2mY0TmMoneJUex.juYECKf6CoZHtdqXup5ld.i.vJTGGEdyAhMH+P4FJhEn8rkyE5ufmMC0uieRoyVTtOxAcLCNefpstpm0X.soNmOzeAbXAjuYEizf1yHVphCDZ6.ICn8LhcuXGxO+DBEHTvV1ABssiA3xZfcn9ZodVmMARiCFXBpWFh72RwfKI.gxA0kFzdc7iY1yZg4LmEvMba9bdZkTRIidZtWHsauy..nqc4N74M+.G7WQYkUFt3Et.Nzg9UTZokhuXYKM.qxHOvMDrgJSXWifKyWCQkqG0DVI3zXGpb8nlPN.38T6JQMfiBtu+YWcqF0XxAbMrvx41oRRnnN62ZrX+xwEWbH0TtYedy48Ke5ScJbxSdBV3WNTTiA35fsMnv5LCa+qDvoy4FH2DU.q.XhpcknFvd.22As6uuA0NXNa.3u3sKH8z6GF5vdLz0tzYzg1mJydvGqviiSbhShsuichSepSgsussh0st0VStEk.Nw1FypTJC7y+BlL4Y4cn..7Iyatr3V5MBUbbXDbcHwq1x0DF9HFE..xy9lvgO7gX0sUNdevYKGJfMvPcNyrxFwGe7jN6J1fOz3LyJaLfANHz6d0Sz5DaEydvh8KejCeHr+BJnl5WNTQiA37Y7Bd6B30YMX6eqDb9l05KjMifq8idxpaHeafLJwP9hIAt.Q8IpYvb1fWbXLtwaECbfC.cuacwkWuzRKC6+.GTnGG6eu6AkTx48382hVzRXpco..fTR4lQ7wEGRHgV5UGOGqviisuicheXyaFy38lt+94PK67feH+pQYvHyrxF2wczEj7MmLZUBIf1mZJH93iymuuB1+APYkcATZYkgCdveUn2fEdriUScT3N9sAsJfQvow0nfkSO89gt1stijRNYjZJ2rOsM4g2IL.v12wNA.P96dWrvwRdfKKAZUmy5EcdOfaZDnE0Ye1v23FuULpQMBOzPd+x7Z0QN7gPYkUlGu+V1xDPyaQK.P09k8k+kZgeYsrFyiM3i1+F5PeDOBfy81+pI5rBz9mVWmqU9L.35LcZ2dmEzN2iCQN3aC73m3D3Dm3jBYdN.6r3B.WLFdUmUqf4jsGIYlU1XricrtHdEr+Cfu9q+F7S+G6Xm6X6HRmkixu1kQkUTge8vhLpnPzwVW..TYDQia61tMzlazD5PGSCcqacSx+PUZokgUrxuFKYQel+DHhVLfNSfaXf8q.4xLqrQ1Ccn395cu7q.2pMr0ssCbfC9qH+cuKr5udU0Ti6E.s4biIe3mNKRO89gAN3gfgL3AxzrYHlB1+AvAN3uhcuqcWa5ssV14rc3m8tN8z6G5Uu6CFzfF.SylgX304C8qGB6bm6nlFfmVUmsCYz3gOhQgINwI3hc6V21Nfc64gM7ceK94eNeDazQgHcVN.fr9miwPcQDQFE..pLhX.fSTQENw0JubL3g7fnUI1Zg.ukxu7wJ73XkqZ038euo6K+GZUMFvKAxM7QLJLlwLZWra4a+aGaeqXy4sINcFNAbVgr5bzwDKhL5XAfm5bu5cePps+VPyaQKPW6xcDns+oU04Zbfb4L5wh6sG8.8oWlQCaX7LuBItCOG4vGpl1F3dfOVffpQvbFAfmoRC.ybVyFO2e6oD9+KO2UgE+oK.a8m9QT4UK0uCdqlPzwDKhLl5h67tuajd+GB5682WOZnc44tJ7JuzK5Kg+Ig1ZHWyE9wbja3iXTXjibjd7E571v5PdaZi3fGb+3Lm4L3rm8r3zm7DRdOZdKS.MsoME..s6lZGhKt3vsbq2FL1nFiLxZnx9r4cRsouei9aPGiFbcDPqfE.LeecQomd+vnd5mFOTFC1kWufeIer909MX+ErObje6H3RW5R327hM1sdab9kZVyZFRnkIfDacavMZ5lvexbuQSZVyk78brBON99MkG9lU+09a.G4As2BPwBXfNW3w9cj+dxuFqywEeCws0oam05rOcNGjwBjPiSJojwTemo4hlt7bWElyGNKru89yHxJtBt9UuLypD7c7txHhFWq7JP1C8wPO6Uu7nSlkVZYXge1h8UPcZMMFfq9rao9E1V3hve4weTg++xycUXIK5yv+cy4gnw0THcNFbsxKGYOzGCctKcEO3PFjGcleAe5hwaNYq9RmMCsU.c987Wdbi2JF4H9qnMso0t754trkf+6OXGm3jmnV2F3Mk7Midz69J6yluCQy6imi+DXmWSnQvNXNImaQIkTx3yVzhEBnnf8e.LwILAr0eby35WL3ZeDig5h6qeCDi7oeNbW2Y2Ed8RKsLLQqSxWoe91g1XQQXB9Hqbomd+vjl7jcIHtbW1RvWrrEisssshKTZoLqxz7Vl.LYxD5bm6BL2m9Jow8xycUXMey23OyCudAswjb1mCIURIkLF2Dr5hC5B9k7w+Z5uM1xV2hrNFpMzf3iGlL0VjVmRC2y8ZVxfnKX+G.KYIeNdy2vputcZoLM6yEuSRIkLdgQOVW5HXA+R9X9y8iv52v5C557wJ73Xtycd9iNqUx1rjZb5o2O7tu26JjknstscfILtWGEr28fJuZYJRmqkhXqaCv0qLJ7jiXTXvCYHt3y5XEdbL1w9hdK.ZshFyiGcxNojRFqbUqxEcdZuyais7e2bPUmiwPcQkQUG7myLa7nCaXtnykVZYXLi8E8l+4UBsyVqgQv092M5sKJyrxFie7iCc7VuEgWy1blExM2uB6ae6kosAdSIkLLYxDRIk1ireLK3laeG73ZVdtqByc1y1WI1P1obTvLXNSfyP1kzd5tg7G7gyAVG+qAC3JAMiXoHxnhB2081GX8slBt0aoZg+C9v4fm+YeZ4daGEb87RM6ghE3iUG73FuU7hiczBoR11blElwLlNSazyaz7Vl.tka4VvS+r4f6sW8wkemejEzR.msjZpw9bQkjYVYioO8oIjk2B9k7wq8xiAa8m9wfSMD.c+ttazm9b+34ewW0kWuf8e.X05j7UFjzBAM6W5rUqSTv+QA+R93slz3w2ug0Gbpgv65769tumu5fxCB0cA9HoFmYVYi48wyUHKMS8smNd2oMEDcEWRU8KGS8Mhg7PYgW9UdEWFAkwOgI4sfmUaMFPlg8St1+pajWGke8qE7qkUQL02H5y82O7Ru7q3xP95CcVKLxIoAt+V60.4beT.e+29sv7m+mDTaC7tu66AC+oedbGcsat767iLgJou4fUvbxNQ7EaLO02d53UekWDI1xlixuj1HisQFUTX3OSNXxu4aI7ZKO2Ug+7CJahBTyIquOWcThSkeA+R93Ed1mB68m2SvntII2TRIiG8QeB7ri4kDdMMdu.s.eDrbNidrXRVmHhO93v4N8ov3dkwfUl6xCZUP2oAwGOdnGJS7JSXxnwM9FDdcMdGS74V1vvGwnv6N8oIDvwK87OE9rOcAAmZmDTK0Y0ryIlAWCed0ubVOxPwWrrkh1zha.W6xWHnWIkhlmXawnekw6RVu8RfFpcG.8q1+3q+ZIcN550H7xu13wy+b+MgWaAe5hgkmXXRc4k.tOqNBN0NOvB7ioiwWshUJLsAxaCqCu1q9hdcZWnzzoz5LdkwMIzq6q5QrxGYb9nPhSminTrZX0XA.KEx3T9kekWC8o28BFLX.4umeAqcMqFFaPcPEkqd8JQLNc5D6Za+D9tu8awPdnLgACFP6SMETQkQfMuY6R8VLCtT6GrcbXC.Ok2tfYNqYiQMhmD..qbEq.+0G+QPg+9uGDpZxy4KpHr471DVwW94nMswDZWRICCFLfAO3A4MMNUvM2tbDLqqnZmE0QtKHyrxFex7lKLXv.169J.CKyAis7i+2fVETJt1UuJ1S96FKb9yCW4hWB2SOLC.ft00tfaKsNik84R5vvH.tJTmryYA.KA9Pm+zEZCFLX.+9wJD8uO2Cxy92GzpfRgXc9hkUFt2dxsE9vqy6Zm6DEUTQt+1pC.pK.91fb00B.VAjPi4G15Az+G...+1+6nvt8MACQb8faMzKbwRKF488eGNWwWD8oO8F..8tWlQIkdArks7Ste40A.sDpS14r.uz92jdi+It2+z8.CFLfB1+AwZWypQb0IZTYkpW1OESkW+JXK+2Mic+K6C8oO8EFLX.o0oNJmu45.toZjsfc8DbSKjOxWWjsEtHLzG4gA.W13x44eFbdO+NYPkSepShuZYKA4soMgaqycEMqYMEML93Q5omNhNFCRoyFAWh3b4WnzAyYAxz3W5o2Orpu9qwClwfgACFvwJ733im6bw4O6YPEWwykZsZyYN0IPdaZSBAz06dYFEr+ChB12dk5xaDBtNNrA+Xu2pxJp.CdvCFm9LmEYN3G.W4RWT4qY9ImunhvJ9xOGW7BWDl688A.NmydIftaG.yNHVEs.+nWeEru8hrdjrQSaZSvik0ChCVfj1GpBW6pWEa4G+OXSabCXfC4gPcpCWGS7R.clQvuiIVPMTmybH8Wyoya8m9uXCe6Zv.y3Oi5Tm5f1mZJn82xshOcgRl4v6DAWc1BjQiyLqrwhW7hQe6CWfnGqvii+06+93xWnXT40uRPp54eTYEki8rqsh+6Osc7HYmM..t665tvt2c93HG4vte4oA0yV1i1+xLqrwW7EeAFP+e.WZ+qjhNGp3pZG+x.b5riCePricsGzu9O.e09mID76nsM.7J9yEZxTawCj98iuc0qBuznedksVUC4DG+XXoKZgvXyZEt8z5jfNKSGT3a+S3KkQpf0szfLNLxYziEKaYetvXwufOcwn+OP5Xqad8v4UKQAqRAF+R96.O7f6mv+2pUY2Po+KH3cHEaC9Qfb7Zd7wGGZSqSDMtwMR4qY0Bl8G7d3wdj+LJsTt.5eiIOQgMPW2nSH3MTqVfeDfQlYkM1WA6Wvt9lL0FEtZU6XWaeK395wch8tuB..vCkwfgsEtH4tbqAq5EpA57V151Ez4N0wawGuC0g8j+tP26TpB5be6SuzB5rEHiFOtwaEK6yWBZchsBkVZY3C9v4f1mZJX++7N0LS6E2oxJp.a8GVOd8We7..H93iCSZxSVtK2ZvpdANeSxpyy6imqKySt6uu8E+zOXGUbII2nGTc304mSzvs5k1+rFLpSUQNvO2DwmxTmFrNwI..fGXfC1GWs5v0u5kwDe4+tf8L.vjrNQjYVY69k1P31hTSoxLG+bDvidjLtwaEScJ+SgdiL1w9R3i+v+EtvebBMyPq5MNyoNAJ8BWF8p28AMsoMwaYNJXjcNavGFxIkTxXoK6Kwe6YdJXvfAbtSeJLlmaTXm6XaJbUq1y+6HGB+3OsULrG6wA.fYylwW9keoTCQUKgxmR+L.2P94UlxTmFd+YLczzl1D..L4W+kQtqb43ZW8pJb0q1Qokbd7MectnW8senoMsIHsN0QT3wOI18t1o6WZZfa0spzokwL3F1OuBuNmXqR..bCUxB+TaZVc9pW8JdnyxzS6fQliLCYz3uZEqDO6yvMKMJX+G.iYruHVxmYCFvUQ4WicaIFJANc5D68m2MZbKZCRqScDI1pD7lsbvH6b7SBeOZ+aJScZX7i6eHz92HG4Sgk7Y1vUK8LnhqcIEtZEX3zoSbLG+ODcciGcuac0as+YBbyq4SovUIKvOFZ0jRJYrl0tN7HY8vnN0w.J3WxGizxihiW3wT3pWsiJqnbru8sOTTIWB8tWlgACFvsbq2J9vYMK2uTWxNmRr.Hjcy5ykIe+9O.9yOzCgKU7YzLS1S+kHiJJ7key2i65N6NNVgGGso0IJ0kUBT1CkXKvGYwv8U5ms4LK7Vu0jX5RtVIwxyLF7+8+8OAfWWzIsEJWJ884InQRIkL9vYOGggkRKLgZqIz46rGXIK6qP7wGm2rkU5Uoleoyh2uyxaCqCu4jmfpt3cpIj7sjFV8ZWufN26d0KoVsZJ4hmxDjY6JR7DBe44tJ75+iWMjzubySrs3q+1MfVmXqvV21Nvc18tJ0kozKPMY2VLDqyaca6.V9KOAtTI+At1kztiFkTDc8ZDFFT68I...H.jDQAQU2F2D5P6S0a9LT5sDFY2u9DSlYkMl6b9HXzHWSwS90eY7oepsPh1.qL13v3s9lBq5VYVHUB6usJQl4VJjXu2Zbi2JFcNbiQ8xycUXPCneHhqUJS2HDCV3zoSTXgm.Yk8PQCiOduMYP2C.NfBTERC.dcyngeqEHwDaEN2oOEFokgh4M2ORylAConf88K3Vu8th1cSsEsO0TvV1x1jZtvDATlION+9HmrKwc9Uh1c1MtFMl7q+x3UeoQq5Sn1ZBm9jEhKd0JQe5SuQCiOdzjl0Rr10rZ2urVBkc9I9svOz4dbu2C.3xF23d8WQ0W7N0DJtnyhS+Gkh90uG.ML93QjQGqT5bagxEzrc3i.LVdtqBO4S7Xv4UNOJ+ZgN9I34xWrLTNhA8oO81aYmSI0X.NaYISjA+Due44tJ7vO3PPjkeQb8qDZEvL.2hh3LEUBxHiLPCiOd4xzbaAvTTnp.eBi7ZxRF23shO5CmIpScpCN2oOEFR+6CVypWUHSafwDIvm+kKG8q+CDI1pDPpojhTiPUDfKlKlOm4x.Rr4dNtwaEuwj4FecdGFMHlJT08Qm.ke5G1H9osrU..LvAN.4tLkXNcwuAeJKh2inN2oOEFx.5aPce2hUbsKeALlW34Dl+bi5okb6cPol2bVgW1eybe+gZXO7fvGMq+kBUUTNprhJvBm+Gistsc..fgL3AJ0k0InbyAzYfZnNOk+4aDRzyZwTYEUfk+EKAqeiaB..Owi8nHojR18K6FgxbhEXERnwybVy1i.4ZPLUnp6ibAB71xGqviC.f9O.I8KqTZL.2bXRxDYvOhTh04PwDYvyF+t0J3yPFctgP47MaC9XejSbLG4sg0g6y7cGxjEedJ+5WCsrYMCSe5bGTAwGeb3EF8Xc+xFBpJnVVlYN9nkcYdBjYVYiO5CmI..V+F2DFV1YER6vfGmNchRuvEwPx3gPhsJA4xZjRz6jWE.Ohb+RwAxk2FVGFx.ueb5SdRFWEBdb8JbhFXrInacsKn8olBVzhVr68LwHX+7fwL7xbwPb.Fm6zmBOZlCF+mMmGCe7AWhDUhicpygLy7g8VllOJ.1Biezlgepy.bAxEJ1oDAp753HNNNdrGaXvfAC3rmqHoz4CB1pyoAIlym4L5whILtWC.5i.4DnxqiJiptn28xLZUBIfoLk+OotpSC1uk6XBbYHQ11+zS5bkW+JndweCnO8o2nc2Takq8uSC1OpIY..YW4E.b11ScJbSOm71v5vH9qONN+ebNFWMBNDcjNwV19tvskVmQ6SMEjZJoHkM8AAP9rLybVgayGijRJYL8oOM.vMG496O2ypKLj4YCqc0B8BrW8tORcIMDrsWflfWLjSJojcIPtQ7We7PtLX3NUbkxv6+dSWH6biXjRtU5YlwOVad6W9gydNBAXLzLGRP8TcPIn7qeMj2l1fPOskISylUfGsWGtqo9NSS+DHG3xbzt201wxycU..XnCUx9jw5rYXy8WH8z6GlTUqDwstscnaBv.fSiW0J9B.vkIib7LSF.JisrM3i1+l33dMciNC.rf4OOA+xcsacWpKwLiej7GGnxRlYkMrNQtUBpdnMvxu90P7wEGl6r4lkKxXSaFfcCypIHwIOv3lfUgk4tUqSBWrnSpaLjA3VFwaXibaTolMK6QzIKClyl29kqbUqRXnUesW8ECoMh4oxJp.kVx4wF9dtgmZPCRwCzHG3kT3OyYMagE6vvd3AExk5d4H1niFKcob60bcuacAomd+b+RLy3GoE3kgWU7P.pGBjiGCwDEVy27M..nCsOUozYV5uvBjPimzjmrvBwvxe4IP7Fftxu74K5bBCm881idH0kHqy5ZIlk5d9gydNBs+M5W3EPIm835JcNJmWWvubO5oj5bm.aWDfd02bRIkLd6oNEzvF1PcQfb7DeCpGV25VqfMsDCqsY.1ELmU2eggOhQILOA9nYOW7S+vlBomi.xwOl2FA.b4PI1MXkyYyvKNgl4rlsKYKJTY0T5ODazQiktDtQJRga.zH7xJca3iXTBqrnW54eJcS.F..35WDyPTFPkHSyMDrcdyYUteQlYksfNO4W+k0U5ryqcI7IyatdKi9rLa9Vc+EF23sJ3qZRSZx3Rk7GgzycYonhqTF10N2E..5ZWtC4tLVFzrU2eggOhQIzouIZcRX+6aO5t1+t1ku.18t3VPocuqJd6eFga6qZtyGN64.SltQcUxL..hnBtErwlyay..B1Uh3FAXSvblfD60YiYLiF.bow+cl5+TytoSFnr8sV8p3QlM2VEywLOha76kdd08rVUIHhJtB9hksTu0.nrY3oFRNPlsGijRJYLwp1vIycYKQUOCPUBJ+5WCQGcLB8zty2Qmk5xLwnGmE3kdXyOzT4sg0ERtnR7Fke8qg5W+5iueSbywRYxnuIF7nr.2z3jRJY7Ru3X..2725a+lUExssX3OTYEUfsuUto9PqSrURsPS.XmsrYHQmr48Ur9MtIr3OaA35WTe192px8KA.2v+IwFaK.6xnur9lA3lmb7A4LBKOptJYF7cBXoKo5MbboFpUVDLmU2egwMdqBYIZhSXBndQWICdLZSNeQ+AJX+b69HIm7MK0kXhAOlzfWxJG+NwcdaXc5tfL.p1Xd66faaFPACzvhb+hWXziEsNwVgyclSCqS70YviR6QCiONe0SaEuiIybVyFsNwVA.fW34eFF83zVT+5UWj+t4xbjLYzmE5rU2egWXziEwGebnzRKCu9+3UgyqpOxbgT7C+vlE94dZ1iLY.nvY+j2FdZScJHVmgFaEF0F9iycNgNYmRJoJ0kvhgY0qYkKojRV3jcv1blUH+bXVJtgF2Xb3CeHg40b67rCJlBzf4LBIlvtiZTi..b896m28N0coWVLkesKiieBtUKZKZYKk5R75Rn1OQVCYwAN+Zu5KxfGk1j3iq5.MRMEEInYKPl+Vkd58SHymi6kGMN8IOQ.9nzlTWCwfsuMtsaGY5oMKbLmA7hN+DOF2Ty3kd9mR2pywFcTX0e8pD9+xjQ+.AyPhrxwqsK7yVLtXoEoql+VtSrQGkPCeoc6R14OVXKaBRzIa91+VvmtXTvd2itaXrESjQ3Dmnp1+ZdKZgTWBKBZNC3krx8BidrngMLdbtSeJ7Vu0jXviS6QrQGM.pNgFRL8AB3f47PjyopLX..L2YOaDUk52.4.3Ro+AO3uB.YCxHPQx.l4g2ww6+1uktJ0xtSLwDCN3A4x.pLCcRf5bVdMtp82tB9k7wJyc4A3iQKSDXcqasByaNI5osIF7P7pNGe7wgB9k70kYXlmHh.3vG9PBYzPhL5aN.eDVb+EFwHeJgrxMmObl51g8imHgSb7Sv0Yf3hKNotDVDjgGcxVbV4V7mt.T4UKiAOFsKQTY4dK.CVgWyJ2iOLtNcN02bh5l4Im6DUTbGVWGop13aepo3w0vhf4bA9UZAeV4zy8JgGGG4f..Ht3ZfbWh4.31KauR3cbbtybZ7Aev6G.OBsO00PL3KV1RE9+RLzIAhyYiPhM6Z.trEwupJeqIMdotDcCQ575..X+GfydVhdZaJ.eDFgLmkvgS5bDUVN.fPFMpeCj0uQsAI672i9nbM3svOawnjyGZtmaUivYEB5qB0Ia.IzY9s0mkm6pPA6cO55reB.TY4WCW7Bd8TrHP6jsI3k4DMWV4ZntuCfQBm..33GmqCfwGebtmPizBjf47nAvzSueBSBw07MeChF5+.4..N0o3bZvObmLFelUtEYad51dj3N7YynksLAVdakUiG5vdL.vkUN8zppzabfpxzbJruQPY04ANXNWIgS5rBkQCyvsN+kYVYKjsnuN2kq6yJG.fyJU7fnRCtMT1omd+DlCjKcIKQ2mUN.tEzygNDm+BoxVDB7Emlr9L3xJ2PAf9uCfvIm8rWRngw.IXNyt+B7qxviU3wwRWxhz0yUtfHlk5EG9HFkfC54O+OIXVeTU7w7yn1hrNLdvgLH.DF3rPDkUFWiPwK8vSEHXVteA+wH1+Z5uMqelZNT3CvdOrkGv.4rgW+F2D12d+Yk7YqY35W8xBKxDEBOzYws+8er+859rx4NwGOy8W.3EeyYOzgILW41VUy02vA3Sng6vzf43Wl8e+lxCMJdlNzAgqXFxLDq7Cmss4LKc6DEOHhYodwbpZ0+8Gm8zgENK3Cx3zm5TJ0ivrTun34Y6F+9MnTOaMG7AajPBRtvopsX18Wn28hyu7lyayHxJtBKeVgDHSFiBT7HHC9Mz7UtpUiXX4AkY3MxtKNvuQEunE7IgMiLEP0Izv8E1CSCliOEy+vlySXitiHfvrb+h6q2boXc8qm0G8cZa3GZJFRZPl.l4283+r4G9LL1..mrpNGv3fLLAYVEq75rs4LqvJclG9.YY.lfDC8G+8eK+3+IrbzRTnLF4xvGlTRIKLMa1bd10k6eep.lk6WjTRIKLktVlngdLbl.IXNWLlEu75Wfs4GV5zvK3nV99jbR8mYVYK3fJbHiQJLxtvI3CXdSaZiAsJiZRr000royvfL.7hNyOmwB25XhBfGZL+Yl4wJ73Xe+R3wPr5NxMrTA.lc+E3m+RkVZYHuME9jcY+fiF.uWY8Yj8PGF.3lis54cwgZB01f47Pj4S42V21NPiajraILgq3nV99LI0K1idZF.bmDAgiYxPFbTKeelj5EEGv7912dqk2ZcGAxLmW1NlvGzX3RGSbOnYFhGZbRIysh219N1IhN5HTpmqliniIVgeleXoXHx1929OvAQrQSiwpHbD.uWYCl6167sC.f0u1uI.t85JrWaClyikaL+PxbfC9qgcFywDac..D1etXHRtRf3yjw+8Gry5mWnLNpkuOyR8h2wcT0TFXSaLrMfYIxnQ9AvsSRGy76kc+2MuovVcVBps5rGZL+1xwg90CATd3yTeIxniUtMKXdbD.2dOZ+iekeu8crSDoyxCfacnEhCZl+jPhgXRteAeafaaaag0OyPVpsAyY18WnUIvsUQbpSdRg8DkvERr0l.P06OWtQsMMylj6WvOgdyeOARaqgVbsx4VYXJv1kgj6CRIeybYzXSaXcr94oYwYjb6x37axpLNiFRpy7YNZGaQ+cD7HKQ3ZmckHn4ZaFP8Pi48UbnC8qzTewUbD.uWyt+B7q76SepSgxuV3iNKNn4xJSxUosi.31Kal43yluCGAxsOzgJiHZecIEyhylU.Tcl4Nzg9Ug8DkvAhNlX801jgiZ4s1jTuXRIkrvv+c1yd1Z4sNziJpf6780KaWF1qk2ZIy9IemSNwwKrVdaC8vYU8AShy8OdXdl43ybTgG62CfacnEUBtg6j+jePhflcTKu0lb+E38UTbQgAaTvhnxHh1krkIALcy1iew+cxSdhvqsjDQcLozxjbjobD.2cImuVhme9gKyWNmn5oHA+gSfaa8N4yrf4DOYoC25YhObZvzzmIdiBLbZKI4OJpHW9+ts8Yv7kNFuy4i7aGg02ZMK7Y+jGIbNGHM.JoiY9NAFNkk4JpfKpYubxO3nVdqk8bf1tc60xaYnIUTgSgN9IyITfhXvcsqdIk31pYoRDgvPdxerV5FNX8yjejCxKLZTStxUq9.XPlCmfZcl4jM8mkVZogU8LoxHhFsppFjjY+4xgR7b+weHOk31pIQ77xPbOfEghEIP3T1Ou3k35DFemSjv4rcV+LY7JlMjfKdYNcV77L1MXh8blYkMKtMgjbsxKWvWA+ITfa3H.t8xt2msluYMAvsMziKe0qKXGeDoyRlCV+L4G4fy6VG70yTRUyGewGgWEdriI9Rp0YlS1yasuHLaOeow2PSDhTl+ff2MXZfF7GiUgSFxQDa8..2dlEO4YeShuDEKXtvkreFYTQgKdwKB.HWmSBjsX.exd+48nj2dME7Y7T77LVDk.FMDfwGe7r31DRRuuu6W3mcyWA.mF6HHVczszzl0bgNjwetg5F1qk2Zy95BNpieqVdqCsH5XhEkWN24ls3Qlacqas7+Xd.09E.grNaD2fa3.l6ceE9YYBj0NKedL9XrJjfJcxMeAZeG5..3V0vG10dAxzf4BGynQzFpdH+36bRvJ6mgSHdaIQlLGYOnVgzgDacafvpQuf8e.28U.P1xLgniIV7.8q+.fymrDs+EH8PymcnIbYd1xmLCfpmmsaca6P7kjOPsOXNY+xPhst00xaYnGwXntnycoq.vCwkGEKcCQEU3y1+RwUkhY9zqKwpF1NKedgiYznhpbEHdxE+IyathuD6A0JjNkJhHF.3ZGFTprLKQFoBKnxHhFc9N3VgkRLD1.AtsrryQ2N0wNFf25PHho9Uu+xtcIa+yd.b2o.tqhxKu5cGDYrqCnf47.w6wZJ3lholhJipNnqcgSbsaWx4vlck5YGSrFTpaslhniIVgg+ieh151BM4nfwCYhayEgvB3CXl24rD6YT1CpUHcJkcANaYuj4H6r5YI99FNEjQoW3Rn6ck+nkbyRcI1CvGgrAZzl111.7VG5v0Jubgy82csycI0kXOXVezqb1hNuvOyu5+O7g7zmAyBliOaI7yoqvAZeGtUggjZm6j48LQRjYRlpeIl5C.tI9I+vR41Rx1d.9D7nW1hlKBgEDig5JQ.ytnwkf.umxdMK0MHLHanQGSrByWN9dXKgNaO.dDxtpntwaJo.31F5PzwDK9qiXTBaIKq9qWk6WRfpwRBemeL1naf02ZMIQFUTnm859Dluba56k7HOztRVGtka81TxaulfXqWCElubhOsb1d0mVNBIyfYCyJuSpl2hV3Oavcg7Dig5hg7POL.31zOkX9BTB.xM.dD1k5EKqJc9lRl4adtZRNeUYLZfCZv.fKCvLd3+7ZPJ2j764Z5FpLJtSvDwAL+Ca1k3BBD6Xd75bfwjovfLZDS0y8E9CIbF2wDOfe5ezpDCSl9KwTObu8nG..X8abSRMe4ryfmhC2eA9ML21kTxtr560qDUchCCXfCB.bAxJQGf2CB7ExiW6.nwF03.71q845h1XP3yl+wJ73h0aAey01f4b39KvuMFz0tbGdreUoGobDKFxfGH..V4pVsTWBKZ.TVZaXP57EmwHdGzRL2LBTcVRGN7SafvgdZe5y9G.n5CuZIBXlE1xNj5E4ynQSZRyYviPay4KkqA+bF8XEdsY7dSW7kDn5rrcxNojSNrX5uXrwMA2Wu4BTdy4I4PrpH1x7S8iTR4lQjwTWF7Hz1TRYWRXHV+5uVxyGUaL3wHouY9N.kRGtEF7HztDYTQ4xPrNnAM...78axkNZaW35qkOGObZvO7esO0TD1upzqDYTQgGYXOtPJOW8pVoTWFKbZ3wvlHtQ1aN0NvfGg1kxAWObSJojwCkAWl4byA8JQf26OIyLG+zFnac+tBvau1lXpuQgz32idxEv7F9dWl37AZFl4wgTuHeFMZeG02CYRr0qgBcLo+Cfyo7xy0ig.LP0YGt+B7ykot1kNiJqZwWnWI150P7XOwSh3iONTZokg27MrJ0kwBaYO7YvuM9jZJ2rvhIRuRLFpK964LVg1+l2GOGotLVny1k5EKspyv41qyGl0npSbB9lSO89ILkt9lU+07WhK9lCDqNWRAJ+bSH93iC8e.CRWmp4HMDGxNatUi1523ljJEyrpAPICzf+rbr82h9cRMGcLwhSdly..WyXjaNnULGF7qVn1cyRtaaqanjx31w5SO89ILzeq4abom1rJCy1k5E4ynQmuitwnGi1jKdUtCecw5raSN+E.EniI7yk2Nz9TQiuA8cVlu3UKGO5ix4WdEq7qk5RXQm+.jPm4mCSsNwVgTSUe6ynbDKF3.qtCIRLT16ArYQoI48P7TZ5VuMIOMF0ETTIUexkLzg8X.viozkMwWefDLmcw+mCe3CILjI2wczEgIttdiHiJJz2GXfBysnOeoRt2xYiQONICliuAvzzwM.VQjUOTEiZTi..RlwHaL3QIoFyuZg5Qu5CCdDZShstMPXX3D6rvsgX0Fidb1k5E4yneODsYXp2H151.giitAN3g..NclwCwJfDZ7WrrkJLkAF7ClIhTmtkFEaca.dlm8uKjsnkrnOSpKyFidbNfaKbp0st0JzI699.CT2lLiniIVz+AmgP6et0wOdlAidb1k6WvOWPa2M0NF8nzVDS8MJjI+jRJY7fCga9It3E6R7F1D+eXVvb.UO14CZPCPXhqq2HRCwgm5oeZ.vYP4VCe7vJiYIcvu6csa..LfAmgtz4r3rxM7QLJAGzyc1yV7kYiQOthgDSzV9dZ2lVmHL0tTXziRaweT1U.fWcVbTv1IkuGSa.wYzuio0EF9nzNbwqwsOQkTRIim3wdT.3w7r8nfcY.0CMluSP8nm8.QUm3XziQawEulSgN8s7bWkTiVBK0XH08hetLMnAM.caxLtVDFvHG4HAfrs+wpQkBfKnYIO4Y3G4jdJZS6WOg3rxk8PGlvTGPzPZmGbKQDARvb4B25cB+xStCsOUz+ALHc2DtM5XhEoOfgHzqjO9i+XotrUB1sum40.MZchsB2RGucF8nzNbsHpdOzSriC2bPyp.lAjv4iK8zteCjgOJsAh642KL5wJkyB..qL9w5gNKNi92SO0eYAMl5aTHqbh04220rxon1x7Cmae6SuPitglxvGk1fXqWCwK8Julbc5iGaL9wZ28WfetL0g1mJRsC2JiebpOwV2FfG8w9K9p8OafQGGcUgjAFxuZ6G5S7j5ts0nnqW09lAbcjoDMj11b+8EnyTSWD50st0Jj9yrG5PE5QpdgKWYLXhSbB.HnjUNdr49Krt0sVgF.y9IFAiebpKwV2Ffyb1yA.trxIiiiE.1tQAKoCC9dZm8vdbc0vlDYTQgSdFt.LDmsnE9YKVryhiB12.nj5LeF8+qibTgi5Lqlt.73gFOi2a5BcLYjO8yoq5jcjQEEhuwMEOySycxkrfOcwxMGlYseYOz4uXYKUvu7y7bOutRmA.h+FZNFyXFM.3lq3pU6e.bKDP9oOv8b22KiejpGQGSr3Tms5yc8oL0oIUmTjz2bfFLmG2vUuZNGyOTFCFcoqcW2XPGc8ZDr9F+SAgchSXBRcY4A1uWQ40F.6682WDS8Mx3Go5PjQEkvP+A.uE3rUF+nyGRjAT94cSGZepHstd2L9QpdTdT0SXURMtIXUtrEYUAdzNfDCCHeF8achsB2aezOmsyNitAB5rOxJGKyjgCHgFyOrtOwi8nvXSaICebpKQXHd7tu+LqdErNYqRcYrViQU2uE39Ktjk74.fq8ujRQ+jctnqmQ7huxqiVmXqPokVFl967NRcYrtS1.b9lkbnV4m9.47xuNiejpGWtxXD7YjTRIKWmTrJ06MPClyNbSney2vpPu.G6K8R5hryEig5h99.C.O2e6o..vG7gyQtSIfbTfGuCvMzst.+vg05DaEF1S7W0EyctxipdBoWdlyZ1xE3rR3v.PhdTJNSyO4n9aHFCg96eTwTeiBY9LyrxF+kGmKaQezrmqRmUNd739tt0sVr9Mx4X9EFyKoK5.Xr0qgBy6yLyJaAeGRjUNVmIC.Iz32+8lNJszxP7wGmtI6bwVuFhmbDOsvpCdhVmjTqrRkRiAjPmE292ylyXzEczNFC0EoOfgH3qXgelrY+zpBUEj7ueKcIKA..o04Niz5bn+7sM550HgojA.vTemoIUmTj02LKh.nX.jg3Wnzxt.F7fGDZ2M0V7qG92v+ywuiJu9Uj4sqsIxnhBMp4sFy6S92ngwGOJX+G.Cb.Rl8fE..ImrFLfSA.KheghJpHbao0Yz9TSAojZp3eOeagrZL.mg7IOc0M98Nu8+G.3Bbdl+KguKWB.xFruW1.b8.bz.nNhewFDW73AR+9Q6SME7Mqcc3rmrPE3QGbHFC0Em7bkhJqrR..7EewWfl1zlfB1+AvC8fCQ7k9j.viCmUFQ9Uc+coUtXi0.F7fGDRrUIfc+y+B9sCVfB83UdhwPcwI+CO04iU3ww8ee8V7k9L.XKJPUvCMtnhJBFazMf+z8b2nacsK3+9SaEG+2+M3zYnYmsiNlXQ26w8goN0o.CFLf0uwMgm8u8zRcoJkFCv0oxdA.Shew52f3Qu6kYz9TSA+x9N.9secegr5bjQEEhqIIhu7K9BXvfAu092TgxsQ4e.v82QW7MWv91K5W+GHRrUIf1z1jvWtjOUgd7JOwXntnvSeNg++vGwnvq7xuH..lw6+AXYUucj7fPljYvhc2Pavsry8IyatBYz3cm9zPcpebgrY03pnNXd+6EHjdYqVmjTWVIPYxJGO1gDCcB+Xn25DaEdtbBcynQr0sAnvSdJg+uUqSD.baeCRLzeNTvphG8.bFu2zElGLu0TdGDc8BM6ocjQEEJ5hWWHE91V3hD1DJcylNOnvmdIPhdvK1mwy9bu.hpdMRgqBJCQFUTnjKWgfNOyYMaAcdRSZxhuz7fxk8S.Iz3W8UdQAa4I+luEhvPn4DGOxnhBFadqwjl7jExbwe6oeJotTkViAjPmey2vpfs7jl7jPSRHz7z5IxnhBW35QguZ4KWPmGynGiTW5QgxkUN.tNuKY143mK08nG8.8s+CVAqBJGQGSr3jEU8BdHojRFu6zmF.3lhQu5q7h7+pUBuLMtX0Xy4Q14N1wJDCdvCFwGebn624cg4Lu+MpaLQDR0CkHLzP7Qe7mHjF+27slBl8G8ARcoCEA9AQtuvAbK6bG4HGFMoYsDcqqcA2VG6H1Td+Gb1SUXHkFykEiKHjEiuZEqD83duG..L1w9RXiaX87W5dfae9U.xGb+szkH1t5UutPViN1wOE90CdPTw0upBWUXG7NkuvEp93j5UdYtiTpO3CmCl16LE9KsD.7.PYx7oXjL6bNQDB570qHBrycsqPprMyqy76ceCeDiRXStdAe5hwDGuvb6IXnyRpwm8r+AxLyGFMsoMAlZ2Mi0u90GRpwewWkKt8z3NA.d7mvB9O+fjGcWOH3FUCkDGPhryw29WyZZSPRozd70WQp+tK...B.IQTPTodMgT5L..hMdL2+sMA+wu5+30wh9rEJ0UJa1hXH4CIxN2t20Nw8bu8Ds6lZK5R2ua7UewmiKeoKJ8cPCRjQEEJ6ZQhKekpO0rVyZWGZ2M0VTZokgm8u8r3HG4v.b9LLC.YMhXUvb4C2LnOxQNLtxUuFdfzuejXqR.o1gaEewWkKLDMBIB1HBCMDevb9XgiQpO3CmC9GUGgrXV..lhT+BFiC.zV.jl3W7P+5AwC9PODZVSaBt4TZO9DaeZHiFGig5hSVzEqdx3Odq3YelpmWhu0aJjwnR.WmETZGyWARzwDwNLt665tvlruYTz4NCprhxU3pSfi6AXjYVYi26cmNLXv.1511A9ytN7pOCTfC6cYvA3FxbA18t1IZa6tYjVm5HtsN1Qrq8rWbxBOZHqNOqOXlBCMkaCu5+..eaPnZkObqCPEru8h5Vu3ve5dtaz9TSAEUxEwd+47QEkesfP0IvfWim+B+LgNXO9ILI45f8ngxmgYdxG.tLFuG4HGFQGiAz6dYFs6lZKtglk.xay4ExDPm6s+M02d53MeCIGUp2GJ2zKRLWopxC39u3Lm9LBAN27V0Vrg08Mgj9L.3FwjAz+zA.Wxil++dd7+JelvHVNq4sCtdBJD47V1xOI3bt8olBRo82hlOftHiJJTQLM.e3bmmfg7xycUvxS7XRc46AbMHEr9Fpc3VuSJpnhDlihI1pDPBIdiXke8ZzzZLfzAx8FSla3UW+F2DdzryT7k+LH3z3GfDcLA.3m9weDVrXAwGeb3N5RWwJV0ZPEW+JZZmFt6rHojRFeypWsvPl7.omNJp5Ib6BfxNTItyA.vsC.WN6i16u7KBcN4Vt0aMjUmmyblCRrpolwHFwH46cM.2PknjSICw3.RnwaXCemv7ss28xLN24KC68m2cHgFO+E9YtDfwDF+qI0kmGbK3JElSAfFAf6T7Kt4MaWPmSqScDEUxEwur28poCnSt1+F0H9qRc46At0wWEls.I7MejibXg4CZG5Pp3bEeQMu8bzwDKJ6ZQ5Rfbia7Vwny44A.Wl7Gyn+67+J+JgQrLXthgDQNm6JVtfAs3.5pWchUyI1wXntnQMu0XZu2+xECY2xdAO7YKxQvqFhq.tFA8HiF7C2ZZcpi3RW9Z3G9wsnI0X.tUT4wN0eHLzpCeDiBy383li.Er+CfGeXCSMCx.PhNlTTQEgydt+.CdvCBMsoMAIjXqw7W3hfQiF0jC4ZLFpKJ9xUJLzpIkTxXkqZUUGfwHGk3glJX2oDd9VHQmSJ73mPXn.6Z25Nd22elnQ2PSzjMBFig5hxtJbIPtUtpUgNz9TEz4UsJgDDw23Wv7ChGZL.vt14Nw8029hl1zlf9zmdihJ6pZ1.M38K+se2FDFxOuDfwQgOFNJEhs.f9AfVH9EW1muTWBbNl5FG1xV1hlTmiNlXQiZwMh4Lu4iA1etlw8Q6elgxOkLbG6vMey.t1Ak9zmdiyU7Ew916OqIy3bLFpKN04urKCsp6IyPjluGHQ1HkBVueVrEHQOAEaP29TSAcoa2I13l1LpvoSMiQcL02HZeGuCLu+87cwggLFx..2ET94ImTb.Hwvst10rZWbZboKeM7sqeiZpfMhLpn.hMdb7ScFgWabi2pKAxMjAOXwau.4gfaO+3oXHSPyUTYDBqTsa815D9348IvXi0VAZDc8ZDN0eTBt5U4pSYlU1XNyYNtDfgnCqYdmxJ8PXKEWAb9LrH9EKXe6UPmSrUIfaKsNiOddyCMrwMEN0P5bL02HN44JUvob5o2OXaA1DVvCtMGaUiN+AHiFWTQEguacemP.c8tWlwM1taFq661flSi69ep2Xge1hPRs6l..WF4jIPN94hnifWMT.dcdnvs.MD29W25ZWvM1taF6ZW4iKcgRzLidRr0qgn8cpqXd+64i6racE.9UfbJ0Jd2aTL37U4Q6Bh6fRW6ZWwgcTnlamzH550HT3oOmPhL.bMPN2RlwQAW1d8qO.JwlS12BezCk1cSsECIiLPAG3PvwwNNhIRfJqrBEnp3ahNlXwUinNH6g8WvG9geHRrp81Lu3v.fqmAAqg8SJxEbS5TWzXwFy8tWlgg5TO7UqXEngMtoHhJutp53H151.b9KUAJp3p6H2Tl5zv3G2+..RFHGeORTquId..DA3bZIvl2rcWl5.2y81Srtua8n3KbYUOSn71xm9rmUvYQlYkMl2GOWgLxMQqSB+6OQ3zzPMcJyiCTcivBr4Ma2k.m6W+GH95ud0ZFct7nqGN4ocUm+zOcgB9OF+Dljvhe.UqypQm+.jQi4CnKw1biBcz9A5+.wN28dPwE8GZBM9EFyqf2+8eOzvpNxlF+DljbCspZqw.bAYHYfFtmPidXt23v+1uiSepSppYOhuC1O+XdY71u8Tcw984jdqdAH3NsWjh7gDIzPbGTRLwVgAO3AgyWxEwV2wtggniP0hw.vUeyh4qVwJElm3t0FXMtiIJQvbWA.KExDPGuC5FFe7HyLeXzrVjH11NyGkWoSffX.GQFUTHp51PbK2d2wG+I+aL7+pEXvfATZokgW8e75xMYOA3BjyVPoR5c7Picu21+o64twskVmgc64gie5hPCaTiB58Rg2o7INyefqe8p2Yq+3OY9XjC+IAfrAxYFA+T36N1gDNMxcEKWXXsa2M0Vbe8sunvieB7SaaGpRfy71xmpnxb4L8aJScZ38mwzErqGwHGkTAxolM9wyVfD5r3.5RrUI3hNGWCuADIJW0z4xtP0GD1tqyO6y82w6OCgsTGshNKoFWTQEgk84KUvdtoMsIHiG7OiFz3li+y+4mPzQ5Ln1HHuFem8nOXly5ivij0CC.tsonQNxmRtE6fVQiA3pCdzIP.t1+3W7IMsoMAY8HOBhodMDG5PGAW4RWHnGrQz0qQ3VRqqXNy6Svij0CKm8q6nUZ+KW3i.53SpQySn0XgKZIvXiaRPMFCfpsmK7LEgKe4pGV0jRJYrl0tNgExy523lviOrgINPNynFZOqTGa.xlx4MuY6nf8ePbK25shl1zlfz5TGwS7DOAhL55f8eneCWuRfnPkJlgcLFpKbFc8QGu8tgWXruBl16LEjXqR..b6oKOP5oi0rlUK0asDv0iDaJREqlijAMWTQEgObVyxkdA9fOzCghKoTXOuMCmQWGzf3hWwMp404Sb1yiKdoKI75CeDiBye9UmJe2LhAzNAxwijNMV6ZVsPfFMsoMAomd5vXitArhUrbb4qCDmwFqn1w.bAJGYchCm97WDEWZ0aRsomd+vhV7RDZHrf8e.Llw9hRMzpZgF+3QRcVb.ch04bW4JvkuNP8iynhGvAuCY204jRJYrzk8k3IpZmw+XEdb7bO2yiO6SENkmzZ5rjZL.m8bgG+j316bmQyZZSP25ZWPVC8QQEQVGr67+kflFeKo0MLNquAl3DFmfe4E7oKFC+u9jxs8in0zX.Y5DH.2b6Rb6ecqqcAO5vdbboqGA1c9+BhI5HUzLhxoyFQG5TWw3mzaf+4a8lB57523lvPF7fDucP4NZk.43wNjHoQ7Az0867tPhsJAjVm5Hx5QxF6N+eF68.GFwarQHxffu4HpS73Lm+Bn3RK0keWNidrXIKYwnc2D29O3B9zEi+7CND9gVUKZOC.t83n7AfSoJybVy1YIkTpSdJojRcNyYMamomd+bFebw4LwV1BmstYFc1hFZHfJstYFclXKagy3iKNmYlU1N+pUrRmh42OVgNyYziUx5XUkhgDewTifrZ7Tl5zb4y4WshU5LojR1I.bFczw3rkMqYNaSKaV.qu7kDaRbB5r60kzSuedn6SYpSy8qKW319hkFBaPBMNyrx14uerBE9LskstcmYlU1B+9l0zl3r0IzbmIz35wDMNgFWOmsNgl6rYMsI902o9tM78B+MuphCncskApA57vGwnD982PiabPUmmxTmVnrNaCx3qKojR14Lm0r8v+3Tl5zbdac7VYtF2lV1LmMqoMw4vGwnjzur3+FKQQK6WFfaitU15ubs+0mdY1YhsrENSrIwwLeysokMyYKaVyjUmCwa+KW3EMVLybVyV36oAaeyomd+btkstcW96sa5d9vsUpqVCifKBZYcdXagKxEiZmNc5beEremSYpSyYlYksyniNFmMqoMwYhsr4NaSKaly1zhaPRC8V2LiNaSKtAmsNgl6LwV1bm2PiaryniNFmCeDixosEtHWZLP7WdbyIr6EMu.CuDPWlYksy8Uv984mYA8UFsURGDUo0srYMyY8qe8k0.1cmV6qf86R.OUUTpyOQVhMHiMr6NH+pUrRO9LFebw4rUsrENaSKale2IkV2LiNaSKalf8rTOe.3bbi2pK12R3nvI39dnVMXYwHYif79JDy2sgu2iF7C1573FuUo7Yn00YqvKAZHUmu306wMdqN6SuL6rUsrENacBMuVqwx4W92OVgRooghZL.2BOoFqy7s+IVmaSKtA+RmSrIww4G2OzYI5PsT5rVMPNwXCx7YX3iXTd7cV2aC7FZbiEhunlZO2JYRfA7xei2xV2tyzSuehuNsbhL7.u1KE9dD5tAGu3ukstcmybVy143FuUmCeDixCG3IkTxBu9Tl5zbZagKxknfc+KJ9gyhPk.LDiMHymE2yjfSmbAb3sd9Fczw37FZbico3MiVwkbF8X8v.VlF97XS5UiiEvUmkzog3.m4+R63FuUu1gA20XuELg3RlYksjcDx1BWjTOOqAI8gUXAdQmc+610VcN5niwm5b5o2OI0YwY5Fgl5bFPFMF93ytSmbAC7ca36cNyYMam4L5w5b3iXTNGxfFnK56s0wa04S9jC2u7Ky+2PuUePnoeYyvG5ruZ+iWm4a+aXOZ10ZctFz9mMDBEfA31+F8p95dlPkpS27EoZ+ye8MipZC761v26w2YjnS1Aq8cRlhYvM7C9UiTRYXWagumNtEMrbEGPhIvZHBx5fVJCZwF0ia7V8W8QRm9ia7Vc9UqXkdb++8iUnbY.Mjp2HhHM3koOv3FuUOBpyoStrZvmsY4dudqjTRI6LmQOVY+twWshUJ0e+bfPidVKE9TmkpAKVny7YyPp+N5Ec1bPRWXIlfWzXwE9NnwR+xaYqaObwurWGgJwE4xjVfPMr8uPsNXKFu5yPt1.+8iUnSaKbQNyYzi0WiTmOaC761v2K48WlL3yLeyQvpaTM.ifq2quf+bwomd+P66PGP6RJYz0tbG..n6cqKxd8kVZYX+G3fnzxJCG7f+Jxe26B4YeShmf8diR.Wu9r5OWrFFifqWUxtI4MtwaE8nm8PX0zHFdM7.G7WQYUsYndjCeHTVYkg3hKNztjRF..wEWbH0TtYz9TSAwGebdbeV+F2D1bdaV7Vz.OGEb8FIXcb6nTXEbeNZnT+xgOhQg9OfAf6q28RR8YqaaG33m3D3Dm3j..3zm5T3jm7D..HsauyBWWW6xcfDRnkn0UssAHliU3wwhW7Rw79343tMtdwVFfA5Lu+..lqy..SBbZsVYQ6TaHGvoyRpwtS5o2Oz0t0cz7VzBz0tbGHt3Zfv9qmTv6Sg2dO+cuK7Iyat0j5mdPiA3x37LPMPmChs+Avc7bYEg15rQvYOOQ4tfjRJYj8PGFF5PeDIsaOVgGGm3DmTx1.aYKS.MuEbq4hDRnknUIjfWaCbMey2fY7dtr5f0S9lA.WOBsgZQDvJPoXvIrghYIxaXF9n2fdq2D0T3GN.uLbW75rdBSvOriYYVM3ylgWx7jMn8mmm0TLA+TmkKiZjN6SLAsiOY8pFCv0NiWm1QpPwFze5rI3kEGAeI8z6myoL0owj1.+8iUnvnbISafy.JTbFpQl4bGSfqA9+hJ7rOJ3Lh0C83yaXFbZbO80Eld58CI15VKjwB9dCJEaeG6D..4u6cgBO1wv5V2Zk6ROJ3zXaP+pylfeZGmTRIidZtWH4juYT+Fz.jRJ2LhONO6UG.D5Y3QN7gvwO9wEu8h3Nk.NGWVg5rC3GrvDT.clOiQm9TmBG7fGHbWmMA0ymLP3gFCTsNmA7yL0o.vebI5Pkd9ACLCtLh5W1yYlU1nUspUncIkrvnOIGhaCzKY.kOSb1f9VmEfO0n9072H.JECNQMTcNADHjFpNfpfQu8xEge5rITsywfgFmO3bTo2xpruf2egCP5rRgZnw4fvKMFf6yqEn7s8EtqylPv02rcD94yvCLANQvFXivaGbQFGtEXg2HCvoIrzAhCv82LKHL2.tJRCbNOrC1owECtfjyA5ugEo1RZfSOrCRmUJTBM1IHM1cLAkQmsCRmECuuYV19GuOCKPEzYsvvr5OXDbheZU8y7+e2oXT8Nmb9fK3h7k35HbEd8zL3LBMU0q0IYt9iBNskWu4KNTxJoN.yfSmMgpse6oLWaInZaW6nZaYxd12XFdpyoAoGJKRmqcXFU6O1bUuFowrG918LgZtNy6e1tBV+zKXF9uuY91+.H6YBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB0gHT6JfNfz.fIQ+qI+78YWzOmO.JF.NppPHMlQ0ZbZ.vne7dJFb5KO1q5e40bBBVgYDX1mh+Yx9TdLCxO.AgKPAyUywD.x.bNTLCfFp.OiiBtf5rCNmM4ivyf7RCbZbF.nmJzyXOfSayGUq2jycB+gzP09BH6SkCRmCtXFtFvrQ.zIe7dxSzOKV6r61+RPn5jA.xE.NUohC.Xqp5gdFi.vB3bHnVZc9.XFfqQDBBwP1mAGHcN3gI.jCTdstXvET2L.W6XlT7OYDDhHCvEHkZ4PQtuTXC5OmLVA2mM0VeEWbTU8xjB8YlHz.iPaae5OC0XnBVg1UmMoPelUCr.tfqTacMbHfYBUDSP8Mz8mhcD5+EAyP6EvrTEaPe4Lmv+vLBcrOCkCpyLHcNXfVLAENqpNkCBs0VBMFVf1qmg9pLCDZ9kfY.0W6pIkhAmCGhvCH6yfCjNq7XBgFInnXn+x1LgJfMn9Fy01hCD5jkNiPcmOLAZwNHmM5YH6yfCjNGbHCD5kfBGfKasDD0XrA02.NPKECtLKpkIT2AtXmMgJAOS3+nWrOyGZa6S8hN6.Zac1JTeMJPJyf4JBgtFaP8MZYYQKupW0CNv4KECssibhZF5k.LDaeZhkBDiPOpyZQ+.1f5qMrnXisxBgdkzf5arFt3bwBTesg0k7QnwPsP3axApu8T3f8IoyJOVg5qIrrXikhCg9DaP8MTUhhCnsbtn25Mt3hc1ISDpDlf909LW1ISALjeffCgZyQN+oXikBDg9ASP+5TgunUbhaF5SmKhKgZqtMhpwLH6yfAlAoyACrB0WGTphEloRD5BRC5emJ7E0d9yYApuFDLJZ04mDg2wBTeamvA6SKP80.8tNqmy5oVPeIzXXApuAYvr3.p2vsZyG0M8VwNKDMhfF1f5ayDLKpUl5++auy6vihp0+3eSmVBgRhjB3Jrg.HJQp1YADCQpx0DBfk0aBArSQu50KkEK2efRyKhBBdcAoIHPR.KHsMwFgZPjj.A7tXn2jjPOk42eLYlL6ryr6xtyryryd977LODlc1YNy27l2y648zL6hkOsxgRny9SInvhzHYD7kwHTdCQk3vjmKc21X1MJmZgCCdtzQvKfYn71J9C1mlkfxru3gAOW5bY7mBji4Po6wIBJHCCJuAnRcbY3cyNmIY3cvW4vhGqdDja701wAjxCuYVi7m0YKdt74R3OFHGEn6wIB9g3uZvy8vnmJhtHF8BuKp8CcdnFRP9vHTd6Ck9PmGpgtBFUf2K01gNOTCcF960qQxNmeFQB04FKr29nPOTGcEzhqYetyAYUKWcBw9z6XeRzY4WmiD92AxQARuf32Q1P4M5TKG57LozgPBZt9iK6YRIAY.Rke0eX0yjRGBwOf2Qm05yZUW8PmGpiZBBToK.dAFO.FpRWHTQHmok1L.tSY796KQSAYhPn1vLn+8BA5+NUt1gXLChe.FjKcdd.nKxv80WDRWs5GfNPZIN+CKdfd5HLnBd2TaGjtZU8f+7jeRrCSdhfJBDcV90YCpf2I0zg2X3CQPggz8pBeHGXUE7do1Nr3A5IAoCR294crOI5r2QmspBdmTaGpossREAsb2rZ.jtWULLHw2OSfzsJBQuU5B.A.POTKH1m1iTaeRzYgQJ0YSfnwBgAkt.nznkClyjRW.TwHkigiHg5XuHTshAkt.3mCw9zwHU9BH5riQJzYhFKNFT5BfRiVMXNCfjUDGgNI7dMdPFT4NBcJcAvOGh8oiQmDceH5riQmDbOHZr3HWSlGeFzpAyQZ8hiQJM7MJg2KsH5T5BfeNDeANFoxW.QmcLdpNSxJmigDLmRW.jAzAxXkyYHUCVTifL9MbF5T5BfeLFAISFNCovWfQPzYmgmpyFAQicD98ZiVLXNxZNiyQpVehHZsyQmRW.7igXe5bjhLZPzYmimpyjrx4b7qyNmVLXNiJcAvOgHAICnDTunCD6SuAD+.xOF.oGPbE7qWdRzZAyoCjUEauEjViSPMiAkt.3m.wOf7iQkt.PP8iVKXNhiEuGDslfZFh8o2AhNK+XPoK.DT+n0BlyfRW.7ivfRW.HPvAXPoK.9IXPoK.ZbRBjtXkfK.IXNBtC5.Y1CQP8RRfXe5MfnyxOFT5B.AeCzRAyoCDGKdKLnzE.BDb.90ypMuHDcV9wfRW.H3afVKXNBdGzozE.BDb.5T5BfeB5T5Bfe.j.lI3RnkBlyfRW.7g3.d32m3fw0wpRW.7CwfRW.7gnPO36ZPpJD9A3t5LY7xQvkPKELGAWmK6gee+50ymaSrpzE.BDb.dpu.BtFtiNaPpKDZbrpzE.kDsTvbjrE45X0C+9Ds10wpRW.7Co2JcAvGBqdv2k3Gv0wpRW.7CvpRW.TRzRAyQxVjqiUO76SlnItNVU5B.ABN.qdv2k3Gv0wpa7czIwkAsLGWoK.JMZof4H35XQoK.9Q3IiIIBDjarnzE.+DbG+.5j5BgFFqJcAPogDLm+IVU5BfeBGGjwjDA0K98YyvKAwOf7ieeilIAy4+Q4fDLm2B+dGLDT0PrO8NPzY4G+dMlDLm+GVT5BfeDVT5B.ABN.KJcAvOAKJcAvO.RvbJcAffWGKJcAvOBKJcAf.AGfEkt.3mfEkt.nwobPBlSSELme+uLcQrHA2i7jf6gVGhCFkCOcQw1efiCO29jnyNGOwOfUIrbnkwhRW.TCnkBliL.ScNRgCbBtFYqzE.+XH9BbNVjf6AQmcNdhe.qRUgPiCwWKzVAyQBRw4HUF8Ds14PbvnbPrOcNRg8IQmcND+.xOVT5BfZ.sTvbVU5BfO.lkn6iUI59nUobPbhqjXUoK.pbjJ6SqRv8PKimpyVjnxgVlC.hcH.zVAyQZkniQJ6hUKRz8QqBIPNkEhu.GCIC8dGjBclrV.5XLqzE.0BZof4.HCLeGw7jv6UgftUmDDFyJcAvOGKJcAPkiT4KvhDcezpHE5rEI3dnkgzv45PqELG4WrhiYI99YQhueZENNHZiZfbT5BfJkC.oMiZDcVXjJc1hDbOzpjCHcwJKjf47OXoP5m4YDsVXLozE.B.fXeJFRYF5AH5rXPx9o7iTaK6SS.JcAPFnP.zEktPnx3tfz2BlHAveIw2SecNNHaN1pEH1m1ibXeRzY6Qp0YRcZ1CwWKOzZYlCfLdk3yRg7jJ5KCRWrvGSJcAf.KD6S6wjLbOI5r8XRhuelk36mV.SJcAff7Sjf1ACE4.TPda8hAUv6mZ4vpGojDjCFFTd6B0xgUOSJcHFTAuepkCqdjRJL5TAuWpoCqdhXRv2ByP4M3TCGl7LYzknPE5cSscLLOUHIHKXEJusgZ3vfmIiNEqpf2Q0vgAOSFEkrUAuapkChuV+HzAk2fSoOrB5rTJ2Px9AYPfqlwHTd6Ck9vaXeZTgd2TSGxoNaPE79oFNr3YxHAeQLAk2vSIOL3oB3sAVjgxuuxwkg2InYBtO9yYO1aZeZwK79nVO7F5rEUv6oRqw57PMjfOH9yicNu8T1NIIrr6qcPR4u5GCP4sS7GrOMHSuC9BGdCc1e1OKE.FumKgD7UwerK.UpsXm4caTF0JGlkBgifWA+Q6SkXc3hnyxK9i5KEHcuJA3eMYHTxzPGI7u5NKx9RouEQB+qAouRYeR7CHu3uYGSAxPYgPc3u3b4xfNM7JIIA+it1tPPbt3KBw9z6.QmkWL3FkUe4CktdMBpH7GbtXTpDKODiP40B47PMDzLA2GiP4sgja6ScRjV4IXDJuVnk8CXRfxjV7vnzHWDzRnkCnynzISRBFgxqIxwgR6.mfzfQn71R9C1mFgxqIZYc1LTdsPNOLJUBEAsGFgxafJ0NUTqylRiP40Go7vJTGNvIHMXFJuMkTdTHTm1mZsAruZSm0pCgHiRnFQPihVICcpkVG5HLBkWmjhCkdLHQPdX7P4ss7GrOMBkWizp5rVbLgaTJEHBZa70CnqPnNFWLtBFgusVaFpOG3DjNLBk2FySsO8EvHTdsRq5GHRnMVPgUy8zDAUL9p+AfRr1Q4ojD78lN8WFjVH5u.w9z6.QmkWLCkWub2C0V2WSvGDSP4MjckCqv6tEcI0DI7c1rns.emLeRPZfXe5cfnyxKFguWOgLOndy5IAeLRBp2rzcYPGvoVwX2.TusN2J7cZENA4ggA0s8oVoanT65rQY581aPRv2XbzYE91InffJF0lCFyv2l8t1lB..f.PRDEDUqkgtJlf5oEjZs.lI34XBD6SuAl.QmkKFOTOZqVVmInhwHTtV1bYPm1Ycx76nZfHAsCGqPYzZq087INUHHDQB5JcrBky9zHz91mpAcVq5GfQaUKA0YF9G0sQPkQRf13ya7GBYCe6T66oLL3cFKMWFz+N0f23khflAh8o2AhNKOnjA0YEjLwQPEgAPmwLKP5LvMC+iVde6Pjf1gtYHcYGsPP+6Nsx3MhfxAW6SqfXeJWP7CHe3MBXlIXY+cs1qQ.JcAvGFCfNcwbObDV.sAdg0cbYYpbo0HRPmgzj37yNJ3WFM1ZcGVj0RGA+cH1mdGH5r7vv.ccYIAfd6g2q7.sNybPvKBIXNBDHPf.AB.0Gjrg59+LANyEtIivBpOvYBDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABDHPf.ABD7gQI2Nu3uW6Iz1FBC72tP3uchPvwvUaEaOMzZcGLP1+Xu8wPc+qirksv4msBa0bBtFFp6eI5r7gg59WhFKu3J0+YgyOS15rbOLT2+pCBuOpyWWsBeL6YuYvbQh52TeM.f6Thu+4Aa2H6Y1jk82fYe0y.jlMOY.ZsEn9.7r.ePicIljP81y5fmaOe.PqsVq6nPN+q+LL5LSC+H5rzCQi8NjDr02rmpyGG0qwLGL084OiAXqN2TO79QrmqigAfrA.kBbXE.lAfQHb1nzRXDdec9x08LGODuUkZIRBz1SWFdWc1B.Lg5acoVGkTmmGn8Yo0gnwdGzA52Wqv6pyEh50Ysdce.z9FMChuYYAiv6a.6rirg1xIRjf1PxJTdskptxw7fvow1WFif14nRquTf1YkYnsriYv.ncNpzZrVVmM.hF6Mv.TO5LEnq6yn785pXXDpm5+XrmMHausdYRBpmJ9D6vJ78MrMBueqPtcNr.eeiZCPc4PVKZGCP6yvBTd8TKqy9BZ73gueVjzA0sNeYPm..ecc1.T2wYXEdYeFR8XlyD.llqdwImbJH9V2ZDSLwh6nUsxgW6UuxUPokdD..jmkcfidzR8jxICGGzBtEo3l4kPGni9u2JawvkIOP6j1Wa7ELO.7ZJcgvE43fViyVoKHtAlvsgOCEFeUc1DbC+xIjP6QiaRSb30JC9kONnKul8zajBv3AcY2kFiVJrNWNnKqyySuQdYX5MJeIeyFgWHFCoJXtHAsCNGFfQxImB5QO6Edzd+nnW8n6HhHB2idnEUbInxJuB18d1Kqw9mujOyctUSGzFHpcL.Zc1SGPmJA9JZbjf9O75hBWNbGxA0mwV0NDcV9wk8K2m91Oz0t0UI2u7YOyYvgObIXsqY02t2l7.c2up10X.Zc1L.FpitH4VmO8oOk6T+Wdn9tpTsiNPaO6K5yXofNXeYydVJBlyoNkyHyrvHROcz+90G69rJpnRTbIGFUTYk3vG9HB986Q26F..BO7lfN0wN3zBTA6ZOX26YuH+7rb63DQs6f1H.9ha2ujd8IfdanOHo6qqH7vCGcHw1y9YcrCIhHhHbT1ININ0oNM64496hiczRQkUVoT0ZP0tFyzUT21AKmQlYw1BaF6U.fXiMFz53ii0NmK6dO6E.f0Q7IJqLr4M+cdR4GfdlWYDp6LglDncJem2teQwzYFeCBoykb3ifJqrRoVmONnC1PspyLSvAQ8KO4oXBCZPCD8pmc2tOiwmfB6WtbTe2ooVwo0+M9ILIjd5oKnNqRp+yWPmcaeyImbJnicpSnc5S.wFaLHtXik8yXpCjIvXF396iB2+9..b2DEwkC.ZeFV8zajP3oAy4PC4LxLKLlwLFaLhK6DmDaeG4g8rqcheJucfKbgKf.opBT0VCp5lWWvGRvgDJBL3PA.PsADB.nvC+vOBhrYMCc5dRBcnicDcHw1iVGeb18cYddqZEK2Ubfe.PaTq1B1vHtMBjK4jSAibzOM5Q26pK8G+2NTvt1CN4oNEJ8Hkh8t283Ns3Vspw2VNKzqOAj4XFK5Z25pfMRwSnnhKAm7TmFG9vGAEt+84NARqlcNqCzkKWVmSejiFOZueThN65nCNPiG+DlDl3DmfM9KY7S9i4mGJ3W+IbwyeADTPA.f.PfTUY+MIffPs0U8AEB.23l2BwEWrnG85AYG1L8n6cisxR9T1INIxI2MgOZty1YZtZUiAbg5+l1zlpf5bg6eeXq+v2gyctygPCNHHpNCfZCHX.TuNC.Lrg+TH7vCGsSeBHwDauCq+Kmb2D1Tt43r5+JGzYNxrydoU.tsCjK0zRGCbPCF8sO8VPcwcgI36RN7QvQKsT2Iqyxl8rmFL23Avb4eR85S.Sdplvy8Lih8bqO6bwFyYCXqa96.p5Zn5ptkG9nqm.CJHDbnMDsQWaQO508iG7Q5Kd792O6bhr9ryEe1BWnyLpWJTWC1YcvEq7SnfmA.tvYOC9o71A94ezB..J7.haG0t11NDd3givino3d6x8gl07lid+XIK50WQEUhB18dP94kOV8pVgqVYXdP8M4Hr.WXbHlbxofrF23vvG1Pr6yxdMqB+1A1OprhxQoGsTb0qdUAuGQGczH1Xnac3C8HF..vvRajN74VTwkfMtwuA6X6ayUypT4f1InUW4h8hXARfNebq+ANQY+IN0oOEN24Nmf2Cgz4G1PeQKi9ND84VTwkfsui7bkJ+XPMFrgEHfFqWeB3SV3hrIn30mctXUqX43myOODRP.UeyqfZqoFO5gy3Ot1.BA0TSsnS2cmvC7vFDLf7JpnRrrkuRmETmZTiAnC7443eRwz4u8a9F7s4lsjqyTAFLpkJ.zgNzAVcVntwc8YmKdy2308E0YqvEyh+jmhILhQjJ57c2IaNeQGrPbjhK1kpCLotPuJawTGXyaYKwi129K50WQEUhst8cf8uu86p0AdbP6aVRSngmDLWjf9W51HxolV5vjoowlQnB10dvje6+INbwGDUcUuWxXBsQMEC+oREoMpmAOv82K1yWQEUhOcgeFdq270czWWMM9tLBmjUtTSKcLoIMIaBhKustYrpkuTbve+f3OjfIKxcDSrHpnhBI0kjvc246ECd3OEZQT1WoHiSKWHkzpofliD.+kit.85S.y7CmkMAWTzAKDe8pWI9weLO76+1A73BQShHBnS2cg1011gN1o6F8OkAhNcO1u78Uvt1C1zl9F7duqImcKUaYA0kzY9MD7Bm8L3S9OywqqyEUbIXUq5q70zYA03TSKcrjE+YrUvukssCLqYNCTzue.T6MqziCrvYDXPAgfZP3nEsHJ7DCdXXzO8nsoWCpnhJwzLMcLu4NawtExREfd.QB5fLroQ1olV5X1ydVrYCpfcsGL0I+u7p5bvg0DTUM.OelYggLzgZS8BtP8epsFAZ..6vYWTFYlEl5TlLZSaZM64xdMqBe+2tQryB1IN6oOkGWPtiXhE5zoCInOA7H8oe3g5ceQKZQKs4ZXBryERZzAfDu1r5tAyI3XdgqCCFil4OuYApaVgraDKFAFTP3Adj9gI8V+KaBpyEZkRefxOKWMAmLKzl+BVHd4Wbrr++O5CderoMkqjTomyns5S.O3C7f3uO1WBcry2qMe1V11Nvr+vOzYFzOIT9YFnA3jIURFYlElyrmEaEgEcvBw6O8ofsu0sH6Et6HlXwce22MRd.CDOaliylOqrSbRL8o+NNKvY0RPyFfKnyb6VJ0jNOm4LWGEnA.83AUoWuzDzu7jmhI7Fu9DY8KOMSSGqc0qPw7KyDXW+d7Tva7OdSaBpaoe4Jgwmczh8UUCZLfHiGQ90+8gyZN3yV3Gifq4ZJV8egz3HQ+d7Tv3dgWxlf5Ve14h+1SJ570PszyICCz5rC6UJyKaE1z3uO5Cde7EewmKIAv4L5781Ez6d2Wj9y97PeBs2lOyEhw3i.cuaJI3NAyYDhjoHFQshJpDYNlrvZWypQqZZXdTATpHvfBBIO3gi48e9j5qTt3RvPGxPDSrUxVB5zYGE+T4m2V2Ldu2Ypdkf3Dh1pOALpQ8r3kl3avdNWrUf5fx0ZaAGl.bYFybV3M+GSB.zYH50dow3UBtPHtiXhE8+w5O9mS+eil27VvddWvogRGzrS04IOES3ceG51sbgydFL42bhHmrWu2nrYGhoyaYa6.u33FqZUmMBm3WtnhKASbBSD6Y2EfPpQ3g.f2lfaTyvK9pS.+y25evdNmDngRaKaDzKmG1EfAiNW1INIlzjdcr8s9CpFcNjFGIdgWYh3kdwWvUq+So6cJmtzPoWeBHmbyksw.YulUASS6e4UBhSH5RRcEuwaOMzuGu9gkjKjwYIKoQ2tAyYBNHSQLYIphJpDMsoQfF23FivCtZOp.J0zl6RO97kuF19TufcsGb+8pGhc4JgAsSmcT7Mh+f2a5Xty5+y6T5bB2QLwhY7gyCCXP02cjNw4rRk4HyPfw6hMW.mV7sseXyXbY9L3JUTgWnn4XZRDQfTSaz3eOq5iORE2vDyvI5L2rKuu8tG72e5zTLGxboIQDA9aOU5XFy4+vdNl.hDIiyJUiSLCQzXtcacQEWBt6N0QDWLsB0bMG1a2dUBNjPQOdn9ByK6KYCzvA9LTxF.JZiRzqOA7ZSXR1T+WzQ0RD3spzKWDEm.CJHziGpu3eOyOjstCmnyJU2sZFNwmA+5.GmwQoXM9iOcsG2Od6o8d3gd3Gl8be7mrH7Juz3D5xONjncLo.uMtVyPj.4RMszwgJpXVGxkWWEdsHhF6gEOom+7+cTj1PF.98CUD..5UO6Nl+BVnXWt2dEI2kV6sN5QKEkT2zldtyctpl.4..N6oOEd9mNMLhgMPTd4z1ACeXCAqaC4H1W44f2Ok9lgSbV..bzRoCL5W2YAv3nSUUDHG.vUpnB7EK4SwizyjPg6e+..nScrCHmbyE50mfPek6DRX57uMvLbAc9rm4L.fVmG5.5qpHPN.Zcdo+2OiVm2G8xSPm5XGvZVyWgjSNEg9JMEdec1LDQiyHyrvd26dsoKn..BnJ0Q1hXn5ptE9UKeO58C+fnnhKA.z9Ll7TLIzkqDZL.sNKXfbYjYVX66XGr0+chSdR..zffn7VkMWhZqoFTP9aAidDoZiNKR8eMEJWhLbpOCtL7A1eUSfb..6a26DoMzjw69tuK64d4WbrhUG3cBIJYFtZvblfHB7Ll4rvZ9pUwFg7G+IKBcrCIh1zpVfacsxkhxnjyecwyYS.cu7KNVjQlYIzk5Mcb3xKhpyXlyBOVeo6d0VDdCk2RkaR9V1FFZJ8C+YYm..zNMLurUH1kaxaUtfKFfQFYlExJqLA.P6aWakzYesTwQORIH0glL9tukNKQNIfNucCSLCWTmG4HGA..Zu91od04gM.rhu7KA.PDQDNlybmiZPmMCQzXyKaEXIKdQrieqY9AyF8rGcGsoUsPUpw..m4OKEuXVYfJpfNaVuwqOQwBZ1aaKOO3f5+VxhWD6ZI4G+IKBc4duWzlXhF255WQnuhhyY9yRwnGQpnrSPGz4K+hiEolV5BcoOG7t6y1lgKtyF8ZSXRrwbzt11VYrH4dTaM0fENuYfgOrgwZO6fFnH3IucIHW3ZLBQZQx51PNXLY77.ft6GdoW9UvpW9WfPp4ZnlpUmNLX3FW+pXe6cOXX+szPXgEFtiVECVxRVrPW58AfEBfaHiEGWJPtjSNErhUtJLhzdJDVXggKb1yfoN42Dm6rmUFKZtOW77mCq+q+Jzm9mBhJpVhj5x8fxq3JXm67W4eo5.c2sJ2cchY3BAXXdYq.ScxuMZZDQ..fY8umN1ytKPlKZtG25l2DaL6uFsosc.28c2IDUTsDMsYs.YuA6ZoZC.vYAvN8BEq4Afw5rKZ9KXg38dWSHpnnmQXy58Mop04s9CeKh+N0iN24Ninhpkni2cmwWtrkx+Ra..tIj+IOkIHvXJRu9Dv29caFC7InG2NErq8fW5EeI7M4rNeB+x+0EOGJ4n+Adxmb3HrvBCMpIgi07U1sNd4szX.55+DrqO3W+WlYNFr90tZz3fqFUeKgWyTUKbkxuD9seuXj9HoyZaatSchU+Wyf2YLJZF.ivYWTpokN9pu5qvfFHcP9EcvBwm7w+G7WW5Rxbw61GJJJbpx9eX+GrXLfAL.DVXgg91GCnnhOLJ5P+N2KMRP2cqdzRBiyBlKI.H3fCYcaHG1kog0mctXvCLEbgS9Gn5anNaMhPb9ydZbyppE8ou8CwGWrnlZC.4muE9WVC.vgg7t16rZ3jVjjQlYgkrjEi1016B..uy+5efW8kGKNwe9mxXwxy45W6p3.+1uggLLZmyO3C7.3q+5uFWx9+3K..78xXQwHbxLCVu9DvO9S+L6jJI60rJj9SMTXY6aUFKVdNTTTXaa8GPxCbHrAMehSdZr+8sW9WZGg7uWLZDhT4GCL57.ehA..5IuynS6IwV77ckAYEJJJrssrY7f89wP7wEKZWauKwZbxcA4UmMBQVeOyI2bw8kD8LK+i+jEgm+YeZT4ENoOieYJJJTl0+GZdqZCRpK2C5XGRD6bm6BG6XGk+kJ2ZL.8v+XC7OIi86i9HOD.nq+6IRIYb0+5rn5qWAnnTWcupXb1ScBDRiZJ5UO6AhOtXEymwcA4OYFiG.uoytnIOESXF+euGZSqoW9QdiWYr30G+qnJCjiAJJJblybFruC76H0TeJ..b2cty3SVvB3eoL5raiil.DBtNxAXafby7ClMlyrlghN8q8DBNjPwOuuhPaZc7NZxPH4qILbvDbRPF7moeiL0gpXyZU2g.CJHL7QkAl+7oGH4hLXPKGxWWmjD.1uit.9qCWpoATqqRqZSBHue5WX2h1ZSqiWnK69f70vDmtRsyec35Mdkwhk+k1kcKUMtnNKWy5RA0X9CH7oL0oiOeQeBBnZOewoUIfqF6fAoubNyV0AQVr1OTQEaiN+oK3+fvvM7I04faTj3W10dQqiONGU+2yC4amgv.bg0QNtSRp715lwa+Vutjr9o5snpfZLdi27sYWYDlxTmtPqekdjuYGkYtUCfdw+jbCjaJSc53Sm+7P.2x2o0H7o1ZqAW4pWCCHkm.wGWrBkBT.fVA50DFot0IIAfU4nKfafb4s0MigNnGW0mMN9PQQgRO1wPmSpancs8tPGRLQgxNWC.cPykHwOdltvVz.E4FH2EN6YvPeh9geLOKRbwP94FW6J3ZUE.5aeLflFQDhko4aB4KCneObvJ0NiNGcTsjUm25V1rLUTjOtw0tBN2ecELfjebmoyxQfFBpw+3O8y1EHG0MK2m0u70tR4HhVFK5YO5N5XGRDqXEqTnr4KWZLp69lH+SttMjCaF4Xz4.qpReVcF0VEpFgf90u9h3iKVwxBZ.fNd.olHA8v9nAN5hV2FxAO2yPu1CZdQK.uwq+Z3rm9zN5qn5nAgFLVeN4hTdhAg3iKVjPBIf4N24v+x7HeyhMAHFFDXMNaFybV1jQNFGF9570qZYrCFzANnAK1kIGKVklczGxMPNyKZAHy+t5XowvcnlaTIl0LmA.nG.4u1DljPWlAY3QaBtP.FrAxMv96Sk0StTcU2Be1B+XTvt1C..FzfFnPWlbsnqZBNXLep0z4uZEeIqNyLAN3gbnylf.Z751PNBFHmuL0VSM3yW3GyN3wSejBtPBKW1xiGBLrWl+BVnMIxPqnyK8KVBa8eibzOsPW1Pg7zqIlgSVPf4l7nO5Cde7OeyI4SVGXU275nEMu4XwKldbI153iSnICgAO4YHTvbQBAFKBYjYVroHb8YmK9vY9u84MjYn5ptEL+E+W..z29H5PWyfD+XMAGT423mvjXCjyW1HlgZqoFbvCTHVe14B.fgNjAIzkI0NmM.GrvSpWeB1EfguTp6EhFDbfX0qltQz8pmcWnYC3cBoeFpkDbvPEPu9DvrqaGzPqnygDDXcL2oN1AglMfMER6PyPGDPim7TLoIafM.vkN+owV2NcOvIRCSjZMFft9OS7OYFYlEa27szubkZJcNfptJ19NxC.d05+L.Grn3CXqs8G8AuOlw+9cczkq5oQMHD74K4ybTis6B7fflEJXtwCdYxPu9Dvzl1TA.8r144e1mFgIqiGRuOe+lnyVeqiONwll1FjvGmN3fk7jTSKcLcS020pe7G+QR3iV4Hjf.9rEROFOEQmuSHss.zji9vbxMWMU.F..25ZkiOd9+Gm0RaCR7i0gCD8kuhUhVGebZNcdol+BmkQeCR3izL+Sjbxof230mH.n2cJzRMvF.3VW+JHucPGLWu5Y2EaofwfD+Xsa2cPu9Dvbl8r..8rC9Ueowg.p12XBk3JTcU2B4rt0..Z+xhrbvH0AMa1QeXFYlkMIyvWOPN.5dmB.Nqw1Fb26O+f4hDBDjwjmpI10QmINgIhl1vf7IGrmNhxr9GrNl6V25tPWhTFngI3fzKaxzzPDQDNJ5fE5S20p74VWqbr4M+crKXkhTAnT4zXXvAyPXyKaErcM0HScnZh.LXn4MqoNqk1RoiYCvA577WvBY2SHyz3nzb57JWIsiYYVmM.Az3o+NuC6jv3Ue4WRy0.a.f0t5Ux9yCZvCQnKQpy9ocKcQy7CmE650mwm6YQDgAMW8e6ZWEvV+We5a+D5RLHgONivAC8E85S.y5CmI.nSlgVHPN.ZalVz7li4M2YyN7ADPqca6Y9AyMdvKHiTSKc1UP7OcgeFNzusOT0MU2qgNtCUcyqicuG5okcW6VWE6xjBGG5fCVqyl+BVHaPFi440NAxwPKZdywF232..Qq.zfD8nDMaQYjYVr1ziy3n7YG6VhQnAGD9lMsQ.HZKskxJ.MI1GjZZoy10TuwqLVTvu9KR3iU4IzfCF6X6aC.hloYcRzixD+SL9ILI1fjm9zeGbsKeNMW.F..AGb.rcKUR2mf9kkUa4LxLK1t56Cm0bvUJ+uTsK5xdBAgZYq+Kg1KXFP8J9L..9jEtHDYjQRuWX+JufD9XUdZXXg..vN7ADHVCCt68lavbBlUtIMI5wIWQEWB92u2zQ0WSo1Szke9kezB..5PhsWrKQmD7XbX2qxsxOsTVLXnggEB16docNKRfF5jfGiA3fV9wLjAxdMqxma4GwUHPppwZWypYaocO5ocSJcYMiQLXhyPEvWa4GwUHPppvl2724nL56Rql8NACBcel3Dm..n6d0uM2rUs61.dLUeS1fLDwurS2wbbQzAAZj8XFyX..c2q9oK3+fpUQ6osRIUeyqfROBc8M8n6cSnKwgSTgaCLBG3aNiLyhcc9LSiiR0r09IUDHnm0y+X94C.v9txAIYLyMLv6WXYjYVrs9aNyYtHxFGp69b7In35VRRZc7wI13yPmG9HhDNXeXiIvYsZke.zFyqcMqlMMycrSch+knSBdLhFv77WvBYWiyLMs+kD7nTevr5yyr+8dec893eIRoiYAYxSwDaFle6250knGm5Bl.nbgL56IXj+Il7TLwZCOqYNCM032hOUcyqiiUWiZYpKR.jhg+hc9L3V+2hW7hQCCrJI3wnNo1ZpAGtjhA.XssD.CRvixni9voNkIC.5UuAsVl7A.psJZeyaZi4xdNdakntciS3FLmcFyiHc5tMnfcsGj85+Zsaq+pie629M1et2FrKhYo.6BXlAtNNdu2YpxwyVc.EcWAUbIGF..sS3fl8DzAQlkT50m.d1mlt6UeiWYrZtV8wPs0TCBN3Pv91K8lCuHYzPmG9XhDhLbAzqOA1Al+67u9GZxLLyPiabiw92G85QsH5rAO31KnFyrmAu9ryEE86GPS18pb43+Q8q6YhzHaoHSyF4eBtYk6ayMaMY2qxkbyo9M6BQ1qx8TzAGjs5IOESnMsgd2cXdya1xwyWwgwF5nGsT1L5mPBh1Sf2VvDLmNvKhvjSNE1T.t3EuXz3PczlEg1f.AEq.KBF7vGgQw9.FGGYulUo4FCWbgIqQLYyPfT56ocMknKuIu1DlD8rW8bmEYmic6ROZJZZDgiRKkNybLYHiG57vGgQw9fLGyXY04u7KM6gOF0MMHrvvoqqQANHiFtK1YKmQlYw9bVzmr.T6MqTpelpNx+G+Q1e1a0H6jSNEaxJWP0p8Fm37InfDaYmkEOMCnh1iI..iIy+N.z1MzF.Hhvo2kgX54jVESL7uD2pwIL+1yNmFCZHzI2nrSbRr5UsBMeV4...UM3TmhdkkVjAaqmPjPj.URMszYcbrfOVt2tAUVXxhvYOyY..P3g2Do9QXTrOfIqby7cmplahkHD4Yo9cIGQVtA7DDMn4QMJ5L56unye9R9L1eVhyngcZ7SLP50lpsrscfCdfB07YkCvkBxvSwNclYI8onhKwuHqb.zSbJlIahHYKxSy.pn9L3lUNsdCsCID5IAwgqKXNAxnuaEzrnAywrnttxUtZzr51uJI3QHpgb5ibj.P6mUNtvjMCQxZj6hNHxXNXxSwDhHhvwEOu1OqbLbTNcuY70s4TKQHZCSF+DlDali1xV2hT9LUkvL6zjAhDBLbAdr9RmYp7yKeDLz9AX.XaPFxD14alYl1uwM9MHDGsoWpQowMQxajsN3fI9.yBn6G8AuueQC.A.t5Uj1DjwDLmMNlSN4TXcHuisuMfptpj9P8SwfPmTu9DXcPu10rRgtDBtNFD6Cdzd+n..X4ewR7KbVvDjgSF1.tKFD6CXxbzG8AuultqR3CSvFwDSrR0sz.+SL95Fl...+m4MaM4RDkyPF5wjjf.KGWL0+s0e36wstl1YgXVAQzjYvsKsWyZjis.V0ILCCFGLwdtsHPHfSClExthJtD7q+xO4WjhYu.FD5joOxQylwns6GjICF310TRHFD5j50m.63+bSaJWgtDMKLCa.oZP1VGFD5jb04crisIkOOeFtiV0Jo5VYf+IXBjYKaaGHhlzHo543uiA9mfYIlonhKAEWzu6sKOZULH1GvDuQdacyZ5IKkbSfPf9AmYJ1u68rOhSCoAcPjTLyrL3Uyom...H.jDQAQkQr7uXIdwhi5BQ19zbGDbLcvrIcewyeV+ltwlB1Ngkj3tMQPclYE5+hm+7ZxkU.uL1ow8n6z9k22d2GBnla50KPZTLX2ILP2QUaeG4gfPsd6xiVEQGucCdvzYyeS4n8VyO8lHXvbLCHuB2+9PfTU6sKSpYr3leOch8ALcw5t10NcyasuOQDQDb+udReZH33kyeLf4abSYMa5Bu2UUWli135Wib9rUUTSMTx0s1NMlY7ktmcWfeYWrB.10bNIDc7OQG6Ph.ft9upuoevD+y0wha98hDhjLC85Sf0t9WHM.jAqtyWJPvyXVu9DXGu.EWTQrKkDD7HLHzISN4TXGCL6ZWE3MKOpJ3NqKAPgt4sQzV9wr7m3OGvr.3t5rNw9.lLGsye8mcyasuG2pZm1XW2YKywtYyF2YIKyVHl+BW+lUwFfUkUJ4KEK1sjbw3Stfe8m7KlsvBgDO37E02LyRMyEN6YHcwZ8X0c9R1kYNtqiO+zOkuekwbsADL6RkgHs.TR2KyX18CNbwGxuXP4C.DbHz6hHbW7OOpz7GwBNct413jCcnCIEOGeBppJaWs5Evd1csk0I1GvzB6C96GzMu0ZRbmflsqxuvqasopnhKQNmAspVhvwqnBVbyaqN9mfYVeWQEUhycty4l2VeSt9MqBwFK8ZdFyfyWhPzkZClr4+S4sCwtDMGkWg7r1PFHDYGInfcsGzfvBSVdnpYXpPRjV.5tYyvfPmjY2O3Wy2hada88HvfoClyAK9mVcyasfs9i6yweZ1UVQc1uhjQCOoqrEzwL2LG4O0BaFGyLMBrv8uOY44v3qnxJuB.z9Kf6bIpnuC1elWV78Tzw+DLyF4hK4vHzf8+VSRbxBesE271JZl4Rrtgz0O+it6s12ipq11FZyaEGvsGT21sZLxcpe6EVrFUUz91W+Zd1IJqLgtDIMybLFxG52+MmbkZGpM.mlUAqt4sVvfLXbN+M9Iqsb.0m8S.Qyng61nD.QbLyj4ne4GyyCt09VDXPAw5XVj0KQ2ULLH1GTxgOBBjR6tGgJDOzC+Hr+r.YwWRM3rc1H6eEzbeerGm8mEX0FPVVeVX1MDNkeRCsCIrFx9yLwZwrhCTGtcLFNLZM+sVlbmsUO6Ou4M+cBcIdRkf1AigrLLNPTsvLbwYVpLDXw.UR0XIbohvmAlrexcWePhFWhhBSli9qKcIo9VqZI3PocLycHCvqQfVkpmU39g9J..BsgMwQ9J.7rFXqSrO3jm5T9UAMGbHgh3hiNqbUHb2.J49L.pumC7a5R6fsu2Nqv1+l1h6dqEMXtSdJ+iHkYH3PBEcs68..h5zP1R4vw9iiIW2ZUGLyxRl8itJruxIYwoQ96XqxwsUURsADL.rcWefWFMrJWO6x9SY6Vq5fQm41U97ZDnUo5YIvV9ieA0FPHHg1SGrrH0I4I9KzI1GvKaIZdBLjFx1frhK4vBcIxheYmLVH0bTKU8Y6kYh4wrsdUGVc26snAywzRP+EBHzFw1smkbXAG7mRtwrTsxO6KwEqKyMLUNwyPtbHSAZTaM9OKwNW+lzYTfIM9xc1O4hHCOAMI2pZ5IGFSliJp3R3eIV7pEHMH2p5pY8UT5QDbrXZwaVdzpTCBjM3hcum8JzkHa9L.fey5+YkWo9cSKlIaBy9Tdc315rnAyEgeVvbUWME5UOnCtRjAwrrZL6OPnMp94ZC20xINH4ZLiCpvZXik5aspEl.lcPiSr3UKPZTtzeQOLhXxbj.5rjYOKRErZdRJotxNdD26dErGS7DM1pXe.SEs9KT4UtJaxEH0+IODXPAw1STbWkEN7gYaDX4PNBlCn9V36Ov.F3fYS46l1nfa4SV7lkGsH0Tm4VpokNqVyarbYQpelhjkUMKbCXlIiFGsTaxng+yLTPFIzF1D1I+.SCF3k4nC.IdBSA3e0iIgDVCQOu+GD.ziiq0Z+91ompwVE6ChK1XAUfA6A2ZeGBNjPwemyrQWjI+.IXNOjfZP8+sKytkCO6ZKdx82tf4XhJmYp16OPnMpon28ou.ftKoDXFScbHCc+m.cKillKW2.qk6deHOsNao9Y5uMfwqptkERtaV3611EjZKd3ivpi9vHaVy7vauuA0T2rxN4jSgUm4k4HKdvsWzJN6Phs2uIHiZCpA3Q68iB.fstcAWRRrH0OStc4Ekrs4dnxHjFiG4Qo04srMA0YI2uLeZKmIQjVkpqtdCJ1g.ytkLeFHPHx5ZRm5XGjsE2N0FUUCPe6C8tmiEKBl3BO0XVPmyzqYT.st057vau5mPBqg3pWkd7Bvcu+kCdZq+r3nO795du7fasuCm+R+E.pOf4xNwI4On78TaYqN5Cuut0SO716a.SCS5QOosqJ6Dmjeli7Dc1trMwrnOGd3MwuIHil07Vf92O5IWxOle9BcIV7vGgc57oqaIxnW8r6x4V0lphJtxUY2VIyOOYQms5rKnQMRauGvGXPAw5aFn93M12dsoNPOx2bffmAM2Tr9jC+uYyZVkVj.CJHjZ5ihs00hrU4XwCeLNrq.huMswCu8pepF0uyOv3fl2XyPVZ8Gy1RSKZYTxwsWUQnMporc8GylW812gMMN43vy6tDqBcRlLZDRnZ+EZ7PaTSYaXBSli3Ml1JGdlOCq7OwIOI8BKZm5XGXm3EZYBsgMACa3oB.5thZdyc17ujxgLzHatC6iN1wN5g2d0OLcwJyvdY0qZEBcYxVC.Y5cpF2Xs8XZNnFDNquYt8ZBm3M73d+KPHfSGFANwD6.BLjFx+i0TDTCBG8tOzAWrkssCgVe4jBmFVD5jLU.ztDRzCu8pa31pjzG4nAffNnkEMlYaoIot1Ug9XMEW8lzyX2jSNE1AM92roMx8Rjh.lsJzIYxnwiZvfD7HT2bipqE.z5rHYNRxq7iaV+F3fFpGd6U+TUsAgQNxQ..Q6hUopweGm6+4nGsT1Uj+6sqZ+UafZBrg3IFHcC+1x11gPCwnbfmO1OEsAjL8NUBZ7tYs7JuF6OOvAMX.PGmEm3M7X64.g.BMyfF+9558wNFbzpjXGuGL7gQOXD+1u4aD5RLKAOFqBcRlrF0UMdW.FXX02pDQbPKEALCHvPFfYoxHxHiDwog6N6fCIT1Yw5fFBck8Bz0elknGmciEAtYzPqqym67W...8ou8C.z5rD2vD.AzXlkXlt18dno6wjfCIT7DCYXrMHY0qZUBcYRUvbhV+WR2WWQnMT6N1wCLnfv8zkjXaPxWsZ6lfI.RiNeYvKnYFXRnw8zk6SBdLpSBsgMwl0S0mbnzAyswMZS7FyySeNBFL29229A.vi029fJt5U4+wZFBowQhW3keE.HnCYFLKAOJqP.iYlAL88bu2CZZyZgD7XTeDXPAgybd5fLRMszEyAsYI5wYms7l272wthl2gNc2RziQ8QMAVeFzG5PFD..xI2Mw8RjhtXkA6tObynQOefGThdLpO3pyiZToC.A6JaonBP6zXlJ95ae5MPHZ2tkpl.aHFyXFC.nCfUfYwpTow.BjQelwMVe6SuYmnKZQBLrvwXewWB.z0+IxrXU1BZFn9wB58+POpD8XTeb0aU+XubxSwDaWZujEuHlSe.HASvx.g.AZvzu4QDQ3H8QNZMaqS3lUtO6yVhPWRdP5p.zB+Sr10rZ1.M54C7PRziQcA2rxk9HGI.DzAsG2pj5vhPmjYFC8v8tuRziQcQHg0Pb551Nbl7TLwNdL9HaabhTow.NYXCz69lrD9nTODbHgJnNupUrbtWlYI5wYg+IxOO5S0oN1AjXGDbuf0mGlrxwrlms3EuXgtLyR3izB+SvL6uac7wg6Wi1vj.CJHzoN2EmU+W1P5VdcrHzIYVFvZeG6jlbFsFZCaBaOl..LnAQ2k1qO6b41k1RhuYlklDah99nGsT1on7SLvAhppU6sGsFbirMqbu26ZRnKyrD9HsHzIYBzXfC4uIgOJ0AbyJWxImBqiiMsIaRubdP5V1WDrUjLszdfCYXZxtm5ZUW+JLTVYkI.ryYQ4vKXKyjQ+TdhmPSpy2Jf5mbGbcJyab1JUAMams7ZWypYy94vdpz0rZL2rxIP1h.jVa4BAujYr4M+cria7Q8LOmlLYFADVD30ey2B.Nr9OAOoahf9lO5QKkUq6RWRRBebpC3lUtLxLK1FovYHcIY9lYpEvtaFSEfCeXCAMNhlpobbDbHgh9OfA4rVkbbHsNMbXfFo7DOglyoAUv0uvpl03FG.DzwgI9eOOfKC5ArqMvsk12cRZqA0bnMpors7ia1h9rEtPtWlYHsKfsBpybyn+.F7vkvGmxSnMrIriUtwOgIIjSY.fkBoUmsSiY557gNjAYSW9pEHzF1Dj03dYmkUtkBoeM+zB+SrpU8U.fdnFEYKakD+3TVBIrFhALvgvNV4lyblqPWlT1HaT28RvkAMlknpLF2qHgONkmfaTyrIqbhzHEIqGSXBlytVm7duqI1VA9ZSXRZJGGWu1Pvq+FuN.nEVuPqR.ncxuT9mjaEfoNpmUhejJGg1vlv1kTYjYVhE3bdP5W3OsKn4Mu4uicvim43dUDXPZiLMGXPAgScd5YIrd8IXSV4jorEwE6zYtYzO4ANLMkNewJuA6+ehSbB.fd1+wKyQlj3GsY9mfoqyac7wgLF6KpYZjcfAEDhnEsBuwqOQ.Hn1xfIY3wa2ee7duqITQEUhHhHbLlW3UzL5L.PSiJVLsoMU.PW+mHiUbSxvi1rPmjYXJzsdzSnu8ZigOPvgDJNy4uH6+exSwjPMRobHCAyAgtoLsB7Ye5QgFGQSQHg46GPWvMpYvz69uYGH9yd1BZHK0YkiA6tmG8nkh0mM83FH0QLJMQ14DqxOABb1l+iDQ1f9ORrAlt18o9aCCQGmNY3w58o5fZDalOesILIz53iCUTQk7yJ2zgLr6k.ZaY6zYlLU8T+sggD5j1naSnBtIrqqbyeAKjM6my9C+PtWlbjwnrg.KcFL9KxJqLQKi4Nk3Gox.UvMAy4ilOhHhvQEUTIdwwMVgtL4PiADHYF..Ka4qD.z0+0bMhNGbihDu9a9uXsgm1TmpPWlbzHa.QpSkaise9w7Bxvi06y0qMDVeybanMuFoLOHgYxmavblAOmyezbmMJ6DmDQDQ33SV3hrYr43KRHg0Pj7.GBd4Wj1QwG+IKRnYJE.fQYpHXAB3zfYlc1qd1cz2ALXe9LZTcPMhsxuYLyYwF3LutMQtbXbYHfSCtYZNiw8p97MLI3F0L1t8K0zRm0ldYKekbyJmj1xOAvt687pymA.vqNo2zmuwIgz3HYyvbxImBd1mdT.f12AOcd7xTQvL+S7YKbgnhJpjN6bi6k0DZbFi8EY61uObVyQn06rxg7z3OFr6d+Qyc1rYmahuw+zl88XeQBsgMAOU5OMdtmQPaXtXTlJBB16T.02X6+9XFqO+RaTvMJRa5d0IOUSrMzlSC.ONjXeybiZ3F.nA.v.yItzktDnP.X.I+3ncs8tvoO64QIG9Hn1ptA+6ipmfCITzojte74e9RPXgEFJp3RvfFXJBco4.fYHiEkKCfgw8DEcneGo7DCBwGWrn8I1Ar7UrZP4Cpw.zFxm9rmG.zAY7tuiIDVXggk9kqDSaJ+KtWZefLrQjWGk.ApbswMIBz29X.8rGcGey2sYbwycZP4Ct2HEZCaBJ6LzZrd8IfEsnEgnhpkBYS+Og7DvLCEBfW.z9MXgQm6XGRDe62uEbgydReRcNjvZHJ6L02UI4twMh3iONT1INIdwwMVbo5cXKm5rcZ7wN1QQbs9NQO6Q2QO6Q2wO+qEfS9m+gOqF2qGtuXlybFHrvBCaYa6.iIymWnKclPd2iPKD.OO.hj4DW5RWBAGRXnu8w.RpK2CN3gNL9iibHeWc9QdLL24NGDVXggB10dve6IEbwm9i.ffY3PhnPHfu47y2BxHywflFQDn05ZGxYcekLVDjOBsQMEkc5yw9+yHyrvTm7aC.f48QeLVzB+DlO54gzsRY..aClCP.GG6bm+JafFO3C7.Hu7+Y7WW77nlpukTVNjUBLnfPyZ0chk7e+BDecQHmYliAG6XGk+kVNnClUNijxNmF..kWQkH0TeJDUTsDUUS.Xu6ae9bAMGRiiDm3zmE.0GjASkeY72edtU9McHuNluL.B.bZXBfsNLhI96DaXC4BTyMkwhgzSHg0Pb5KdETasz6BAK9y+B7nOB8xZyK8xuBJ5P+NyklG.FmLWbrqAf.1pyI1gNgU+UesuoNeoqxpylW1JXybzjlzafss0svboG.xWlL.DQi+tucSHsQjNhJpVhNeucAe22uEb0Jtjf2.0JgDVCQG6ROwxWwJPDQDNJp3RvyL5Qy0OACG..o6EJR10P67y2Ba8e8oO8Ae0Wudb0x8sz4.CJHzr6n0XAe5BYq+a.ImrP57wAsNKmU7bY.bW.vtwfAShiRHgDvt2ydww+C6peVUCsu4JX8YnWeBX8qacrAOm9HRk4RyAxPVl4GL2M.vY.OCZKVr.iFMhHhHbz0t0Mrgb1DpopafZqoZot7H4DXPAglGiN7Uqccrc22K8xuJV6ZELx+QBINZYQnPvqBfhNzui6pcsGI0k6A268bOXu6+.3zm7O8IzX.5.4J6Tmk8+yMHiwLlwhe5GY2ti7VNlELqQbyz7kq7Z32N3g7YBZlI.ClwhwjmhI7RuP8CYfY8grITtb.L.HeY9jKEB5+twlFmTQkWACYHCFwGWrnZp.wd1quSiSDRmmv3omoce7mrH79u2zYtzxAsuxyHyEIA03ye9KhjSNYDe7wA8I1Qj6l9VelL5GbHghlzh3v+0rY1.Ll3jdct9I3RJP90X.Zc9IAfMSe0xJ6DXHCYHHhHBG81PewOrksgq3iDPmP0+8LOqQwz4mDz8pgbiEHfuYtINJ4ALPr5UsZbsqTgWn334v2mgd8IfbxMWVa6W5EeIljGIaILRnAmUgftKvzwbhKcoKgi8G+O1LG0290Orgb+VUe.cAGRnnYs5NswPdJSc53ilmfS5gOBx63KhKVAv8A.al5N+9AOHdxgObDcTsDc9dtGeBMFv9.4V2Fxgc1qNkoNcrvO8iY9HFCYuQPFB1vjctyeEOzizazt1dWn6cqaX+G3f3zm4Lp9.M36rHiLyByatyB.zCp1Qkdpbu7W.xa2qxka.Z6YaBPe+6aurMN4genGD6bW60mTmSMszwm9IyG.Pntl5E.v26EJVBpwEcnemsa.aWauKnqcsGaYKawmPiaRKhCqa8q2l.LDY7KOAHuYwmOk.dMz9XG6n37W3hXHCYvHpnZIZW66HxNmMo5y1rPAxwyeLWlNjmI8mPbi5NF.+OfafyIzw6Aa5a9FeB6Yt9L.rMYFu0+7egUr7kw7QxVBiDaj1aAzcEHajyEcneG0Ta.nu8wfMAzUas0hZpR8YTy3vXsqa81XHKxxPxA.uJ88B78fWqStzktDNwIOEaPyI1wNgEs3+KhrogqZ6V6fZTyX6ZU.ayVz5yNW7xunM8z2HAvN8hEuBg.AM+q+xufGq+8GwGebH4jSFe9WrTTKBP05zHzF0Tb5KVgMAXrfOd9ri8SdcM0Rg7NPwEhRfHMN4w5e+QTQ0RjbxICK4+y3BW5xp1rGEZCaBN8EuhM57RV7mwpyok5Soj5rfZb94aAsL5XPO6Q2QG6Ph3NaW6w28c+fpMPiPaXSPudjGCKeEq.5aWaAfCCvXo.3s7lkOPGzby.v8y8j6ee6ks9u1016B2aW6N90ctKUaF5BIrFhvaY7tZ8e4A4cnBHD6D7RZD.cfy23l2hs2StSUdCTBowQhSegJrIPNtIyXlevrw68trYxWVSXjXAycYHPVMxOeK1EP29J7f3hW5uTUhcvMpY3A58igku7kaiCCQLjONn+CWu8KvM.sAsQtmjaPysqs2E5781Er3O++hHadTpJMNvfBB2JvFgyd9yydt4ufEh25eLI.PGHGurXLA38Z4GWDLnYltnJhHBGOV+6O950sdTUM.AhpUUCv4faTjnrSeN1wgQFYlEVvGSuDNT1INIRY.Cf6L+KO38aTBCeO30Uf704t1stgudcqG2n5ZQPnF0mNelyypyLAxwrTYjYliA6d2Evb4G.BjUAu.1ow.ziet6MothN1gDQG6Ph3g6cev1sjOt9UKWkowMC+sQ9rXtycNH5nZIpnhJw689yvQMvVtG+Vhw2CA5tUt0+0t1dW3QMzW7sadK3FWsBUkNGRiiD85g6K1XcSXG.mlHiA.kQms.dIMBft2SXxpO2Fnn17MyjHCFeF.1FH25yNWjUl+clOJGHyAL6n0.iBg.sPge.cCYHCAm6B+E1+ucHDVvAfZqsF4r75PBNjPwMCnAXBu9ahOZdyFMMhHblCClwVjUuY4jCVgHCTetFy2aRcEKYIKAMJhloJpDLzF0Tbl+5prK+H.zFwO2yLZ.HXfbJQKrY3Ff14rMoHrnC863X+w+icLG8X8u+nvCbPT5+6OQ3MtQJdlPCIrFhqTUf37WjyTbeJlv7l6rPXgEFpnhJwK+xuB+whnR4TF08bs.5fMrIi9b04mb3CGG9HGE+dwGQUnyL9LNacKyK.z57Lmw+t9.4FSVH2bY6ouC.4eRRIFBpw..q4qVMaF5hOtXwfFxPwgJoTb1ybZUiFOYSuKl5jeaV62LGSVhkQNFM1aLbLDiuGBDnA+5+d5m9YwQsdR7mG+3JdOTEXPAgZBoI3sm56hY8gyjUmeq+4+BevL++D5qvLlOs5UKn0ykg.Iz..H6MrdaZfxckPhXsqKaznFDphOriX7MeAN9l0qOAr5070XPOAca73UGHSO+Iq9Lb1BZ12CAl4I4muEbhSdZXvfA5stmjebnOwNBK+3ufZQ..0VkWMfi.CJHDXCiDc995EV7m+ewHR6o..8VG0K+xuhXNLXF+VdiI7fivBDPi4aLm1HRGaXCYiSd1KgHhr4JRV5XbJelyeAalwN+3O8yriO.QBjyn2trxiy.5LvZ2RByMt4svC9.O.hO93vPFxPvIO0Yfk7+IDdSaghzRv.CJHDTCaJNwYuHt4spuRXyKaErCB+hJtDzm9zG9AxY.Jake.z5rcYzma.cQWWWtxUmCNPJudi.Y04ycIb8qec1yyjc4vBKLV+GbFKWpAc9LftKWsaRD8ce6lPM0F.5d25FhNpVhzFwHPHMponzROFtw0thhow8efOIV7m+4Xf0UQWQEWBxLywvM.YtnFzXT2ymISn1EPG25+F5PGBZdzwge6PEgap.5L.clkefd2eL+E7orYFpnhKASbRuN9uetfaKZpk5+rBA7MCP2.Et0A18dd+XyaY6nFDHBvKGiAP8wYbhydAa7MmZZoiEsnEg6um8..BFHmA3ErmckUm1rg.Aar+8sW70e8Widc+O.hOtXQG6PhvnQiH7HaI9sCUBppVffPsxpgcvgDJBnAQfHZQqvzdm2Gy5CmAhOtXA.8fBOsTeJwl0NpECYFDTiWyWsZaZEnQiFw0t9MvOrks.pfa.ZR3Q3UBbN3PBETgzDbpy+W1Tw23mvjvpV0JYSk+R+xUxef3qFBjiAlU4c6lPDLAZDQDgigLjAiVFcLXMq8qv0qBHhHaFBTlsiApuhuy9WWEWth5mAWolV5XsqcsrKKFErq8fzR8o310ppkJ+XPPcla.c704Jt1s7ZA0IlNmbxofUrxUYSEgoLfAnFCXFfNXNAq.L+7sfi8G+Ob2ctyHpnZI5YO5NF3fGJpIvFf8W3A8pZ7cmTOwjM8t3sdy2.QEUKA.sOhG+w5qPKKT.pKMFPjFm.PW+292egHg1mHhOtXQRc4dvnG8yf.CKbTvt1qWSmCtQMEcpK8.SY5uGl1TmLa8eK8KWIdwwMVek5+JDBzCU.1FPW6Z6cgmb3CGm7TmA+RA6EQDYy7J0+w0mQ4UX6rqcFybV3ceGS1zc1bFm3pM6YVLC.JgNlwLmEU4kWAECkWdETyeAKjRu9DnhNpVR05XuCpXadinZUSCyiOhs4Mhp0wdGTsn4MmJ4jSgZcaHGad1+YYmfZ7SXRBVNq6nPvaPWphvLDnLO9ILIadG+gstcpjSNEJ.PEbvgPEWLshp0QGojnuBoy7KOImbJT+vV2tM+9V.M2aMyfucwHDPiSN4TnNTQEy9NcnhJlJiLyh8yiI5noZSLQKoZbqZZXTsIlnohI5nsq7nWeBTyeAKjhKlW1JDxVNR6dCUGXDBny50mfM57eV1IrQmiNpVJq5bvAGhcko4ufEZyee4CoyFg394r685OK6DTSdJlnt2Ne2xiF2pVPESzQS8TOUpTqaC4XisK++dRfC0pFC3Dcle8e+YYmfZ9KXgT268zYIstOliVGcjTwESqnd79+31oy7+6IANTy0+YBhTtm7TLY2eipWeBJhuY.PkZZoSsyB1sM0AxS2yFpW6Y.3f.5Xp7gqfyD3wjmhIJ85SfJhvCmJtXZEUahIZWN3i1zpVP05XuCp3hoUTQDd3TImbJTyXlyxlJD3F.oXkO3iHvPDMN4jSwNiGl.lYtlF23FSESzQS05XuCp3aY3tky33ioUTQGUKc4eGuyB1MafkbNL5sEsaSLBGTAHe6WtueAGbHTQGUKoh2MCft0QGIUqi8NDMvBTmiq+rrS3LGzl8xZl6fQP2pTmpy6rfcay6HWctMspEdEcVjFkXFpaeFFgHZLpyuA+J7Ku7JnV2Fxg5Ue0wKYZ7i2+GWP+xLAPJV4C9FZL.culHpNqWeBTlW1Jn3CS8e2amua2VmiukgypyOV+dLQq+aFybVNSms.0uNaDhT9SMszs48lecfAGbHr0+4t9lYp+SLeFB82S+vV2tM0CC0ahLrCivAFLhETGEEcqyLurUPM4oXxFG2sn4M2liHBOb16UFYlE0jmhIp0sgbry.lqyBdhoPGx0dmnbv3gHuG7agBii4TSKcAudFMMtXZEU7wbG1bDcTsjpEMu4TMtwM1gZWpoktfUHHfSZqPfU1aUJh5bleqtnnnC1PrL9FQ380Pw+K..T.7IQTPTgS0hl2bpXhNZ6z33hoU1XSK1QxImB07WvBsI3BJJJ6BXutxrQunN4o3SnybaoOmCeEeFIA5+1ygu2lW1JDzu7NKX2TyeAKjZ7SXRTO4PGpS03Ne2chJiLyhZFybVTqaC4XmVx3q2EBhyWRiAn04BgKXeIlNadYqfZ7SXRTFMZzo578dOc1o5rKFrLE79KWQdBFfCBbVndBz7xVgPIUf0mgP0+ESzQ6R9L..UFYlkqjETeMey.vELpAn6dP9cCpP7mkcBpcVvtYObkq2QAvv6nP36DfAWL.QbPKV.y+YYmfx7xVgyRytKcjQlYQYdYqvNGHBkQv5NlGT+s5iOQB5VqJnFvOyMLu+qaC4PM9ILIWoADN7H0zRWvVYSQQQstMjiPNm7UskiDzYEWTaM9A0Ik5LS174Nz.zf5rC0XlC85SfZxSwjfZAeNTQEea6W17xVgV2ubjvA8PE2C4r9uaCc1JDXrn4CfN3.eyhUG3gJpXpYLyY4pZiS+8mP0AJR.zVfB280A3geeS.XZtxElQlYgjtuthDSr8nCI1dz55FvftBEUbInjCeDT5QJE6cu6QrUJb9TdckOelTdJ.QB52gWSnOTu9DP5ibzXjibDrKLjbofcsGTxgOBNyoOMJszi..fJpnBV8K0zRGQDQD..HgDZOZULwfNjX6Qu5Y2s6dUTwkfUspuBqdUqf6fuGfdfXaDducb.4fwCZctoB8gYjYVXDomN6jPfKL1lm5TmFEt+8wd9OeIeF.nGX8w25VC.fXhIVbGspUnGcuanicHQDQDgay8prSbRjStaBaJ2bvl272w8izB1x.z1IyCNPmehANP1Ig.W3pyG6nkhJqrR..jmkcfidzRusz4JpnRrgb1HV0JVtP577fuUVL3yv.cvFBpw7gwubO5d2PrwFyske4B10dvIO0ov9229wt2UA70RwPKnw.z577.vc5JWrBT+G.8t5v7fJb.3eafC8MqWeBHywLVLpQktcZZEUTIJtjCicum8hqdkqHXcfYjYVrWeR2WWQ3gGtn0AVvt1C1zl9F9KyYpFeydZvb.zQiZB.O2s6WjqCXgfwQsafVvHlKF.sF2awtfjSNEzm91OL3AOPACrycnnhKAabieC1w12lPNpUMFwRDQB52EQsiYBd9Q68iJXfctCkchShsui7Pg6eeXdyUvsYtkBZGZZEa4aKctW8n61ELl6PYm3jX26Yu3GyOeGoylfxslaIk3vFA5L31HOgvC7KqkzX.Zcd70c3RAOyEYr9ubpqLY0c9xpPzAWHFiTSKcLvAMXzit2UIqNvB10dfEK4IVcfeTckKUguYoHXNFzA2LnNIBlV7okBhiOFfSBpCftxvdanOHo6qqH1XiAwEarNrU2kchShScpSiSdpSwlgIG3H43ftk+ZUcVGbQ63LxLKaxnY3g2DG5DofcsGTQkUhCe3ificzRQwEUjixlgVqhO9nCtnNmZZoit0steaoy..6dO60eWm0Ak0mLCZYMFndcdXvMBpSBYof12rEErLHmjDnCR0o1y50m.FzfGBZm9DPhI1dDQ3gKXV5YfacfkdjRQokdD1dWgGkC5gyfIncsmYQGp+E0i62ZW3Ha3CNnC8PL.WbbaHgGVf+kNqCzArZEdOM1JncV4qM1C8DzAuq+BFc1D7ezYcP4zXcx5al5BlL0YEdWcddv+Rm0g5Slf2RmKD9e9lsgj.8ePaARmndYTe.b9sBacDIn0grg7XXaAzFv57JuMpWXFGRVg73jXdv2bvfK0vLNjrBhNKWjDHZr2.Fc1oSTP23vZc26g4sdYTwH29lMAeD6YoraVcEL.ZgIRT+LrIIHbpoONn+EzkAsnxbXUdKh9zjDpWi0Amzcr73.fVaKDzAwYQJKXZHzg50XlCWsqUXrosf50YsXWUKEnCRmNS7aHL5fsZrN3hCnePz3aGzg50XCv8zYFM1BH5rXjj.G9M9l81AyQPYfI.ZgvhWrbnkQGDOClEBeLGCpXhDh2RYhNKMPzXuCDc16fNHtuYKdsRgLy++.FDw8X0hdBC....PRE4DQtJDXBB" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-125",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 447.0, 198.0, 545.0, 309.441786283891588 ],
					"pic" : "zz_boletes_newkeys.png",
					"presentation" : 1,
					"presentation_rect" : [ 1.0, 2.363636, 301.500015000000019, 171.186611387559822 ]
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"background" : 1,
					"bgcolor" : [ 0.811764705882353, 0.811764705882353, 0.811764705882353, 1.0 ],
					"hidden" : 1,
					"id" : "obj-130",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 528.0, -35.0, 360.0, 220.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 0.0, 360.0, 220.0 ],
					"proportion" : 0.5,
					"prototypename" : "referencepanel_360x220"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"order" : 1,
					"source" : [ "obj-10", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"order" : 0,
					"source" : [ "obj-10", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-45", 0 ],
					"order" : 0,
					"source" : [ "obj-10", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"order" : 1,
					"source" : [ "obj-10", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 5 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"order" : 1,
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-259", 0 ],
					"order" : 0,
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"order" : 2,
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"order" : 1,
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-259", 0 ],
					"order" : 0,
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"order" : 2,
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 3 ],
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 3 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 1 ],
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-109", 0 ],
					"source" : [ "obj-115", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-115", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"source" : [ "obj-116", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"source" : [ "obj-117", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 2 ],
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 6 ],
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 0 ],
					"source" : [ "obj-120", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 0 ],
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 1 ],
					"midpoints" : [ 1406.949951000000056, 1402.0, 1194.0, 1402.0, 1194.0, 1698.0, 1281.0, 1698.0, 1281.0, 1771.0, 1313.085205000000087, 1771.0 ],
					"order" : 1,
					"source" : [ "obj-122", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-214", 1 ],
					"midpoints" : [ 1399.699951000000056, 1402.0, 1194.5, 1402.0 ],
					"order" : 1,
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"midpoints" : [ 1406.949951000000056, 1375.0, 1634.5, 1375.0 ],
					"order" : 0,
					"source" : [ "obj-122", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"midpoints" : [ 1399.699951000000056, 1378.0, 1590.5, 1378.0 ],
					"order" : 0,
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-123", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 2 ],
					"source" : [ "obj-126", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 1 ],
					"source" : [ "obj-126", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 2 ],
					"source" : [ "obj-127", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 1 ],
					"source" : [ "obj-127", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 2 ],
					"source" : [ "obj-128", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 1 ],
					"source" : [ "obj-128", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-159", 0 ],
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 2 ],
					"source" : [ "obj-129", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 1 ],
					"source" : [ "obj-129", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 0 ],
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 3 ],
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"order" : 1,
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-255", 0 ],
					"order" : 2,
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"order" : 0,
					"source" : [ "obj-131", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"source" : [ "obj-133", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-135", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 7 ],
					"source" : [ "obj-138", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 3 ],
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-106", 0 ],
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-131", 0 ],
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 8 ],
					"source" : [ "obj-143", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 1 ],
					"order" : 1,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 1 ],
					"order" : 0,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-147", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"source" : [ "obj-148", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-269", 0 ],
					"source" : [ "obj-150", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"source" : [ "obj-154", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 503.5, 705.0, 1154.5, 705.0 ],
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"order" : 0,
					"source" : [ "obj-156", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-287", 1 ],
					"order" : 1,
					"source" : [ "obj-156", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 0 ],
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 581.5, 705.0, 1154.5, 705.0 ],
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 0 ],
					"source" : [ "obj-159", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 734.5, 705.0, 1154.5, 705.0 ],
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"source" : [ "obj-161", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 810.5, 705.0, 1154.5, 705.0 ],
					"source" : [ "obj-162", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-154", 0 ],
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 0 ],
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 894.5, 705.0, 1154.5, 705.0 ],
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 2 ],
					"source" : [ "obj-167", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 1 ],
					"source" : [ "obj-167", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"source" : [ "obj-168", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 612.989379999999983, 927.0, 1154.5, 927.0 ],
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-147", 0 ],
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 2 ],
					"source" : [ "obj-170", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 1 ],
					"source" : [ "obj-170", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-171", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-173", 0 ],
					"source" : [ "obj-172", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 528.989379999999983, 927.0, 1154.5, 927.0 ],
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"source" : [ "obj-174", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 452.989379999999983, 1086.0, 1154.5, 1086.0 ],
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 2 ],
					"source" : [ "obj-176", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 1 ],
					"source" : [ "obj-176", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 0 ],
					"source" : [ "obj-176", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-174", 2 ],
					"source" : [ "obj-177", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-174", 1 ],
					"source" : [ "obj-177", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-174", 0 ],
					"source" : [ "obj-177", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-179", 0 ],
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 856.989379999999983, 927.0, 1154.5, 927.0 ],
					"source" : [ "obj-179", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 2 ],
					"source" : [ "obj-180", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 1 ],
					"source" : [ "obj-180", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 0 ],
					"source" : [ "obj-181", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"source" : [ "obj-182", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 772.989379999999983, 927.0, 1154.5, 927.0 ],
					"source" : [ "obj-183", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 0 ],
					"source" : [ "obj-184", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 696.989379999999983, 1086.0, 1154.5, 1086.0 ],
					"source" : [ "obj-185", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-181", 2 ],
					"source" : [ "obj-186", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-181", 1 ],
					"source" : [ "obj-186", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-181", 0 ],
					"source" : [ "obj-186", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-184", 2 ],
					"source" : [ "obj-187", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-184", 1 ],
					"source" : [ "obj-187", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-184", 0 ],
					"source" : [ "obj-187", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-192", 0 ],
					"source" : [ "obj-191", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 935.989379999999983, 927.0, 1154.5, 927.0 ],
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-198", 0 ],
					"source" : [ "obj-196", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 0 ],
					"source" : [ "obj-197", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"source" : [ "obj-199", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 1 ],
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"source" : [ "obj-200", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"source" : [ "obj-201", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-196", 0 ],
					"order" : 0,
					"source" : [ "obj-202", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-200", 0 ],
					"order" : 1,
					"source" : [ "obj-202", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-201", 0 ],
					"order" : 2,
					"source" : [ "obj-202", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 0 ],
					"source" : [ "obj-203", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 0 ],
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-203", 0 ],
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 0 ],
					"source" : [ "obj-205", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-202", 0 ],
					"source" : [ "obj-206", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-205", 0 ],
					"midpoints" : [ 1304.12768600000004, 1651.700012000000015, 1243.542533999999932, 1651.700012000000015, 1243.542533999999932, 1463.700012000000015, 1266.12768600000004, 1463.700012000000015 ],
					"order" : 2,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"order" : 1,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-211", 0 ],
					"order" : 0,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-207", 0 ],
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 0 ],
					"order" : 0,
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-214", 0 ],
					"order" : 1,
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-207", 0 ],
					"source" : [ "obj-210", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"source" : [ "obj-212", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-216", 1 ],
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"source" : [ "obj-214", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-216", 0 ],
					"source" : [ "obj-214", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 2 ],
					"source" : [ "obj-217", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 1 ],
					"source" : [ "obj-217", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 0 ],
					"source" : [ "obj-217", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-219", 0 ],
					"source" : [ "obj-218", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 930.234069999999974, 1302.0, 1154.5, 1302.0 ],
					"source" : [ "obj-219", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 1 ],
					"source" : [ "obj-22", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-218", 2 ],
					"source" : [ "obj-220", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-218", 1 ],
					"source" : [ "obj-220", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-218", 0 ],
					"source" : [ "obj-220", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-222", 0 ],
					"source" : [ "obj-221", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 851.234069999999974, 1302.0, 1154.5, 1302.0 ],
					"source" : [ "obj-222", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 2 ],
					"source" : [ "obj-223", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 1 ],
					"source" : [ "obj-223", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 0 ],
					"source" : [ "obj-223", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 0 ],
					"source" : [ "obj-224", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 767.234069999999974, 1302.0, 1154.5, 1302.0 ],
					"source" : [ "obj-225", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-227", 0 ],
					"source" : [ "obj-226", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 691.234069999999974, 1302.0, 1154.5, 1302.0 ],
					"source" : [ "obj-227", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-224", 2 ],
					"source" : [ "obj-228", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-224", 1 ],
					"source" : [ "obj-228", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-224", 0 ],
					"source" : [ "obj-228", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 2 ],
					"source" : [ "obj-229", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 1 ],
					"source" : [ "obj-229", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-226", 0 ],
					"source" : [ "obj-229", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-231", 0 ],
					"source" : [ "obj-230", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 607.234069999999974, 1302.0, 1154.5, 1302.0 ],
					"source" : [ "obj-231", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 2 ],
					"source" : [ "obj-232", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 1 ],
					"source" : [ "obj-232", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 0 ],
					"source" : [ "obj-232", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-234", 0 ],
					"source" : [ "obj-233", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 523.234069999999974, 1302.0, 1154.5, 1302.0 ],
					"source" : [ "obj-234", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 0 ],
					"source" : [ "obj-235", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 447.234069999999974, 1302.0, 1154.5, 1302.0 ],
					"source" : [ "obj-236", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-233", 2 ],
					"source" : [ "obj-237", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-233", 1 ],
					"source" : [ "obj-237", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-233", 0 ],
					"source" : [ "obj-237", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 2 ],
					"source" : [ "obj-238", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 1 ],
					"source" : [ "obj-238", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-240", 0 ],
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 888.744689999999991, 1086.0, 1154.5, 1086.0 ],
					"source" : [ "obj-240", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 2 ],
					"source" : [ "obj-241", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 1 ],
					"source" : [ "obj-241", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"source" : [ "obj-241", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-243", 0 ],
					"source" : [ "obj-242", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 804.744689999999991, 1086.0, 1154.5, 1086.0 ],
					"source" : [ "obj-243", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-245", 0 ],
					"source" : [ "obj-244", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 728.744689999999991, 1086.0, 1154.5, 1086.0 ],
					"source" : [ "obj-245", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-248", 0 ],
					"source" : [ "obj-246", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"source" : [ "obj-247", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 575.744689999999991, 1086.0, 1154.5, 1086.0 ],
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-250", 0 ],
					"source" : [ "obj-249", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"midpoints" : [ 497.744689999999991, 1086.0, 1154.5, 1086.0 ],
					"source" : [ "obj-250", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-242", 2 ],
					"source" : [ "obj-251", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-242", 1 ],
					"source" : [ "obj-251", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-242", 0 ],
					"source" : [ "obj-251", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 2 ],
					"source" : [ "obj-252", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 1 ],
					"source" : [ "obj-252", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 0 ],
					"source" : [ "obj-252", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 2 ],
					"source" : [ "obj-253", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 1 ],
					"source" : [ "obj-253", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-246", 0 ],
					"source" : [ "obj-253", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-249", 2 ],
					"source" : [ "obj-254", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-249", 1 ],
					"source" : [ "obj-254", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-249", 0 ],
					"source" : [ "obj-254", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"source" : [ "obj-256", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-266", 0 ],
					"order" : 1,
					"source" : [ "obj-259", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"order" : 0,
					"source" : [ "obj-259", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"order" : 2,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 1 ],
					"order" : 1,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-140", 0 ],
					"order" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-261", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"source" : [ "obj-262", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"source" : [ "obj-266", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-268", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-152", 0 ],
					"source" : [ "obj-269", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-294", 0 ],
					"source" : [ "obj-269", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-295", 0 ],
					"source" : [ "obj-269", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-296", 0 ],
					"source" : [ "obj-269", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-297", 0 ],
					"source" : [ "obj-269", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 1 ],
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"source" : [ "obj-34", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-197", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-36", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 1 ],
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 2 ],
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-193", 0 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 1994.5, 638.0, 1994.5, 638.0 ],
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-195", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"order" : 1,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 0 ],
					"order" : 0,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"source" : [ "obj-45", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 3 ],
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"midpoints" : [ 1994.5, 668.0, 1994.5, 668.0 ],
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"order" : 1,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"order" : 0,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"source" : [ "obj-54", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 1 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 1 ],
					"source" : [ "obj-58", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"order" : 1,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"order" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 0 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-63", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"source" : [ "obj-63", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 0 ],
					"source" : [ "obj-63", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"source" : [ "obj-63", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"source" : [ "obj-63", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"source" : [ "obj-63", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"source" : [ "obj-63", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"source" : [ "obj-63", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"source" : [ "obj-64", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-66", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"order" : 0,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"order" : 1,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"order" : 1,
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"order" : 0,
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"source" : [ "obj-72", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-134", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-247", 0 ],
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-82", 1 ],
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"order" : 0,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"order" : 3,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-188", 0 ],
					"source" : [ "obj-82", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"order" : 2,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-194", 0 ],
					"source" : [ "obj-82", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-76", 0 ],
					"order" : 1,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 1 ],
					"source" : [ "obj-89", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"order" : 0,
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"order" : 1,
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"order" : 3,
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"order" : 0,
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"order" : 1,
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"order" : 2,
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-92", 0 ],
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 1 ],
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 4 ],
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-122" : [ "live.gain~[1]", "live.gain~", 0 ],
			"obj-1" : [ "vst~", "vst~", 0 ],
			"parameterbanks" : 			{

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "zz_boletes_newkeys.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "nomsnotes.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "volslider.maxpat",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "zz_faderhueco.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "buttotog.maxpat",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "sliderGold-1",
				"default" : 				{
					"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ],
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
