{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 1,
			"revision" : 0,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 585.0, 108.0, 1026.0, 902.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1284.666625999999951, 755.0, 113.0, 22.0 ],
					"text" : "receive~ #0_dfdbck"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1205.666625999999951, 707.0, 72.0, 22.0 ],
					"text" : "tapout~ 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-124",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "tapconnect" ],
					"patching_rect" : [ 1205.666625999999951, 610.0, 78.0, 22.0 ],
					"text" : "tapin~ 10000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-111",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1475.0, 688.0, 72.0, 22.0 ],
					"text" : "tapout~ 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "tapconnect" ],
					"patching_rect" : [ 1475.0, 593.0, 78.0, 22.0 ],
					"text" : "tapin~ 10000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3598.0, 505.0, 87.0, 22.0 ],
					"text" : "s #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3677.0, 452.0, 75.0, 22.0 ],
					"text" : "route OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-439",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 10,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 2555.200038075447083, 1789.400027334690094, 12.0, 58.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 215.747385172853512, 149.0, 12.0, 58.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-438",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 10,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 2546.400037944316864, 1789.400027334690094, 12.0, 58.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 206.947385041723294, 149.0, 12.0, 58.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3551.25, 282.5, 73.0, 22.0 ],
					"text" : "fromsymbol"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-197",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 3532.0, 249.5, 96.0, 22.0 ],
					"text" : "regexp (.+)\\\\..+$"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-319",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3442.0, 225.0, 57.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-431",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3510.0, 180.0, 111.0, 22.0 ],
					"text" : "loadmess hidden 1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.376471, 0.384314, 0.4, 0.06 ],
					"bgcolor2" : [ 0.290196, 0.309804, 0.301961, 0.07 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.0,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.376471, 0.384314, 0.4, 0.06 ],
					"bgfillcolor_color2" : [ 0.290196, 0.309804, 0.301961, 0.07 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontface" : 0,
					"fontsize" : 8.0,
					"gradient" : 1,
					"hidden" : 1,
					"id" : "obj-432",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3513.25, 322.5, 43.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 273.36459747276308, 30.549999892711639, 58.399999618530273, 17.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-433",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 3435.0, 180.0, 71.0, 23.0 ],
					"text" : "strippath"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-434",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3513.25, 352.5, 37.0, 22.0 ],
					"text" : "open"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-429",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4729.0, 519.0, 83.0, 22.0 ],
					"text" : "s #0_toctIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-430",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4783.384766000000127, 1123.0, 94.0, 22.0 ],
					"text" : "r #0_toctOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-427",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4753.384766000000127, 1082.0, 124.0, 22.0 ],
					"text" : "r #0_ctoutnumOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-428",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4714.0, 554.0, 143.0, 22.0 ],
					"text" : "s #0_ctoutnumIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-219",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 466.914795000000026, 1835.40002400000003, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-264",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "int" ],
					"patching_rect" : [ 476.914795000000026, 1658.300048999999944, 30.0, 22.0 ],
					"text" : "t b i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-313",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 426.414795000000026, 1731.90002400000003, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-315",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 476.914795000000026, 1732.90002400000003, 43.0, 22.0 ],
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-408",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 476.914795000000026, 1870.90002400000003, 126.0, 22.0 ],
					"text" : "s #0_ctoutnumOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-409",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 472.914795000000026, 1610.90002400000003, 111.0, 22.0 ],
					"text" : "r #0_ctoutnumIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-410",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 453.914795000000026, 1913.40002400000003, 107.0, 22.0 ],
					"text" : "sprintf set CT%sR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-411",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 342.914795000000026, 1913.40002400000003, 105.0, 22.0 ],
					"text" : "sprintf set CT%sL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-412",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 447.914795000000026, 1804.0, 61.0, 22.0 ],
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-413",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 349.914795000000026, 1700.90002400000003, 57.0, 22.0 ],
					"text" : "hidden 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-414",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 409.914795000000026, 1700.90002400000003, 57.0, 22.0 ],
					"text" : "hidden 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-415",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 274.0, 1627.0, 46.0, 22.0 ],
					"text" : "sel 1 0"
				}

			}
, 			{
				"box" : 				{
					"autoscroll" : 0,
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"bordercolor" : [ 0.376471, 0.384314, 0.4, 0.0 ],
					"fontface" : 0,
					"fontname" : "Comic Sans MS",
					"fontsize" : 8.0,
					"hidden" : 1,
					"id" : "obj-416",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 447.914795000000026, 1766.90002400000003, 33.0, 27.502929999999999 ],
					"presentation" : 1,
					"presentation_rect" : [ 295.506749926437465, 179.576110999999969, 21.199999928474426, 19.1484375 ],
					"text" : "1",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.862745, 0.870588, 0.878431, 0.0 ],
					"bgoncolor" : [ 0.862745, 0.870588, 0.878431, 0.0 ],
					"fontlink" : 1,
					"fontname" : "Comic Sans MS",
					"fontsize" : 8.0,
					"id" : "obj-417",
					"maxclass" : "textbutton",
					"mode" : 1,
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 312.0, 1766.90002400000003, 26.257811546325684, 19.1484375 ],
					"presentation" : 1,
					"presentation_rect" : [ 267.106785926437453, 179.576110999999969, 26.257811546325684, 19.1484375 ],
					"rounded" : 13.01,
					"text" : "CT",
					"textcolor" : [ 0.0, 0.0, 0.0, 0.29 ],
					"texton" : "CT",
					"textoncolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"usebgoncolor" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-418",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 312.0, 1572.300048999999944, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-419",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 312.0, 1819.300048999999944, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-420",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 392.0, 1572.300048999999944, 81.0, 22.0 ],
					"text" : "r #0_toctIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-421",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 342.914795000000026, 1819.300048999999944, 96.0, 22.0 ],
					"text" : "s #0_toctOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-422",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 288.957457999999974, 1942.0, 51.0, 22.0 ],
					"text" : "gate~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-423",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 288.957457999999974, 2095.300048999999944, 116.0, 22.0 ],
					"text" : "send~ intramoduls2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-424",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 170.372253000000001, 1937.0, 51.0, 22.0 ],
					"text" : "gate~ 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-425",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 137.872253000000001, 2095.300048999999944, 116.0, 22.0 ],
					"text" : "send~ intramoduls1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-426",
					"maxclass" : "ezdac~",
					"numinlets" : 2,
					"numoutlets" : 0,
					"patching_rect" : [ 209.872253000000001, 2010.300048999999944, 44.0, 44.0 ],
					"prototypename" : "helpfile"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-212",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3198.0, 187.0, 47.0, 22.0 ],
					"text" : "route s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-54",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2847.0, 392.0, 47.0, 22.0 ],
					"text" : "route s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-208",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 123.0, 223.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-244",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 232.182861000000003, 185.0, 47.0, 22.0 ],
					"text" : "route s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-242",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4727.5, 1051.0, 107.0, 22.0 ],
					"text" : "r #0_ctnumOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-243",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4676.787597999999889, 593.0, 96.0, 22.0 ],
					"text" : "s #0_ctnumIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-225",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 217.182861000000003, 316.0, 109.0, 22.0 ],
					"text" : "s #0_ctnumOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-220",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 188.432861000000003, 212.0, 43.0, 22.0 ],
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-216",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 232.182861000000003, 153.0, 94.0, 22.0 ],
					"text" : "r #0_ctnumIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-215",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 188.432861000000003, 153.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-213",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 150.682861000000003, 153.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-210",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 150.682861000000003, 185.0, 63.0, 22.0 ],
					"text" : "hidden $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-207",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 150.682861000000003, 316.0, 61.0, 22.0 ],
					"text" : "route text"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-206",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 270.5, 366.0, 107.0, 22.0 ],
					"text" : "sprintf set CT%sR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-200",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 150.682861000000003, 366.0, 105.0, 22.0 ],
					"text" : "sprintf set CT%sL"
				}

			}
, 			{
				"box" : 				{
					"autoscroll" : 0,
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"bordercolor" : [ 0.376471, 0.384314, 0.4, 0.0 ],
					"fontface" : 0,
					"fontname" : "Comic Sans MS",
					"fontsize" : 20.0,
					"id" : "obj-199",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 150.682861000000003, 254.0, 66.0, 50.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 29.227599999999995, 56.25, 50.999999999999993, 35.0 ],
					"text" : "1",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-404",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4702.0, 1011.5, 98.0, 22.0 ],
					"text" : "r #0_plugOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-405",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4635.0, 566.0, 86.0, 22.0 ],
					"text" : "s #0_plugIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-406",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4592.307617000000391, 531.0, 89.0, 22.0 ],
					"text" : "s #0_loadvst"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-407",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4668.0, 971.0, 90.0, 22.0 ],
					"text" : "r #0_savevst"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-400",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 3252.0, 150.0, 69.0, 22.0 ],
					"text" : "opendialog"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-401",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3323.0, 235.5, 99.0, 22.0 ],
					"text" : "s #0_plugOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-402",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3159.0, 150.0, 84.0, 22.0 ],
					"text" : "r #0_plugIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-403",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3248.0, 242.0, 50.0, 22.0 ],
					"text" : "plug $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-356",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2976.0, 420.333312999999976, 82.0, 22.0 ],
					"text" : "prepend read"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-357",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2838.0, 355.0, 87.0, 22.0 ],
					"text" : "r #0_loadvst"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-363",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2908.0, 276.0, 92.0, 22.0 ],
					"text" : "s #0_savevst"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-389",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3054.0, 226.0, 35.0, 22.0 ],
					"text" : "#0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-390",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 3007.0, 180.0, 33.0, 22.0 ],
					"text" : "t s b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-396",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3007.0, 276.0, 145.0, 22.0 ],
					"text" : "sprintf symout %s-%s.fxp"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-398",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3007.0, 334.0, 83.0, 22.0 ],
					"text" : "prepend write"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-399",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3007.0, 150.0, 57.0, 22.0 ],
					"text" : "r savedir"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-397",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 172.0, 1527.333374000000049, 109.0, 22.0 ],
					"text" : "s #0_outvolOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-395",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 337.0, 1193.0, 120.0, 22.0 ],
					"text" : "s #0_outmuteOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-393",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 316.0, 1079.0, 105.0, 22.0 ],
					"text" : "r #0_outmuteIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-394",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 137.153808999999995, 1435.0, 93.0, 22.0 ],
					"text" : "r #0_outvolIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-392",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3510.0, 576.333312999999976, 107.0, 22.0 ],
					"text" : "s #0_vstvolOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-391",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3550.0, 416.333312999999976, 92.0, 22.0 ],
					"text" : "r #0_vstvolIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-383",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2612.84619100000009, 1258.333374000000049, 105.0, 22.0 ],
					"text" : "s #0_h3volOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-384",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2469.666748000000098, 1105.166625999999951, 123.0, 22.0 ],
					"text" : "s #0_h3transpOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-385",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2670.333251999999902, 1158.333374000000049, 116.0, 22.0 ],
					"text" : "s #0_h3onoffOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-386",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2589.666748000000098, 1182.333374000000049, 90.0, 22.0 ],
					"text" : "r #0_h3volIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-387",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2451.666748000000098, 1014.333374000000049, 108.0, 22.0 ],
					"text" : "r #0_h3transpIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-388",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2653.333251999999902, 1075.833251999999902, 101.0, 22.0 ],
					"text" : "r #0_h3onoffIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-377",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2641.666748000000098, 738.333312999999976, 105.0, 22.0 ],
					"text" : "s #0_h2volOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-378",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2338.666748000000098, 609.833312999999976, 123.0, 22.0 ],
					"text" : "s #0_h2transpOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-379",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2688.0, 635.0, 116.0, 22.0 ],
					"text" : "s #0_h2onoffOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-380",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2612.84619100000009, 659.0, 90.0, 22.0 ],
					"text" : "r #0_h2volIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-381",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2466.666748000000098, 513.0, 108.0, 22.0 ],
					"text" : "r #0_h2transpIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-382",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2688.0, 566.5, 101.0, 22.0 ],
					"text" : "r #0_h2onoffIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-376",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2645.666748000000098, 327.333344000000011, 105.0, 22.0 ],
					"text" : "s #0_h1volOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-375",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2485.666748000000098, 166.833327999999995, 123.0, 22.0 ],
					"text" : "s #0_h1transpOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-374",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2688.0, 207.0, 116.0, 22.0 ],
					"text" : "s #0_h1onoffOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-371",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2623.84619100000009, 254.0, 90.0, 22.0 ],
					"text" : "r #0_h1volIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-372",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2475.666748000000098, 91.0, 108.0, 22.0 ],
					"text" : "r #0_h1transpIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-373",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2682.0, 134.0, 101.0, 22.0 ],
					"text" : "r #0_h1onoffIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-370",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2539.166748000000098, 1718.0, 119.0, 22.0 ],
					"text" : "s #0_harmvolOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-369",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2508.666748000000098, 1591.0, 104.0, 22.0 ],
					"text" : "r #0_harmvolIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-368",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1969.666625999999951, 282.0, 107.0, 22.0 ],
					"text" : "s #0_dlyvolOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-367",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1715.833251999999902, 316.0, 115.0, 22.0 ],
					"text" : "s #0_dlyfdbkOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-366",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1374.666625999999951, 428.0, 115.0, 22.0 ],
					"text" : "s #0_dlytimeOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-365",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 968.5, 273.0, 119.0, 22.0 ],
					"text" : "s #0_dlymuteOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-364",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 868.0, 240.0, 118.0, 22.0 ],
					"text" : "s #0_dlyonoffOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-358",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 993.5, 80.0, 103.0, 22.0 ],
					"text" : "r #0_dlymuteIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-359",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 887.0, 34.0, 103.0, 22.0 ],
					"text" : "r #0_dlyonoffIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-360",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1955.666625999999951, 140.0, 92.0, 22.0 ],
					"text" : "r #0_dlyvolIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-361",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1706.538330000000087, 237.5, 99.0, 22.0 ],
					"text" : "r #0_dlyfdbkIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-362",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1244.666625999999951, 309.333344000000011, 99.0, 22.0 ],
					"text" : "r #0_dlytimeIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-352",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 680.53857400000004, 1199.666625999999951, 109.0, 22.0 ],
					"text" : "s #0_dryvolOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-353",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 748.846190999999976, 1148.833251999999902, 93.0, 22.0 ],
					"text" : "s #0_dryOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-354",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 59.0, 468.0, 103.0, 22.0 ],
					"text" : "s #0_muteOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-355",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 375.0, 748.0, 99.0, 22.0 ],
					"text" : "s #0_gainOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-351",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 278.0, 748.0, 101.0, 22.0 ],
					"text" : "s #0_involOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-346",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 445.692383000000007, 1112.0, 93.0, 22.0 ],
					"text" : "r #0_dryvolIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-347",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 722.24377400000003, 1079.0, 78.0, 22.0 ],
					"text" : "r #0_dryIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-348",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 55.044800000000002, 377.0, 88.0, 22.0 ],
					"text" : "r #0_muteIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-349",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 334.198607999999979, 650.0, 84.0, 22.0 ],
					"text" : "r #0_gainIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-350",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 261.653809000000024, 545.0, 86.0, 22.0 ],
					"text" : "r #0_involIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-344",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 516.346190999999976, 397.0, 96.0, 22.0 ],
					"text" : "s #0_ch2OUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-345",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 484.5, 366.0, 96.0, 22.0 ],
					"text" : "s #0_ch1OUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-342",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 293.5, 254.0, 81.0, 22.0 ],
					"text" : "r #0_ch2IN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-343",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 261.653809000000024, 223.0, 81.0, 22.0 ],
					"text" : "r #0_ch1IN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-341",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 95.0, 105.0, 125.0, 22.0 ],
					"text" : "s #0_inputtypeOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-340",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 95.0, 16.000011000000001, 110.0, 22.0 ],
					"text" : "r #0_inputtypeIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-339",
					"maxclass" : "newobj",
					"numinlets" : 32,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3733.0, 1163.0, 1041.740722999999889, 22.0 ],
					"text" : "pak i i i s f f i i f i i f f f f i i f i i f i i f f f i s s i i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-304",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4064.461425999999847, 872.0, 117.0, 22.0 ],
					"text" : "r #0_dlymuteOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-305",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4032.61547900000005, 837.0, 116.0, 22.0 ],
					"text" : "r #0_dlyonoffOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-307",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4000.769287000000077, 958.0, 107.0, 22.0 ],
					"text" : "r #0_dryvolOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-309",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3967.076904000000013, 927.0, 91.0, 22.0 ],
					"text" : "r #0_dryOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-311",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4640.0, 913.0, 118.0, 22.0 ],
					"text" : "r #0_outmuteOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-312",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4608.153809000000365, 882.0, 107.0, 22.0 ],
					"text" : "r #0_outvolOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-314",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4576.307617000000391, 847.0, 105.0, 22.0 ],
					"text" : "r #0_vstvolOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-318",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4491.807617000000391, 923.0, 103.0, 22.0 ],
					"text" : "r #0_h3volOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-320",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4470.307617000000391, 884.0, 121.0, 22.0 ],
					"text" : "r #0_h3transpOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-321",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4461.692382999999609, 847.0, 114.0, 22.0 ],
					"text" : "r #0_h3onoffOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-322",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4370.538574000000153, 923.0, 103.0, 22.0 ],
					"text" : "r #0_h2volOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-323",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4348.038574000000153, 872.0, 121.0, 22.0 ],
					"text" : "r #0_h2transpOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-324",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4325.692382999999609, 832.0, 114.0, 22.0 ],
					"text" : "r #0_h2onoffOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-325",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4272.538574000000153, 923.0, 103.0, 22.0 ],
					"text" : "r #0_h1volOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-326",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4228.038574000000153, 884.0, 121.0, 22.0 ],
					"text" : "r #0_h1transpOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-327",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4208.692382999999609, 825.0, 114.0, 22.0 ],
					"text" : "r #0_h1onoffOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-328",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4192.153809000000365, 1024.0, 117.0, 22.0 ],
					"text" : "r #0_harmvolOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-329",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4159.153809000000365, 989.0, 105.0, 22.0 ],
					"text" : "r #0_dlyvolOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-330",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4128.153809000000365, 950.0, 113.0, 22.0 ],
					"text" : "r #0_dlyfdbkOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-331",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 4096.307617000000391, 915.0, 113.0, 22.0 ],
					"text" : "r #0_dlytimeOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-332",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3937.076904000000013, 903.0, 101.0, 22.0 ],
					"text" : "r #0_muteOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-333",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3904.230712999999923, 872.0, 97.0, 22.0 ],
					"text" : "r #0_gainOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-334",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3873.38452099999995, 837.0, 99.0, 22.0 ],
					"text" : "r #0_involOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-335",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3841.538574000000153, 940.0, 84.0, 22.0 ],
					"text" : "r #0_sfOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-336",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3809.692383000000063, 903.0, 94.0, 22.0 ],
					"text" : "r #0_ch2OUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-337",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3777.84619100000009, 872.0, 94.0, 22.0 ],
					"text" : "r #0_ch1OUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-338",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3746.0, 837.0, 123.0, 22.0 ],
					"text" : "r #0_inputtypeOUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-303",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4051.461425999999847, 554.0, 105.0, 22.0 ],
					"text" : "s #0_dlymuteIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-302",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4019.61547900000005, 519.0, 105.0, 22.0 ],
					"text" : "s #0_dlyonoffIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-300",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3987.769287000000077, 640.0, 95.0, 22.0 ],
					"text" : "s #0_dryvolIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-299",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3954.076904000000013, 609.0, 80.0, 22.0 ],
					"text" : "s #0_dryIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-292",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4561.0, 597.0, 107.0, 22.0 ],
					"text" : "s #0_outmuteIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-287",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4542.153809000000365, 566.0, 95.0, 22.0 ],
					"text" : "s #0_outvolIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-286",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4497.307617000000391, 531.0, 94.0, 22.0 ],
					"text" : "s #0_vstvolIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-283",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4466.538574000000153, 605.0, 92.0, 22.0 ],
					"text" : "s #0_h3volIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-284",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4434.538574000000153, 566.0, 110.0, 22.0 ],
					"text" : "s #0_h3transpIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-285",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4402.692382999999609, 531.0, 103.0, 22.0 ],
					"text" : "s #0_h3onoffIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-275",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4370.538574000000153, 605.0, 92.0, 22.0 ],
					"text" : "s #0_h2volIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-277",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4338.538574000000153, 566.0, 110.0, 22.0 ],
					"text" : "s #0_h2transpIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-280",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4301.692382999999609, 531.0, 103.0, 22.0 ],
					"text" : "s #0_h2onoffIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-274",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4265.538574000000153, 605.0, 92.0, 22.0 ],
					"text" : "s #0_h1volIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-273",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4233.538574000000153, 566.0, 110.0, 22.0 ],
					"text" : "s #0_h1transpIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-272",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4201.692382999999609, 531.0, 103.0, 22.0 ],
					"text" : "s #0_h1onoffIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-270",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4179.153809000000365, 706.0, 106.0, 22.0 ],
					"text" : "s #0_harmvolIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-269",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4146.153809000000365, 671.0, 94.0, 22.0 ],
					"text" : "s #0_dlyvolIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-268",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4115.153809000000365, 632.0, 101.0, 22.0 ],
					"text" : "s #0_dlyfdbkIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-265",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 4083.307616999999937, 597.0, 101.0, 22.0 ],
					"text" : "s #0_dlytimeIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-263",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3924.076904000000013, 585.0, 90.0, 22.0 ],
					"text" : "s #0_muteIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-261",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3892.230712999999923, 554.0, 86.0, 22.0 ],
					"text" : "s #0_gainIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-260",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3860.38452099999995, 519.0, 88.0, 22.0 ],
					"text" : "s #0_involIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-259",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3828.538574000000153, 622.0, 73.0, 22.0 ],
					"text" : "s #0_sfIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-257",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3796.692383000000063, 585.0, 83.0, 22.0 ],
					"text" : "s #0_ch2IN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-255",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3764.84619100000009, 554.0, 83.0, 22.0 ],
					"text" : "s #0_ch1IN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-245",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3733.0, 519.0, 112.0, 22.0 ],
					"text" : "s #0_inputtypeIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-166",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 32,
					"outlettype" : [ "int", "int", "int", "", "float", "float", "int", "int", "float", "int", "int", "float", "float", "float", "float", "int", "int", "float", "int", "int", "float", "int", "int", "float", "float", "float", "int", "", "", "int", "int", "int" ],
					"patching_rect" : [ 3733.0, 484.333312999999976, 1005.612427000000025, 22.0 ],
					"text" : "unpack i i i s f f i i f i i f f f f i i f i i f i i f f f i s s i i i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-226",
					"linecount" : 24,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3733.0, 69.333327999999995, 194.0, 328.0 ],
					"text" : "SAVE LOAD\n\ninput type (1-3)\nch1 ch2 sf\n\ninvol, gain, mute\ndry, dryvol\n\ndlyonoff, dlymute\ndlytime, dlyfdbck, dlyvol\n\nharmvol\nh1onoff h1transp h1vol\nh2onoff h2transp h2vol\nh3onoff h3transp h3vol\n\nvstvol\n\noutvol outmute\n\nvstpreset vstplug ctnum toct ctoutnum\n\n32 parametres"
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-28",
					"index" : 0,
					"maxclass" : "outlet",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3733.0, 1235.0, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "",
					"id" : "obj-23",
					"index" : 0,
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3733.0, 408.333312999999976, 30.0, 30.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-224",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 258.0, 1037.0, 101.0, 22.0 ],
					"text" : "route /ct/outmute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-223",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 792.044800000000009, 89.0, 73.0, 22.0 ],
					"text" : "route /ct/dly"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-193",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 792.044800000000009, 55.0, 85.0, 22.0 ],
					"text" : "r #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-254",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 2622.5, 1604.0, 91.0, 22.0 ],
					"text" : "route /ct/hfader"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-253",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2622.5, 1630.0, 99.0, 22.0 ],
					"text" : "scale 0. 1. 0 158"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-251",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2622.5, 1574.0, 85.0, 22.0 ],
					"text" : "r #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-250",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 0,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 4,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 358.25, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-10",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 303.25, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 202.25, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 154.0, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 250.5, 192.0, 36.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 202.25, 196.0, 36.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 250.5, 251.0, 29.5, 22.0 ],
									"text" : "inc"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.25, 251.0, 30.0, 22.0 ],
									"text" : "dec"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 6,
									"outlettype" : [ "", "", "", "", "", "" ],
									"patching_rect" : [ 154.0, 138.0, 276.0, 22.0 ],
									"text" : "route /ct/h3tog /ct/h3- /ct/h3+ /ct/h3vol /ct/h3transp"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-245",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 154.0, 97.0, 133.0, 22.0 ],
									"text" : "r #0_OSCin"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-1", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-1", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-1", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-245", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "sliderGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
									"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 2572.0, 1037.0, 100.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p harmony3OSC"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-248",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 0,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 59.0, 104.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 4,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 352.25, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-10",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 303.25, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 202.25, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 154.0, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 250.5, 192.0, 36.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 202.25, 196.0, 36.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 250.5, 251.0, 29.5, 22.0 ],
									"text" : "inc"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.25, 251.0, 30.0, 22.0 ],
									"text" : "dec"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 6,
									"outlettype" : [ "", "", "", "", "", "" ],
									"patching_rect" : [ 154.0, 138.0, 276.0, 22.0 ],
									"text" : "route /ct/h2tog /ct/h2- /ct/h2+ /ct/h2vol /ct/h2transp"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-245",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 154.0, 97.0, 107.0, 22.0 ],
									"text" : "r #0_OSCin"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-1", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-1", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-1", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-245", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "sliderGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
									"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 2588.666748000000098, 544.0, 100.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p harmony2OSC"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-246",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 8,
							"minor" : 1,
							"revision" : 0,
							"architecture" : "x64",
							"modernui" : 1
						}
,
						"classnamespace" : "box",
						"rect" : [ 438.0, 271.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 1,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 1,
						"objectsnaponopen" : 1,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"lefttoolbarpinned" : 0,
						"toptoolbarpinned" : 0,
						"righttoolbarpinned" : 0,
						"bottomtoolbarpinned" : 0,
						"toolbars_unpinned_last_save" : 0,
						"tallnewobj" : 0,
						"boxanimatetime" : 200,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"style" : "",
						"subpatcher_template" : "",
						"boxes" : [ 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-2",
									"index" : 4,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 351.25, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-10",
									"index" : 3,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 303.25, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-9",
									"index" : 2,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 202.25, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-8",
									"index" : 1,
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 154.0, 357.0, 30.0, 30.0 ]
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-7",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 250.5, 192.0, 36.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-6",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 2,
									"outlettype" : [ "bang", "" ],
									"patching_rect" : [ 202.25, 196.0, 36.0, 22.0 ],
									"text" : "sel 1"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 250.5, 251.0, 29.5, 22.0 ],
									"text" : "inc"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-3",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 202.25, 251.0, 30.0, 22.0 ],
									"text" : "dec"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-1",
									"maxclass" : "newobj",
									"numinlets" : 6,
									"numoutlets" : 6,
									"outlettype" : [ "", "", "", "", "", "" ],
									"patching_rect" : [ 154.0, 138.0, 276.0, 22.0 ],
									"text" : "route /ct/h1tog /ct/h1- /ct/h1+ /ct/h1vol /ct/h1transp"
								}

							}
, 							{
								"box" : 								{
									"id" : "obj-245",
									"maxclass" : "newobj",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 154.0, 73.0, 97.0, 22.0 ],
									"text" : "r #0_OSCin"
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"source" : [ "obj-1", 3 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-2", 0 ],
									"source" : [ "obj-1", 4 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-6", 0 ],
									"source" : [ "obj-1", 1 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-7", 0 ],
									"source" : [ "obj-1", 2 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"source" : [ "obj-1", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-1", 0 ],
									"source" : [ "obj-245", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-3", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-3", 0 ],
									"source" : [ "obj-6", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"source" : [ "obj-7", 0 ]
								}

							}
 ],
						"styles" : [ 							{
								"name" : "AudioStatus_Menu",
								"default" : 								{
									"bgfillcolor" : 									{
										"type" : "color",
										"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
										"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
										"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
										"angle" : 270.0,
										"proportion" : 0.39,
										"autogradient" : 0
									}

								}
,
								"parentstyle" : "",
								"multi" : 0
							}
, 							{
								"name" : "sliderGold-1",
								"default" : 								{
									"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
									"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ]
								}
,
								"parentstyle" : "",
								"multi" : 0
							}
 ]
					}
,
					"patching_rect" : [ 2630.0, 91.0, 100.0, 22.0 ],
					"saved_object_attributes" : 					{
						"description" : "",
						"digest" : "",
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p harmony1OSC"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-238",
					"linecount" : 2,
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 3399.0, 465.0, 57.0, 35.0 ],
					"text" : "route /ct/vstvol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-237",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3454.0, 416.333312999999976, 85.0, 22.0 ],
					"text" : "r #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-234",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 772.0, 1007.0, 125.0, 22.0 ],
					"text" : "route /ct/dry /ct/dryvol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-233",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 772.0, 974.0, 85.0, 22.0 ],
					"text" : "r #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-232",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 11.410399999999999, 280.0, 93.0, 22.0 ],
					"text" : "route /ct/inmute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-231",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 11.410399999999999, 254.0, 85.0, 22.0 ],
					"text" : "r #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-230",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 384.5, 570.0, 99.0, 22.0 ],
					"text" : "scale 0. 1. 0 160"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-229",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 506.0, 545.0, 150.0, 22.0 ],
					"text" : "route /ct/invol /ct/gainknob"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-228",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 506.0, 519.0, 85.0, 22.0 ],
					"text" : "r #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-227",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 467.0, 602.033324999999991, 99.0, 22.0 ],
					"text" : "scale 0. 1. 1 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-218",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 172.0, 671.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-221",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 766.044800000000009, 649.5, 87.0, 22.0 ],
					"text" : "s #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-222",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 766.044800000000009, 569.0, 79.0, 22.0 ],
					"text" : "prepend port"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-240",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 766.044800000000009, 606.0, 99.0, 22.0 ],
					"text" : "udpreceive 8001"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-209",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 125.0, 1037.0, 89.0, 22.0 ],
					"text" : "route /ct/outvol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-217",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 172.0, 992.0, 85.0, 22.0 ],
					"text" : "r #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-214",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 50.0, 25.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-205",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 17.910399999999999, 333.0, 60.0, 22.0 ],
					"text" : "loadbang"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-194",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 2469.5, 1718.0, 34.0, 22.0 ],
					"text" : "+ 50"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-57",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 404.5, 687.0, 43.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 50.25, 89.0, 33.0, 15.0 ],
					"text" : "GAIN"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 88.0, 617.333374000000049, 79.0, 22.0 ],
					"text" : "loadmess 50"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"id" : "obj-21",
					"maxclass" : "dial",
					"needlecolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"outlinecolor" : [ 0.376471, 0.384314, 0.4, 0.36 ],
					"parameter_enable" : 0,
					"patching_rect" : [ 356.0, 687.0, 40.0, 40.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 77.238799999999998, 86.0, 18.522400000000001, 18.522400000000001 ],
					"size" : 100.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1808.666625999999951, 49.0, 82.0, 22.0 ],
					"text" : "loadmess 0.3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-56",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 206.0, 705.0, 34.0, 22.0 ],
					"text" : "+ 20"
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-10",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1715.833251999999902, 153.0, 103.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 110.147400000000005, 97.5, 69.0, 12.5 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-317",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 53.660400000000003, 163.0, 29.5, 22.0 ],
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-316",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 312.0, 1148.833251999999902, 41.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 272.36459747276308, 150.0, 35.0, 15.0 ],
					"text" : "MUTE"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-310",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 233.0, 1287.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-308",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 312.0, 1264.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-306",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 285.0, 1216.833251999999902, 36.0, 22.0 ],
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-301",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 285.0, 1145.833251999999902, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 276.864597472763137, 130.449998319149017, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-297",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2401.166748000000098, 1623.333374000000049, 125.0, 22.0 ],
					"text" : "receive~ #0_harm2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-298",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2275.166748000000098, 1623.333374000000049, 125.0, 22.0 ],
					"text" : "receive~ #0_harm1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-296",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2530.166748000000098, 1670.333374000000049, 85.0, 19.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 154.480731999999989, 135.449998319149017, 72.466653041723305, 15.0 ],
					"relative" : 1,
					"size" : 158.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-295",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2530.166748000000098, 1633.0, 85.0, 22.0 ],
					"text" : "loadmess 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-294",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2401.166748000000098, 1766.0, 57.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-293",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2275.166748000000098, 1766.0, 57.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-290",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2401.166748000000098, 1813.333374000000049, 102.0, 22.0 ],
					"text" : "send~ #0_out2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-291",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2275.166748000000098, 1813.333374000000049, 102.0, 22.0 ],
					"text" : "send~ #0_out1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 10.0,
					"id" : "obj-289",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 84.772400000000005, 197.0, 45.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 46.544799999999995, 127.375, 40.0, 18.0 ],
					"text" : "MUTE"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-288",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 359.0, 72.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-282",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 443.5, 44.0, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-281",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 484.5, 232.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-279",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 611.544800000000009, 232.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-278",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 649.63439900000003, 232.0, 29.5, 22.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-276",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 390.5, 196.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-271",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 443.5, 101.0, 120.5, 22.0 ],
					"text" : "sel 1 2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-267",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 611.544800000000009, 275.0, 63.0, 22.0 ],
					"text" : "hidden $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-266",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 390.5, 239.0, 63.0, 22.0 ],
					"text" : "hidden $1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 9.0,
					"hidden" : 1,
					"id" : "obj-262",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 649.63439900000003, 326.0, 79.0, 28.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 21.75, 50.5, 82.0, 28.0 ],
					"text" : "Drag Sound File \n"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"hidden" : 1,
					"id" : "obj-256",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 394.0, 290.0, 74.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 30.25, 50.5, 57.977599999999995, 15.0 ],
					"text" : "CHANNELS"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-252",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 517.0, 1082.666625999999951, 72.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-249",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 17.910399999999999, 388.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-247",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 43.910400000000003, 440.0, 49.0, 20.0 ],
					"text" : "mute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-241",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 17.910399999999999, 520.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-239",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 17.910399999999999, 492.0, 36.0, 22.0 ],
					"text" : "sel 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-236",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 166.682861000000003, 568.033324999999991, 70.0, 22.0 ],
					"text" : "selector~ 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-235",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 70.0, 568.033324999999991, 70.0, 22.0 ],
					"text" : "selector~ 3"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 8.0,
					"id" : "obj-211",
					"items" : [ "Internal", "(CT)", ",", "Live", "Input", ",", "Sound", "File" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 47.0, 60.000011000000001, 100.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 25.227599999999995, 31.25, 67.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 194.682861000000003, 448.992096000000004, 128.0, 22.0 ],
					"text" : "receive~ intramoduls2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-195",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 159.0, 416.800903000000005, 128.0, 22.0 ],
					"text" : "receive~ intramoduls1"
				}

			}
, 			{
				"box" : 				{
					"clipheight" : 26.0,
					"data" : 					{
						"clips" : [ 							{
								"absolutepath" : "172_Am_LowReeseBass_01_673.wav",
								"filename" : "172_Am_LowReeseBass_01_673.wav",
								"filekind" : "audiofile",
								"loop" : 1,
								"content_state" : 								{
									"pitchcorrection" : [ 0 ],
									"followglobaltempo" : [ 0 ],
									"pitchshift" : [ 1.0 ],
									"timestretch" : [ 0 ],
									"quality" : [ "basic" ],
									"formantcorrection" : [ 0 ],
									"play" : [ 0 ],
									"formant" : [ 1.0 ],
									"speed" : [ 1.0 ],
									"originallength" : [ 0.0, "ticks" ],
									"mode" : [ "basic" ],
									"originallengthms" : [ 0.0 ],
									"originaltempo" : [ 120.0 ],
									"basictuning" : [ 440 ],
									"slurtime" : [ 0.0 ]
								}

							}
 ]
					}
,
					"hidden" : 1,
					"id" : "obj-192",
					"maxclass" : "playlist~",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "signal", "signal", "signal", "", "dictionary" ],
					"patching_rect" : [ 611.544800000000009, 377.0, 139.455199999999991, 27.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.75, 65.5, 68.977599999999995, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 10.0,
					"id" : "obj-198",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2945.25, 29.0, 88.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 239.36459747276308, 10.549999892711639, 54.0, 18.0 ],
					"text" : "VST FX"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-196",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3255.0, 77.0, 32.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 239.36459747276308, 30.549999892711639, 32.0, 17.0 ],
					"text" : "LOAD"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-182",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3254.0, 692.0, 102.0, 22.0 ],
					"text" : "send~ #0_out2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-183",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 3154.0, 692.0, 102.0, 22.0 ],
					"text" : "send~ #0_out1"
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-93",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3399.0, 516.5, 81.0, 27.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 239.36459747276308, 47.25, 92.399999618530273, 13.399999856948853 ],
					"relative" : 1,
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-160",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3154.0, 613.0, 59.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-161",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 3315.0, 567.0, 99.0, 22.0 ],
					"text" : "scale 0. 1. 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-162",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 3227.0, 613.0, 59.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"autosave" : 1,
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 8,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "signal", "signal", "", "list", "int", "", "", "" ],
					"patching_rect" : [ 3159.0, 459.0, 92.5, 22.0 ],
					"save" : [ "#N", "vst~", "loaduniqueid", 0, ";" ],
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_invisible" : 1,
							"parameter_longname" : "vst~",
							"parameter_shortname" : "vst~",
							"parameter_type" : 3
						}

					}
,
					"saved_object_attributes" : 					{
						"parameter_enable" : 1,
						"parameter_mappable" : 0
					}
,
					"text" : "vst~",
					"varname" : "vst~",
					"viewvisibility" : 0
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-45",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 3236.666748000000098, 375.333312999999976, 107.0, 22.0 ],
					"text" : "receive~ #0_in2"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-46",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 3135.5, 375.333312999999976, 107.0, 22.0 ],
					"text" : "receive~ #0_in1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-190",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1941.666625999999951, 77.0, 72.0, 22.0 ],
					"text" : "loadmess 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-188",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 75.477599999999995, 1295.833374000000049, 82.0, 22.0 ],
					"text" : "loadmess 0.8"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-187",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 287.5, 577.166687000000024, 85.0, 22.0 ],
					"text" : "loadmess 120"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 0,
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-185",
					"maxclass" : "number~",
					"mode" : 2,
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "float" ],
					"patching_rect" : [ 2221.666748000000098, 401.333344000000011, 56.0, 22.0 ],
					"sig" : 0.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-179",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1553.666625999999951, 55.0, 92.0, 22.0 ],
					"text" : "loadmess 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-177",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2404.666748000000098, 1235.333374000000049, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-178",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2335.666748000000098, 1235.333374000000049, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-173",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2420.666748000000098, 714.333312999999976, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-176",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2351.666748000000098, 714.333312999999976, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-171",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2425.666748000000098, 285.333312999999976, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-170",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2347.666748000000098, 285.333312999999976, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-165",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2625.666748000000098, 1117.333251999999902, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 108.147400000000005, 183.5, 15.199999868869781, 15.199999868869781 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-164",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2633.666748000000098, 600.333374000000049, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 108.147400000000005, 168.300000131130219, 15.199999868869781, 15.199999868869781 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-163",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2645.666748000000098, 179.333327999999995, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 108.147400000000005, 152.0, 15.199999868869781, 15.199999868869781 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-141",
					"maxclass" : "incdec",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2522.666748000000098, 1066.333251999999902, 20.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 124.647399999999976, 183.724548499999997, 12.000000000000028, 15.199999868869781 ]
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-142",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2581.5, 1213.333374000000049, 81.0, 27.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 158.947399952316289, 184.724548499999997, 48.949998140335083, 16.0 ],
					"relative" : 1,
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-144",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2322.666748000000098, 1274.333374000000049, 59.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2505.666748000000098, 1258.333374000000049, 99.0, 22.0 ],
					"text" : "scale 0. 1. 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-146",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2407.666748000000098, 1270.333374000000049, 59.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-148",
					"maxclass" : "dial",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2505.666748000000098, 1189.333374000000049, 40.0, 40.0 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-151",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2441.666748000000098, 1388.333374000000049, 112.0, 22.0 ],
					"text" : "send~ #0_harm2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-152",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2281.666748000000098, 1388.333374000000049, 112.0, 22.0 ],
					"text" : "send~ #0_harm1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-153",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2364.166748000000098, 1192.083374000000049, 107.0, 22.0 ],
					"text" : "receive~ #0_in2"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-154",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2247.333496000000196, 1192.083374000000049, 107.0, 22.0 ],
					"text" : "receive~ #0_in1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 7.0,
					"id" : "obj-155",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2451.666748000000098, 1067.333251999999902, 41.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 138.047383664855971, 183.724548499999997, 18.897400000000033, 16.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-156",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2450.666748000000098, 1137.833251999999902, 132.0, 23.0 ],
					"text" : "expr pow(2.\\,$f1/12)"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-157",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2450.666748000000098, 1166.833374000000049, 52.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-158",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 2313.666748000000098, 1301.333374000000049, 160.0, 22.0 ],
					"text" : "pfft~ harmonizer_pfft 4096 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-94",
					"maxclass" : "incdec",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2534.666748000000098, 566.5, 20.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 124.647399999999976, 168.300000131130219, 12.000000000000028, 15.199999868869781 ]
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-95",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2588.666748000000098, 686.333312999999976, 81.0, 27.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 158.947399952316289, 168.299997925758362, 48.949998140335083, 16.0 ],
					"relative" : 1,
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-96",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2335.666748000000098, 747.333312999999976, 59.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-98",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2518.666748000000098, 731.333312999999976, 99.0, 22.0 ],
					"text" : "scale 0. 1. 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-103",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2420.666748000000098, 743.333312999999976, 59.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-110",
					"maxclass" : "dial",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2518.666748000000098, 662.333312999999976, 40.0, 40.0 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-115",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2454.666748000000098, 861.333312999999976, 112.0, 22.0 ],
					"text" : "send~ #0_harm2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-116",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2294.666748000000098, 861.333312999999976, 112.0, 22.0 ],
					"text" : "send~ #0_harm1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2397.166748000000098, 676.333312999999976, 107.0, 22.0 ],
					"text" : "receive~ #0_in2"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2285.666748000000098, 671.333312999999976, 107.0, 22.0 ],
					"text" : "receive~ #0_in1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 7.0,
					"id" : "obj-121",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2463.666748000000098, 564.0, 41.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 138.047383664855971, 168.299997925758362, 18.897400000000033, 16.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-126",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2463.666748000000098, 592.5, 132.0, 23.0 ],
					"text" : "expr pow(2.\\,$f1/12)"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-127",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2463.666748000000098, 639.833312999999976, 52.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-129",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 2326.666748000000098, 774.333312999999976, 160.0, 22.0 ],
					"text" : "pfft~ harmonizer_pfft 4096 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-88",
					"maxclass" : "incdec",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2546.666748000000098, 133.0, 20.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 124.647399999999976, 152.0, 12.000000000000028, 15.199999868869781 ]
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-79",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2605.166748000000098, 285.333312999999976, 81.0, 27.5 ],
					"presentation" : 1,
					"presentation_rect" : [ 158.947399952316289, 151.0, 48.949998140335083, 16.0 ],
					"relative" : 1,
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-72",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2347.666748000000098, 331.333344000000011, 59.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-68",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2534.666748000000098, 344.500030999999979, 99.0, 22.0 ],
					"text" : "scale 0. 1. 0 127"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-59",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2432.666748000000098, 327.333344000000011, 59.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-50",
					"maxclass" : "dial",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2534.666748000000098, 275.5, 40.0, 40.0 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2466.666748000000098, 445.333344000000011, 112.0, 22.0 ],
					"text" : "send~ #0_harm2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2306.666748000000098, 445.333344000000011, 112.0, 22.0 ],
					"text" : "send~ #0_harm1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2363.666748000000098, 223.833327999999995, 107.0, 22.0 ],
					"text" : "receive~ #0_in2"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 2324.666748000000098, 194.833327999999995, 107.0, 22.0 ],
					"text" : "receive~ #0_in1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-42",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1400.666625999999951, 288.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1465.666625999999951, 246.0, 24.0, 24.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-34",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "float", "" ],
					"patching_rect" : [ 1465.666625999999951, 284.0, 37.0, 22.0 ],
					"text" : "timer"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 9.0,
					"id" : "obj-33",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1465.666625999999951, 218.0, 27.0, 19.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 184.147400000000005, 13.75, 27.0, 19.0 ],
					"text" : "TAP"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 10,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1275.666625999999951, 1001.0, 58.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 203.647385000000043, 65.75, 12.0, 58.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 10,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 1542.166625999999951, 1006.0, 58.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 214.947385041723294, 65.75, 12.0, 58.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 47.0, 1527.333374000000049, 105.0, 22.0 ],
					"text" : "scale 0. 1. 0. 128."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-12",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 103.977599999999995, 1591.833374000000049, 36.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-13",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 47.0, 1482.333374000000049, 111.955200000000005, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 242.443897472763069, 109.5, 25.841400000000021, 84.899999439716339 ],
					"prototypename" : "Slider01float",
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 47.0, 1591.833374000000049, 36.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 20.0,
					"id" : "obj-9",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 152.5, 1178.833251999999902, 96.0, 29.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 239.36459747276308, 80.0, 96.0, 29.0 ],
					"text" : "OUTPUT"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 10.0,
					"id" : "obj-4",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 367.5, 617.333374000000049, 44.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.772399999999998, 87.0, 34.0, 18.0 ],
					"text" : "VOL"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-138",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1143.089600000000019, 140.0, 40.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 194.947385041723294, 51.25, 36.0, 15.0 ],
					"text" : "CLEAN"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-137",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 993.5, 240.0, 35.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 197.947385041723294, 37.25, 31.399999916553497, 15.0 ],
					"text" : "MUTE"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-135",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 897.0, 208.0, 34.910400000000003, 20.0 ],
					"text" : "ON"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-132",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 108.5, 1619.333374000000049, 58.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 309.836997282028221, 109.5, 12.0, 84.899999439716339 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-133",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numleds" : 20,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 47.0, 1619.333374000000049, 58.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 320.417197282028212, 109.5, 12.0, 84.899999439716339 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-131",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1535.166625999999951, 124.0, 70.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 109.147400000000005, 65.75, 54.102599999999995, 15.0 ],
					"text" : "Max. Delay"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-130",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1485.666625999999951, 88.0, 50.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-128",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1350.666625999999951, 92.0, 43.0, 22.0 ],
					"text" : "set $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1350.666625999999951, 62.0, 129.0, 22.0 ],
					"text" : "sprintf symout 0-%ims"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-122",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1670.666625999999951, 349.0, 47.0, 22.0 ],
					"text" : "$1 100"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-120",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "bang" ],
					"patching_rect" : [ 1670.666625999999951, 382.0, 48.0, 22.0 ],
					"text" : "line~ 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-117",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 1549.166625999999951, 223.0, 57.0, 22.0 ],
					"text" : "del 1000"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-114",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1618.666625999999951, 309.0, 29.5, 22.0 ],
					"text" : "0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 1582.666625999999951, 189.0, 34.0, 22.0 ],
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1582.666625999999951, 153.0, 93.0, 22.0 ],
					"text" : "r #0_dlyclean"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-107",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1104.0, 105.0, 36.0, 22.0 ],
					"text" : "sel 0"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-106",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1104.0, 57.999996000000003, 91.0, 22.0 ],
					"text" : "r #0_dlyonoff"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1105.0, 212.0, 95.0, 22.0 ],
					"text" : "s #0_dlyclean"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-104",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1105.0, 140.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 184.147400000000005, 50.25, 15.0, 15.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-102",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 993.5, 178.0, 79.0, 22.0 ],
					"text" : "scale 1 0 0 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-100",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1211.166625999999951, 1015.0, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1261.166625999999951, 960.800902999999948, 91.0, 22.0 ],
					"text" : "r #0_dlymute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-99",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1470.166625999999951, 1015.0, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-92",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1520.166625999999951, 960.800902999999948, 91.0, 22.0 ],
					"text" : "r #0_dlymute"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-90",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 993.5, 212.0, 93.0, 22.0 ],
					"text" : "s #0_dlymute"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.71 ],
					"id" : "obj-89",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 993.5, 143.0, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 184.147400000000005, 36.25, 15.0, 15.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-53",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 897.0, 170.0, 93.0, 22.0 ],
					"text" : "s #0_dlyonoff"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-109",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 897.0, 80.0, 72.0, 22.0 ],
					"text" : "loadmess 0"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.71 ],
					"id" : "obj-97",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 897.0, 143.0, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 112.25, 13.75, 18.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 7.0,
					"id" : "obj-86",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2475.666748000000098, 134.0, 42.0, 16.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 138.249999999999972, 151.0, 18.897400000000033, 16.0 ],
					"triangle" : 0
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 2475.666748000000098, 194.833327999999995, 132.0, 23.0 ],
					"text" : "expr pow(2.\\,$f1/12)"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"format" : 6,
					"id" : "obj-87",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 2475.666748000000098, 223.833327999999995, 52.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 2338.666748000000098, 358.333344000000011, 160.0, 22.0 ],
					"text" : "pfft~ harmonizer_pfft 4096 4"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 608.0, 1082.666625999999951, 92.0, 22.0 ],
					"text" : "r #0_dryonoff"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 8.0,
					"id" : "obj-83",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 2410.166748000000098, 7.333334, 72.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 104.147400000000005, 134.449998319149017, 53.0, 15.0 ],
					"text" : "HARMONY"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 16.0,
					"id" : "obj-82",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 253.666672000000005, 9.000011000000001, 80.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 29.227599999999995, 6.75, 59.0, 24.0 ],
					"text" : "INPUT"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-81",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 545.0, 1199.666625999999951, 105.0, 22.0 ],
					"text" : "scale 0. 1. 0. 128."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-80",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 608.0, 1115.666625999999951, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.772399999999998, 170.0, 16.75, 16.75 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.279348, 0.29304, 0.244776, 1.0 ],
					"id" : "obj-77",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 652.5, 1384.666748000000098, 102.0, 22.0 ],
					"text" : "send~ #0_out2"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.279348, 0.29304, 0.244776, 1.0 ],
					"id" : "obj-78",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 545.0, 1384.666748000000098, 102.0, 22.0 ],
					"text" : "send~ #0_out1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"color" : [ 0.490196, 0.498039, 0.517647, 1.0 ],
					"id" : "obj-75",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 693.24377400000003, 1237.666625999999951, 107.0, 22.0 ],
					"text" : "receive~ #0_in2"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"color" : [ 0.490196, 0.498039, 0.517647, 1.0 ],
					"id" : "obj-76",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 578.910399999999981, 1237.666625999999951, 107.0, 22.0 ],
					"text" : "receive~ #0_in1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-74",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 602.0, 1300.166625999999951, 36.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 10.0,
					"id" : "obj-73",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 670.211487000000034, 1157.166625999999951, 47.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 43.25, 172.0, 46.0, 18.0 ],
					"text" : "DRY"
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-70",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 545.0, 1154.666625999999951, 111.955200000000005, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.772399999999998, 188.724548499999997, 66.477599999999995, 11.525451500000003 ],
					"prototypename" : "Slider01float",
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-71",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 545.0, 1300.166625999999951, 36.0, 18.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-69",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1348.666625999999951, 288.0, 41.0, 22.0 ],
					"text" : "$1 10"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 1348.666625999999951, 316.0, 43.0, 22.0 ],
					"text" : "line 0."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1517.166625999999951, 759.0, 113.0, 22.0 ],
					"text" : "receive~ #0_dfdbck"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"fontface" : 1,
					"fontsize" : 12.0,
					"format" : 6,
					"htricolor" : [ 0.815686, 0.858824, 0.34902, 0.0 ],
					"id" : "obj-64",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1902.333251999999902, 247.0, 50.0, 22.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"tricolor" : [ 0.490196, 0.498039, 0.517647, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1475.166625999999951, 468.0, 91.0, 22.0 ],
					"text" : "r #0_dlyonoff"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1670.666625999999951, 435.0, 100.0, 22.0 ],
					"text" : "send~ #0_dfdbck"
				}

			}
, 			{
				"box" : 				{
					"degrees" : 297,
					"floatoutput" : 1,
					"id" : "obj-61",
					"maxclass" : "dial",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1670.666625999999951, 147.0, 40.0, 40.0 ],
					"size" : 1.0,
					"thickness" : 81.260000000000005
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"fontface" : 1,
					"fontsize" : 8.0,
					"format" : 6,
					"htricolor" : [ 0.815686, 0.858824, 0.34902, 0.0 ],
					"id" : "obj-60",
					"maxclass" : "flonum",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1348.666625999999951, 387.0, 44.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 159.0, 33.25, 34.0, 17.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"tricolor" : [ 0.490196, 0.498039, 0.517647, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1217.666625999999951, 455.800903000000005, 91.0, 22.0 ],
					"text" : "r #0_dlyonoff"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 10.0,
					"id" : "obj-52",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1482.166625999999951, 10.0, 54.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 132.25, 13.75, 48.0, 18.0 ],
					"text" : "DELAY"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"hidden" : 1,
					"id" : "obj-49",
					"maxclass" : "number",
					"maximum" : 256,
					"minimum" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 453.5, 320.5, 45.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 55.25, 68.0, 24.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"hidden" : 1,
					"id" : "obj-47",
					"maxclass" : "number",
					"maximum" : 256,
					"minimum" : 1,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 390.5, 327.0, 45.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 24.25, 66.5, 24.0, 17.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 390.5, 373.0, 82.0, 22.0 ],
					"text" : "pak"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"fontface" : 0,
					"fontsize" : 8.0,
					"htricolor" : [ 0.815686, 0.858824, 0.34902, 0.0 ],
					"id" : "obj-36",
					"maxclass" : "number",
					"maximum" : 9999,
					"minimum" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1461.166625999999951, 122.0, 53.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 162.147400000000005, 65.75, 32.0, 17.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0,
					"tricolor" : [ 0.490196, 0.498039, 0.517647, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1475.166625999999951, 523.0, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1217.666625999999951, 518.0, 41.0, 22.0 ],
					"text" : "gate~"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-204",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1868.833251999999902, 316.0, 92.0, 22.0 ],
					"text" : "scale 0. 1. 0 80"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-203",
					"maxclass" : "newobj",
					"numinlets" : 6,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1350.666625999999951, 218.0, 112.0, 22.0 ],
					"text" : "scale 0. 1. 0. 1000."
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-202",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 1654.166625999999951, 79.0, 203.0, 22.0 ],
					"text" : "route /ct/dlytime /ct/dlyfdbk /ct/dlyvol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-201",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1701.166625999999951, 34.0, 85.0, 22.0 ],
					"text" : "r #0_OSCin"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-191",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1470.166625999999951, 1062.0, 102.0, 22.0 ],
					"text" : "send~ #0_out2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-189",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1212.666625999999951, 1062.0, 102.0, 22.0 ],
					"text" : "send~ #0_out1"
				}

			}
, 			{
				"box" : 				{
					"fontsize" : 8.0,
					"id" : "obj-181",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1955.666625999999951, 247.0, 57.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 182.897398092651372, 109.5, 25.0, 15.0 ],
					"text" : "Vol"
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-180",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1868.833251999999902, 212.0, 103.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 184.897398092651372, 67.75, 16.65000182390213, 42.25 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-175",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1470.166625999999951, 905.5, 132.0, 18.5 ],
					"size" : 100
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-174",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1212.666625999999951, 905.5, 132.0, 18.5 ],
					"size" : 100
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-167",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1525.166625999999951, 534.0, 133.0, 22.0 ],
					"text" : "receive~ #0_tofeedR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-168",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1505.166625999999951, 822.0, 120.0, 22.0 ],
					"text" : "send~ #0_tofeedR"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-169",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1470.166625999999951, 793.0, 29.5, 22.0 ],
					"text" : "*~"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 8.0,
					"id" : "obj-159",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1400.666625999999951, 387.0, 92.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 112.25, 34.25, 51.0, 15.0 ],
					"text" : "Time (ms)"
				}

			}
, 			{
				"box" : 				{
					"floatoutput" : 1,
					"id" : "obj-147",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1350.666625999999951, 152.0, 103.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 111.147400000000005, 50.25, 68.0, 14.0 ],
					"size" : 1.0
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 8.0,
					"id" : "obj-143",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1728.666625999999951, 284.0, 90.0, 15.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 109.147400000000005, 80.0, 48.0, 15.0 ],
					"text" : "Feedback"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-140",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1275.666625999999951, 573.0, 131.0, 22.0 ],
					"text" : "receive~ #0_tofeedL"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1247.666625999999951, 828.0, 118.0, 22.0 ],
					"text" : "send~ #0_tofeedL"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"fontface" : 1,
					"fontsize" : 8.0,
					"format" : 6,
					"htricolor" : [ 0.815686, 0.858824, 0.34902, 0.0 ],
					"id" : "obj-136",
					"maxclass" : "flonum",
					"maximum" : 1.0,
					"minimum" : 0.0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1670.666625999999951, 284.0, 43.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 162.147400000000005, 79.0, 43.0, 17.0 ],
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"triangle" : 0,
					"tricolor" : [ 0.490196, 0.498039, 0.517647, 0.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1212.666625999999951, 793.0, 29.5, 22.0 ],
					"text" : "*~"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1497.166625999999951, 491.0, 107.0, 22.0 ],
					"text" : "receive~ #0_in2"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 1239.666625999999951, 483.800903000000005, 107.0, 22.0 ],
					"text" : "receive~ #0_in1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 68.0, 833.333312999999976, 58.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.772399999999998, 146.0, 67.705199999999991, 12.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "meter~",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "float" ],
					"patching_rect" : [ 145.0, 833.333312999999976, 58.0, 13.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.772399999999998, 155.75, 67.705199999999991, 12.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "slider",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 287.5, 617.333374000000049, 71.0, 17.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 22.772399999999998, 106.5, 67.955199999999991, 14.5 ],
					"size" : 160.0
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 133.0, 713.0, 36.0, 18.0 ],
					"size" : 200
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-14",
					"maxclass" : "gain~",
					"multichannelvariant" : 0,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 70.0, 713.0, 36.0, 18.0 ],
					"size" : 200
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 53.5, 1222.333374000000049, 114.0, 22.0 ],
					"text" : "receive~ #0_out2"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "signal" ],
					"patching_rect" : [ 27.5, 1182.333251999999902, 114.0, 22.0 ],
					"text" : "receive~ #0_out1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.290196, 0.309804, 0.301961, 0.0 ],
					"checkedcolor" : [ 0.0, 0.0, 0.0, 1.0 ],
					"id" : "obj-5",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 17.910399999999999, 436.0, 24.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 25.227599999999995, 125.375, 20.0, 20.0 ],
					"uncheckedcolor" : [ 0.376471, 0.384314, 0.4, 0.29 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 206.0, 803.333312999999976, 94.0, 22.0 ],
					"text" : "send~ #0_in2"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ],
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 88.0, 803.333312999999976, 94.0, 22.0 ],
					"text" : "send~ #0_in1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "signal", "signal" ],
					"patching_rect" : [ 390.5, 412.0, 57.0, 22.0 ],
					"text" : "adc~ 1 2"
				}

			}
, 			{
				"box" : 				{
					"autofit" : 1,
					"background" : 1,
					"data" : [ 28543, "png", "IBkSG0fBZn....PCIgDQRA..EvN..LfxHX....f4YpA3....DLmPIQEBHf.B7g.YHB..f.PRDEDU3wI6c+GjbedeeX+yQ.BfauC.GHvg..RfaOPXwPPKR3ekD4RBrwjJYPsrIA8uZUsffb5DOwNdHckaqyjIiniSlD2opg1pY7DOx1PTdTiGWKR15V1XaJcjR0N1wxFTVlN1kh2QpeNDDh.j7.3AAfq+whi73g8t86t62uee9t6950L2Phae1mmO1hC16d+778yyHAo1ghHlXMd85W8qrZhqNmCiNaDwo5x26ot56OuGK......jIij5BnOS83sGd9J+yQz5.3mHh3NJnZhpoWHhXt030a2lKrVaJfML......X.zvZf8q7Tn2XY+6KOvcAsS+jyEsdS.VsMGX0991P......fDXPIv9FW8etVAwWOhXpxobfANs5IFnUA92pu2bs38B.....vJzODX+RgvWuEeI.dn+zyDu8SweqB5ekmz+4BA+C.....CvpZA1WOZFN+ghlmN9CEQr0DVO.UWqrE.sxP+a2eF.....nRI0A1WOZFL+8c0+ov4AJKqrM+r7Sz+JC2elxoj.....fgYoHv9CEQbhnY.8tPWA52r714ybwaE5+p8uC.....jIkUf8SDMCo+DgP5AFtr7Sx+Rmh+keB9mKDtO.....DEef80iHdnnYKuQ6toOz8b2GN1ydtgbYt14NFKlb6WetLWK47W3Jwru3qlay2W8q9Mhe+m7oys4C5PO0U+mKOPesmm7WiU7mW5xMe0jk6+fY5g5oUZWMQ2atvljA...PEUQEX+DQyf5efBZ9GZb72280xu+z6aKQsQutV9Z0uwqKpsoqrpyYsMcon9tmOWpugYu9EVe7he8wVywb9235h49Jq9+awoOy2LdoWt0+uEelO6mOlc1mumpQF30p.9mYE+ygUMhlAdWeY+yoRW4vPtmp8CoiLSO99W9c1QmXtvlc....TnJh.6OTDwiECgAiL8z6Otq671e6euVDr9Au4q88tucMeL9nWpHKOFP7RuxFiW9ra5Z99uvW65h4O+aeyAZ0Sfvq+5KDexG8IJzZjJiyEMClakeMHpQ7VWf4Z8ZP0wxu2OZmr7zrrj4hrs4AcxbB...jb4cf8mHh3gi9z1ey8eriFiO9Fey+7301PL0dq8l+4Iug0EStsK+l+4cLwaD6baKTp0HTDZ0l.rxM.XkOM.B9uu04hlmN2Yhlat5bIrV5UKcIlehnO8yc.pLxxFKztmLgTz9r...fAL4Yf82WDwiliyWWY4mx8k2yzGq10ESs62J7QsEFH+8ry91yL8Y+hu0+9JOs+tu.pLdlHhSF8Wg22HZ110NRZKC.xcqU6SpcaHvLqwq0ssAI.VMqr0CVOZ1Zb61mzwWHt1eVzU6u6pWd5o72GB.Ud4Uf8GJZ9KIT3mvwktDTWpUyrT+ZWKkA5uM2War37uw5iHt1d++K7kNe75m+hQDB5uf83QDOXTcCtudz7o35dSbc.vfj05oKXlU46OWr5eVwp8d.5uUOdq1OXiX37oarauSZlK59e95tcCFzR3.nOVdEX+ohBnmAe+G6nw9qeCQ88twXpcek3fSet7dI.5ys7S0+xOQ+B4uqctn4uL1LItNVoSD8wsbM.XU2XfUKLpU66OSNVS.qsIhl+bgOX3NBZXRqdZGxpY5x2mmZB.Vl7Hv9GJh3CkCySb+G6nw67fSFG7.qO9aV+0bh4AxcKue8e5WYcwo+FMuWJVdO5W.+w4hlmbppxox4jQDu+TWD.Pk0RWx5qzbQqCcZlNXrvvnIhlgz+fgCKA8+dlnYtUOVhqC.xrdMv9Ihl+fsc8Gh+S7i+CGem2wliac54cAtBT4r7V0yxuHd+K9qZdHNFfu7cepnYn8o1ICg0C.owpsQ.yzhuWqNgmN0mzO59hlOUiSk5BAxYGKDZO8lIhlsD7TOGKoQNMOqk4hN6.MrV+rO94h5.8Zf8OTzEmt96+XGMdOu68DeG25q5TzCLvX4md+kZOOK+x18y7Y+7wry97op75F+ciz15ANYHrd.XvRqZMPs5WfclU7m0OpIKNwU+5PgSFO.TssT62ZoeFm4t5WyjpBpJoWBruiOc8G+8cewOz+kSnWzCLzaodu+xaKOKcp8iHhG4iWIN7GOdz7TVkBOXDw+lDs1..8CZUeldkg+2pf9cB2FL8PQN0pZA.RrmIZFb+ReMz8yszKA1ehHhe8rN3eg+EmH9AuGA0CPmZ0ZKOKcw5Vv8b+sEk+GNdnHh+rRdMGZc+G8cGao1FyzX+V243YddO3XK1skTkybKbcw4uTw8+87RW3xwK8ZWnmmmW87KDexm32KGpHfgLqr0+LWz7WN9jInVn6zyspV.fJrGOZ1RqdrXHI79dIv9SEY7lh+C8O83ww+9dsdXo.f1YoVxyK70tt3zm4Rwru3qlGmT+TzqGmIh3Hk7ZRBchef2SlGaQroAiMxUhadwdOv79cewQFMlewqqieeO67c1ON4ru5Bw7uw2LSi8Kc5yFO4S+Y63ZBHW7LQyCokVwS0WiHhOcpKB.fRvGKZdWqLP+ymzsA1WOhX1rN3m4S+CoW0CPhL2War3ub1MDO6e0qGOwu6eRm1G8+EilsmlxxIhN3o2BJaSO89ii7sevLM1w1z0GSukr8zKTa8iD023Ux17ZCFZoOeLVlFWV1fgrroBm7292ISqGzm6Yh76xwihSiPf8.vvkmJZ1N3lIskQwnaCrOy8V3ehe7e33C9Ax1onB.Jd+I+kaK9Q9.+ZYc3OUTN297KIyO8V.sWmrAC6byiF6bz0kowN4FGIlb8YaCF14HeyXWKdwLM1AIe8Q1P7RKd8q5qe5KccwoWX0eJPVqMMP6OhR1GHzdbp5lHh3URcQ..j.OUz7f+MWZKi7U2FX+iEQbuYYf5c8.T87e+uvaDexG8Ix5v6k1mVmPuqGFx8c9s+s8F020NeirL1ctsZKLYsqusicyq+5lZpZQL1HYqEIs+QdiX7EublFaU0Z8zFrZOgAye4EiYOy7s70zZhF5kxKgdxtGNh3ARcQ..jH+bQySb+.g02kuuFYcfG7l6xU..JLuyCNY7IezLO7CEkS+g6DkvZ.Tg8m7m9mso+jH1TpqiNvyDY+hu5Todry9.26DuyZWZ0auIadU9902ZDeWeus7k9nmarVNey9Ml+.m+hW5ZtzId0W6B65qdluwtV42+q809565Lm4k+arp0FozDot.HSdvn4eWvIhHlJskBT9duG+XotDfgRG3Va88c9Bmew3K8BuZKes+vOyyzosp2r3CEMOfAmHF.5u8cyolriNAj+e9+1IhCNsSXO.UIO0e5MD+X+D+pYc3k0iB+bgeAS.JROUGL1Yp.iMhl+tGsJv3V88mHZc+Vud3yW5Ekc6wihW8HC2IcSO89i+G94aDG3cl08OD.H6V3BqOd4uxnwW+Kut3Ud4KG+m+BuT73YuS.rV56amecSf8mH5fKDvu3+Q6xI.UMuzqrw3ccz+8Yc3kwEOa8nCtLymd58WD6HO.Tsj0MX3rQ1OIUKM1UKb+kuQ.KM15W8qkahX34NWoru.5o3ko6jtS9a+9ia7.uVITN..MsvEVe7kdtwim8O6hwuym7OnW989+XQe7Swe2zRbV8GiV.nuvN21BcRn2kweueiNYv+a+A9thSeo+1wi+W8xt3EAXv0Q5fwlo6WqtPVm2yEMeRwFDCw+gScAPtqQ6Fv+5ewer3FOfSVO.Tt13nWJNv67rwAdmQ78e7CEO2edi3y8+67wG8W92rSmp2+U+mK0x35qzMmv9YhL9COe7228Eenexx5tJD.5D+b+aWLdjO9ik0gWz+k4OTzrmy0V2+Qe2wuzsswLMoewQFMlewqKSic0tHFakuvK85Ydrm7292IyiE.nh4bQDOVzbyHxh4JnwR95rQDstoCGM6E3efOneOd.n53UOyFhY9+Z8wG4C+azou0mIZtQ08Ug16D1CvPpo22V5jgWzW7rMx5.umCriHhr83YeyKdgLW.29XYdnQLc1G5+hel2SlG6mOxdQTDavvqd9E7DK..K2Vi25DpkR8UWvyUb0i0Hr9Hh3G3DaLh3hkRw..jEaY6WL99O9EiFeu+WE+1mbg3S7HOZVeq2Qzre1eeEVwU.5l.6WyObe4tsaYhn4gx..pZl5Fu9NY30ihMv9VcgB1R21ltTDKVfURBc6w7YerExFLrw3W51x1FL75irt34WbSYZryu3HwKb9rUAye4EiYOS19+O7kN8Yim7o+rYahAf9YcRqFpSZkREkdcCFdrn394tVyCf2O0G7GM1x1y3GZC.Tx1x1uX7A9fiD20euer3+o+Yyj01768FMCs+DEYskm5z.6c55AX.wja6xcxvOTz7WdrnjoeQ7omd+czolmhy3Kd4ruACiDw6pS1fgr9veL8DQ72JaavvWejMDuzhYaSpN8ktt3zKjscE5ktvkiW50x1+MoMX.fgF85FL7ghHd7nYvB48o5eM+c5O321FhHDXO.Tscf24Yi+m+U+aEOxG4VhG+Qehr7Vd+QyVw2CUj0UdoSCrOym.xHh3f2bGN6.PoYe6J6ml6nX2v1L+YKG4a+fEXYvfrcs3Eick0Gu+0GY+mPZrHhcjwwVPavvbKbcw4uT11fgYe0Eh4eiuYlF6S8m9rY8Dq..4ukNMf48ivei05EOv6bPnq+..CC1x1uX7O9gFM129+QyZus+CEMeB1JxCiXtnSCrudQTD.P4a7QuTmL7NZCa6PYdy.14lGMhvIrmAecxFLb6aLhHa2CyQGbMIDwzGLhHaaRVmbAO2IavfK3Yfgb2azLv97LXg5q1K7dO9wxwkA.nb78e7yG2399whe1G3WKKC+jQyOKrRuC0EZf8GbZ8ud.pxN9669hG4imoeGvpP+fM14nqK0k.PKzQWvycxFL3BdFf7Nv9oVsWX6SNVnc3..8i9NZb13eyux+f3m9e3uZ6F5ViletZiBun5AcykNK.LfX7ZanSFd8nYOeKu0n.lS.VUtfm6rK3Yav.jT0Kq4Za6P7..P+qu0+1uR7+3+r2e7K7y+wZ2PORDwCFQ7vEeU0cJrdX+wee4cq1C.xaSs2ZcxvqGESf8Y1AGKasQC.FTTItfm6fMXvE7LToUesdwZ0xVqMC.np5d9Aes37mOS8z9GJZ1dbpjsFmNMv9h7RGD.JYi0Y+hY0Knx..FPLHeAO2IavfK3YpnpuVu3ttoKWRkA.Pw4u+OzEiW74OZ73O5SrVCaqQySX+IJkhpCUXOya21sLQDgdXO.UYSs6qzICudAUFMJn4E.XMUnavPVUAtfm6jMXvE7bes5ot...JZabzKE+H+2t83yep82tCEw6OZdR6mqLpqNglTG.jUdJq..RnB6BdtC1fg9oK34HFH1fA+7W..cnIuoyG+3OXi3m8AZ6Sw3IiJ3gHrvBrexaXcE0TC.4jCNcG8jPk46wjhRmbQQB.PZk9K34ruACk8E7bGbWKr0rsRYhv+AfgFeGMNa7dO9whOwi7nq0vNRzLv9YJiZJqJt.62l9eG.CXpWPyax2H...FtU1Wvy+624MEO4S2AyQ9XM+Yt171VnrpC.fRw64GYr3S7HscXOXTwBruvtF3qsoKUTSM.jit+iczrNzoJnR3NJn4E..Hi1x1y384..PehIuoyG+Tevez1Mr6MpX2yKcZf8GIqCr9t01B.nev3im0FbaDgSCO..zuxOGG.Lzow2alNT4OTAWFcjB6D1C.CjRVuO8tO7clpkF..FD3oZD.F5rksewrbJ6uunBsw1ERf82yce3hXZAfBvscKUlOSZMs2I6OpS....fpiLbJ62ZzLz9JgBIv98rmanHlV.H8R1IrG..FTbvwVL0k..vPisr8KFu2ier1MrSTBkRlnk3..cBGyc..X.zBWX8otD..JL20eus0tgbjnhb4yVHA12uzdE.fHN3MmzkuQRWc..fHhHd4uxnotD..JLG3cd1X5o2e6FVkns33D1C.chFoZg24l8KQB.vPo5ot...FD7C+idWsaHmnDJi1Rf8.PegcN55RcI...oP8bXNZjCyA.Pes63uyka6PhJPq.tPBrOwsWA.nCrucMepKA.....JTSdSmuuns33D1CvPtwG8RcxvORQUG.....TjdO2+2c6FRiRnLVSBrG......Xf29ukM1tgLXdB60dE.n+x8b2GN0k......Epuka+0Z2P1ZDwgJgRYUUHA12gsWA.Hw1ydtgNY30KnxXMUa8ijhkE....X.wFG8Rw8dri1tg0nDJkUUmDXe8hpH.f9J0SxhtwqjhkE..xciMR06mqYG23ERcI..TJ1292d6FReyIrudVFTFtocA...fgV27hUuvw2nmTd.XHQF5i8MJgxXUk6sDm65Nu87dJAfB1scKSj5R.....fB2eiabg1MjohHRVPIEROrG.FnIce....f9RSdSmOKCKYsEmNIvdAz..Qj3d4F..LjodQu.Y3x2C.XfRU9hmsSBrOSAzrycLVWVJ.PpLVMOvU..PEU8hdAFayapnWB.fJkI20VZ2PpWBkQKk6IzL41u97dJAfB1T69JotD.....nTrm8MZ6FR8RnLZIGoR.nuv9G4MRcI.....L.XaaecsaHGoLpiVQOrG.5T0SwhN9hWNEKK..LHnwZ8hG3V2ZIUF..UCaa6WJKCqdAWFsTt2C60GjAn+yNlniN850Knx.....fB2NtwKjkgUufKiVJ2SWWePFf9O6baKjpk1SuE....PoZiiloSXelN.64MGGd.HkRxG9A....Lba5o2e6FRRNjgBrG....JY2+Qe2otDdS6+crXpKA.fR265tti1MjJ+Ir2ofDfAX2yce3TWB..vPisTaiotD..XsU4Og8Y5Ziu1lxT++A.pX1ydtgrNTafK..zew8FD.vJr8IGqcCodITFWibuk3Te2ym2SI.TsjoMvMOc2G9NK6kD..Fj3.W..rBaaGqucCYpxnNVI8vd.nxauS5PgA.vPq5E8Bria7BE8R..PFk0.6kTB.C3Fu1FRcI...bspWzKvFGUqsE.XUT5OkZYMvdO9b.Lfap8VK0k...PwvgvC.n6T5eFpVhC.jR9kGA.fh2cj5B..npY+uiEScIzR4Zf82+wNZdNc.P0UdEztmfK..HgtW+d7..qk5k8BlqA1O93aLOmN.n5RP6..v.fw17lRcI..TkUurWP8vd.HhHh52ntjF.....oTVSmQOFFfAb01zURcIrpFaSWepKA..neUiTW...jcNNk.Pk2zaQKWC..JB0FaCotD..XYDXO....Txld6ik5RHhHh8ruQScI..Tk0nrWPA1C.oT8TW...PJL15FI0k...UPYMv9FYYP21snU2CP+pCN845jgmWWF4SkSyC..PqUO0E...YmSXO.zMrCs..P+g5ot...H6DXO....TcUnGTh8+NVrHmd..5PBrG....pttiTW...TdDXO.T4MUsTWA.....T7xZf8GoPqB.nR33uu6K0kPKM1HdTsA.ftTiTW...jcNg8.P23P4vbTOGlC..fdvl21BotD..XYDXO.zMxiK+r54vb...zC1x1uXpKA..VlbMv9Cdy44rA......vvCmvd....XvU8TW...jcYIv97nOEC.8AldeaI0k...P9ZpTW...jcYIv97nOEC.8ApMZ07AuZ+i7FotD..fAN26wNZpKA..VgpYxL.PU2QJyEa7EubYtb..PgaxMNRpKgXrMuoTWB..rBBrG.Rk5ot...fTYx0ekTWB..TAIvd.HUpm5B...Fv4NoC.nOSVBrudQWD.....4N2Ic..8YDXO.7lpeidvq..fgEG3V2ZpKA..VgbMYl8sq4yyoC.JY01TG0KUchs.....HGkqA1O9nWJOmN.nZqT5Ip28guyxXY..fprFot...nbn2G..UZ6cRGje..nK0H0E...cFA1C....CgpUSj...UMY4SmaTzEA.LTpQpK...XX1ttoKm5R..fUv1oC.uocLwaj5R.....fgVBrG.dS6baKj5R.....fphSU1Kn.6AftUiTW....qoCk5B..nO2YK6ETf8....vfoIRcA..PmQf8.Pk1zaerTWB..v.oa7.uVpKA..VgrDX+QxxDcO28g6wRA.3ZM15FI0k.....v.lMroKm5RnkxsSX+d1yMjWSE.LbndpK.....fgSSdSmO0kPKok3..uMk3SL0Tk0BA..UM6bjuYYrL0KiEA.f7i.6Af2lN3IlxkXF..zk10hWrLVFGPB.fdybk8BJvd.nacnTW....cm68XGM0k..P+f4J6ETf8....vPlw17lRcI..PKzt.6c5IA......nDzt.60ehAfj5fisXpKA..nejeed.f9PZIN....vfGOw7..8gDXO.71LdsMTFKS8xXQ..fAD4d360FqT9Y9..nCkaA1ea2hm1N.FDL0dqk0g1K+E+06g2K..LrI2+Et2y9FMumR.fAQyT1KnSXO.zstiTW......LHocA1WuLJB.......F1Ivd....XvS8TW...z4zRb....fAO0ScA..PmSf8.Pk1sGym5R...F3rssutTWB..zBBrG....FxrsseoTWB..zBBrG.RgIRcA.....vZ3boXQys.6m7F73zAvffCdyczvOTWtLc66C....fxvoRwh1t.6qm0IZxsc4dqR.f9QNo7..P0jCHA.PenbKvd....fJCGrB.f9P5g8....vPlMusERcI..PKHvd....XHyV19EScI..PKHvd.nx5tO7cl5R.....fRi.6AfJq8NoVuJ..Ctt+i9tScI..PEi.6A...fDXK01XQN80KxIG.fhQ6Br2QaDfgL6Xh2nSFd8tbYNTW99...xloRcA..PmqcA1eGYch12tluGKE.nJXmaagNY306xkwFBC..IxzSu+TWB..rJxsVhy3ido7Zp.....JHuq6JymMO..JY5g8.......TAHvd.......p.DXO.......U.BrG.prld6ik5R...5G0H0E...cGA1C.UVistQRcI.....PoYsBrehRqJ.....VMMxyIauSsk7b5..HGsVA1enRqJ.fJkomd+Ycn06xk3Hc46C..nGswZdJFA.ppxkVhy8erilGSC.TQbW24sm0gVu.KC.....FpjKA1O93aLOlF.......FZ4RmE......f2t5oXQEXO....LXoQpK..fA.SkhEUf8.......TAHvd.nxZpZotB..fAOaa6qK0k...qBA1C.UViMxhotD..fANaa6WJ0k...qh0Jv9FkUQ.......vvNmvd.nrUO0E......UQ4Rf8iWaC4wz..UDSuusj0gdjtX5q2EuG.....F3kKA1O0dcq.BvfjZi5AvB..5icnTW...zcjHC....LXYhTW...zcDXO.......U.BrG....FhbiG30RcI..vpPf8.......TAHvd.nx51i4ScI.....PoYsBr2sJO.......kj0Jvd2p7.......TRzRb.fxlmfK..nX4m2B.nOUtDX+X0j6O.CRl7FVWmL75c3z6I3B..JVaM0E...cmbIo8o18UxioA.pHlbaWtSFd8BpL.....XnhiFO....Lj3dO1QScI..vZPf8....vPhw17lRcI..vZPf8.......TAHvd.......tVST1Kn.6AfJo6+nu6TWB..PgZmadzTWB..r1NTYufqUf80Kqh..Xk1RsMl5R...JT6bz0k5R..fJl0Jv9oJsp.....HOzH0E...cOsDG.nrU58+M.....5GHvd.nrU58+M.....5GjKA1uucMedLM.PEg+dc..XvzdmZKotD..XMjKA1O9nWJOlF.nhnC+60ch4A.f9Dar1HotD..XMnk3..8J8jd.....xABrG......fJ.A1C......Lz4dO1QScIbMDXO......vPmw17lRcIbMDXO.TI8stywScI.....PoZ0Br2EHH....z+oQpK...5dqVf8GpTqB.......Fxok3..kMOEW.....zBBrG.Ja2QpK...XXUsZhA..nJqm+j56+XGMOpC.nhY5o2epKA..fb1ttoKm5R..f0POGX+3iuw7nN.fJl65Nu8TWB.....LTwyBG.zqzS5A....FD0nrWPA1C.8pCk5B.....fAABrG......fJ.A1C......PEf.6AfJoZqejTWB.....TpDXO.TIUeiWI0k...zOpdpK...5dBrG....FbTO0E...cuUKv95kYQ.......vvNA1C.koIRcA.....PUkVhC.TlNTpK...XX1F1zkScI..vZnmCre58sk7nN.....JXSdSmO0k...qgdNv9Zi5P5CvfHaHK.....kKosC.sjMjE....fxkzX.......nBPf8.PuxEIK.....4.A1C.8psl5B.....fAABrG......fJ.A1C.UR6bjuYpKA.....JUBrG.pj10hWL0k......kJA1C....CNlH0E..v.jFk8BJvd....Xvwcj5B..ftm.6A......nBX0Br+PkZU..CKZj5B.....fppUKv9L2y6N3MmSUB.......CwzRb.fVZrZ9HB.....JSRiA.Zoo18URcI...ji9dt6Cm5R..f1Pf8....PBL+kWrTWucciauTWO..5bBrG....RfYOy7otD..nhQf8.PdHyWV4.....PqIvd.HObnTW......zuSf8.......TAHvd.nx4tO7cl5R.....fRm.6AfJm8NoVhO....vvGA1C......PEf.6A......nBPf8.......TAzyA1WaSWJOpC.X3flSO.....qhdNv956d97nN.fgCGJ0E......UUZIN.......TAHvd.nkzxy.....nbsZA1qkE.vPNs7L.....JWqVf8asTqB....fdU8TW...zazRb....fAC0ScA..PuQf8.......TAHvd.HOTO0E......86jLTRjb...H.jDQAQEXO.jGpm5B.....f9cBrG......fJ.A1C.UNisoqO0k......kNA1C.UNSukMl5R.....fRm.6A......nBPf8.......TAzSA1eO28gyq5.......fgZ8Tf86YO2PdUG.......LTSKwA.JSSj5B.....fpJA1C.ko6H0E...zG5T4wjr8IGKOlF..JPBrG....p1NadLIaaGqOOlF..JPBrG.VUtbwA....n7Hvd.XU4xEG....XP0At0sl5R3ZHvd.......p.DXO.......U.BrG......fJ.A1C......PEf.6Af7PiTW......zuqUA12nrKB....XXyqd9ERcI..PEiSXO....j.exm32K0k...ULBrG......fJ.A1C......PEPOEX+301PdUG..uoCN1hotD.....nz0SA1O0dqkW0A.......C0zRb.......nBPf8.......TAHvd.......p.DXO......vPmcu2pW73UuJB......fB1nid4TWBWCA1C.kk5ot......nJSf8.vpZm6Xr7b5pmmSF.....CZDXO.rplb6WepKA.....FZHvd....XHPU7h0C.f2NeZM....Tsc17XRphWrd..71Ivd....nZ6Tot...nbHvd.......p.DXO.jGpm5B.....f9cBrG.xCSk5B.....fbVo2V5DXO.......Wqb4heuSHvd.......p.DXO....Tx95irgTWB..TA0SA1evaNuJC....X3wKs30m5R..fJHmvd....Xvvbot...n2Hvd....Xvvbot...n2Hvd.......p.DXO.......U.BrG......fJ.A1C......Lz4UNy5ScIbMZUf8GpzqB.fk41i4ScI.....Lf6UNyka2PNaYTGKWqBrehxtH.....xEOSpK..fAHmprWPsDG....XvwpdR.e9+5QJy5..ftf.6A......nBPf8.vpZrZ9XB..nBXlNXrkdu1E.f7ijX.fU0T69JotD...5LqZu1849KOWYVG..Ude0W7BotDtFBrG......Xny4m+hsaHtzYA....5ZqZvB+gelmoLqC.fJu4es2ncCoza0bBrG....FbrpAKL6rOeYVG..UdO9i9Dq0KmjdIm.6A...fR1ouTg8qiulO59ekmayE05B.zW4UOyFZ2PJ81gSDBrG.xOSj5B...5Wb5EVrnl507Q2+UNy5Kp0E.nuxq8JarcCYtRnLtFBrG.xKGpMudixnH...hmZ0dgu9W5RkYc..TY87+0iztgLWITFWCA1C....CVla0dg+yegWpDKC.fpqu5Kdg1MjYJgx3ZzSA1uucMedUG.....4i4VsWnMWtd..CMdx+e9O0tgz+0C6GeTOJc....PEyLq0K9b+4t5g.fgam9KWKlc1mesFxKDs4dgonnk3.....CVVySD3b+UWtrpC.fJoW741P6FxLkPYzRBrG....Frb1HhmY0dw+S+AyVhkB.P0ym+y8JsaHyTBkQKIvd....XvyLq1K7odxmNN8WtVIVJ..UGKbg0GehG4Qa2vloDJkVRf8....vfmYVqW7Y9OttRpL..pV9+6yu41MjmIViKv8hl.6A...fpqUs01zFyrVun1hC.LrZlm3q11gTBkwpRf8....P00Y6g22iuZu3m5Ie5349ymnKmZ.f9Sm9KWKd7G8IZ2vNYITJqJA1C....Cldr05E+L+ts8B2C.Xfxe3mpsC4EhHNUwWIqNA1C....ClVy.6+DOxiFekmqs8wW.fABKbg0GejO7uQ6F1CWF0xZQf8....PIa9KuXYrLqYawIhH9ce7WsLpC.fj6O9SWKKCaM2r6xf.6A...fR1rmY9xZoN4Z8hNk8.vvfEtv5iO9uxS2tg8whHlq3ql0l.6A...fAWOVzre7tpbJ6AfAc+G9s1PL6rOe6F1IKgRosDXO....LXaM6GuehG4Qiuvez1JqZA.nTc5ubsrz65epHhYJ9po8DXO....LX6jQDmasFvuz+pOcrvEVe4TM..knemeyL0F5dnBtLxLA1C....C1NazlSY+ry97wi8wFojJG.fxwmalIhOwi7nsaXUlSWeDBrG....FF7vQa5k8eze4ey3yMyDkT4..TrN8WtV7u6gmIKC8gJ1JoyHvd....Xv2YiLDHwO6C7qEekmayEe0..TfV3BqO9M+nmIKWzrernBc55iPf8....vvhSFMer+WS+b+Lel3ze4ZEe0..TP9O7asg3wezmncC6bQDOXITNcDA1C.qpCN8Zd2jA..T7lKmmuSDs4Bnc1Ye93e5+3+.g1C.8k98+eeywG4C+ajkgdhn4SfVkh.6A...fpq4Jf46gZ2fDZO.zO5K7Gss3W3m+ikkg93QDOVAWNcEA1C....CWd3nYPEqokBs+49ycQzB.Ueeg+nsE+z+C+UyxPOWz7z0WIIvd....X3yIhHdl1MnYm84i+Q+n+Zwm4+6sT7UD.PW5yMyDYMr9HhnQTAaENKoqCr+9O1Qyy5.....FZ7pmegTWBmMxP+reI+y+mbx3W+CuX7pmYCEZQA.zo9+3QpE+rOvuVVG9GHh3TEX4zy55.6Ge7Mlm0A.z+qQpK...newm7I98RcIDQy.KZj0A+IdjGM9Y9G7G6z1C.UBKbg0G+u9PWHqWvrQDwGKh3jEWEkOzRb....fgWmJZdZCyjYm84i+4+SNY7u5+tWN9B+Qaq.KK.fU2y8mOQ7S8e8eR73O5Sj02xGKpv8s9ka8s36cnRuJ.....RkSd0+4udVeCepm7oiO0S9zw2yce3366G4aI9Vt8WK13nWpXpN.fq5UOyFhm32Zw3i9Km4VfSD8Qg0GQqCr20+N....Lb4jQy9Z+IiH1ZVeSKEb+zSu+38b+e2w656IhIuoyWPkH.LrZgKr93O9SWK93+JOcL6rOem7V6qBqOhVGXO....vvmGKZ1S6mI5fP6inYqx4i7ge93i7gi364tOb7c8cOcT+VVWbf24YKfxD.FVzCA0GQeXX8QHvd....f2xohlsJ2GKh3N5lIXoSc+RduG+XwAt0sF6duWWry8b9XKa+h4SkB.Cr9JO2liO2evk6jKT1U5mNh3gywRpzHvd....fkatnYn8ObDwCzqS1m3Qdzq468dO9whZisgXO6azHhH1+6XwHhH1vltrVpC.CgV3BqO9RO23wy9mcw3u3TuX7odxOY2NUmKZdp5erbq3JYBrG....ptlKgq8CFMaONmL5vVjS6zpP7WMeO28gicciaOOWdxnCbq45+yN84111WWrss6xkll5lMXcgKr93k+JM2n1W4LqOdkyb43q9hWHd9+5u1a6IypG7TQyv5mKOlrTQf8....P00bId8erHh5QDOTjCm19tQNEhC.L35bQyOmpurE3rRWWpK.....fJsyFMOs8SGMO8h..UEOd7VswsABBrG....HKlKhnQDwe2HhOVRqD.XX2SEM+7n6KR+SiVtRf8....PI5KNxnotD5UyDM6QvSGQ7yEQ7BorX.fgJKETein4mGMvQf8....PIZ9EGX9UwmKZ1yfqGQ7sEQ7KFQ7Loqb.fATmKZ9jcMcL.GT+RboyB....zqNUzrO2GQy.7OTzLTkCc0u1ZRpJ.ne04hlW74K80PCA1C....jml6pes7.VlHZFbe8q9UDMCze4NRgVU.PU1KDM272Yt5WmJkESJIvd....fh1YiA7VXPWpQpK.pLpGu0lYA0ipy+8PmtYpmKV6v1O6xd8SE97gqg.6A....HMlI0E..TsLvbS2.......P+LA1C....UWyk5B..fxi.6A...fpq4RcA..P4Qf8.......TAHvd.......p.DXO....Thd14GI0k...UTBrG......fJ.A1C......PEPWGX+301PdVG.......LTqqCrep8VKOqC.......Fpok3.......PEv5ScA.....zRmK0E.8j5W8q0RibbtVs22Tcw6aXvKDQLWNMWy0iy0ohHNaI99.pvDXO.rlld58GyN6ym5x...FFcpTW.CvpGsN.7Fs36MQDwgVk44PQDaMWpHJaSE42lYbjbZdJCmK5t+tktcyAloDWKXff.6Af0zccm2t.6A..pJNTzL.8kTOt1f2arh+b8voLGVxVitaCF51Mk3C0kuuNQm7zRb1H6aXQV23fYx37AYh.6A....xSs5Douxf1ak5w0F9d8PX6.qsN8ok3dKpBYM7LQ6C+eld306jMhfJNA1C.4k5ot...f9AegW50ScIjGZDuU.6Mt52qepsf.PY5NxvXZ2eGZu7zJ7TqwqMyp78WqMAXtH+tCHXEDXO.jWpm5B...nPLQzLT9k9JKAOA.UGq0lATja1ZmdmIztmTfYx32qul.6A....Zk6Kh3DQZZeD.P+ut4NSXs9Lm18TFr7MHX4g+O2U+pun0AIvd....fk6DQDOTn2wC.8WV4FDrZg+uTv9KEf+bW8qYJtRK6DXO....PDMa2MmLDTO.LXa4A6uxP8egnYH9K80LQ6uvfyUBrG....3giHdfTWDjFG+8ceotDpzdjO9ik5R.n7L0U+Z4A4+KFMexyJkf6EXO....TMUF8Y2Ihlmdvg1KR1omd+wccm2dlG+scKSzSq2j2v5hI21k6p2asMcon9tmumVe5benexiUpq2yN6VKz4+zux5hS+M5t+avUy4uvUhYewWsmmmu5W8aD+9O4SmCUDjqdf3st30K7P6EXO....TMUFmjuGK5SBq+9O1QiwGeiu4ed58skn1nW2aaL0uwqKpsoqbMu2CN84xwJIOmK3Zku+2qsvzE0DORNLGaOh+kc2Fj7RuxFiW9rapidOuvW65h4O+092YzJm9Ley3kd41ugY1zgAV2QzbCtaDE7mO2p.66ssJF....nevCGu8KmuR0RsgkwqsgXp8VKh3se5ya+oI+0J5RDnOxN21BwN21Bcz64fc7lWjkMkn8a5P61bgy+FWWL2WY02HgW3Kc930O+Ea4qYCCJT2Qzr037fE4hzp.66K1Yc....nezS8m9rotDhn4IDrv5Y8KEF+RsOlkN466Xh2nEAp8MCmZcfgIYYyE9Nu01MKq1lGr5aXvp0tkVsmzf+h+pq8fjaCAhGHZ9zoMSQs.ZIN....PIZ1Ye9TWBQz7DB1SN9669hctiwhI290+lAxessxCAwCPUwp0tkVsmzfev6oUaJv0tg.u9EVe7he8wdaeuV8TBzp65fOym8yWU9bwNwCEM236Bg.6A...fgKMhNrU3bO28gi+K96TOl5Fu93Vmd9kcxPOetWb.P+kwG8Rsby.Z8SIvJ1Dfex6HVdCeoUg++rew29aYkm7+G4i+XcP0lKNRDwghB5xgWf8....vvkSj0A9y7S+eSbzCOxU6k75Y7.PwpUg+uxm.fUdx++P+ju8S7+beswhy+FuUr2q7z9u76.fdHr+GL5fOOsSHvd....X3wDQD2W6FzzSu+3i9+x2catzWA.pdZ0mccsm1+lg9uTX+O6raMN8qrt3E9Jey3m6e4ijkkoseVZ255JpIF....nxoQDQqu0AWlO3OUCg0C.CMN3zmKNx292HN922q8lWb5swViB5D1Kvd....nZpH5MtsMEh64tObbzu6Wo.VZ.fpu+Qu2Mk0gVHmxdA1C....USms8Coi0ncC389C7sT.KK.P+gctsEx5or+dilsZtbk.6A...fgCGJhXp1Mnuia8UKgRA.n5pw6Z6Ydn48ZKvd....nj7EGYzTt7GpcC3m3G+GNFezKUF0B.PkUGr404dawQf8....PIY9ES5uFdaCr+f2x3kQc..ToM9nWJqsEGA1C....zUZaf825zWrLpC.fJua6VxT6oeqQF970Ng.6A...fgCGocCn9tmuLpC.fJu52XliNuQdttBrG....F7UucCHiO5+..CE9aV+0x5Pajmqq.6A...fAe0a2.14NFqDJC.f9CiO5kh64tObVFZi7bc65.6m7FVWdVG.....ucyjiyU81MfI290miKG.P+u2wA1UVFVt1G669.621kyqZ.....nXUusCH68pW.fgBG7VFOqCsQdsl9zX....njb5KkreM75sa.01zUJgx..n+wT6Nye1X5Og8.....clSuvhoZoq2tAriIdiRnL..5ebvoOWVGZi7ZMEXO....L3ah1MfctsEJi5..nux8erilkgMUdsdBrG....F7cGot...5Gsqct4rNzF4w5Ivd.nrb1TW....s18b2GN0k..PkzT6sVVGZtzG6EXO.TVNUpK...XHUaCPXO64FJi5..nuS8aLyQnKvd....X.0SkiyUa6e8..zZ6XhKl0gVOOVOA1C......PKTe2ym0gdj7X8DXO....TRl+xKlhksQ6Fv301PITF..8mt+iczrNzd9oZSf8....PIY1yj4SoWopCtP8..F5L93aLqCsm6i8BrG....FrkKWBd..CqtsaIyGb9585ZIvd....XvlKcV.fdvX0xbL50600ZkqjODG....FrTucCXxaXckPY..zeZpcekrNzbuk33wjC....FrLU6Fvja6xkQc..zWp1ltTVGpKcV.XvyWbjQScI...o1Lot...no56NyWZ7tzYAfAOyunOdB..xIMxxf12txbPD..Ckld58mkgs0dccjHB....TR9Rm9rotDZowGMyOp+..Cktq671y5Pq2Kqi.6A...fRxS9ze1xdIq2tAjwSLH..YS8d4MKvd.Hu3hKG..pdp2tAzAmXP.fgV21sj46S1d5hmUf8.vZpC9.odtOsA....Petd5.MJvd....XvUi1Mfo22VJgx..n+V8arbhRWf8....P0ybk0BUaTQC..zN01zUx5Pq2KqiOUF....pdlK0E...ukZa5RYcn06k0Qf8....vfqiztAbvatLJC.f9a0287kx5Hvd....nD74iwRcI..Pwqdu7lEXO.......swwee2WVF1T8xZHvd....XvTirLnCN84J3x..frRf8.......zFiWaCYcnSzsqg.6A...fpmyl5B..f2to1asrNzC0sqg.6A...fpmSkCyQi1MfL1KdA.njHvd.......Zi52XwGmt.6A...fRvyN+HotD..nGTaSWIqCsd2tFBrG....FLUucC31tkt9NwC.fUW8t8M10A1uiIdit8sB....T7pm5B..XPxAm9bE9Zz0A1uyssPdVG.vfuYRcA.....PUlVhC....TsT7GeuqpLt77..FjL8z6OKCqq64b9jY....nZ4T4z7bn1MfN3xyC.fHh65Nu8rLr19YvqFA1C....Cl1ZpK...5LBrG....JAegW50ScIbMpsoKk5R..nux301PgN+BrG....FRUe2ym5R..nuxT6sVgN+BrG.pblaAe7D..zi55dmK..8rizsuQIh..UNm+RKl5R...52MQpK..fAQiUqXiTWf8....P0xYKiE4dt6CWFKC.v.ko18UJz4ekA16QlC....RqSUFKxd1yMTFKC..cfUFXuGYN....n.7pmegxb4pWlKF..Wi5cyaRKwA....JAexm32qLWt5k4hA.Lr3fSetrNz5cy7Kvd....XHzN2wXotD..XEDXO....LDZxse8otD..XEDXO.......Yz8erilkgUualaA1C....UKmJGliF4vb..PKL93aLKCqd2L2BrG....pVNaYrHiUSj...UM9zY....nf8EGYzTWBWio18URcI..zWZ7ZanvlaA1C....Er4Wzu9M.vfho1asrLrC0MysehA.......xWSzMuIA1C.koyk5B...FRbjTW...z4DXO.TlNUpK...nOPoboytucMeYrL..Cbl7FVWgM2BrG....pVJkC4v3idoxXY..F3L41tbVFVW8ztIvd....nfM+hij5R..f9.BrG....JXuv4ScE..P+.A1C.UNye4EScI...8yZj5B..XPVQdOvHvd.nxY1y3BPC..JR2+wNZpKA.f9Vcv8.SiNctEXO..++yd28AYWm02Gv+oUutqVIsqdK9ErzckYJMhh8llLcnfeQESmhSlFj8LzNIisiCkNYBCSBgLojogVvsvzlVlxKSABMAFgLjAnwF6DmZZiEckJ3gPAORDhIIE7txD7KRHqUurRZQqj5erdg0R6Kmy8dtmmm6d+7YFOg8deNmmuvLwm686449b..nKS+8u5TGA..lCJrG....xG6O0A..fzQg8.......TBsqsWNE1C....sYeqib55b5FtNmL.ftQEb6kqzWSVg8....vRKCrXCn+9VUcjC.ftcK50jubJrG....5xr8qquTGA..lCJrG......fRnc8qUSg8....P93foN...r3J3uVsFk87pvd....HeLdEbNJ89kK..sEMJ6Anvd....nMaOOviTmS2v04jA.P0ooJreng1QUmC.n6vXoN....Qr19r98..xQM0Unu4a5Fp5b..cGFK0A...Hhse0WL0Q..niViqs8byucK0A......nD5aME5leeqk87pvd....HeLRpC...oiB6A...fkVZj5...Py4xKrefjjB.fYYOOvij5H...UluYr15dJ2dcOg..TMt7B6GNIo.......fND6bnSTzgVpEIusDG....nKylG3boNB..cKJ0hjWg8....PWlsN3joNB..LGTXO....jOFI0A..fzQg8....PazDWZY04z0nNmL..pVJrG....ZiN7Yp0oqQsNa..cwty631Kxv7PmE......f1o96e0EYXdnyB.Umcd8oNA.....zcPg8....Pd3DoN...jVJrG.pSGL0A...xX0xmUpf62t..j.JrG.pSim5....0sib1Kj5H7RTv8aW..VD822pp7yoB6AfpTodxmC..cCNxoNacNc6pNmL.ftYa+55qHCaWk4bpvd.nJUpm74.....vOlB6Afrz2cY8l5H.....PsRg8.PVZhK4RT..z0oVdnyB.P9RaH....PdX7TG...RKE1C....sQ64AdjTGA..ZCVaeEpd8Fk4bpvd....XoiFK1.5uuUUCw..Xouse0WrPCqLmSE1C....KczXwFv1ut9pgX..PyPg8....PdvdXO.PWNE1C....4gCl5...PZovd....nM44Vl8Kd..JNE1C.0owRc...f5zQtzJScD..H8ZTzApvd.nNMVpC......UgscUSTzg1nnCTg8....vRGMRc...5VzeuSU4mSE1C....4gQpfyw1qfyA..IhB6AfrzXS5RT..z4ahKsrTGA..5fnMD.HKclotTpi...zxN7YRcBtRMtVUA..jqbUZ....nKReq4hoNB..caFnnCTg8.......zDFZncTjgMbQOeJrG....RuCk5...P4cy2zMTomOE1C....o23oN...jdMUg8acyqspyA....rjy25HmtNmtcUmSF..Uulpv9sroUV04.......ftZ1Rb.f51gSc......HGovd.ntMVpC...jgFK0A..f1lFEcfWdg8CWs4.....n.FK0A..fxq+9VUQFVihd9t7B62PYBC....v7aOOvij5HbE17.mK0Q..XIisec8UomOaIN.PV5acjSm5H...KIs0AmL0Q..f4gB6A......HCnvd....H8FuBNG6pBNG..jPJrG....RuCl5...P5ovd....nM36trdScD..nCiB6A...f1fItjuxM..QDQbqEcf9zC.......zD140WsmOE1C.0sp3ApF..rTiOiD..JrG.pcdfpA..WIeFI..TXO....zNL1j9J2..TN9zC....PavYl5R08T1XwFv8b26tFhA..MKE1C.Yo87.ORpi...zooQpC...sFE1C....o0gSc...nsafhLHE1C....o0XoN...zb17.mqnCc3hLHE1C....sAeqib5TGA..Zy15fSVomOE1C......PFPg8.......jATXO....jVik5...PdPg8.Pc6foN...Pc3jmov6osi0FiA..cPTXO.T2FO0A...pCO3i9mU2S4.08DB.P0Rg8....vRC23hMf96aU0QN..nIovd.Ha8cWVuoNB..vRJa+55K0Q..XImgFZGEYXCWjAovd.HaMwkbYJ..5J3Y7C.PGra9ltghLrBs00oID....nh8bKqTa8LdF+..PDgB6A...fJ2QtzJScD..nCjB6A......HCnvd.......x.JrG....RqQRc...HOnvd.ntMRpC...ztM1j951..Td9DD....PE6LScoTGA..5.ovd....ny2tRc...5VMz1VeQFVihLHE1C.Yqmbhkk5H...saGN0A..fVSe8VnZ1aTjAovd....HcFqNmrFWqZ...xYM0UpWaet.O....Le9VG4zoNByo9VyEScD..XAzTMuu8q1E3A......nJYoxC......PFPg8....P5LRpC...4iYWX+vIKE.P2lCm5....sSeuiNdpi...cflcg8CjrT..caFK0A...Zm12A9xoNB..zAxVhC.......MoFWa0UytB6Afr0nmbxTGA..ncydmC.PGt9VyEKxvJzVRuB6Afr0Dm67oNB..P61AqnySiJ57..P6wFJxfTXO....Tg9lwZSwz1HESJ..UKE1C......PFPg8.......jATXO....jNiTmS1NG5D04zA.PIovd.HEppGtZ..P14nS4qZC.Pywmh..RgwSc...f1kiN4kRcD..nCkB6A......HCnvd.Has+m3IScD..f1oCm5...PqaaW0DU14Rg8.P1ZzQepTGA..nzl3BEdKwYr1XL..nlzeuSUzg1XwFfB6A...fJznGq5Vkc..rjRiEa.JrG......fLfB6A...fzX7TG...xKJrS8qPJD...B.IQTPTE....RiCVgmqgqvyE..IhB6AfTXjTG...ncYOOvijhocfTLo..TsTXO.......Y.E1C.YsSurkm5H.....PsPg8.PV6otzZRcD..f1kQRc...Hunvd....nh7cWVuoNByq64t2cpi..vRVCMzNpjyiB6A...fJxDWxWyF.nazMeS2Pkbd7II.......n8qwhM.E1C....owASc...nV0XwFfB6AfTvWNE.fkjN5Tk5qYOd6JG..zYRg8.PJ3KmB.vRRGcxKk5H..PGLE1C.YsItzxRcD.....nVnvd.Hqc3yj5D...sEGN0A..f7iB6A...fJxDWnvaINi0FiA..cnTXO....TQF8XSj5H..PGLE1C....c9FN0A..fVmB6A...f523U74aCU74C.fDPg8.PpbnTG...npcxyLYQG5Aam4..f50V27ZqjyiB6AfTopWUY..Px8fO5eVpi...IvV1zJqjyiB6Afr1DW3RoNB.....TEFXwFfB6Afr1nGahTGA..ncn1+0FVU+T8A.nosnOj3apB66aMS0LGF....vzp88v9p5mpO..sOqnYNnFWsU6H....4umaYqJNxkV7hpGaxdhyL0BuU7MwEtzB9q+qDOvYA.f4TSUXO....DQDmdYKOdpKslEcbSbokEG9LK94awJEeFeuiNdruC7kKRDA.fNFytv9Ec+yA.nBU66aq..cZJZY3QDwSNwxJz3F8jSFSbtyuniSg3sc9rP..bElcg8K5SnV.fJzAiHdioND.P2ou6x5Ml3RE6Q5UQKBunqL7HhX+OwSFiN5SUnwxRV09dXO..4OaIN...PKqLEfezo5IN5jK7dE9LNxYuPbjSc1BMVqHb..fNcJrG.xZ6+IdxHFZmoNF.TaJyVfRDE6Ak4LJyJ.OhH1yC7HEdr...PqSg8.PVa5sK.E1CTNeyXskZ7EcKOYFeqib5RMdEeCbY1epC...4IE1C..7iTls0jHhXhKsr3vmobyQY1hShHhSdlIiG7Q+yJ2j.PdaOoN...jmTXO..zjJ6VWxLJ6p4NhHF8jSFSbtyWpiwC0R.xRGJTXO.vRNMt1huvmVHJrG.f1pxthsmQYdnTNakc0aOCkaC.0fGNh3dScH..n502ZtXkbdTXO.jJGL0AHkZ1Rrin4KxNhlaUZOC6A2.PWpSDE6ysbvHhwmm2a7HhGJhXrJJS..rDkB6AfTY99BsWg46gG4XS1SbloZthqin0JuNBEXC.TgNbTrxrGqfiagJOe1Fo.ioSvvoN...TH25hM.E1C.Yue12+mK0Q..nS29K33JZQ2iEUaA6zZFH0A..fpgB6A...ZdGJJVA2iGEe6fqnklWlyI..PG.E1C...4jhtegGQ4JrtLicrvpBG..HATXO...KsUzsBkHJ9J6NhxUpsUBN...EfB6A..XwUzs8jYLRIFaYKydrvp+FnIrkMt7TGA..VDJrG.RkwRc..pMksr6lY0XORad7.zwaKCdgTGA..VDJrG.RkwhHNbDw1SbNfbUY1FSlwH0vwXqMA...nMQg8.PJsmHh2cpCAczJyCmxYqYKctL6u2s5bA...PWFE1C.oz6IhX2QD2XhywRUGNZ9sdnQZxiarlbNUpM....c8TXO.jZ6Jlt39e8177zLauHynUKStYVU1yXrv98O....zUPg8.PpMdDwa+E+mcs.iwpuF....HK02ZlpRNOJrG.xIij5......PY03pmnRNO8TImE.......nknvd.......pGMVn2Tg8.......TOZrPuoB6A......HCnvd.......x.JrG......fLfB6AfbxvQDOTDw3QDiEQrmHhARXd.....n1rhTG..fWTiHhQhH1vK92aHh3W5Ee8ckh.A....PcxJrG.xE2a7iKqe1t0XQdBpC....vRAJrG.xEKzVeSi5JD..PMXfX5sBP..3kvVhC.jKVnuz5GLldesuca7HhCVCyyk6fQ87e+lsT8eWA.5lLbLc476JldAHz3EesY9UEdhX5Omy6oEmmFs3wC.PlPg8.PmfarFmq2XMNWciNTT+2bhwdw+otMRBlywhz7eWAnaUiE3e1dAN9MDQ7tio+2cumVLG..rDfB6A.nNUm27kYbqIXNiX5BX5Vr+DLmiE0+MmHU+xTFKbiXfTXWu3+2FwOtP7YdsYuJ4qB2azZE1C.vRDJrG..nUkhaJRptQLcSbiX5LN+TL6ZV+mu78O9Y11Zl4+bUVDeVYaW0DoNB..rHTXO...LWbiXRmCGE+FWT1aHPUeCDJx4a1EhWVK1Cm0cMGuVinXaGM4jl8+8oT5u2opioA.fVfB6A...xKaOJWgyd9qz4KEaYb..Twtm6d2wdu+GpkNG8TQYA.......ZAVg8.Ptnw78Fuou3muxljAO1Iidl77U14qHV1jmON+yezZcNiHhS88etXxyd1ZcNO2oNc7Ee3Va0D..rTvtuq6JhHhM+xGJ5YMqN5YqaJF6g+e55j..rfTXO.jKpk8Z1iuo0WGSyUZnqJAS5qJVYMOiqLh3M8q9KVyypaDS6laDC.uTCMzNha709ZhHltP9HhXk+DaItzpWYbpMug3Gtpe7W09Dy53Vy55uNiI..4ogiHFY9dyRWX+q+1tkVIL..kxPCsiTGA5.3FwzdkpaDyldliU6yYbxSGSM9Iq8o8G7cFs1myW3Yd13.eo8U6yKjydCuwc+iJUevW10DKeMq9Gs53iHhoVWuwIVWeuji4DWwYA..VPK3Ca9YWX+tJxY6ZtlM1JgA.nTlY0qAz84XWylp+IMEyYDwJe0upZeN+IhHdS+q9Up84ME2HlKdjiEW7bSV6yaUbiXbiUJmY1FZlsYVA7yXlUB+Llu+cMmtZiF..TH1Rb.fEz1tpIRcD.fkPbiXJml8FqzJ2Xjod5mooO1VwrWI6KjKt5UVpeYUVA7..zIQg8.vBp+dmJ0Q..fRpktwHI5Fb...DQOoN........fB6A...fbvB9.nC.ftCJrG.xA9Bp..zsa3TG...ROE1C.4.eAU....ftdJrG.xZqt2dScD..fJw51zFScD..Hyovd.Hqstq8pRcD..fJwpGbCoNB..j4TXO.......Y.E1C......PFPg8.......jATXO.......Y.E1C.4fARc......H0TXO.jCFN0A.....fVwPaa8s74Pg8....P5sqTG...ZM80aqW2tB6Afr1J110j5H.....PsPg8.......TOVvsEXE1C....0fT9KG7NuiaOYyM..uDCrPuoB6A...fk35u+Um5H..PAnvd.......x.JrG......fLfB6AfbvB9.WA....ftAJrG.xAK3CbE.....5Fnvd.HqM055M0Q...xc6J0A..fpgB6Afr1IVWeoNB..Pcvu3P..TXO....jA7L8A..E1C......PNPg8....PM3hqdkoNB..j4TXO....TCN9lVepi...YNE1C......PFPg8.PN3VSc......H0TXO.......Y.E1C.Yq2vab2oNB.....TaTXO.jsVy55O0Q.....fZiB6A......HCnvd.......x.JrG....RugSc...n0rkMt7V9bnvd....H81PpC...slsL3EZ4ygB6A......ndzXgdSE1C.o1tRc...f5xa3Mt6TGA..RqsuPu4rKr+VayAA....5psl00epi...YrRuB6GZaqucjC.fqvfurqI0Q.....fZSoKruudsK5..0ikulUm5H.....PsQ66.......jATXO.......Y.E1C......PFPg8.......jATXO.......Y.E1C.o1voN...Plns84hdkuhAZWmZ..pPJrG.RMe6Q..XZ9bQ..c4TXO.js5YMqN0Q.....fZiB6AfrUOacSoNB..PkZcaZioNB..jwTXO....TSV8faH0Q..fLlB6A......HCnvd.......x.JrG......fLfB6A......HCnvd.......x.JrG.RsgSc......HGnvd.H0FH0A.....fbfB6Afr0Tqq2TGA.....pMJrG.xVmXc8k5H...0ock5...PZovd.nJ0H0A.....fNUJrG.pRMRc......nSkB6A...fZxJFX8oNB..zlr4ANWKeNTXO....TWVe+oNA..zlr0AmrnCsw78FJrG......f5Si46MTXO.......Y.E1C.oViTG......xAJrG.Rssm5......PNPg8.......jATXO.jk18ccWoNB.....TqTXO.......Y.E1C......PFPg8....PdXfTG...RKE1C....4ggSc...HsTXO.......Y.E1C....c1t0TG...pFJrG....pISstdScD..HiMSg81m7.fTvCVM..5pbh00Wpi...YrYJrWgI.PJ3FFC....vKpzaINMtV6hN.......T0Jc668slK1NxA.vKwfurqI0Q.....fZkkKO.jkV9ZVcpi......0JE1C......PFPg8.......jATXO....rD2Nu9Tm...JBE1C......PFPg8.......jATXO....jGFH0A..fzRg8....Pd3FSc...HsTXO.jRVEY.....7hTXO.jRCm5......PtPg8....PMZng1Qpi...YJE1C.YoULv5ScD..f1ha709ZRcD..HSovd.HOs99ScB.....nVovd.......x.JrG......f5y.y2anvd.......p.24cb6EYXCOeugB6A......nBze+qtkNdE1C......PFPg8.......jATXO.......Y.E1C......PFPg8.......jATXO.jRCm5......PtPg8.PJMPpC...jYrfF..5hovd....HeXAM..zESg8.PV5TadCoNB.....TqTXO.jk9gqZEoNB.....TqTXO....TiVcu8l5H..PlRg8....PMZcW6Uk5H..PlRg8.......jATXO.......Y.E1C......PFXlB6ajxP.......P2NE1C......PFnzaINadfy0NxA.......c0Jcg8acvIaG4.......ftZdnyB......PFPg8.PJMPpC......4BE1C.ozMl5......PtPg8.......jATXO.......Y.E1C.Yma40caoNB.....T6TXO.jc130b0oNB..PpLbpC...oiB6A...f7w.oN...jNJrG......fLfB6A...fk311UMQpi...EfB6A...fk35u2oRcD..n.TXO....TiVw.qO0Q..fLkB6A...f5z56uJOadH0B.rDhB6A...fNWCm5...P0Qg8.......jATXO.......0mFy2anvd.......pOMlu2Pg8.......jATXO.......Y.E1C......PFPg8.......jATXO.jJMRc......Hmnvd.HUZj5......PNQg8.......jATXO.......Y.E1C......PFPg8.......jATXO.jc17KenTGA.....pcJrG....xGCj5...P5LSg89.A....P5MbpC...oyLE16CD.......PBUpsDm67Nt81UN.......nqVoJru+9Wc6JG.......zUyCcV.......x.JrG......fLfB6A...fZzTqq2TGA..xTJrG....pQmXc8k5H..PlRg8.......jATXO.......Y.E1C......PFPg8.vB5IGcCoNB.....zUPg8.......jATXO.......Y.E1C......PFPg8.......jATXO.......Y.E1C......PFPg8....vRXCMzNRcD..nfTXO....rD1MeS2Ppi...EjB6A...fNW6J0A..fpiB6A......HCnvd.......x.JrG......f5yvy2anvd.......pOaX9dCE1C......PFPg8.......jATXO.......Y.E1C......PFPg8.PpLPpC......4DE1C.oxvoN...PFxmQB.nK1LE16CD.....o2FRc...Hclov9B8ABdkuB6dA.......zNXKwA......fLfB6A......HCnvd.......x.JrG......fLfB6A......HCnvd.......x.JrG......fLfB6AfpzvoN...Pmfa40caoNB..jgTXO.TkFH0A...5Drwq4pScD..HCovd.......x.JrG......fLfB6A......nBr0Mu1V53UXO.......UfsroU1RGuB6A......HCnvd.......x.JrG.RkCl5......PNQg8.PpLdpC......4DE1C....ctZrXCXnss9ZHF..TETXO....z4pwhMf950W8G.nSgqZC......PFPg8.......jATXO.......Y.E1C......PFPg8.......jA5IhXWoND.......zEY345EK0JreKab4UST.......n60.y0KVtB6G7BUST.......fWB6g8.......jATXO.......Y.E1C......PFPg8.......jATXO.......Y.E1C......PFPg8.vBZyCbtTGA.....5Jnvd.XAs0AmL0Q.....ftBJrG......fLfB6A......HCnvd.......x.JrG.xNSd7Sj5H.....PsSg8.P14TG6ERcD.....n1ovd.......x.JrG......fLfB6A...fkvVae9p+..cJbUa.HUFI0A...VB3VWrAr8q9h0QN..nBnvd.......x.8DQzH0g.......ftcJrG......fLfsDG.......x.JrG......fLPoJruu0LU6JG.......zQamWegG5vy0KVpB6ab0STlgC.......WoAlqWzVhC.......Y.E1C......PFPg8.P14PekGO0Q.....fZmB6AfpTip3jL5nOUUbZ.....nihB6Afpz1Sc......nSkB6AfTZ+oN......jKTXO.......Y.E1C......PFPg8.......jATXO.rnFZncj5H.....vRdJrG.VT27McCoNB.....rjmB6A......HCnvd.HKM3wNYpi......0JE1C.Yodl77oNB..PtaWEYPa6plnMGC..lwNG5Dszwqvd.HkFK0A...Vpq+dmJ0Q..fBpmHhARcH.ftVik5......PBzXtdwdhHFtdyA.......c0ZLWunsDG.......x.JrG......fLfB6A......HCnvd.HOcxSm5D.....PspvE1eO28tam4..3kXpwOYpi......0Jqvd.HkNXpC......4BE1C.oz3oN......TkFZnczzGqB6AfEU+8spTGA.....5Hby2zMzzGqB6AfE01ut9JyvGnckC.....VJSg8.PUa3TG......5Dovd.HKcpu+yk5H.....PsRg8.PVZxyd1TGA.....pUJrG.RoQRc......HAt045EUXO....zYZfTG...pVJrG....5LM7hMf64t2ccjC..pHJrG......fJxPaa8M8wpvd.HKcnuxim5H.....Po0WuMes6JrG.xRiN5Sk5H.....PsRg8.vhZs84xE.....ztoAF.XQs8q9hoNB.....rjmB6AfTa+oN...PGpARc...nZovd....nyzvoN...TsJbg8Css02NyA.......c71xFWdSerEtv9950hwG......fExVF7BEcn65xegdB64c.P0ZWoN...PWhFoN...Ts5Ih3FKx.OyYuXaNJ..uTa3TmI0Q...xYaO0A..fpUg2maF8oOY6LG..WgUbpyl5H.....PswFSO.jZik5......PNPg8.vhZmCch14oer14IG..VhZWoN...T8TXO....rD0Paa8oNB..ccZkE9XOQDGt5hB....PtnudsN8..5jzSXqH.....5zLPpC...UuBeq1268+Psyb.....TbCm5...PK6Jtdteab.PpMVpC...jY1UpC...0hq3WLWoJr+zmcEUWT..l1XoN......TkFZnczTGWOQDiTzA+zO2ZapIA.570rWnA....ftM27McCM0wUpUX+QO9xapIA.57UhKzznplyod5mopNU.....j85Ih3fEcvG8EtPaLJ.vRDMRc......nSTOQDiWzA+W9WW3gB.TTE9FGC..TN675ScB..nLJ0dX+y7LuP6KI.P2J2MX..n7FN0A..f4W+8sphLrq354yrG1enhbzO19NPbjiu5RDK....f1.+JEA.xXa+55qHCafK+Elov9Beg9u8nqsnCE.nnNwb8hW3bSV24.....fjozE1+j++NWaJJ.PNqf+TtZVy40gN9e6yzNmS.....xJyTX+HE8.N32760dRB.j0J3OkK..fBXyu7gRcD..HCM6UX+btcDb4dr8cfXrm01hC.Lut0TG...focly0yhOH..pbaYiKuHCqwk+By9J2OTQmrC70cAe.nRMxb8huvy7r0bL..fNFiUnA88uXaNF..LW1xfWnHCa6W9KzTE1+U9piUzgB.zzNvWZeoNB..PtZrhLnybVE1C.zI4xKruvaKNe8u8fsmDA.Yo17W1ar14IG..xMqXf02pmhwJxfF8oOYqNO..Tit781lBuJ6+S+RuPEGE.HmUxur2.k7zO178Fa3TmojmJ..nCv56uUOCiUjAs26uveMe..RiFy9Ot7B62SQOK689en3IGcCUPd.fkfFtjie746MVwoNaKFE..XIqCUjA4WHO.PVqwr+iKuv9QhHNbQOS+2+eLu8q..Kwzeeqpcd5O378FKaxy2NmW..nS179Ynls8+UOc6NG..z7ZL6+3xKrOhHdOE8LYU1CP2isec8kj487O+QSx7B..c.FoHC5i9w+7wQN9payQA.flTiY+GyUg8E9gOaDVk8.vbZWMwwr+45Euv4lr0RB..rzUg2f5u+G5hsyb..Py6krsBOWE1OdDwGrnms8d+OTr+mXisZn..lSG+u8YRcD..fJ2wtlMUEmlwiH9TEYfezO9m2dYO.PdZfY+GyUg8QLcg8EdU1+u++7+63zmcEsRn.fL2V13xKyve6Q4evyNxb8hm6T1yUA.fEPgWvc+1+6FIF6YWa6LK..7h14PEtd8Fy9OluB6K0prezQep39+iaqOLBAfDaKCdgxL7MDSW.+.Kx3ls4bOV6K9vE9W5M..zM5fQDObQF3ni9Tw+g+qOsEbG.PdY6y9OluB6injqx92+G3y3APK.KgUh6L7L1PDw8Vhwev46MF7XmrryM..zIarRN92dQG3isuCD22G9zJsG.Heb3Y+GKTg8iGk3h9QDwG5O3oZl.A.cHdq+J+yJ6gr6RL14sv9kexyT14E..5jMVSL96qnC9A+BOZr6272v1iC.Pa18b2EpVjwl8erPE1GQD6Ih3PEM.O19NP7G8XVk8.rT0suq9J6gbqkXriGyyurqy+7GsryK..zs48Dk36uO5nOU7VdGOtGDs..sQacyE5lieqwr1gBVrB6injqx9246ZOwQN9pKyg..cH14PmH9jez+Ek8vJy9X+btJ6+AemQK6bB..ci1cThs11QG8oh+4+xexXu+IqqMFI.ftWMttB2S9GLhX3HJVg8iDQ7gJSP9X+gmqLCG.5fbq+8eg3288duk4PFtDicNKr+PekGuLyG..zsZrnbOCghHh39de6M9s9cOmsHG.fJ1O8OYg2he2PDwCEQLPQJrOho+o0c3EaPyXu2+C4mUG.KQc5yth3O+a77sqS+bVX+ni9TdvyB.vRJswOayCEQ7aT1C5A+BOZba2wm11bK.PEZqCNYQ2G6iHhsGQ71KZg8k9AP6u+m963oNO.KA8fOVuwC9Edz10oedevy1yQOd6ZNA.fZWOSd914o+CFQ7oZlC7c9t1iUaO.PE5M8yVlcJ33dKZg8QL8couvWv+w12Ahu3WwE3AXojib7UG226auk8vFqDicdKr+3+k+MkcdA.ftY2azjk1a01C.Tc14PmHd2+N2SQG91KSg8QL8pruvaMNuy20dbW4AXIjG3+0xalCarRN98OWu3W6w1WyL2..PVZYy+JruvOzXKf6MZxR6iX5uS+uxuywhmbTE2C.zrF6YWar2+vubQG9gJag8iGk7AXyGYuGqjSA.jiNxwWc79+.elxdXyY46KhQlqWz9XO..Kkb9m+ny2aMu+hCaR2azBk1+X66.w+zeg8D68OYc11aA.Jo8+DaL9H68Xwni9TE8PForE1GwzEo7gJ5fevuvi5mQG.KAzjqt9l4KbNuGy4+Krs3...Mg6Mh39ZkSv8891a7a9dedq1d.fB3Qe7AiW+uvgh27a8ST1mCfevlov9Hh38D1Zb.nqwoO6JZlUWeDyypkuYOlu6W6azLY...xN+fuyn08T9dhH9kakSfUaO.vBarmcswu0u64h2163SVlUU+L9TQDi0rE12TaMNtfN.clZxGh3mHl9AVdYMdDwglq23.eo8YawA.fkDdgm4YSwztmHhepnE2m7uu22di66CeZKLO.fYY+OwFi2x63wK6JpeFmHl94GazrE1Gwzq.xGtnC9A+BOZb++wqpElN.HEN8YWQ768INPybn6oEl1Qlu2v1hC..KEbfuz9lu2Zj17TevHhgi4YARTTO3W3Qi2x63wiG8wGrZRE.PGpSe1UDerOWewa9s9IZlUU+Ld6wzKfwVpv9HldU1W36L+6+C7Yh8+DarEmR.nN8E+JqsYufyGrEl14ck4Oxm6OJV0ObpV3TC..o0ldlik5HLVLco8E94S2bYzQep3s8N9jw88QtTbjiu5JIX..cRNxwWcbee3S2raivy3SEyZQO1pE1OdL89fWg8leqeB+r4.nCwQN9pi246ZOMyg9oho+hfMqQh44FBO5nOUz6S7sagSM..jVS8zOyB81iUSwHhHe3fJ...OHQRDEDUoWMe2QzhaQN689en3e3s+YiO1mqOE2C.cM95e6Aiew25WqY2Bblw9iKaqme4sxY6E8UiH9GEQznnGv+mu1oia40d8w.q67UvzC.sKu+O4Twg9l+UMygdGwK9S4pEbUQDu545MdhQNPbS29+j3b84KDB.Pmmevex9huye879Yr9PQ8VZ+eUDwmMldusuQqbhd7u5eQ7I9Leq3u8XCEWnmsDan+KEqs2KTEYD.Hab5yth3y9EWa719M2SL93GuUNUGJh3MDQbtY+hKqUNiyRiHhR8Hteng1Q7G7e40DMt5IpnH..UoG8wGLdauiOYybn2WTxe8UyiFwBbskgFZGw+326+533aZ8UvTA..0iU8CmJ9L+7+hKzPFLZ8E9Py5sGS+431PUcBuy631iW0N2R7y72qmXmC0RKje.fj6q+sGL98+zem3w1WS8r9a1NTDwth43Z9UUg8QL8E0e2k4.TZO.4owd10F21c7oalC8vwz6GpU0WxbjHhac9dSk1C.PmlAO3eS7e629cMeu8giVbUtWAZDS+rH5M1NN42ycu63U9JFH140Gw1tpIh960ylH.H+80+1CF+oeoWH168OuOx8JiOUbYaCNyVUVXeDS+zl+FKyALzP6H9O9ucWwOyOYK8yG..pHm9rqH9MeuOeyd2h+GESWxdUoQTfeAWuk2y+l3Du5WUENs..Pwsom4X+n8k9UrsqIN10ro4crO++oOdbfuz9lu2dA+B70rcGSWb+1amSxcdG2driFaLZbcqN19UeQqBe.HKb5yth3uZr0E+e+lSFG7a98phUT+L9Mhou957ppKre3X5hZJ8Oet28uy8D24q+rt65.jX22G4RM6cLtp1Jbtbumn.+BttkW2sE67m+MDG6uaa86TBbYF7XmL5Yxh8bI5hG4XwEO2js4DkOVw1tlV53O0l2P7CW0Jpnz.ztrlG8KG2+G5C+Rds69W+WKt3s8puh++g2zybr326M+qtPmte4Hh8T0YrELPL81jyaOpvsImEycdG2dbUaccw1ut9hFWaOQeqQQ9.P60oO6Jhm94Va7je2H9y+FOeq9fjctbhX5aF9HK1.q5B6iX5Kj+AZlC70ea2R7u7td4Vs8.jHerOWew6+C7YZlCc+wz68ZsKE9Wv0s75ts3uys7Zh3Ud8wIVWeswHA4mM8LGateiSd5XpwO4b9Vm56+bwjm8ry46ctSc53K9vUxO4SpICMzNha709ZJ0wL3K6Zhkulh+P7dk+DaItzpWYgG+EW8Js0kwRRCdrSFO8m+Ql2+8jug23tiW1cs6WxmG47e5+33g9zK31NXJ2+5WHCDSuZ.+kRYHFZncD27McCwV27ZisroUFaYiKO1xfWH5aMSYq1E.Jkwd10FG9YWcb3u+4i+hm7nsiB5msGNl9WPWgtFe6nv9HldEAzzWH+dt6cG+butMp3d.pQsPY8U89V+bYfX56Bco110tkW2sEW++fe5XMa+kEmeyqWA9jba3TmIVwotxxwmuUd9O36L26HTKRYOvRB69ttql9XWcu8Fq6ZupVNCk8lSLWlZc855OKwrgSclXxu7SbEqp94xred6r4Qet3i8q9qsPCOm1NblOMho+0Olzh6WH24cb6Q+8O8Mg7U9JF3G85675+wiQA+.zc4IGcCwge1dhidrohQe5SVU6C8EwgioWb6kZBaWE12TEqb4d821sD69m6kG27O0orU4.PaxoO6JhO9mcYwG8i+4alC+Dwzqr9CVogZtUIWaY220c8iJxom0r5nms9i2iYWn8aV5Nrpe3Tw59AW4O49kM44iy+7G8+e6c2DiabVGGG+mbCMoYC65rMDQSEIyVPpPPBVNvAtjZQ5cVIj3BpU8DHwEZkPhKbfJwANBG.QO.D3.GPPa3TjfTIuoEpDJExVotUQPqGuoIaTdY2rIa7tIUTwgG+ryiGOieYrmWrmuejrr83Y8NRIqGO+d9+7+oqs++18AZyO7Zcs8Mt158p+DC.LTRxLmHJC6roHJ6q5rRydnQ9XYR6btycuVpx6+gZs25hIZlG8c9o+D82dkeiZz3C50tMtWKfRSdxDb+RJCaUNoom+4VpqssvwmUG7wpD49aaUOw4HU2UG8vkm1.G.Pdye8YTqc2mV88kZsyGqFqcW8Fu46zuy8lldYYlcZCcwMlVA1KYNA9kzX5j2+fW5aqu5WZ+T08..iQ9qOi9E+9aOJS8quhxlv5spJyEG98yvem8Lnji74VnimGUPFTckIyfzazsKveQ4Aatkt2s2HxWak+9+HO+ha..HgN0W+zZ9i8D68b2AgH7.wmjy+t24dZ2Jwt0+sQVcNizt8BlVpJyrB3EUJu3zNs4YO8ozwN17cs8CcvGUm3yz4+uclCVQm3I5bvAXVB.fxDaX7s1sh7up4yCadkVZ6VOLuCkOJ+NYxsvOouAoYf8RivhPabVXgmReyk9ZDdO.vH5Oc94zO7GclQ4sHOWTzpIyI.elb52+XyfzxG9jO97Z+Gt3T7Z8JH79gJOG..HRYcQPjFVTlv6WRDdetv1i+Cys0.YY6++gc7O88oCG.fTwM1b+5V24.crslqWQ2uUv.R9tWNnXzKfAw2OibP8Vocf8RoPn8t9de2ukN4SeH8EV3gL5x..Cfk+Wyq+ve9+ny+5WXTdaxyv5cUSlKLrv1GUA...5iWVlKveZhmLA2unLeeMBveBm6ZCfK6B.bT5UaChAF.Xzs8N6Sqc8YF38+la9H5laz8.0MnrUzd+jg8G9hfsjIajelFCA0akEA1KYNI8YUJeRZ6nI+Ee5px6IqnOuG89d..IyHYeg29.5W8quvnNB0aIyEeUerbfM9TUliqZhKJDHurbed8Ko92+FuiRVEl5qw3WPNgVTlOKZT3091vn1PruS7yJIfBrsj4h08zvUHAqHymeLsqpBBu2q8M9LIDo3lo.VQ01fBKp1HTTn0BgjHpJEOJCR.48KD7IvpLuLXKYxD4LZHWLYGTYUf8RlSPeVkwmT1MDe6T95jKz8hIG.vzls2Ye5seuYU8251iqQ3NKWfYGUdxbQgtWXHg3ixjlJ9.r6Uv406w64fD3NlN3oAafCFz8SZ3FXgE0TxhXIJErA06tnx8BR52Nf+rKp7e.Gya0BcumB9rEB0GEJws1CLHhp0DMNDW6MpHaTqz63XWnQShRVUgig2JxbsR0UJERuqrLvdqWTlo6Wt9kvsA4am9V1opESKK.LIyFR+EW4d5W9J+ww4a8JxTA69iy2zbPMETgWdJHXeBFBYksT7C5U8X1tuh+u8h6mAnrw9Y6486gz3YgCkyMUrYqrtyp3aQf0Z+5w8uiSREBQQfmBBwuVnsUURe4L93A..SurWyVcm6yzBWJOBrWxbR0erJv8bXa+YyFnu6zohJzG.EI9qOiduFOp9m+6MRqpB3uHSkhMsWYs11YgmBt3uEcdMBNY5UuBQuWsnEekrpXG.HKTTF.hg83nHcNW2yOTWlOy+RZvCYeQYBzObXxDVe5I72mSpy.9YFWB..K6rR9Rpyywm6WGWdEXukmJ3A22KwEpO8.M.jlVswbp45UzpWdact+5ES69Y2KIyT7FAb6S0dpy1AQ3ma2FWbXz5WOO2ZP9RS8aeJDewK..L14ogeseHJ9JclIgUk46RsjLCDwxxLqyIr97k86y4NfRdJ3+K4I99a..SpbGvc2BvxdMg9pf28.x6.6s7zDbv8wwcgRw1qxN4m07Zz5c.vfX0Fyoat4inlW8iTi0taV1W8ZJyEVxESl9pk2G.injtHgB...LIvsXMh6wdh.9A.RKtE5UcmG6qff2mpttzhRf8VUkosK7BpjzC5rUou6pbNg5CTtr8N6Sqc8Y1ag2o4UZoqei6oW80NWdcH8ykYPToZjA....FNgaCT0bdrm5bFgvhpK.JarsgFotCYudLOtzonEXuqEkI39kTIejpcqT+EN9r5fOVkN5o9DrOPw1M1b+5V24.p0tUj+UM+c66dYSV3ErUh9Uj4ycmZFUZ....fIDgC5ueOWh.+AP9XE0YA94qNawLgaGo0S8inoLE4.6c4ISv80jz2HWORJ3d9maIIoNpXe2v8o+5CLdXCgWR6UY7RR271ejtwsL+MVAKL9dooLUT+Yx2CC.....LB7T2qoCtstmdssppjzoC.JgBGvtU8H1VTq8WQseHEMoDXeX0Zeaw12OWNdrLwxsx8kB5y9RRdOYEcvCPH+n7vM.9lqWQ2uk4++27Jsz1sdnjjt101Pm+0uPtcLlBHnd.....zKdJ5E14np3+A40ruNCN.JSbWDT6kAoOrWOguFlfLoFXeXdxbx.a.9KJBwO07rm9T5XGa98dtsM8X4F1uDA9ir2pMB9ye21PiTmAv+Fu46nFM9fL+3q.XYYBo+L46gA.....Pj5Wn+IceCKpYaPZnnNHEK2+cISEU0cOtUeL7djEGmnDaZIv9nX+.a6G9VSrxsWXXWrccEN3eoNamOtXP.lN4Fztka0tKI0ZmOVMV6tcrOk3f2GFKKoy19le9dn........fnLMGXeuXCw2MLeIVvVlJDdF.D1QOxL5S83eh999D2fELnNR0c0QO7CR7OeZxs8uLrBGfdTb6i6gs81OPu5qctD86FCklxT4.ms88L5+........Ebk0.66mZJHPeOEzxcnM6.fhJa.81a942gB........RBBre34VU9tUoeQrWjAfoWKKSeyqd668yyCF........L5Hv9wK2.7CeOA5CfjxFNu6M........Lkg.6yd0ZeuMHeu12HTe.rrLUJuuBZqM940AC........xVDXewTs12609lTP.+DrOvjqsjo53uiBZiM9NaC........kXDX+jMO0cf9Q8XVrbAxFqHSv69suYClWxTw7.........wh.6KWbCx2SAg8KETU+1W6DYwADvDflJnsz367XaUw6FJO........PhQf8XP3FzuTmg6G0yelz7fAXDsryicaEM9J5f4A........xDDXOxBdpyp4Wp6P9qJy.CDdazu9QTVNzyC2C3q67XeQ36........XB.A1iIQ0hXadp6AEPJ5ABv80X.ARetsTFW9wr85gdNsbF........TJPf8.wqWg8mj8KNdJ5AaXbxWIuJyCW85i62e.........Ho+O.jds0cejP5LB....PRE4DQtJDXBB" ],
					"embed" : 1,
					"forceaspect" : 1,
					"id" : "obj-22",
					"maxclass" : "fpic",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "jit_matrix" ],
					"patching_rect" : [ 766.044800000000009, 428.0, 100.0, 63.98416886543535 ],
					"pic" : "zz_quadradetsct.png",
					"presentation" : 1,
					"presentation_rect" : [ 14.25, 3.183047, 325.647400000000005, 208.362782321899743 ]
				}

			}
, 			{
				"box" : 				{
					"angle" : 270.0,
					"background" : 1,
					"bgcolor" : [ 0.811764705882353, 0.811764705882353, 0.811764705882353, 1.0 ],
					"hidden" : 1,
					"id" : "obj-436",
					"maxclass" : "panel",
					"mode" : 0,
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 822.0, 1616.0, 360.0, 220.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 0.0, 0.0, 360.0, 220.0 ],
					"proportion" : 0.5,
					"prototypename" : "referencepanel_360x220"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 2 ],
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 2 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-90", 0 ],
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 1 ],
					"source" : [ "obj-103", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"source" : [ "obj-106", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-104", 0 ],
					"source" : [ "obj-107", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"order" : 0,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"order" : 1,
					"source" : [ "obj-109", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"order" : 0,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"order" : 1,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 0 ],
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-114", 0 ],
					"source" : [ "obj-112", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"source" : [ "obj-112", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-134", 0 ],
					"source" : [ "obj-113", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"source" : [ "obj-114", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-117", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-173", 1 ],
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-176", 1 ],
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"order" : 1,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-422", 1 ],
					"order" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"source" : [ "obj-120", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"order" : 1,
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-378", 0 ],
					"order" : 2,
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"order" : 0,
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-120", 0 ],
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"source" : [ "obj-125", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"source" : [ "obj-126", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 1 ],
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-115", 0 ],
					"source" : [ "obj-129", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-116", 0 ],
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"order" : 1,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-397", 0 ],
					"order" : 0,
					"source" : [ "obj-13", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"order" : 0,
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-174", 0 ],
					"order" : 1,
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"order" : 1,
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-367", 0 ],
					"order" : 0,
					"source" : [ "obj-136", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"order" : 1,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"order" : 0,
					"source" : [ "obj-14", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"source" : [ "obj-140", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"source" : [ "obj-141", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"order" : 1,
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-383", 0 ],
					"order" : 0,
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 0 ],
					"source" : [ "obj-144", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"order" : 1,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"order" : 0,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 1 ],
					"source" : [ "obj-146", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-203", 0 ],
					"source" : [ "obj-147", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"source" : [ "obj-148", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"order" : 1,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-3", 0 ],
					"order" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 1 ],
					"source" : [ "obj-153", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 1 ],
					"source" : [ "obj-154", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"order" : 0,
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-156", 0 ],
					"order" : 2,
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-384", 0 ],
					"order" : 1,
					"source" : [ "obj-155", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-157", 0 ],
					"source" : [ "obj-156", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-158", 1 ],
					"source" : [ "obj-157", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-151", 0 ],
					"source" : [ "obj-158", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-152", 0 ],
					"source" : [ "obj-158", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-351", 0 ],
					"order" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"order" : 1,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-183", 0 ],
					"source" : [ "obj-160", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 0 ],
					"order" : 1,
					"source" : [ "obj-161", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"order" : 0,
					"source" : [ "obj-161", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-182", 0 ],
					"source" : [ "obj-162", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 0 ],
					"order" : 2,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 0 ],
					"order" : 1,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-374", 0 ],
					"order" : 0,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-173", 0 ],
					"order" : 1,
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-176", 0 ],
					"order" : 2,
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-379", 0 ],
					"order" : 0,
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-177", 0 ],
					"order" : 1,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-178", 0 ],
					"order" : 2,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-385", 0 ],
					"order" : 0,
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-243", 0 ],
					"source" : [ "obj-166", 29 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-245", 0 ],
					"source" : [ "obj-166", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-255", 0 ],
					"source" : [ "obj-166", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-257", 0 ],
					"source" : [ "obj-166", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-259", 0 ],
					"source" : [ "obj-166", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-260", 0 ],
					"source" : [ "obj-166", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-261", 0 ],
					"source" : [ "obj-166", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-263", 0 ],
					"source" : [ "obj-166", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-265", 0 ],
					"source" : [ "obj-166", 11 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-268", 0 ],
					"source" : [ "obj-166", 12 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-269", 0 ],
					"source" : [ "obj-166", 13 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-270", 0 ],
					"source" : [ "obj-166", 14 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-272", 0 ],
					"source" : [ "obj-166", 15 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-273", 0 ],
					"source" : [ "obj-166", 16 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-274", 0 ],
					"source" : [ "obj-166", 17 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-275", 0 ],
					"source" : [ "obj-166", 20 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-277", 0 ],
					"source" : [ "obj-166", 19 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-280", 0 ],
					"source" : [ "obj-166", 18 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-283", 0 ],
					"source" : [ "obj-166", 23 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-284", 0 ],
					"source" : [ "obj-166", 22 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-285", 0 ],
					"source" : [ "obj-166", 21 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-286", 0 ],
					"source" : [ "obj-166", 24 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-287", 0 ],
					"source" : [ "obj-166", 25 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-292", 0 ],
					"source" : [ "obj-166", 26 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-299", 0 ],
					"source" : [ "obj-166", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-300", 0 ],
					"source" : [ "obj-166", 8 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-302", 0 ],
					"source" : [ "obj-166", 9 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-303", 0 ],
					"source" : [ "obj-166", 10 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-405", 0 ],
					"source" : [ "obj-166", 28 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-406", 0 ],
					"source" : [ "obj-166", 27 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-428", 0 ],
					"source" : [ "obj-166", 30 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-429", 0 ],
					"source" : [ "obj-166", 31 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-167", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-168", 0 ],
					"order" : 0,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"order" : 1,
					"source" : [ "obj-169", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"source" : [ "obj-170", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"source" : [ "obj-171", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"source" : [ "obj-173", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 1 ],
					"order" : 1,
					"source" : [ "obj-174", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"order" : 0,
					"source" : [ "obj-174", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"order" : 0,
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 1 ],
					"order" : 1,
					"source" : [ "obj-175", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"source" : [ "obj-176", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-146", 0 ],
					"source" : [ "obj-177", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-144", 0 ],
					"source" : [ "obj-178", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"source" : [ "obj-179", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-204", 0 ],
					"order" : 2,
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-368", 0 ],
					"order" : 0,
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-64", 0 ],
					"order" : 1,
					"source" : [ "obj-180", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-187", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-188", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"source" : [ "obj-190", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 3 ],
					"source" : [ "obj-192", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 3 ],
					"source" : [ "obj-192", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-223", 0 ],
					"source" : [ "obj-193", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-293", 0 ],
					"order" : 1,
					"source" : [ "obj-194", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-294", 0 ],
					"order" : 0,
					"source" : [ "obj-194", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 1 ],
					"source" : [ "obj-195", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-400", 0 ],
					"source" : [ "obj-196", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"source" : [ "obj-197", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-207", 0 ],
					"source" : [ "obj-199", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-133", 0 ],
					"order" : 1,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-424", 1 ],
					"order" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-195", 0 ],
					"source" : [ "obj-200", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-202", 0 ],
					"source" : [ "obj-201", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-202", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-147", 0 ],
					"source" : [ "obj-202", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"source" : [ "obj-202", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-203", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-174", 0 ],
					"midpoints" : [ 1878.333251999999902, 674.0, 1222.166625999999951, 674.0 ],
					"order" : 1,
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-175", 0 ],
					"midpoints" : [ 1878.333251999999902, 674.0, 1479.666625999999951, 674.0 ],
					"order" : 0,
					"source" : [ "obj-204", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-249", 0 ],
					"source" : [ "obj-205", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"source" : [ "obj-206", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-200", 0 ],
					"order" : 2,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-206", 0 ],
					"order" : 0,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-225", 0 ],
					"order" : 1,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-199", 0 ],
					"source" : [ "obj-208", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-209", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-218", 0 ],
					"order" : 2,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-355", 0 ],
					"order" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 1 ],
					"order" : 1,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-199", 0 ],
					"source" : [ "obj-210", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 0 ],
					"order" : 1,
					"source" : [ "obj-211", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-288", 0 ],
					"order" : 0,
					"source" : [ "obj-211", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-317", 0 ],
					"order" : 3,
					"source" : [ "obj-211", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-341", 0 ],
					"order" : 2,
					"source" : [ "obj-211", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-401", 0 ],
					"order" : 1,
					"source" : [ "obj-212", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-403", 0 ],
					"order" : 2,
					"source" : [ "obj-212", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-433", 0 ],
					"order" : 0,
					"source" : [ "obj-212", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-210", 0 ],
					"source" : [ "obj-213", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-211", 0 ],
					"source" : [ "obj-214", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-210", 0 ],
					"source" : [ "obj-215", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-244", 0 ],
					"source" : [ "obj-216", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-209", 0 ],
					"order" : 1,
					"source" : [ "obj-217", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-224", 0 ],
					"order" : 0,
					"source" : [ "obj-217", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"source" : [ "obj-218", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-408", 0 ],
					"source" : [ "obj-219", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-199", 0 ],
					"source" : [ "obj-220", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-240", 0 ],
					"source" : [ "obj-222", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"source" : [ "obj-223", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-301", 0 ],
					"source" : [ "obj-224", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-227", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-229", 0 ],
					"source" : [ "obj-228", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-227", 0 ],
					"source" : [ "obj-229", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-230", 0 ],
					"source" : [ "obj-229", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-230", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-232", 0 ],
					"source" : [ "obj-231", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-232", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-234", 0 ],
					"source" : [ "obj-233", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"source" : [ "obj-234", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-234", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"source" : [ "obj-235", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"source" : [ "obj-236", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-238", 0 ],
					"source" : [ "obj-237", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"source" : [ "obj-238", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-214", 0 ],
					"source" : [ "obj-239", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-241", 0 ],
					"source" : [ "obj-239", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 1 ],
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-221", 0 ],
					"source" : [ "obj-240", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"order" : 1,
					"source" : [ "obj-241", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 0 ],
					"order" : 0,
					"source" : [ "obj-241", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 29 ],
					"source" : [ "obj-242", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-208", 0 ],
					"order" : 1,
					"source" : [ "obj-244", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-220", 0 ],
					"order" : 0,
					"source" : [ "obj-244", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"source" : [ "obj-246", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"source" : [ "obj-246", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-246", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"source" : [ "obj-246", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"source" : [ "obj-248", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"source" : [ "obj-248", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"source" : [ "obj-248", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-248", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-249", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 1 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"source" : [ "obj-250", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-250", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"source" : [ "obj-250", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"source" : [ "obj-250", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-254", 0 ],
					"source" : [ "obj-251", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-252", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-296", 0 ],
					"source" : [ "obj-253", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-253", 0 ],
					"source" : [ "obj-254", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-134", 1 ],
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-313", 0 ],
					"source" : [ "obj-264", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-315", 0 ],
					"source" : [ "obj-264", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-256", 0 ],
					"order" : 1,
					"source" : [ "obj-266", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"order" : 2,
					"source" : [ "obj-266", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"order" : 0,
					"source" : [ "obj-266", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-192", 0 ],
					"order" : 1,
					"source" : [ "obj-267", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-262", 0 ],
					"order" : 0,
					"source" : [ "obj-267", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"order" : 2,
					"source" : [ "obj-271", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-213", 0 ],
					"order" : 2,
					"source" : [ "obj-271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-215", 0 ],
					"order" : 2,
					"source" : [ "obj-271", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-276", 0 ],
					"order" : 1,
					"source" : [ "obj-271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-278", 0 ],
					"order" : 0,
					"source" : [ "obj-271", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-278", 0 ],
					"order" : 0,
					"source" : [ "obj-271", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-279", 0 ],
					"order" : 0,
					"source" : [ "obj-271", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-281", 0 ],
					"order" : 1,
					"source" : [ "obj-271", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-281", 0 ],
					"order" : 1,
					"source" : [ "obj-271", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-266", 0 ],
					"source" : [ "obj-276", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-267", 0 ],
					"source" : [ "obj-278", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-267", 0 ],
					"source" : [ "obj-279", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-266", 0 ],
					"source" : [ "obj-281", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 0 ],
					"source" : [ "obj-282", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-271", 0 ],
					"source" : [ "obj-288", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-291", 0 ],
					"order" : 1,
					"source" : [ "obj-293", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-438", 0 ],
					"order" : 0,
					"source" : [ "obj-293", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-290", 0 ],
					"order" : 1,
					"source" : [ "obj-294", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-439", 0 ],
					"order" : 0,
					"source" : [ "obj-294", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-296", 0 ],
					"source" : [ "obj-295", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-194", 0 ],
					"order" : 1,
					"source" : [ "obj-296", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-370", 0 ],
					"order" : 0,
					"source" : [ "obj-296", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-294", 0 ],
					"source" : [ "obj-297", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-293", 0 ],
					"source" : [ "obj-298", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-306", 0 ],
					"order" : 1,
					"source" : [ "obj-301", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-395", 0 ],
					"order" : 0,
					"source" : [ "obj-301", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 10 ],
					"source" : [ "obj-304", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 9 ],
					"source" : [ "obj-305", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-308", 0 ],
					"source" : [ "obj-306", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-310", 0 ],
					"source" : [ "obj-306", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 8 ],
					"source" : [ "obj-307", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-308", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 7 ],
					"source" : [ "obj-309", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"source" : [ "obj-310", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 26 ],
					"source" : [ "obj-311", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 25 ],
					"source" : [ "obj-312", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-416", 0 ],
					"source" : [ "obj-313", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 24 ],
					"source" : [ "obj-314", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-416", 0 ],
					"source" : [ "obj-315", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-235", 0 ],
					"order" : 1,
					"source" : [ "obj-317", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 0 ],
					"order" : 0,
					"source" : [ "obj-317", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 23 ],
					"source" : [ "obj-318", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-432", 0 ],
					"source" : [ "obj-319", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-171", 1 ],
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 22 ],
					"source" : [ "obj-320", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 21 ],
					"source" : [ "obj-321", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 20 ],
					"source" : [ "obj-322", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 19 ],
					"source" : [ "obj-323", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 18 ],
					"source" : [ "obj-324", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 17 ],
					"source" : [ "obj-325", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 16 ],
					"source" : [ "obj-326", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 15 ],
					"source" : [ "obj-327", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 14 ],
					"source" : [ "obj-328", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 13 ],
					"source" : [ "obj-329", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 12 ],
					"source" : [ "obj-330", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 11 ],
					"source" : [ "obj-331", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 6 ],
					"source" : [ "obj-332", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 5 ],
					"source" : [ "obj-333", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 4 ],
					"source" : [ "obj-334", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 3 ],
					"source" : [ "obj-335", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 2 ],
					"source" : [ "obj-336", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 1 ],
					"source" : [ "obj-337", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 0 ],
					"source" : [ "obj-338", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"source" : [ "obj-339", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-211", 0 ],
					"source" : [ "obj-340", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"source" : [ "obj-342", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"source" : [ "obj-343", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"source" : [ "obj-346", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-347", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-348", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-349", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-170", 1 ],
					"order" : 0,
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-185", 0 ],
					"order" : 1,
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"source" : [ "obj-350", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-356", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"source" : [ "obj-357", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-89", 0 ],
					"source" : [ "obj-358", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-97", 0 ],
					"source" : [ "obj-359", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"order" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-125", 0 ],
					"order" : 2,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-203", 4 ],
					"order" : 1,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-180", 0 ],
					"source" : [ "obj-360", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-361", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-362", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-296", 0 ],
					"source" : [ "obj-369", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 1 ],
					"order" : 0,
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"order" : 1,
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-79", 0 ],
					"source" : [ "obj-371", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-372", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"source" : [ "obj-373", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"source" : [ "obj-380", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"source" : [ "obj-381", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-164", 0 ],
					"source" : [ "obj-382", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"source" : [ "obj-386", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-155", 0 ],
					"source" : [ "obj-387", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-165", 0 ],
					"source" : [ "obj-388", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-396", 1 ],
					"source" : [ "obj-389", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-389", 0 ],
					"source" : [ "obj-390", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-396", 0 ],
					"source" : [ "obj-390", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"source" : [ "obj-391", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-301", 0 ],
					"source" : [ "obj-393", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-13", 0 ],
					"source" : [ "obj-394", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-363", 0 ],
					"order" : 1,
					"source" : [ "obj-396", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-398", 0 ],
					"order" : 0,
					"source" : [ "obj-396", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-398", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-390", 0 ],
					"source" : [ "obj-399", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-401", 0 ],
					"order" : 1,
					"source" : [ "obj-400", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-403", 0 ],
					"order" : 2,
					"source" : [ "obj-400", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-433", 0 ],
					"order" : 0,
					"source" : [ "obj-400", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-212", 0 ],
					"source" : [ "obj-402", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-403", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 28 ],
					"source" : [ "obj-404", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 27 ],
					"source" : [ "obj-407", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-264", 0 ],
					"source" : [ "obj-409", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-423", 0 ],
					"source" : [ "obj-410", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-425", 0 ],
					"source" : [ "obj-411", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-219", 0 ],
					"order" : 0,
					"source" : [ "obj-412", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-410", 0 ],
					"order" : 1,
					"source" : [ "obj-412", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-411", 0 ],
					"order" : 2,
					"source" : [ "obj-412", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-416", 0 ],
					"source" : [ "obj-413", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-416", 0 ],
					"source" : [ "obj-414", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-413", 0 ],
					"source" : [ "obj-415", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-414", 0 ],
					"source" : [ "obj-415", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-412", 0 ],
					"source" : [ "obj-416", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-415", 0 ],
					"midpoints" : [ 321.5, 1809.0, 260.91478699999999, 1809.0, 260.91478699999999, 1621.0, 283.5, 1621.0 ],
					"order" : 2,
					"source" : [ "obj-417", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-419", 0 ],
					"order" : 1,
					"source" : [ "obj-417", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-421", 0 ],
					"order" : 0,
					"source" : [ "obj-417", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-417", 0 ],
					"source" : [ "obj-418", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-422", 0 ],
					"order" : 0,
					"source" : [ "obj-419", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-424", 0 ],
					"order" : 1,
					"source" : [ "obj-419", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-69", 0 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-417", 0 ],
					"source" : [ "obj-420", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-423", 0 ],
					"source" : [ "obj-422", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-426", 1 ],
					"source" : [ "obj-422", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-425", 0 ],
					"source" : [ "obj-424", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-426", 0 ],
					"source" : [ "obj-424", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 30 ],
					"source" : [ "obj-427", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-339", 31 ],
					"source" : [ "obj-430", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-432", 0 ],
					"source" : [ "obj-431", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-434", 0 ],
					"source" : [ "obj-432", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-197", 0 ],
					"order" : 0,
					"source" : [ "obj-433", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-319", 0 ],
					"order" : 1,
					"source" : [ "obj-433", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-434", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-236", 1 ],
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 1 ],
					"source" : [ "obj-45", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-345", 0 ],
					"order" : 0,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"order" : 1,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-160", 0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-162", 0 ],
					"source" : [ "obj-48", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-344", 0 ],
					"order" : 0,
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 1 ],
					"order" : 1,
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-239", 0 ],
					"order" : 1,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-354", 0 ],
					"order" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-356", 0 ],
					"order" : 0,
					"source" : [ "obj-54", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-363", 0 ],
					"order" : 1,
					"source" : [ "obj-54", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-14", 0 ],
					"order" : 1,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"order" : 0,
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 1 ],
					"source" : [ "obj-59", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-166", 0 ],
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-111", 0 ],
					"order" : 0,
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"order" : 2,
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-366", 0 ],
					"order" : 1,
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-136", 0 ],
					"source" : [ "obj-61", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-169", 1 ],
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-59", 0 ],
					"order" : 0,
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-72", 0 ],
					"order" : 1,
					"source" : [ "obj-68", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"source" : [ "obj-69", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-352", 0 ],
					"order" : 0,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-81", 0 ],
					"order" : 1,
					"source" : [ "obj-70", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"source" : [ "obj-72", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"source" : [ "obj-75", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"source" : [ "obj-76", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-376", 0 ],
					"order" : 0,
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-68", 0 ],
					"order" : 1,
					"source" : [ "obj-79", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-353", 0 ],
					"order" : 0,
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"order" : 1,
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-71", 0 ],
					"order" : 1,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"order" : 0,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"source" : [ "obj-85", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"source" : [ "obj-85", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"order" : 2,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-375", 0 ],
					"order" : 1,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"order" : 0,
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 1 ],
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"order" : 0,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-365", 0 ],
					"order" : 1,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-432", 1 ],
					"source" : [ "obj-91", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-99", 0 ],
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-161", 0 ],
					"order" : 1,
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-392", 0 ],
					"order" : 0,
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"source" : [ "obj-94", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-377", 0 ],
					"order" : 0,
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-98", 0 ],
					"order" : 1,
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-364", 0 ],
					"order" : 1,
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"order" : 0,
					"source" : [ "obj-97", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"order" : 0,
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"order" : 1,
					"source" : [ "obj-98", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 0 ],
					"source" : [ "obj-99", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-48" : [ "vst~", "vst~", 0 ],
			"parameterbanks" : 			{

			}

		}
,
		"dependency_cache" : [ 			{
				"name" : "zz_quadradetsct.png",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/media",
				"patcherrelativepath" : "../media",
				"type" : "PNG",
				"implicit" : 1
			}
, 			{
				"name" : "harmonizer_pfft.maxpat",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Programació/aSistema_Dev/Aglaya_DT_Redisseny4/patchers",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "172_Am_LowReeseBass_01_673.wav",
				"bootpath" : "~/Google Drive/Proyectos/Adolf/AGLAYA /Recursos/Sons/SAMPLERS_CAIXA_DE_SONS/AutonomicDnB_Mini_SP",
				"patcherrelativepath" : "../../../../Recursos/Sons/SAMPLERS_CAIXA_DE_SONS/AutonomicDnB_Mini_SP",
				"type" : "WAVE",
				"implicit" : 1
			}
 ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "sliderGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
					"color" : [ 0.907107, 0.934609, 0.842715, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ],
		"bgcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
	}

}
